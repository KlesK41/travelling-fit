<?php

class Pages
{


	public $name; // title of the page
	public $type; // is this one page or many (i.e. the same page used multiple times)
	public $number;
	public $description;


	function __construct() {
		
	}

	public static function getPages()
	{
		/****************************************
		*	id 			- identifier
		*	type 		- displayed one or many consecutive times
		*	name 		- view name
		*	description - description of the view
		*
		*/
		
		$pages[] = array("id"=>"1", "type"=>"1", "name"=>"package", 		"description"=>"Get package details");
		$pages[] = array("id"=>"2", "type"=>"M", "name"=>"guests",			"description"=>"Get guest details details");
		$pages[] = array("id"=>"3", "type"=>"1", "name"=>"optional_tours", 	"description"=>"Get optional tour details");
		$pages[] = array("id"=>"4", "type"=>"1", "name"=>"additional_info",	"description"=>"Get additional information");
		$pages[] = array("id"=>"5", "type"=>"1", "name"=>"payment_method",	"description"=>"Get payment method details");

		return $pages;

	}

	public static function findById($id=0) 
	{
		$database = new self;
		$sql = "SELECT * FROM categories WHERE user_id={$id}";
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private static function populate($record)
	{
		// Could check that $record exists and is an array
		$object = new self;
	
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  $attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
		  if($object->hasAttribute($attribute)) {
			  // check to see if attribute is a clob and load the value if it is
			  if(is_object($value))
			  {
				  $object->$attribute = $value->read($value->size());
			  }
			  else
			  {
			  	$object->$attribute = stripslashes($value);
			  }
		  }
		}
		return $object;
	}

	private function hasAttribute($attribute) 
	{
	  // get_object_vars returns an associative array with all attributes 
	  // (incl. private ones!) as the keys and their current values as the value
	  $objectVars = get_object_vars($this);
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $objectVars);
	}

}

?>