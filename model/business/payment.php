<?php
class Payment extends DatabaseObject 
{


	public $package; // title of the page
	public $type; // is this one page or many (i.e. the same page used multiple times)
	public $number;
	public $description;

	public $total_tours_cost;
	public $total_package_cost;
	public $total_pass_park_fee;

	public $total_amount_paid;

	public $deposit;
	function __construct() {
		parent::__construct();
	}

	/*
		Deposit is calculated for adults only and not children
		$550 per adult
		
		If "Red Earth" or "Gold" packages are select and one of these tours:
		Kata Tjuta (Valley of the Winds), Uluru Sunset, Uluru Sunrise add $26 "Park Pass" fee


		Credit Card fees:

		VISA                  	1.5%
		Master card         	1.5%
		AMEX                 	Amount x 1.0317787
		DINERS               	Amount x 1.0252651
	*/

	public static function deposit($booking_id)
	{
		
		$database = new self;
		$sql = "SELECT count(g.user_id) * " . DEPOSIT_AMOUNT . " AS deposit_for_adults
				FROM guests g
				WHERE g.booking_id = $booking_id AND g.child_YN <> 'Y'";
		$result_set = $database::findBySql($sql);
		return $result_set[0]['deposit_for_adults'];
	}

	public static function deposit_amount_due($booking_id)
	{
		
		$database = new self;
		$sql = "SELECT count(g.user_id) * " . DEPOSIT_AMOUNT . "  AS deposit_for_adults
				FROM guests g
				WHERE g.booking_id = $booking_id AND g.child_YN <> 'Y'";
		$result_set = $database::findBySql($sql);
		return $result_set[0]['deposit_for_adults'];
	}

	public static function total_cost($booking_id=null)
	{
		// package cost for each guest
		// optional tours cost
		// pass fee for each guest if applicable
		
		if(!isset($booking_id))
		{
			$booking_id = $_SESSION['booking_id'];
		}

		$database = new self;
		$package_cost 			= $database::package_cost($booking_id);
		$tours_cost 			= $database::tours_cost($booking_id);

//		$pass_park_fee			= $database::tours_park_pass($booking_id);
//		$accommodation_cost		= $database::accommodation_cost($booking_id);
//		$non_runners_discount	= $database::non_runners_discount($booking_id);

		count($package_cost) > 0 ? 			$total_package_cost = $package_cost[0]['total_package_cost'] : $total_package_cost = 0;
		
		count($tours_cost) > 0 ? 			$total_tours_cost 	= $tours_cost[0]['tours_cost'] : $total_tours_cost = 0;
		
//		count($pass_park_fee) > 0 ? 		$total_pass_park_fee = $pass_park_fee[0]['total_pass_park_fee'] : $total_pass_park_fee = 0;
		
//		count($accommodation_cost) > 0 ? 	$total_accommodation_cost = $accommodation_cost[0]['total_accom_cost'] : $total_accommodation_cost = 0;


		// echo count($package_cost) > 0 ? "total_package_cost $total_package_cost<br>": "";
		// echo count($tours_cost) > 0 ? "total_tours_cost $total_tours_cost<br>" : "";
		// echo count($pass_park_fee) > 0 ? "total_pass_park_fee $total_pass_park_fee<br>"	: "";
		// echo count($accommodation_cost) > 0 ? 	"total_accommodation_cost $total_accommodation_cost<br>" : "";
		
//		return $total_package_cost + $total_tours_cost + $total_pass_park_fee + $total_accommodation_cost - $non_runners_discount;

        return $total_package_cost + $total_tours_cost;

	}

	public static function non_runners_discount($booking_id)
	{
		$database = new self;
		// count the number of non-runners for specified booking and multiply by 100. 'Non-Runner event_id = 9'
		$sql = "SELECT COUNT( g.user_id ) *100 AS non_runner_discount
				FROM guests AS g
				WHERE g.prefered_event_id =9
				AND g.booking_id = $booking_id";

		$result_set = $database::findBySql($sql);
		$result_set[0]['non_runner_discount'] . "<br>"; 
		return $result_set[0]['non_runner_discount'];
	}

	public static function accommodation_cost($booking_id)
	{	
		$database = new self;
		$sql = "SELECT (ac.cost_adult * b.number_adults) + (ac.cost_child * b.number_children)  + (ac.pre_cost_extra_night *  b.extra_nights_pre * b.number_adults) + (ac.post_cost_extra_night * b.extra_nights_post * b.number_adults) total_accom_cost
				FROM
				( SELECT b.booking_id, cost_adult, cost_child, cost_extra_night pre_cost_extra_night, cost_extra_night post_cost_extra_night
				FROM bookings b, accommodation_bedding ab
				WHERE ab.accommodation_bedding_id = b.accommodation_bedding_id 
				AND b.booking_id = $booking_id ) ac,
				(SELECT b.booking_id, b.number_adults, b.number_children, b.extra_nights_pre, b.extra_nights_post
				FROM bookings b
				WHERE b.booking_id = $booking_id) b 
				WHERE ac.booking_id = b.booking_id";
				
		return $database::findBySql($sql);

	}

	public static function amount_paid()
	{
		$database = new self;

		$sql = "SELECT sum(amount) amount_paid
				FROM transactions
				WHERE booking_id = " . $_SESSION['booking_id'] . " AND funds_received_YN = 'Y'";

		$amount_paid = $database::findBySql($sql);

		return count($amount_paid) > 0 ? $total_amount_paid = number_format($amount_paid[0]['amount_paid'], 2, '.', '') : $total_amount_paid = 0;
	}

	public static function amount_paid_with_fee()
	{
		$database = new self;

		$sql = "SELECT sum(amount_with_fee) amount_paid
				FROM transactions
				WHERE booking_id = " . $_SESSION['booking_id'] . " AND funds_received_YN = 'Y'";

		$amount_paid = $database::findBySql($sql);

		return count($amount_paid) > 0 ? $total_amount_paid = $amount_paid[0]['amount_paid'] : $total_amount_paid = 0;
	}

	public static function get_amount_due($bookin)
	{
		$database = new self;

		return $database::total_cost() - $database::amount_paid();
	} 

	public static function is_desosit_paid()
	{
		$database = new self;
		if((DEPOSIT_AMOUNT - $database::amount_paid()) <= 0)
		{
			$_SESSION['deposit_paid_YN'] = 'Y';
			return "Y";
		}
		else
		{
			$_SESSION['deposit_paid_YN'] = 'N';
			return "N";
		}
	}

	public static function package_cost($booking_id)
	{
		$database = new self;

		$sql = "SELECT (r.price * (b.number_adult_guests + b.number_children_guests + 1) + 80*(b.tshirt_quantity + b.singlet_quantity)) AS total_package_cost FROM
                room_type r INNER JOIN bookings b ON b.package_id = r.package_id WHERE b.booking_id = " . $database->escapeValue($booking_id);


        // old code
//		$sql = "SELECT g.number_guests * c.cost AS total_package_cost
//				FROM
//				(SELECT count(user_id) number_guests, booking_id
//				FROM guests g
//				WHERE g.booking_id = $booking_id) g,
//				( SELECT p.cost, booking_id
//				FROM bookings b, packages p
//				WHERE b.package_id = p.package_id AND b.booking_id = $booking_id ) c
//				WHERE c.booking_id = g.booking_id";

		return $database::findBySql($sql);
	}

	public static function tours_cost($booking_id)
	{
		$database = new self;

        $sql = "SELECT ot.cost * (b.number_adult_guests + b.number_children_guests + 1) AS tours_cost FROM bookings b
                INNER JOIN optional_tour_package otp ON b.package_id = otp.package_id
                INNER JOIN optional_tours ot ON otp.optional_tour_id = ot.optional_tour_id
                WHERE b.booking_id = " . $database->escapeValue($booking_id);

        //old code
//		$sql = "SELECT sum(ot.cost) * (SELECT count(user_id)  number_guests FROM guests g WHERE g.booking_id = $booking_id) tours_cost
//				FROM optional_tours ot, booking_option_tours bot
//				WHERE bot.optional_tour_id = ot.optional_tour_id AND bot.booking_id = $booking_id";

		return $database::findBySql($sql);
	}

	// If "Red Earth" or "Gold" packages are select and one of these tours:
	// Kata Tjuta (Valley of the Winds), Uluru Sunset, Uluru Sunrise add $26 "Park Pass" fee
	// only charge people over 16 years old
	//
	//
	// returns total cost and names of tours
	public static function tours_park_pass($booking_id)
	{
		$database = new self;
		$sql = "SELECT g.number_guests * ot.pass_park_fee total_pass_park_fee, ot.title
				FROM
				(SELECT count(distinct user_id) number_guests, booking_id, user_id
				FROM guests g
				WHERE g.booking_id = $booking_id AND g.user_id IN (
					SELECT t.user_id
					FROM
					(SELECT ot.cost, g.*,  TIMESTAMPDIFF(YEAR,str_to_date(g.dob,'%d/%m/%Y'),ot.tour_date) AS age
					FROM guests g, booking_option_tours bot, optional_tours ot
					WHERE g.booking_id = bot.booking_id AND ot.optional_tour_id = bot.optional_tour_id AND g.booking_id = $booking_id
					AND ot.potential_pass_park_fee_YN = 'Y') t
					WHERE t.age >= 16)
				) g,
				(SELECT DISTINCT b.booking_id, ot.pass_park_fee AS pass_park_fee, ot.title
				FROM bookings b, optional_tours ot, booking_option_tours bot, packages p
				WHERE bot.optional_tour_id = ot.optional_tour_id AND b.booking_id = $booking_id 
				AND bot.booking_id = b.booking_id AND p.package_id = b.package_id
				AND ot.potential_pass_park_fee_YN = p.potential_park_pass_fee_YN) ot
				WHERE ot.booking_id = g.booking_id";

		return $database::findBySql($sql);
	}

	public static function tours_park_titles($booking_id)
	{
		$database = new self;
		$sql = "SELECT ot.*
				FROM optional_tours ot, booking_option_tours bot
				WHERE bot.optional_tour_id = ot.optional_tour_id AND bot.booking_id = $booking_id";

		return $database::findBySql($sql);
	}



	public static function primary_guest($booking_id)
	{
		$database = new self;
		$sql = "SELECT g.*, e.event_name, t.size
				FROM guests g, events e, tshirts t
				WHERE g.booking_id = $booking_id AND g.primary_contact_YN = 'Y' AND g.prefered_event_id = e.event_id
				AND g.tshirt_id = t.tshirt_id";

		return $database::findBySql($sql);
	}

	public static function primary_guest_for_payment_invoice($booking_id)
	{
		$database = new self;
		$sql = "SELECT g.*, t.size as tshirt_size, s.size as singlet_size FROM guests g LEFT JOIN tshirts t ON g.tshirt_id = t.tshirt_id
                LEFT JOIN singlets s ON s.singlet_id = g.singlet_id WHERE g.booking_id = $booking_id AND g.primary_contact_YN = 'Y'";

//        var_dump($sql);exit;

		return $database::findBySql($sql);
	}

    public static function guests_for_payment_invoice($booking_id)
    {
        $database = new self;
        $sql = "SELECT g.*, t.size as tshirt_size, s.size as singlet_size  
        		FROM guests g LEFT JOIN tshirts t ON g.tshirt_id = t.tshirt_id
                LEFT JOIN singlets s ON s.singlet_id = g.singlet_id
                WHERE g.booking_id = $booking_id AND g.primary_contact_YN = 'N'";

        return $database::findBySql($sql);
    }

	public static function guests($booking_id)
	{
		$database = new self;
	/*	$sql = "SELECT g.*, e.event_name, t.size
				FROM guests g, events e, tshirts t
				WHERE g.booking_id = $booking_id AND g.prefered_event_id = e.event_id
				AND g.tshirt_id = t.tshirt_id";
	*/
		// updated as somehow user managed to enter null value for an event

		$sql = "SELECT g.*, e.event_name, t.size
				FROM guests AS g
				LEFT JOIN events e ON g.prefered_event_id = e.event_id
				LEFT JOIN tshirts t ON g.tshirt_id = t.tshirt_id
				WHERE g.booking_id = $booking_id"; 

		return $database::findBySql($sql);
	}

	public static function accommodation($booking_id)
	{
		$database = new self;
		$sql = "SELECT p.package_name, a.accommodation_name, bt.bedding_type_name AS bedding_name, b.extra_nights_pre, b.extra_nights_post, b.share_request_YN, b.rollaway_or_cot
				FROM accommodation_bedding ab, bookings b, accommodation a, bedding bed, packages p, room_type rt, bedding_type bt
				WHERE ab.accommodation_bedding_id = b.accommodation_bedding_id AND b.booking_id = {$booking_id} 
				AND a.accommodation_id = ab.accommodation_id AND bed.bedding_id = ab.bedding_id AND b.package_id = p.package_id
				AND b.room_type_id = rt.room_type_id AND bt.bedding_type_id = rt.bedding_type_id";

		return $database::findBySql($sql);
	}

    public static function accommodation_for_payment_invoice($booking_id)
    {
        $database = new self;
        $sql = "SELECT p.name as package_name, bed.bedding_name AS bedding_name, b.extra_nights_pre, b.extra_nights_post, b.share_request_YN
				FROM bookings b, bedding bed, packages p
				WHERE b.booking_id = {$booking_id}
				AND bed.bedding_id = b.bedding_configuration_id
				AND b.package_id = p.package_id";

       // var_dump($sql);exit;

        return $database::findBySql($sql);
    }

	public static function invoice_final_section($booking_id)
	{

		$database = new self;
		$sql = "SELECT b.travel_insurance, IF(CHAR_LENGTH(b.inspirational_story) > 0, b.inspirational_story, 'No') as inspirational_story, IF(b.book_travel_partner_YN = 'Y', b.book_travel_partner_YN, 'No') as book_travel_partner, b.travel_partner_id, b.ec_firstname, b.ec_lastname, b.ec_phone, b.ec_email, b.inspirational_story, h.name, 
					   CASE b.travel_insurance 
						WHEN 1 THEN  'I will take out travel insurance online via the Travelling Fit website and earn 25% discount'
						WHEN 2 THEN  'I would like to take out Travel Insurance but would prefer Travelling Fit to issue my policy at the retail price. Please send me all the details'
						WHEN 3 THEN  'I do not wish to take out travel insurance through Travelling Fit - Warning: please refer clause 13.2 & 13.3 of the Booking Terms & Conditions before accepting this option'
						ELSE 'No travel insurance selected'
					   END as travel_insurance_title
				FROM bookings b, how_hear h
				WHERE b.booking_id = $booking_id AND h.how_hear_id = b.how_hear_id";

		return $database::findBySql($sql);

	}

	// if count($results) == 0, "None"
	public static function optional_tours($booking_id)
	{
		$database = new self;
		$sql = "SELECT ot.title
				FROM optional_tours ot, booking_option_tours bot
				WHERE bot.optional_tour_id = ot.optional_tour_id AND bot.booking_id = $booking_id";


		return $database::findBySql($sql);

	}

	// if count($results) == 0, "None"
	public static function get_code_by_booking_id($booking_id)
	{
		$database = new self;
		$sql = "SELECT booking_code 
				FROM bookings
				WHERE booking_id = {$booking_id}";

		$booking_code = $database::findBySql($sql);


		return isset($booking_code[0]['booking_code']) ? $booking_code[0]['booking_code'] : '';

	}

	public static function get_email_invoice($booking_id, $payment_type)
	{

		// if credit card, display amount owing as well

		$database = new self;

        $package_accommodation = $database::accommodation_for_payment_invoice($booking_id);
        $primary_guest = $database::primary_guest_for_payment_invoice($booking_id);
        $optional_tours = $database::optional_tours($booking_id);
        $guests = $database::guests_for_payment_invoice($booking_id);


//		$package_accommodation = $database::accommodation($booking_id);
//		$primary_guest = $database::primary_guest($booking_id);
//		$optional_tours = $database::optional_tours($booking_id);
//		$invoice_final_section = $database::invoice_final_section($booking_id);
		$deposit = $database::deposit($booking_id);
		$payment_type_text = ($payment_type == 'DD' ? 'Direct Debit' : 'Credit Card');

		$payment_status = "Unpaid"; // force

		$output = "<table width='100%'>
					<tr>
						<td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td>
						<td width='310'>
							<h3 style='font-size: 28px; margin: 0;'>PAYMENT RECEIPT</h3><br />
							Email: <a style='text-decoration: none; color: #f78a4f;' href='mailto:sales@australianoutbackmarathon.com'>sales@australianoutbackmarathon.com</a> <br />
							Within Australia: 1300 728 296 <br />
							International: <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2455'>+61 2 4385 2455</a> <br />
							Facsimile <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2476'>+61 2 4385 2476</a>
						</td>
					</tr>
				  </table><br><br>
				  
				  <table style='border: 1px solid #B7B7B7; border-collapse: collapse;' width='100%'>
					<tr style='border-bottom: 1px solid #B7B7B7;'>
						<td width='50%' style='border-right: 1px solid #B7B7B7;padding: 15px 30px 10px;'><h3 style='margin:0;'>BILLED TO:</h3></td>
						<td style='padding: 15px 30px 10px;'><h3 style='margin:0;'>INVOICE INFO</H3></td>
					</tr>
					
					<tr>
						<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 15px 30px 5px;'>{$primary_guest[0]['email']}</td>
						<td style='padding: 15px 30px 5px;'>Invoice : {$booking_id}</td>
					</tr>
					
					<tr>
						<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['firstname']} {$primary_guest[0]['lastname']}</td>
						<td style='padding: 0 30px 5px;'>Invoice Date : ". date('d/m/Y') ."</td>
					</tr>
					
					<tr>
						<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['address_1']} </td>
						<td style='padding: 0 30px 5px;'>Payment Total : {$deposit}</td>
					</tr>
					
					<tr>
						<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['suburb']} {$primary_guest[0]['state']} {$primary_guest[0]['postcode']}</td>
						<td style='padding: 0 30px 5px;'>Payment Type : Direct Debit</td>
					</tr>
					
					<tr>
						<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'>{$primary_guest[0]['country']}</td>
						<td style='padding: 0 30px 20px;'>Payment Status : {$payment_status}</td>
					</tr>
				  </table><br><br>
				  
				  <table style='border: 1px solid #B7B7B7;background: #F5F5F5;padding: 20px 30px 18px; width: 100%;'>
					<tr><td><h3 style='margin:0 0 10px;'>PLEASE NOTE</h3> The deposit charges on this invoice relate to the number of adults ONLY</td></tr>
				  </table><br><br>
				  
				  <table frame='box' style='width: 100%;border: 1px solid #B7B7B7;border-collapse: collapse; width:100%;'>
				  <tr style='background: #F38A51;color: #FFF;'><th style='padding: 12px 20px;text-align: left; width:220px;'>ITEM</th><th style='padding: 12px 20px;'>NO. OF CHILDREN</th><th style='padding: 12px 20px;'>NO. OF ADULTS</th><th style='padding: 12px 20px;'>AMOUNT</th></tr>
				  <tr style='font-size: 12px;'>
					<td style='padding: 20px 20px 12px;'>{$package_accommodation[0]['package_name']}</td>
					<td style='padding: 20px 20px 12px; text-align:center;'>{$database::get_number_children($_SESSION['booking_id'])}</td>
					<td style='padding: 20px 20px 12px; text-align:center;'>{$database::get_number_adults($_SESSION['booking_id'])}</td>
					<td style='padding: 20px 20px 12px; text-align:center;'>Deposit per adult - <span style='font-weight:bold;'>$550</span></td>
				  </tr>
				  <tr>
					<td style='padding: 0px 20px 12px;'>{$package_accommodation[0]['bedding_name']}</td>
					<td colspan='3'></td>
				  </tr>
				  <tr><td colspan='4'>&nbsp;</td></tr>
				  <tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px;'><strong>OPTIONAL TOURS</strong></td><td colspan='3'></td></tr>
 				  <tr><td colspan='4'>&nbsp;</td></tr>
				  <tr><td colspan='4' style='padding: 0px 20px 10px;'>";

	  			    for ($i=0; $i<count($optional_tours); $i++) {
	  			    	$output .= '<p style="margin: 0 0 7px;">' . $optional_tours[$i]['title'] . '</p>';
	  			    }
	  			    if(count($optional_tours) == 0) {
	  			    	$output .= '<p style="margin: 0 0 7px;">None</p>';
	  			    }

				$output .= "</td></tr></table><br/><br/>";
				
				$output  .= "
				<table width='100%' style='border-collapse: collapse;'>
					<tr>
						<td width='50%'></td>
						<td width='50%'>
							<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
								<tr style='background: #F38A51;color: #FFF;'><td colspan='2' style='padding: 12px 20px; text-align:center;'><strong>TOTAL</strong></td></tr>
								<tr><td colspan='2'>&nbsp;</td></tr>
								<tr>
									<td align='left' style='padding: 0px 20px 12px;'>Total ex GST</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'>$" . ($deposit - ($deposit / 11))  . "</td>
								</tr>
								<tr>
									<td align='left' style='padding: 0px 20px 12px;'>GST</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'>$" . ($deposit / 11) . "</td>
								</tr>
								<tr>
									<td align='left' style='padding: 0px 20px 12px;'><strong>Total Inc GST</strong></td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'><strong>\${$deposit}</strong></td>
								</tr>
								<tr><td colspan='2'>&nbsp;</td></tr>
							</table><div style='clear:both;'></div><br/><br/>
						</td>
					</tr>
					
					<tr>
						<td width='50%' style='vertical-align: top'>
							<table width='98%' style='float:left; border: 1px solid #B7B7B7; padding: 45px 25px 38px; background:#f5f5f5; line-height: 2;'>
								<tr><td><h3 style='margin:0 0 10px;'>PAYMENT INFORMATION</h3>
								This is an unpaid deposit only invoice for the 2015 Australian Outback Marathon.<br/> Please direct deposit to the account below. The deposit secures your booking for the number of guests as outlined on this invoice. One of our sales representatives will be in touch to finalise the booking.</div></tr>
							</table><div style='clear:both;'></div>
						</td>
						<td width='50%'>
							<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
								<tr style='background: #9c9c9c;color: #FFF;'><td colspan='2' style='padding: 12px 20px; text-align:center;'><strong>TRAVELLING FIT</strong></td></tr>
								<tr><td colspan='2'>&nbsp;</td></tr>
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
								</tr>
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>BSB</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>082-574</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8081</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
								</tr>
								
								<tr>
									<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>ABN 49 641 402 174 Total Includes GST.</td>
								</tr>
								
								<tr>
									<td colspan='2' style='padding: 0px 20px 10px;'>Direct Deposit (via internet only) <span style='font-weight:bold;'>- from an Australian Account</span></td>
								</tr>
						
								<tr><td colspan='2'>&nbsp;</td></tr>
							</table><div style='clear:both;'></div><br/><br/>
						</td>
					</tr>
					
					
					<tr>
						<td width='50%' style='vertical-align: top'>
							<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:left;'>
								<tr style='background: #9c9c9c;color: #FFF;'><td colspan='2' style='padding: 12px 20px; text-align:center;text-transform:uppercase; font-size: 12px;
'><strong>Direct Deposit at a National Australia Bank (walk in)</strong></td></tr>
								<tr><td colspan='2'>&nbsp;</td></tr>
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
								</tr>
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>BSB</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>082-574</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8081</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
								</tr>
								
								<tr>
									<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>ABN 49 641 402 174 Total Includes GST.</td>
								</tr>
								
								<tr>
									<td colspan='2' style='padding: 0px 20px 10px;'>Direct Deposit (via internet only) <span style='font-weight:bold;'>- from an Australian Account</span></td>
								</tr>
						
								<tr><td colspan='2'>&nbsp;</td></tr>
							</table><div style='clear:both;'></div><br/><br/>
						</td>
						
						<td width='50%'>
							<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
								<tr style='background: #9c9c9c;color: #FFF;'><td colspan='2' style='padding: 12px 20px; text-align:center;text-transform:uppercase;'><strong>Overseas Telegraphic or Wire Transfer *</strong></td></tr>
								<tr><td colspan='2'>&nbsp;</td></tr>
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Address</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Shop 21, 148 The Entrance<br/> Road, Erina, NSW, 2250, Australia</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
								</tr>

								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8006</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 5px;'>Swift Code</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>NATAAU3302S</td>
								</tr>
								
								<tr>
									<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
									<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
								</tr>
			
								<tr>
									<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>* Note: Payment from an overseas bank account (outside of Australia) will incur a bank fee of $25 per transaction.</td>
								</tr>
						
								<tr><td colspan='2'>&nbsp;</td></tr>
							</table><div style='clear:both;'></div><br/><br/>
						</td>
					</tr>
	
				</table><br/><br/>";

//        echo $output;exit;
		return $output;
	}

	public static function get_email_invoice_CC($booking_id, $payment_type)
	{

		// if credit card, display amount owing as well



		$database = new self;
//		$package_accommodation = $database::accommodation($booking_id);
//		$primary_guest = $database::primary_guest($booking_id);
		$package_accommodation = $database::accommodation_for_payment_invoice($booking_id);
		// var_dump($package_accommodation);exit;
		$primary_guest = $database::primary_guest_for_payment_invoice($booking_id);

		$optional_tours = $database::optional_tours($booking_id);
//		$invoice_final_section = $database::invoice_final_section($booking_id);
		$deposit = $database::deposit($booking_id);
		$payment_type_text = ($payment_type = 'DD' ? 'Direct Debit' : 'Credit Card');
		$code = urlencode($database::get_code_by_booking_id($_SESSION['booking_id']));
		$code_link = "<a ref='https://australianoutbackmarathon.com/booking/index.php?v=payment_confirmation_CC&c={$code}'>https://australianoutbackmarathon.com/booking/index.php?v=payment_method&c={$code}</a>";
		$code_link = "";	

		$amount_with_fee = $database::amount_paid_with_fee();
		$amount_paid = $database::amount_paid();

		$payment_status = "paid";




		$output = "
		<p style='font-size: 18px; margin-bottom: 30px;'>Thank you for your payment.</p>

		<table width='100%'>
		<tr>
			<td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td>
			<td width='310'>
				<h3 style='font-size: 28px; margin: 0;'>PAYMENT RECEIPT</h3> <br />
				Email: <a style='text-decoration: none; color: #f78a4f;' href='mailto:sales@australianoutbackmarathon.com'>sales@australianoutbackmarathon.com</a> <br />
				Within Australia: 1300 728 296 <br />
				International: <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2455'>+61 2 4385 2455</a> <br />
				Facsimile <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2476'>+61 2 4385 2476</a>
			</td>
		</tr>
		</table><br/><br/>
		
		<table style='border: 1px solid #B7B7B7; border-collapse: collapse;' width='100%'>
			<tr style='border-bottom: 1px solid #B7B7B7;'>
				<td width='50%' style='border-right: 1px solid #B7B7B7;padding: 15px 30px 10px;'><h3 style='margin:0;'>BILLED TO:</h3></td>
				<td style='padding: 15px 30px 10px;'><h3 style='margin:0;'>INVOICE INFO</H3></td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 15px 30px 5px;'>{$primary_guest[0]['email']}</td>
				<td style='padding: 15px 30px 5px;'>Invoice : {$booking_id}</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['firstname']} {$primary_guest[0]['lastname']}</td>
				<td style='padding: 0 30px 5px;'>Invoice Date : ". date('d/m/Y') ."</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['address_1']} </td>
				<td style='padding: 0 30px 5px;'>Total Cost: $ ".  Payment::total_cost($_SESSION['booking_id']) . "</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['suburb']} {$primary_guest[0]['state']} {$primary_guest[0]['postcode']}</td>
				<td style='padding: 0 30px 5px;'>Total Paid : $ {$amount_paid}</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'>{$primary_guest[0]['country']}</td>
				<td style='padding: 0 30px 5px;'>Total Paid with Fee : $ {$amount_with_fee }</td>
			</tr>
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 20px;'>Payment Type : Credit Card</td>
			</tr>
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 20px;'>Payment Status : {$payment_status}</td>
			</tr>
		</table><br/><br/>
		
		<table style='border: 1px solid #B7B7B7;background: #F5F5F5;padding: 20px 30px 18px; width: 100%;'>
			<tr>
				<td><h3 style='margin:0 0 10px;'>PLEASE NOTE</h3> The deposit charges on this invoice relate to the number of adults ONLY</td>
			</tr>
		</table><br><br>
		
		<table frame='box' style='width: 100%;border: 1px solid #B7B7B7;border-collapse: collapse; width:100%;'>
		
			<tr style='background: #F38A51;color: #FFF;'>
				<th style='padding: 12px 20px;text-align: left; width:220px;'>ITEM</th>
				<th style='padding: 12px 20px;'>NO. OF CHILDREN</th><th style='padding: 12px 20px;'>NO. OF ADULTS</th><th style='padding: 12px 20px;'>AMOUNT</th>
			</tr>
			
			<tr style='font-size: 12px;'>
				<td style='padding: 20px 20px 12px;'>" . $package_accommodation[0]['package_name'] . "</td>
				<td style='padding: 20px 20px 12px; text-align:center;'>{$database::get_number_children($_SESSION['booking_id'])}</td>
				<td style='padding: 20px 20px 12px; text-align:center;'>{$database::get_number_adults($_SESSION['booking_id'])}</td>
				<td style='padding: 20px 20px 12px; text-align:center;'>Deposit per adult - <span style='font-weight:bold;'>$550</span></td>
			</tr>
			
			<tr>
				<td style='padding: 0px 20px 12px;'>{$package_accommodation[0]['bedding_name']}</td>
				<td colspan='3'></td>
			</tr>
		  
			<tr><td colspan='4'>&nbsp;</td></tr>

			<tr style='background: #F38A51;color: #FFF;'>
				<td style='padding: 12px 20px;'><strong>OPTIONAL TOURS</strong></td>
				<td colspan='3'></td>
			</tr>

		  
		  <tr><td colspan='4'>&nbsp;</td></tr>
		  
			<tr>
				<td colspan='4' style='padding: 0px 20px 10px;'>";
					for ($i=0; $i<count($optional_tours); $i++) {
						$output .= '<p style="margin: 0 0 7px;">' . $optional_tours[$i]['title'] . '</p>';
					}
					if(count($optional_tours) == 0) {
						$output .= '<p style="margin: 0 0 7px;">None</p>';
					}
				$output .= "
				</td>
			</tr>
		</table><br/><br/>";
		
		
		$output .= "
		<table width='100%' style='border-collapse: collapse;'>
			<tr>
				<td width='50%'></td>
				<td width='50%'>
					<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
						<tr style='background: #F38A51;color: #FFF;'>
							<td colspan='2' style='padding: 12px 20px; text-align:center;'><strong>TOTAL</strong></td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td align='left' style='padding: 0px 20px 12px;'>Total ex GST</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'>$" . ($deposit - ($deposit / 11))  . "</td>
						</tr>
						<tr>
							<td align='left' style='padding: 0px 20px 12px;'>GST</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'>$" . ($deposit / 11) . "</td>
						</tr>
						<tr>
							<td align='left' style='padding: 0px 20px 12px;'><strong>Total Inc GST</strong></td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 12px;'><strong>$". $deposit . "</strong></td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</table>
					<div style='clear:both;'></div><br/><br/>
				</td>
			</tr>

			<tr>
				<td width='50%' style='vertical-align: top'>
					<table width='98%' style='float:left; border: 1px solid #B7B7B7; padding: 30px 25px; background:#f5f5f5; line-height: 1;'>
						<tr>
							<td>
								<h3 style='margin:0 0 10px;'>ADDITIONAL PAYMENT INFORMATION</h3>
								<p>To make further payments, please click on this link or paste it in your browser:<br/>
								{$code_link}</p>
								<p><strong>Deposit Amount:</strong> $" .Payment::deposit($_SESSION['booking_id']) . "</p>
								<p><strong>Total Amount:</strong> $" . Payment::total_cost($_SESSION['booking_id']) . "</p>
								<p><strong>Amount Due:</strong> $" .  Payment::get_amount_due($_SESSION['booking_id']) . "</p>
								<i>Please Note: Additional Direct Deposits will be deducted from the amount due once processed.</i>
							</td>
						</tr>
					</table><div style='clear:both;'></div>
				</td>
				
				<td width='50%' style='vertical-align: top;'>
					<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
						<tr style='background: #9c9c9c;color: #FFF;'>
							<td colspan='2' style='padding: 12px 20px; text-align:center;'><strong>TRAVELLING FIT</strong></td>
						</tr>
						
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
						</tr>
						
						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>BSB</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>082-574</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8081</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
						</tr>

						<tr>
							<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>ABN 49 641 402 174 Total Includes GST.</td>
						</tr>

						<tr>
							<td colspan='2' style='padding: 0px 20px 10px;'>Direct Deposit (via internet only) <span style='font-weight:bold;'>- from an Australian Account</span></td>
						</tr>

						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</table><div style='clear:both;'></div><br/><br/>
				</td>
			</tr>

			<tr>
				<td width='50%' style='vertical-align: top'>
					<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:left;'>
					
						<tr style='background: #9c9c9c;color: #FFF;'>
							<td colspan='2' style='padding: 12px 20px; text-align:center;text-transform:uppercase; font-size: 12px;'>
								<strong>Direct Deposit at a National Australia Bank (walk in)</strong>
							</td>
						</tr>
						
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
						</tr>
						
						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>BSB</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>082-574</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8081</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
						</tr>

						<tr>
							<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>ABN 49 641 402 174 Total Includes GST.</td>
						</tr>

						<tr>
							<td colspan='2' style='padding: 0px 20px 10px;'>Direct Deposit (via internet only) <span style='font-weight:bold;'>- from an Australian Account</span></td>
						</tr>

						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
					</table><div style='clear:both;'></div><br/><br/>
				</td>

				<td width='50%'>
					<table width='98%' style='border: 1px solid #B7B7B7; border-collapse: collapse; float:right;'>
						<tr style='background: #9c9c9c;color: #FFF;'>
							<td colspan='2' style='padding: 12px 20px; text-align:center;text-transform:uppercase;'><strong>Overseas Telegraphic or Wire Transfer *</strong></td>
						</tr>
						
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Bank</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>National Australia Bank</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Address</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Shop 21, 148 The Entrance<br/> Road, Erina, NSW, 2250, Australia</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Name</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>Travelling Fit</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Account Number</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>83-544-8006</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 5px;'>Swift Code</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 5px;'>NATAAU3302S</td>
						</tr>

						<tr>
							<td align='left' style='padding: 0px 20px 18px;'>Deposit Amount</td>
							<td align='right' style='font-weight:bold; padding: 0px 20px 18px;'>$ {$deposit}</td>
						</tr>

						<tr>
							<td colspan='2' style='padding: 20px 20px 10px;font-weight: bold;border-top: 1px solid #B7B7B7;'>* Note: Payment from an overseas bank account (outside of Australia) will incur a bank fee of $25 per transaction.</td>
						</tr>

						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</table><div style='clear:both;'></div><br/><br/>
				</td>
			</tr>
		</table><br/><br/>";
		return $output;
	}


	public static function get_email_invoice_CC_manual($booking_id, $payment_type)
	{

		// if credit card, display amount owing as well

		$database = new self;
		$package_accommodation = $database::accommodation($booking_id);
		$primary_guest = $database::primary_guest($booking_id);
		$optional_tours = $database::optional_tours($booking_id);
		$invoice_final_section = $database::invoice_final_section($booking_id);
		// $deposit = $database::deposit($booking_id);
		$payment_type_text = ($payment_type = 'DD' ? 'Direct Debit' : 'Credit Card');
		// $code = urlencode($database::get_code_by_booking_id($_SESSION['booking_id']));
		// $code_link = "<a ref='https://australianoutbackmarathon.com/booking/index.php?v=payment_confirmation_CC&c={$code}'>https://australianoutbackmarathon.com/booking/index.php?v=payment_method&c={$code}</a>";
		// $amount_with_fee = $database::amount_paid_with_fee();
		// $amount_paid = $database::amount_paid();

		$payment_status = "paid";

		$output = "
		<p style='font-size: 18px; margin-bottom: 30px;'>Thank you for your payment.</p>

		<table width='100%'>
		<tr>
			<td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td>
			<td width='310'>
				<h3 style='font-size: 28px; margin: 0;'>PAYMENT RECEIPT</h3> <br />
				Email: <a style='text-decoration: none; color: #f78a4f;' href='mailto:sales@australianoutbackmarathon.com'>sales@australianoutbackmarathon.com</a> <br />
				Within Australia: 1300 728 296 <br />
				International: <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2455'>+61 2 4385 2455</a> <br />
				Facsimile <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2476'>+61 2 4385 2476</a>
			</td>
		</tr>
		</table><br/><br/>
		
		<table style='border: 1px solid #B7B7B7; border-collapse: collapse;' width='100%'>
			<tr style='border-bottom: 1px solid #B7B7B7;'>
				<td width='50%' style='border-right: 1px solid #B7B7B7;padding: 15px 30px 10px;'><h3 style='margin:0;'>BILLED TO:</h3></td>
				<td style='padding: 15px 30px 10px;'><h3 style='margin:0;'>INVOICE INFO</H3></td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 15px 30px 5px;'>{$primary_guest[0]['email']}</td>
				<td style='padding: 15px 30px 5px;'>Invoice : {$booking_id}</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['firstname']} {$primary_guest[0]['lastname']}</td>
				<td style='padding: 0 30px 5px;'>Invoice Date : ". date('d/m/Y') ."</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['address_1']} </td>
				<td style='padding: 0 30px 5px;'></td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$primary_guest[0]['city']} {$primary_guest[0]['state']} {$primary_guest[0]['postcode']}</td>
				<td style='padding: 0 30px 5px;'>Total Paid : $ {$_SESSION['client_payment_amount_enter']}</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'>{$primary_guest[0]['country']}</td>
				<td style='padding: 0 30px 5px;'>Total Paid with Fee : $ {$_SESSION['amount_with_fee']}</td>
			</tr>
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 20px;'>Payment Type : Credit Card</td>
			</tr>
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 20px;'>Payment Status : {$payment_status}</td>
			</tr>
		</table><br/><br/>";
		
	
	
		return $output;
	}

public static function get_email_invoice_manual($amount, $payment_type, $booking_id, $amount_with_fee = "")
	{

		if(!session_id()) session_start();
		// if credit card, display amount owing as well

		// get the prefered_event_name based on prefered_event_id		
		switch($_SESSION['form2']['prefered_event_id'])
		{
			case '5':
				$prefered_event_name = "Marathon";
			break;

			case '6':
				$prefered_event_name = "Half Marathon";
			break;

			case '7':
				$prefered_event_name = "11km Fun Run";
			break;

			case '8':
				$prefered_event_name = "6km Fun Run";
			break;
		}
		
		$database = new self;
		$package_accommodation = $database::accommodation($booking_id);
		$primary_guest = $database::primary_guest($booking_id);
		// $optional_tours = $database::optional_tours($booking_id);
		// $invoice_final_section = $database::invoice_final_section($booking_id);
		// $deposit = $database::deposit($booking_id);
		$payment_type_text = ($payment_type == 'DD' ? 'Direct Debit' : 'Credit Card');
		// $code = urlencode($database::get_code_by_booking_id($_SESSION['booking_id']));
		// $code_link = "<a ref='https://australianoutbackmarathon.com/booking/index.php?v=payment_confirmation_CC&c={$code}'>https://australianoutbackmarathon.com/booking/index.php?v=payment_method&c={$code}</a>";
		// $amount_with_fee = $database::amount_paid_with_fee();
		// $amount_paid = $database::amount_paid();

		$payment_status = "paid";

		$output = "
		<p style='font-size: 18px; margin-bottom: 30px;'>Thank you for your payment.</p>

		<table width='100%'>
		<tr>
			<td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td>
			<td width='310'>
				<h3 style='font-size: 28px; margin: 0;'>PAYMENT RECEIPT</h3> <br />
				Email: <a style='text-decoration: none; color: #f78a4f;' href='mailto:sales@australianoutbackmarathon.com'>sales@australianoutbackmarathon.com</a> <br />
				Within Australia: 1300 728 296 <br />
				International: <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2455'>+61 2 4385 2455</a> <br />
				Facsimile <a style='text-decoration: none; color: #f78a4f;' href='tel:+61 2 4385 2476'>+61 2 4385 2476</a>
			</td>
		</tr>
		</table><br/><br/>
		
		<table style='border: 1px solid #B7B7B7; border-collapse: collapse;' width='100%'>
			<tr style='border-bottom: 1px solid #B7B7B7;'>
				<td width='50%' style='border-right: 1px solid #B7B7B7;padding: 15px 30px 10px;'><h3 style='margin:0;'>BILLED TO:</h3></td>
				<td style='padding: 15px 30px 10px;'><h3 style='margin:0;'>INVOICE INFO</H3></td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'>{$_SESSION['form2']['firstname']} {$_SESSION['form2']['lastname']}</td>
				<td style='padding: 0 30px 5px;'>Invoice Date : ". date('d/m/Y') ."</td>
			</tr>

			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 15px 30px 5px;'>{$_SESSION['form2']['email']}</td>
				<td style='padding: 15px 30px 5px;'>Invoice : {$booking_id}</td>
			</tr>
						
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'></td>
				<td style='padding: 0 30px 5px;'>Event: {$prefered_event_name}</td>
			</tr>
			
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 5px;'></td>
				<td style='padding: 0 30px 5px;'>Total Paid : $ {$amount}</td>
			</tr>";
$payment_type == "CC" ? $output .= "
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 5px;'>Total Paid with Fee : $ {$amount_with_fee}</td>
			</tr>" : "";

		$output .= "
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>
				<td style='padding: 0 30px 20px;'>Payment Type : {$payment_type_text}</td>
			</tr>
			<tr>
				<td width='50%' style='border-right: 1px solid #B7B7B7; padding: 0 30px 20px;'></td>";

		$payment_type == "CC" ? $output .= "<td style='padding: 0 30px 20px;'>Payment Status : {$payment_status}</td>" : $output .= "<td style='padding: 0 30px 20px;'>&nbsp;</td>";

		$output .= "
			</tr>
		</table><br/><br/>";
		
	
		return $output;
	}



	public static function get_email_confirmation($booking_id, $payment_type)
	{
		$database = new self;
//		$package_accommodation = $database::accommodation($booking_id);
        $package_accommodation = $database::accommodation_for_payment_invoice($booking_id);
//        var_dump($package_accommodation);exit;
//		$primary_guest = $database::primary_guest($booking_id);
        $primary_guest = $database::primary_guest_for_payment_invoice($booking_id);
		$optional_tours = $database::optional_tours($booking_id);
//		$invoice_final_section = $database::invoice_final_section($booking_id);
//		$guests = $database::guests($booking_id);
        $guests = $database::guests_for_payment_invoice($booking_id);
		// dump($guests);
		
		$output = "<table width='100%' style='margin-bottom:15px;'><tr><td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td><td><p>A booking has been made for the 2015 Australian Outback Marathon.</p>"
			    . "Booking number: <strong># $booking_id</strong><br/>"
			    . "Below are the details of the booking.</td></tr></table>"
			    . "<table frame='box' style='width: 100%;border: 1px solid #B7B7B7;border-collapse: collapse; width:100%;'>"
			    . "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;'>PACKAGE</td><td style='padding: 12px 20px; font-weight:bold;'>{$package_accommodation[0]['package_name']}</td></tr>"
			    
			    . "<tr><td style='padding: 15px 20px 8px;'><strong>Accommodation</strong></td><td></td></tr>"

			    . "<tr><td style='padding: 0 20px 8px;'>Number of Guests</td><td style='padding: 0 20px 8px;'>{$database::get_number_guests($_SESSION['booking_id'])}</td></tr>"
			    . "<tr><td style='padding: 0 20px 8px;'>Number of Adults</td><td style='padding: 0 20px 8px;'>{$database::get_number_adults($_SESSION['booking_id'])}</td></tr>"
			    . "<tr><td style='padding: 0 20px 8px;'>Number of Children</td><td style='padding: 0 20px 8px;'>{$database::get_number_children($_SESSION['booking_id'])}</td></tr>"
//			    . "<tr><td style='padding: 0 20px 8px;'>Accommodation Type</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['accommodation_name']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Bedding configuration</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['bedding_name']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Extra Night(s) - Pre Itinerary</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['extra_nights_pre']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Extra Night(s) - Post Itinerary</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['extra_nights_post']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Share Request</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['share_request_YN']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 15px;'>Do you require a rollaway or cot?</td><td style='padding: 0 20px 15px;'>{$package_accommodation[0]['rollaway_or_cot']}</td></tr>"

  			    . "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>PRIMARY CONTACT</td></tr>"

  			    . "<tr><td style='padding: 15px 20px 8px;'>First Name</td><td style='padding: 15px 20px 8px;'>{$primary_guest[0]['firstname']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Last Name</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['lastname']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Preferred Name</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['prefered_name']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Email Address</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['email']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Gender</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['gender']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Date of Birth</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['dob']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Preferred Event</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['event_name']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Shirt Size</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['tshirt_size']}</td></tr>"
                . "<tr><td style='padding: 0 20px 8px;'>Singlet Size</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['singlet_size']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Nationality</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['nationality']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Address</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['address_1']} " . " {$primary_guest[0]['suburb']} " . " {$primary_guest[0]['state']} " . " {$primary_guest[0]['postcode']} " . " {$primary_guest[0]['country']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Contact Phone Number</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['phone1_preferred']}</td></tr>"
  			    . "<tr><td style='padding: 0 20px 8px;'>Special Dietary Requirements</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['dietry_requirements']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 15px;'>Additional Information</td><td style='padding: 0 20px 15px;'>{$primary_guest[0]['additional_info']}</td></tr>"

  			     . "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>ADDITIONAL GUESTS</td></tr>";
  			    if(count($guests) <= 1) // only the primary guest, so no other guests
  			    {
  			    	$output .= "<tr><td style='padding: 15px 20px 15px;'>None</td><td></td></tr>";
  			    }
  			    else
  			    {		// *** GUESTS ***
  			    	for($i=1;$i<count($guests); $i++)
  			    	{
				  		$output.= "<tr><td style='padding: 15px 20px 8px;'>First Name</td><td style='padding: 15px 20px 8px;'>{$guests[$i]['firstname']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Last Name</td><td style='padding: 0 20px 8px;'>{$guests[$i]['lastname']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Preferred Name</td><td style='padding: 0 20px 8px;'>{$guests[$i]['prefered_name']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Email Address</td><td style='padding: 0 20px 8px;'>{$guests[$i]['email']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Gender</td><td style='padding: 0 20px 8px;'>{$guests[$i]['gender']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Date of Birth</td><td style='padding: 0 20px 8px;'>{$guests[$i]['dob']}</td></tr>"
						// . "<tr><td style='padding: 0 20px 8px;'>Preferred Event</td><td style='padding: 0 20px 8px;'>{$guests[$i]['event_name']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Shirt Size</td><td style='padding: 0 20px 8px;'>{$guests[$i]['tshirt_size']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Singlet Size</td><td style='padding: 0 20px 8px;'>{$guests[$i]['singlet_size']}</td></tr>"
						// . "<tr><td style='padding: 0 20px 8px;'>Nationality</td><td style='padding: 0 20px 8px;'>{$guests[$i]['nationality']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Address</td><td style='padding: 0 20px 8px;'>{$guests[$i]['address_1']}{$guests[$i]['suburb']}{$guests[$i]['state']}{$guests[$i]['postcode']}{$guests[$i]['country']}</td></tr>"
						. "<tr><td style='padding: 0 20px 8px;'>Contact Phone Number</td><td style='padding: 0 20px 8px;'>{$guests[$i]['phone1_preferred']}</td></tr>"
						. "<tr><td style='padding: 0 20px 15px;'>Special Dietary Requirements</td><td style='padding: 0 20px 8px;'>{$guests[$i]['dietry_requirements']}</td></tr>";
  			    	}
  			    }
				
  		$output .="<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; height:14px;' colspan='2'></td></tr>";
  		$output .="<tr><td style='padding: 15px 20px 8px;'><strong>Optional Tours Added</strong></td><td style='padding: 15px 20px 8px;'>";
  			    for ($i=0; $i<count($optional_tours); $i++)
  			    {
  			    	$output .= $optional_tours[$i]['title'] . '<br>';
  			    }
  			    if(count($optional_tours) == 0)
  			    {
  			    	$output .= "None<br>";
  			    }
  		$output .="</td></tr>";
  		// *** INSPIRATIONAL STORY ***
//  		$output .="<tr><td style='padding: 0 20px 8px;'><strong>Inspirational Story</strong></td><td style='padding: 0 20px 8px;'>";
//  		if(strlen($invoice_final_section[0]['inspirational_story']) > 0)
//  		{
//  			$output .= $invoice_final_section[0]['inspirational_story'];
//  		}
//  		else
//  		{
//  			$output .= "Not disclosed";
//  		}

  		$output .="</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Travel Insurance</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['travel_insurance_title']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Does anyone on the booking have an<br>inspirational story that you would like to<br>share?</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['inspirational_story']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Are you an International Travel Partner?</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['book_travel_partner']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Emergency Contact First Name</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['ec_firstname']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Emergency Contact Last Name</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['ec_lastname']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Emergency Contact Phone</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['ec_phone']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Emergency Contact Email Address</td><td style='padding: 0 20px 8px;'>{$invoice_final_section[0]['ec_email']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 15px;'>How did you hear about us?</td><td style='padding: 0 20px 15px;'>{$invoice_final_section[0]['name']}</td></tr>"
   		
 			    . "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>PAYMENT (DIRECT DEBIT)</td></tr>";
 		// if($payment_type == 'DD')
 		// {
 			$deposit = $database::deposit($booking_id);

 			$output .="<tr><td style='padding: 15px 20px 8px;'>Account Name</td><td style='padding: 15px 20px 8px;'>Travelling Fit</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;' colspan='2'>Direct Deposit (via internet only) – from an Australian Account</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Bank</td><td style='padding: 0 20px 8px;'>National Australia Bank</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>BSB</td><td style='padding: 0 20px 8px;'>082-574</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Account Number</td><td style='padding: 0 20px 8px;'>83-544-8081</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Deposit Amount</td><td style='padding: 0 20px 8px;'>$ {$deposit}</td></tr>"
 					. "<tr><td style='padding: 8px 20px 15px;' colspan='2'><strong>Direct Deposit at a National Australia Bank (walk in)</strong></td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Account Name</td><td style='padding: 0 20px 8px;'>Travelling Fit</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Bank</td><td style='padding: 0 20px 8px;'>National Australia Bank</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>BSB</td><td style='padding: 0 20px 8px;'>082-574</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Account Number</td><td style='padding: 0 20px 8px;'>83-544-8006</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Deposit Amount</td><td style='padding: 0 20px 8px;'>$ {$deposit}</td></tr>"
 					. "<tr><td style='padding: 8px 20px 15px;' colspan='2'><strong>Overseas Telegraphic or Wire Transfer *</strong></td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Bank</td><td style='padding: 0 20px 8px;'>National Australia Bank</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Address</td><td style='padding: 0 20px 8px;'>Shop 21, 148 The Entrance Road, Erina, NSW, 2250, Australia</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Account Name</td><td style='padding: 0 20px 8px;'>Travelling Fit</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Account Number</td><td style='padding: 0 20px 8px;'>83-544-8006</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Swift Code</td><td style='padding: 0 20px 8px;'>NATAAU3302S</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;'>Deposit Amount</td><td style='padding: 0 20px 8px;'>$ {$deposit}</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;' colspan='2'>&nbsp;</td></tr>"
 					. "<tr><td style='padding: 0 20px 8px;' colspan='2'><strong>* Note:</strong> Payment from an overseas bank account (outside of Australia) will incur a bank fee of $25 per transaction.</td></tr>"
 					. "<tr><td style='padding: 0 20px 15px;' colspan='2'>Please remember to add this fee to the total when making payment.</td></tr>"
 					. "</table>";

 		// }
//        echo $output;exit;
		return $output;
	}

	public static function get_email_confirmation_BP($booking_id, $payment_type)
	{
		$database = new self;
//		$package_accommodation = $database::accommodation($booking_id);
		$package_accommodation = $database::accommodation_for_payment_invoice($booking_id);
//        var_dump($package_accommodation);exit;
//		$primary_guest = $database::primary_guest($booking_id);
		$primary_guest = $database::primary_guest_for_payment_invoice($booking_id);
		$optional_tours = $database::optional_tours($booking_id);
//		$invoice_final_section = $database::invoice_final_section($booking_id);
//		$guests = $database::guests($booking_id);
		$guests = $database::guests_for_payment_invoice($booking_id);
		// dump($guests);

		$output = "<table width='100%' style='margin-bottom:15px;'><tr><td><img src='http://australianoutbackmarathon.com/wp-content/themes/AOM/images/logo.png' alt='Australian Outback Marathon Logo'></td><td><p>A booking has been made for the 2015 Australian Outback Marathon.</p>"
			. "Booking number: <strong># $booking_id</strong><br/>"
			. "Below are the details of the booking.</td></tr></table>"
			. "<table frame='box' style='width: 100%;border: 1px solid #B7B7B7;border-collapse: collapse; width:100%;'>"
			. "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;'>PACKAGE</td><td style='padding: 12px 20px; font-weight:bold;'>{$package_accommodation[0]['package_name']}</td></tr>"

			. "<tr><td style='padding: 15px 20px 8px;'><strong>Accommodation</strong></td><td></td></tr>"

			. "<tr><td style='padding: 0 20px 8px;'>Number of Guests</td><td style='padding: 0 20px 8px;'>{$database::get_number_guests($_SESSION['booking_id'])}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Number of Adults</td><td style='padding: 0 20px 8px;'>{$database::get_number_adults($_SESSION['booking_id'])}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Number of Children</td><td style='padding: 0 20px 8px;'>{$database::get_number_children($_SESSION['booking_id'])}</td></tr>"
//			    . "<tr><td style='padding: 0 20px 8px;'>Accommodation Type</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['accommodation_name']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Bedding configuration</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['bedding_name']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Extra Night(s) - Pre Itinerary</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['extra_nights_pre']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Extra Night(s) - Post Itinerary</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['extra_nights_post']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Share Request</td><td style='padding: 0 20px 8px;'>{$package_accommodation[0]['share_request_YN']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 15px;'>Do you require a rollaway or cot?</td><td style='padding: 0 20px 15px;'>{$package_accommodation[0]['rollaway_or_cot']}</td></tr>"

			. "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>PRIMARY CONTACT</td></tr>"

			. "<tr><td style='padding: 15px 20px 8px;'>First Name</td><td style='padding: 15px 20px 8px;'>{$primary_guest[0]['firstname']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Last Name</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['lastname']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Preferred Name</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['prefered_name']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Email Address</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['email']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Gender</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['gender']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Date of Birth</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['dob']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Preferred Event</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['event_name']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Shirt Size</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['tshirt_size']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Singlet Size</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['singlet_size']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 8px;'>Nationality</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['nationality']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Address</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['address_1']} " . " {$primary_guest[0]['suburb']} " . " {$primary_guest[0]['state']} " . " {$primary_guest[0]['postcode']} " . " {$primary_guest[0]['country']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Contact Phone Number</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['phone1_preferred']}</td></tr>"
			. "<tr><td style='padding: 0 20px 8px;'>Special Dietary Requirements</td><td style='padding: 0 20px 8px;'>{$primary_guest[0]['dietry_requirements']}</td></tr>"
//  			    . "<tr><td style='padding: 0 20px 15px;'>Additional Information</td><td style='padding: 0 20px 15px;'>{$primary_guest[0]['additional_info']}</td></tr>"

			. "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>ADDITIONAL GUESTS</td></tr>";
		if(count($guests) <= 1) // only the primary guest, so no other guests
		{
			$output .= "<tr><td style='padding: 15px 20px 15px;'>None</td><td></td></tr>";
		}
		else
		{		// *** GUESTS ***
			for($i=1;$i<count($guests); $i++)
			{
				$output.= "<tr><td style='padding: 15px 20px 8px;'>First Name</td><td style='padding: 15px 20px 8px;'>{$guests[$i]['firstname']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Last Name</td><td style='padding: 0 20px 8px;'>{$guests[$i]['lastname']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Preferred Name</td><td style='padding: 0 20px 8px;'>{$guests[$i]['prefered_name']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Email Address</td><td style='padding: 0 20px 8px;'>{$guests[$i]['email']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Gender</td><td style='padding: 0 20px 8px;'>{$guests[$i]['gender']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Date of Birth</td><td style='padding: 0 20px 8px;'>{$guests[$i]['dob']}</td></tr>"
					// . "<tr><td style='padding: 0 20px 8px;'>Preferred Event</td><td style='padding: 0 20px 8px;'>{$guests[$i]['event_name']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Shirt Size</td><td style='padding: 0 20px 8px;'>{$guests[$i]['tshirt_size']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Singlet Size</td><td style='padding: 0 20px 8px;'>{$guests[$i]['singlet_size']}</td></tr>"
					// . "<tr><td style='padding: 0 20px 8px;'>Nationality</td><td style='padding: 0 20px 8px;'>{$guests[$i]['nationality']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Address</td><td style='padding: 0 20px 8px;'>{$guests[$i]['address_1']}{$guests[$i]['suburb']}{$guests[$i]['state']}{$guests[$i]['postcode']}{$guests[$i]['country']}</td></tr>"
					. "<tr><td style='padding: 0 20px 8px;'>Contact Phone Number</td><td style='padding: 0 20px 8px;'>{$guests[$i]['phone1_preferred']}</td></tr>"
					. "<tr><td style='padding: 0 20px 15px;'>Special Dietary Requirements</td><td style='padding: 0 20px 8px;'>{$guests[$i]['dietry_requirements']}</td></tr>";
			}
		}

		$output .="<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; height:14px;' colspan='2'></td></tr>";
		$output .="<tr><td style='padding: 15px 20px 8px;'><strong>Optional Tours Added</strong></td><td style='padding: 15px 20px 8px;'>";
		for ($i=0; $i<count($optional_tours); $i++)
		{
			$output .= $optional_tours[$i]['title'] . '<br>';
		}
		if(count($optional_tours) == 0)
		{
			$output .= "None<br>";
		}
		$output .="</td></tr>";


		$output .="</td></tr>"


			. "<tr style='background: #F38A51;color: #FFF;'><td style='padding: 12px 20px; font-weight:bold;' colspan='2'>PAYMENT (BPAY)</td></tr>";
		$deposit = $database::deposit($booking_id);

//		var_dump($_SERVER['SCRIPT_NAME']);exit;

		$path = str_replace('client/index.php', '', $_SERVER['SCRIPT_NAME']);

//        var_dump("http://{$_SERVER['HTTP_HOST']}/{$path}includes/img/bpay_logo2edited.jpg");exit;

		$output .= "<tr><td><div class='wrapper'>
                                <div class='bpay-block'>
                                    <div class='logo' style='float: left; margin-right: 15px'>
                                        <img src='http://{$_SERVER['HTTP_HOST']}{$path}includes/img/bpay_logo2edited.jpg' style='width: 110px;'>
                                    </div>
                                    <div class='code-input'>
                                        <label for='BillerCode' style='font-weight: 800'>Biller Code: </label><input
                                            id='BillerCode' type='text' placeholder='<XXXX>' size='6'>
                                        <br/>
                                        <label for='Ref' style='font-weight: 800'>Ref: </label>
                                        <input id='Ref'  type='text' placeholder='<XXXX XXXX XXXX>' size='15' value='{$_SESSION['Bpay_ref']}'>
                                    </div>
                                    <div class='info'>
                                        <p><b>Telephone &amp; Internet Banking Bpay</b></p>

                                        <p>Contact yout bank or financial institution to make this payment from your
                                            cheque, savings, debit, credit card or transaction account.</p>

                                        <p>More info: <a href='www.bpa.com.au'>www.bpa.com.au</a></p>
                                    </div>
                                </div>
                            </div></td></tr>";

		$output .=  "</table>";

		// }
//        echo $output;exit;
		return $output;
	}

	public static function get_number_guests($booking_id)
	{
		$database = new self;
		
		// number of guests
		$sql = "SELECT count(user_id) number_guests FROM guests g WHERE g.booking_id = $booking_id";
		$number_guests = $database::findBySql($sql);

		return $number_guests[0]['number_guests'];

	}

	public static function get_number_adults($booking_id)
	{
		$database = new self;
		// number of adults

		$sql = "SELECT count(user_id) number_adults FROM guests g WHERE g.booking_id = $booking_id AND g.child_YN <> 'Y'";
		$number_adults = $database::findBySql($sql);

		return $number_adults[0]['number_adults'];

	}

	public static function get_number_children($booking_id)
	{

		$database = new self;

		// number of children
		$sql = "SELECT count(user_id) number_children FROM guests g WHERE g.booking_id = $booking_id AND g.child_YN = 'Y'";
		$number_children = $database::findBySql($sql);

		return $number_children[0]['number_children'];

	}

	public static function findById($id=0) 
	{
		$database = new self;
		$sql = "SELECT * FROM categories WHERE user_id={$id}";
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = $row;
		}
		return $object_array;
	}

	private static function populate($record)
	{
		// Could check that $record exists and is an array
		$object = new self;
	
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  $attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
		  if($object->hasAttribute($attribute)) {
			  // check to see if attribute is a clob and load the value if it is
			  if(is_object($value))
			  {
				  $object->$attribute = $value->read($value->size());
			  }
			  else
			  {
			  	$object->$attribute = stripslashes($value);
			  }
		  }
		}
		return $object;
	}

	private function hasAttribute($attribute) 
	{
	  // get_object_vars returns an associative array with all attributes 
	  // (incl. private ones!) as the keys and their current values as the value
	  $objectVars = get_object_vars($this);
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $objectVars);
	}


    public static function generateBpayRef($number, $user, $event) {

        $number = $number . $user . $event;

        $number = preg_replace("/\D/", "", $number);

        // The seed number needs to be numeric
        if(!is_numeric($number)) return false;

        // Must be a positive number
        if($number <= 0) return false;

        // Get the length of the seed number
        $length = strlen($number);

        $total = 0;

        // For each character in seed number, sum the character multiplied by its one based array position (instead of normal PHP zero based numbering)
        for($i = 0; $i < $length; $i++) $total += $number{$i} * ($i + 1);


        // The check digit is the result of the sum total from above mod 10
        $checkdigit = fmod($total, 10);

        // Return the original seed plus the check digit
        return $number . $checkdigit;

    }

}

?>