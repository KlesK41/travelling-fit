<?php

class Accommodation_bedding extends DatabaseObject
{
	protected static $table_name="Accommodation_bedding";

	// Accommodation_bedding members
	public $accommodation_bedding_id; // primary key
	public $accommodation_id;
	public $bedding_id;
	public $cost;
	public $available_yn;
	public $archived_yn;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function get_accommodation_bedding_id($accommodation_id=NULL, $bedding_id=NULL, $package_id=NULL)
	{
		$database = new self;

//		$sql =	"SELECT accommodation_bedding_id FROM accommodation_bedding
//				 WHERE accommodation_id = {$accommodation_id} AND bedding_id = {$bedding_id} 
//				 AND package_id = {$package_id} 
//				 LIMIT 1";

		$sql = "SELECT a.accommodation_bedding_id FROM
				(SELECT accommodation_bedding_id,  '1' as number  FROM accommodation_bedding
				 WHERE accommodation_id =  {$accommodation_id} AND bedding_id = {$bedding_id}
				 AND package_id = {$package_id}
				UNION
				# if no results must be no accommodation which always has bedding_id 7
				SELECT accommodation_bedding_id, '2' as number  FROM accommodation_bedding
				 WHERE accommodation_id = {$accommodation_id} AND bedding_id = 7
				 AND package_id =  {$package_id} ) a
				ORDER BY a.number ASC
				LIMIT 1";

		$result_set = $database->query($sql);
		$object_array = array();

		$row = $database->fetchArray($result_set);


		return $row['accommodation_bedding_id'];

	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO Accommodation_bedding (";
		$sql .= "accommodation_id, bedding_id, cost, available_yn, archived_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->accommodation_id) . "',";
		$sql .= "'". $database->escapeValue($this->bedding_id) . "',";
		$sql .= "'". $database->escapeValue($this->cost) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE Accommodation_bedding SET ";
		$sql .= "accommodation_id = '". $database->escapeValue($this->accommodation_id) . "',";
		$sql .= "bedding_id = '". $database->escapeValue($this->bedding_id) . "',";
		$sql .= "cost = '". $database->escapeValue($this->cost) . "',";
		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "'";
		$sql .=  " WHERE accommodation_bedding_id = ". $database->escapeValue($this->accommodation_bedding_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM Accommodation_bedding WHERE  accommodation_bedding_id =" . $database->escapeValue($this->accommodation_bedding_id);

		$database->query($sql);
	}

}

?>