<?php
class Accommodation extends DatabaseObject
{
	protected static $table_name="Accommodation";

	// Accommodation members
	public $accommodation_id; // primary key
	public $accommodation_name;
	public $available_yn;
	public $sort_order;
	public $archived_yn;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $number_people, package_id
	*  @return  object
	*/
	public static function getAccommodation($number_people, $package_id)
	{
		$database = new self;

		// $sql = "select DISTINCT a.*
		// 		from accommodation a, accommodation_bedding ab, bedding b, accommodation_package ap
		// 		where b.max_people = " . $number_people . " and ab.bedding_id = b.bedding_id 
		// 		and a.accommodation_id = ab.accommodation_id and ap.accommodation_id = a.accommodation_id
		// 		and ab.available_YN = 'Y' and a.available_YN = 'Y' and b.available_yn = 'Y'
		// 		and ap.package_id = ". $package_id . "
		// 		order by sort_order asc
		// 	    ";
		$sql = "-- select all the relevant accommodation according to the number of people
				SELECT DISTINCT ACC.*            
				FROM 	accommodation ACC,
						accommodation_bedding ACB,
						bedding BED,
						accommodation_package ACP
				WHERE ACB.bedding_id = BED.bedding_id AND BED.max_people = $number_people
				  AND ACC.accommodation_id = ACB.accommodation_id
				  AND ACP.accommodation_id = ACC.accommodation_id
				  AND ACB.available_YN = 'Y'
				  AND ACP.package_id = $package_id
				UNION

				-- select just the NO Accommodation line
				SELECT DISTINCT ACC.*
				FROM    accommodation ACC,
						accommodation_bedding ACB,
						bedding BED,
						accommodation_package ACP
				WHERE ACB.bedding_id = 7
				  AND ACC.accommodation_id = ACB.accommodation_id
				  AND ACP.accommodation_id = ACC.accommodation_id
				  AND ACB.available_YN = 'Y'
				  AND ACP.package_id = $package_id
				ORDER BY 1";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;
	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO Accommodation (";
		$sql .= "accommodation_name, available_yn, sort_order, archived_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->accommodation_name) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->sort_order) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE Accommodation SET ";
		$sql .= "accommodation_name = '". $database->escapeValue($this->accommodation_name) . "',";
		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
		$sql .= "sort_order = '". $database->escapeValue($this->sort_order) . "',";
		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "'";
		$sql .=  " WHERE accommodation_id = ". $database->escapeValue($this->accommodation_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM Accommodation WHERE  accommodation_id =" . $database->escapeValue($this->accommodation_id);

		$database->query($sql);
	}

}

?>