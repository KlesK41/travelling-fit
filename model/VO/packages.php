<?php

class Packages extends DatabaseObject
{
	protected static $table_name="Packages";

	// Packages members
	public $package_id; // primary key
	public $package_name;
	public $nights;
	public $available_yn;
	public $special_price_flag_yn;
	public $special_price_type;
	public $cost;
	public $sort_order;
	public $archived_yn;

    public $event_id;
    public $name;
    public $start_date;
    public $end_date;
    public $available;
    public $description;
    public $display_order;
    public $hotel;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getAllActive()
	{
		$database = new self;

		$sql =	"select * from packages where available_yn = 'Y' order by sort_order asc";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

    /**
     *  @param int $id
     *  @return object
     */

    public static function getAllForEvent($id) {
        $database = new self;

        $sql =	"SELECT * FROM packages WHERE available = 'Y' AND event_id = {$database->escapeValue($id)} ORDER BY display_order ASC";

//        return $sql;

        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = self::populate($row);
        }

        return $object_array;
    }

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO packages (";
		$sql .= "package_name, nights, available_yn, special_price_flag_yn, special_price_type, cost, sort_order, archived_yn ";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->package_name) . "',";
		$sql .= "'". $database->escapeValue($this->nights) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->special_price_flag_yn) . "',";
		$sql .= "'". $database->escapeValue($this->special_price_type) . "',";
		$sql .= "'". $database->escapeValue($this->cost) . "',";
		$sql .= "'". $database->escapeValue($this->sort_order) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

    public function add()
    {
        $database = new self; // instance of database object

        $sql  = "INSERT INTO packages (";
        $sql .= "event_id, name, start_date, end_date, available, description, display_order ";
        $sql .= ") VALUES (";
        $sql .= "'". $database->escapeValue($this->event_id) . "',";
        $sql .= "'". $database->escapeValue($this->name) . "',";
        $sql .= "'". $database->escapeValue($this->start_date) . "',";
        $sql .= "'". $database->escapeValue($this->end_date) . "',";
        $sql .= "'". $database->escapeValue($this->available) . "',";
        $sql .= "'". $database->escapeValue($this->description) . "',";
        $sql .= "'". $database->escapeValue($this->display_order) . "')";

//		var_dump(get_class_methods('Packages'));exit;

        if($database->query($sql))
        {
            return $database->insertId();
        } else {
            return false;
        }
    }

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE packages SET ";

//		$sql .= "package_name = '". $database->escapeValue($this->package_name) . "',";
//		$sql .= "nights = '". $database->escapeValue($this->nights) . "',";
//		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
//		$sql .= "special_price_flag_yn = '". $database->escapeValue($this->special_price_flag_yn) . "',";
//		$sql .= "special_price_type = '". $database->escapeValue($this->special_price_type) . "',";
//		$sql .= "cost = '". $database->escapeValue($this->cost) . "',";
//		$sql .= "sort_order = '". $database->escapeValue($this->sort_order) . "',";
//		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "'";
//		$sql .=  " WHERE package_id = ". $database->escapeValue($this->package_id);

		$sql .= "name = '". $database->escapeValue($this->name) . "',";
		$sql .= "start_date = '". $database->escapeValue($this->start_date) . "',";
		$sql .= "end_date = '". $database->escapeValue($this->end_date) . "',";
		$sql .= "available = '". $database->escapeValue(ucfirst($this->available)) . "',";
		$sql .= "description = '". $database->escapeValue($this->description) . "'";
		$sql .=  " WHERE package_id = ". $database->escapeValue($this->package_id);
        $sql .=  " AND event_id = ". $database->escapeValue($this->event_id);
//        var_dump($sql);exit;
		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM packages WHERE  package_id =" . $database->escapeValue($this->package_id);
		// var_dump($sql);exit;

		return ($database->query($sql)) ? true : false;


	}

	public function removeOptionalToursWithPackage()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM optional_tour_package WHERE  package_id =" . $database->escapeValue($this->package_id);
		// var_dump($sql);exit;

		return ($database->query($sql)) ? true : false;


	}

    public static function preparePackagesToJSON(array $packages) {
        $result = array();
        if (!empty($packages)) {
            foreach ($packages as $package) {
                $result[$package->package_id] = $package;
            }
        }
        return $result;
    }

	public static function getById($id)
	{
		$database = new self;

		$sql =	"select package_id, name from packages where available = 'Y' and package_id = " . $database->escapeValue($id);

//		var_dump($sql);exit;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array = self::populate($row);
		}



		return $object_array;

	}

}

?>