<?php

class Tshirts extends DatabaseObject
{
	protected static $table_name="Tshirts";

	// Tshirts members
	public $tshirt_id; // primary key
	public $size;
	public $type;
	public $available_yn;
	public $sort_order;
	public $archived_yn;
	public $user_id;

	public $singlet_id;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;

		$sql =	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO Tshirts (";
		$sql .= "size, type, available_yn, sort_order, archived_yn, user_id";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->size) . "',";
		$sql .= "'". $database->escapeValue($this->type) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->sort_order) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "',";
		$sql .= "'". $database->escapeValue($this->user_id) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE Tshirts SET ";
		$sql .= "size = '". $database->escapeValue($this->size) . "',";
		$sql .= "type = '". $database->escapeValue($this->type) . "',";
		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
		$sql .= "sort_order = '". $database->escapeValue($this->sort_order) . "',";
		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "',";
		$sql .= "user_id = '". $database->escapeValue($this->user_id) . "'";
		$sql .=  " WHERE tshirt_id = ". $database->escapeValue($this->tshirt_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM Tshirts WHERE  tshirt_id =" . $database->escapeValue($this->tshirt_id);

		$database->query($sql);
	}

}

?>