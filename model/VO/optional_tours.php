<?php

class Optional_tours extends DatabaseObject
{
	protected static $table_name="Optional_tours";

	// Optional_tours members
	public $optional_tour_id; // primary key
	public $title; // primary key
	public $tour_date;
	public $cost;
	public $description;
	public $available_yn;
	public $sort_order;
	public $archived_yn;

	public $event_id;
    public $package_id;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;

		$sql =	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	public static function getAllForEvent($event_id) {
		$database = new self;

		$sql =	"SELECT ot.optional_tour_id, ot.title, ot.description, ot.available_YN, ot.cost, otp.package_id
				FROM optional_tours ot INNER JOIN optional_tour_package otp ON otp.optional_tour_id = ot.optional_tour_id WHERE ot.available_YN = 'Y' AND ot.event_id = $event_id";

//        var_dump($sql);exit;
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;
	}

    public static function getAllForEventAdmin($event_id) {
        $database = new self;

        $sql =	"SELECT ot.optional_tour_id, ot.title, ot.description, ot.available_YN, ot.cost, otp.package_id
				FROM optional_tours ot INNER JOIN optional_tour_package otp ON otp.optional_tour_id = ot.optional_tour_id WHERE ot.available_YN = 'Y' AND ot.event_id = $event_id";
        $sql .= " AND otp.booking_id IS NULL AND otp.event_id IS NULL";

       // var_dump($sql);exit;
        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = self::populate($row);
        }

        return $object_array;
    }

//	public static function getAllForBooking($event_id, $booking_id, $user_id) {
//		$database = new self;
//
//		$sql =	"SELECT ot.optional_tour_id, ot.title FROM optional_tours ot INNER JOIN optional_tour_package otp ON ot.optional_tour_id = otp.optional_tour_id";
//		$sql .= " WHERE otp.event_id = " . $database->escapeValue($event_id);
//		$sql .= " AND otp.booking_id = " . $database->escapeValue($booking_id);
//		$sql .= " AND otp.user_id = " . $user_id;
//
//		$result_set = $database->query($sql);
//		$object_array = array();
//
//		while ($row = $database->fetchArray($result_set)) {
//			$object_array[] = $row['optional_tour_id'];
//		}
//
//		return $object_array;
//	}

    public static function getAllForBooking($event_id, $booking_id, $user_id) {
        $database = new self;

        $sql =	"SELECT ot.optional_tour_id, ot.title FROM optional_tours ot INNER JOIN booking_option_tours bot ON ot.optional_tour_id = bot.optional_tour_id";
        $sql .= " AND bot.booking_id = " . $database->escapeValue($booking_id);

        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = $row['optional_tour_id'];
        }

        return $object_array;
    }

//	public static function getAllForBookingPayment($event_id, $booking_id, $user_id) {
//		$database = new self;
//
//		$sql =	"SELECT ot.optional_tour_id, ot.title FROM optional_tours ot INNER JOIN optional_tour_package otp ON ot.optional_tour_id = otp.optional_tour_id";
//		$sql .= " WHERE otp.event_id = " . $database->escapeValue($event_id);
//		$sql .= " AND otp.booking_id = " . $database->escapeValue($booking_id);
//		$sql .= " AND otp.user_id = " . $user_id;
//
//		$result_set = $database->query($sql);
//		$object_array = array();
//
//		while ($row = $database->fetchArray($result_set)) {
//			$object_array[] = $row;
//		}
//
//		return $object_array;
//	}

    public static function getAllForBookingPayment($event_id, $booking_id, $user_id) {
        $database = new self;

        $sql =	"SELECT ot.optional_tour_id, ot.title FROM optional_tours ot INNER JOIN booking_option_tours bot ON ot.optional_tour_id = bot.optional_tour_id";
        $sql .= " WHERE bot.booking_id = " . $database->escapeValue($booking_id);

        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = $row;
        }

        return $object_array;
    }


	public static function getAllForPackages($packages) {
		$database = new self;

		if (!empty($packages)) {
			$packagesId = '';
			foreach ($packages as $package) {
				$packagesId .= $package->package_id . ',';
			}
			$packagesId = substr($packagesId, 0, -1);

			$sql =	"SELECT ot.optional_tour_id, ot.title, ot.description, otp.package_id  FROM optional_tours ot INNER JOIN optional_tour_package otp ON ot.optional_tour_id = otp.optional_tour_id";
			$sql .= " WHERE otp.package_id IN (" . $packagesId . ")";

			// var_dump($sql);exit;
			$result_set = $database->query($sql);
			$object_array = array();

			while ($row = $database->fetchArray($result_set)) {
				$object_array[] = self::populate($row);
			}
			return $object_array;
		} 
		return false;
	}

	public static function getAllForPackagesWP($packages) {
		$database = new self;

		if (!empty($packages)) {
			$packagesId = '';
			foreach ($packages as $package) {
				$packagesId .= $package->package_id . ',';
			}
			$packagesId = substr($packagesId, 0, -1);

			$sql =	"SELECT ot.optional_tour_id, ot.title, ot.description, otp.package_id  FROM optional_tours ot INNER JOIN optional_tour_package otp ON ot.optional_tour_id = otp.optional_tour_id";
			$sql .= " WHERE otp.package_id IN (" . $packagesId . ")";
			$sql .= " AND user_id IS NULL OR user_id = 0";

			// var_dump($sql);exit;
			$result_set = $database->query($sql);
			$object_array = array();

			while ($row = $database->fetchArray($result_set)) {
				$object_array[] = self::populate($row);
			}
			return $object_array;
		}
		return false;
	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}
	

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO optional_tours (";
		$sql .= "tour_date, cost, description, available_yn, sort_order, archived_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->tour_date) . "',";
		$sql .= "'". $database->escapeValue($this->cost) . "',";
		$sql .= "'". $database->escapeValue($this->description) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->sort_order) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

	public function add()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO optional_tours (";
		$sql .= "title, description, available_yn, event_id, cost ";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->title) . "',";
		$sql .= "'". $database->escapeValue($this->description) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->event_id) . "',";
		$sql .= "'". $database->escapeValue($this->cost) . "')";

		if($database->query($sql))
		{
			return $database->insertId();
		} else {
			return false;
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

//		$sql  = "UPDATE Optional_tours SET ";
//		$sql .= "tour_date = '". $database->escapeValue($this->tour_date) . "',";
//		$sql .= "cost = '". $database->escapeValue($this->cost) . "',";
//		$sql .= "description = '". $database->escapeValue($this->description) . "',";
//		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
//		$sql .= "sort_order = '". $database->escapeValue($this->sort_order) . "',";
//		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "'";
//		$sql .=  " WHERE optional_tour_id = ". $database->escapeValue($this->optional_tour_id);

		$sql  = "UPDATE optional_tours SET ";
		$sql .= "title = '". $database->escapeValue($this->title) . "',";
		$sql .= "description = '". $database->escapeValue($this->description) . "',";
        $sql .= "cost = '". $database->escapeValue($this->cost) . "',";
		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "'";
		$sql .=  " WHERE optional_tour_id = ". $database->escapeValue($this->optional_tour_id);
		$sql .=  " AND event_id = ". $database->escapeValue($this->event_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object
		$sql = "DELETE FROM optional_tours WHERE  optional_tour_id =" . $database->escapeValue($this->optional_tour_id);
		return ($database->query($sql)) ? true : false;
	}

    public function removeFromPackages()
    {
        $database = new self;// instance of database object
        $sql = "DELETE FROM optional_tour_package WHERE  optional_tour_id =" . $database->escapeValue($this->optional_tour_id);
        $sql .= " AND package_id=" . $database->escapeValue($this->package_id);
//        var_dump($sql);exit;
        return ($database->query($sql)) ? true : false;
    }

    public function removeFromBookings()
    {
        $database = new self;// instance of database object
        $sql = "DELETE FROM booking_option_tours WHERE  optional_tour_id =" . $database->escapeValue($this->optional_tour_id);
        return ($database->query($sql)) ? true : false;
    }

	public static function prepareOptionalToursToJSON(array $optionalTours) {
		$result = array();
		if (!empty($optionalTours)) {
			foreach ($optionalTours as $optionalTour) {
				$result[$optionalTour->optional_tour_id] = $optionalTour;
			}
		}
		return $result;
	}

//	public static function bookOptionalTours(array $data, $event_id, $booking_id, $user_id, $package_id) {
//
//		$database = new self;
//		$insertCnt = round(count($data) / 2);
//		$i = 1;
//
//		$sql = "INSERT INTO optional_tour_package (";
//		$sql .= "optional_tour_id, package_id, event_id, booking_id, user_id)";
//
//		$sql .= " VALUES ";
//
//		foreach ($data as $key=>$value) {
//			if ($i == 1) $sql .= "(";
//			$sql .= "'" . $database->escapeValue($value) . "',";
//			if ($i == $insertCnt) {
//				$sql .= $database->escapeValue($package_id) . ",";
//				$sql .= $database->escapeValue($event_id) . ",";
//				$sql .= $database->escapeValue($booking_id) . ",";
//				$sql .= $database->escapeValue($user_id) . "),";
//				$i = 0;
//			}
//			$i++;
//		}
//		$sql = substr($sql, 0, -1);
//		if ($database->query($sql)) {
//			return true;
//		} else return false;
//	}

    public static function bookOptionalTours(array $data, $event_id, $booking_id, $user_id, $package_id) {

        $database = new self;
        $insertCnt = round(count($data) / 2);
        $i = 1;

        $sql = "INSERT INTO booking_option_tours (";
        $sql .= "optional_tour_id, booking_id)";

        $sql .= " VALUES ";

        foreach ($data as $key=>$value) {
            if ($i == 1) $sql .= "(";
            $sql .= $database->escapeValue($value) . ",";
            if ($i == $insertCnt) {
                $sql .= $database->escapeValue($booking_id) . "),";
                $i = 0;
            }
            $i++;
        }
        $sql = substr($sql, 0, -1);

        // var_dump($sql);exit;
        if ($database->query($sql)) {
            return true;
        } else return false;
    }



//	public static function deleteFromOptionalTourPackage($booking_id, $user_id)
//	{
//		$database = new self;// instance of database object
//		$sql = "DELETE FROM optional_tour_package WHERE  booking_id = " . $database->escapeValue($booking_id);
//		$sql .= " AND user_id = " . $database->escapeValue($user_id);
//
//		return ($database->query($sql)) ? true : false;
//	}

    public static function deleteFromOptionalTourPackage($booking_id, $user_id)
    {
        $database = new self;// instance of database object
        $sql = "DELETE FROM booking_option_tours WHERE  booking_id = " . $database->escapeValue($booking_id);
//        $sql .= " AND user_id = " . $database->escapeValue($user_id);
        return ($database->query($sql)) ? true : false;
    }


	public static function getForBook($book_id) {
		$database = new self;

		$sql =	"SELECT ot.optional_tour_id, ot.title FROM optional_tours ot INNER JOIN optional_tour_package otp WHERE available_YN = 'Y' AND event_id = $event_id";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;
	}

	public function addOptionalTourToPackage() {
        $database = new self;// instance of database object
        $sql = "INSERT INTO optional_tour_package (package_id, optional_tour_id) VALUES (";
        $sql .= "'" . $database->escapeValue($this->package_id) . "',";
        $sql .= "'" . $database->escapeValue($this->optional_tour_id) . "')";
//        var_dump($sql);exit;

        return ($database->query($sql)) ? true : false;
    }

}

?>