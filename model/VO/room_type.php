<?php

class Room_type extends DatabaseObject
{
	protected static $table_name="Room_type";

	// Room_type members
	public $room_type_id; // primary key
	public $accommodation_id;
	public $room_type_code;
	public $bedding_type_id;
	public $available_yn;
	public $sort_order;
	public $archived_yn;

	// bedding_type
	public $bedding_type_name;

	public $package_id;
	public $name;
	public $price;
	public $capacity;
	public $status;
	public $hotel_name;
	public $event_id;
	public $optional_tour;


	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// getByAccommodationId function
	/**
	*   
	*
	*  @param  int  $accommodationId 
	*  @return  object
	*/
	public static function getByAccommodationId($id)
	{
		$database = new self;

		$sql =	"select room_type_id, bedding_type_name
				from room_type rt, bedding_type bt
				where bt.bedding_type_id = rt.bedding_type_id
				and rt.accommodation_id = {$id}
				and rt.available_yn = 'Y'
				";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

    public static function getAllForEvent($event_id) {
        $database = new self;

        $sql =	"SELECT room_type_id, name, price, capacity, status, hotel_name, event_id, optional_tour, package_id
				FROM room_type WHERE status = 'Y' AND event_id = {$database->escapeValue($event_id)}";

        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = self::populate($row);
        }

        return $object_array;
    }

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function add() {
		$database = new self; // instance of database object

		$sql  = "INSERT INTO room_type (";
		$sql .= "name, price, capacity, status, hotel_name, event_id, optional_tour, package_id";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->name) . "',";
		$sql .= "'". $database->escapeValue($this->price) . "',";
		$sql .= "'". $database->escapeValue($this->capacity) . "',";
		$sql .= "'". $database->escapeValue($this->status) . "',";
		$sql .= "'". $database->escapeValue($this->hotel_name) . "',";
		$sql .= "'". $database->escapeValue($this->event_id) . "',";
		$sql .= "'". $database->escapeValue($this->optional_tour) . "',";
        $sql .= "'". $database->escapeValue($this->package_id) . "')";

        if($database->query($sql))
        {
            return $database->insertId();
        } else {
            return false;
        }


	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO room_type (";
		$sql .= "accommodation_id, room_type_code, bedding_type_id, available_yn, sort_order, archived_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->accommodation_id) . "',";
		$sql .= "'". $database->escapeValue($this->room_type_code) . "',";
		$sql .= "'". $database->escapeValue($this->bedding_type_id) . "',";
		$sql .= "'". $database->escapeValue($this->available_yn) . "',";
		$sql .= "'". $database->escapeValue($this->sort_order) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		// to return the id, use the following:
		/*
		$sql .= " returning  into :";

		if($database->query($sql, "")) {
			$this-> = $database->insertId();
		} else {
			return false; 
		}
		*/
		//die($sql);

		if($database->query($sql)) 
		{
			//$this-> = $database->insert_id();
		} else {
			return false; 
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

//		$sql  = "UPDATE Room_type SET ";
//		$sql .= "accommodation_id = '". $database->escapeValue($this->accommodation_id) . "',";
//		$sql .= "room_type_code = '". $database->escapeValue($this->room_type_code) . "',";
//		$sql .= "bedding_type_id = '". $database->escapeValue($this->bedding_type_id) . "',";
//		$sql .= "available_yn = '". $database->escapeValue($this->available_yn) . "',";
//		$sql .= "sort_order = '". $database->escapeValue($this->sort_order) . "',";
//		$sql .= "archived_yn = '". $database->escapeValue($this->archived_yn) . "'";
//		$sql .=  " WHERE room_type_id = ". $database->escapeValue($this->room_type_id);

        $sql  = "UPDATE room_type SET ";
        $sql .= "name = '". $database->escapeValue($this->name) . "',";
        $sql .= "price = '". $database->escapeValue($this->price) . "',";
        $sql .= "capacity = '". $database->escapeValue($this->capacity) . "',";
        $sql .= "status = '". $database->escapeValue($this->status) . "',";
        $sql .= "hotel_name = '". $database->escapeValue($this->hotel_name) . "'";
        $sql .=  " WHERE room_type_id = ". $database->escapeValue($this->room_type_id);
        $sql .=  " AND event_id = ". $database->escapeValue($this->event_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object
		$sql = "DELETE FROM room_type WHERE  room_type_id =" . $database->escapeValue($this->room_type_id);
        return ($database->query($sql)) ? true : false;
	}

    public function deleteByPackageId()
    {
        $database = new self;// instance of database object
        $sql = "DELETE FROM room_type WHERE  package_id =" . $database->escapeValue($this->package_id);
        return ($database->query($sql)) ? true : false;
    }

	public static function prepareRoomTypesToJSON(array $roomTypes, $optional_tour = 'N') {
		$result = array();
		if (!empty($roomTypes)) {
			foreach ($roomTypes as $roomType) {
				if ($roomType->optional_tour == $optional_tour) {
					$result[$roomType->room_type_id] = $roomType;
				}
			}
		}
		return $result;
	}


	public static function getById($id) {
		$database = new self;

		$sql =	"SELECT * FROM room_type WHERE room_type_id = " . $database->escapeValue($id);

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array = self::populate($row);
		}

		return $object_array;
	}

}

?>