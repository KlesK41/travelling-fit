<?php

class Bookings extends DatabaseObject
{
	protected static $table_name="Bookings";

	// Bookings members
	public $booking_id; // primary key
	public $booking_code;
	public $total_cost;
	public $deposit;
	public $accommodation_bedding_id;
	public $accommodation_id;
	public $room_type_id;	// as requested 30/11/14
	public $bedding_id;
	public $bedding_type_id; // as requested 30/11/14
	public $package_id;
	public $number_adult_guests;
	public $number_children_guests;
	public $extra_nights_pre;
	public $extra_nights_post;
	public $share_request_yn;
	public $rollaway_or_cot;
	public $travel_insurance;
	public $inspirational_story;
	public $book_travel_partner_yn;
	public $travel_partner_id;
	public $ec_firstname;
	public $ec_lastname;
	public $ec_phone;
	public $ec_email;
	public $how_hear_id;
	public $ack_tc_yn;

    public $travel_insurance_option_id;
    public $additional_information;
    public $active_living_membership;
    public $event_id;
	public $user_id;
	public $step;
	public $completed;
	public $paid;
	public $total_paid;
	public $bedding_configuration_id;
	public $archieved_yn;

    public $tshirt_id;
    public $tshirt_quantity;
    public $tshirt_name;
	public $singlet_id;
	public $singlet_quantity;


	// child objects
	public $Guests;
	public $Accommodation;
	public $Bedding;
	public $Accommodation_bedding;
	public $Optional_tours;
	public $Guest_option_tours;


	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );

		$this->Guests = new Guests;
		$this->Accommodation = new Accommodation;
		$this->Bedding = new Bedding;
		$this->Accommodation_bedding = new Accommodation_bedding;
		$this->Optional_tours = new Optional_tours;
		$this->Booking_option_tours = new Booking_option_tours;
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;

		$sql =	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	public static function put_booking_id_into_session($booking_code)
	{
		$database = new self;

		$sql =	"select booking_id from bookings where booking_code = '" . $database->escapeValue($booking_code) . "'";

		$result_set = $database->query($sql);
		$row = $database->fetchArray($result_set);
		if($result_set->num_rows == 0)
		{
			die('You\'ve entered an invalid code. Please check the link and try again.');
		}
		else
		{
			$_SESSION['booking_id'] = $row['booking_id'];
		}

	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostData($postArray){
		$this->package_id	 				= $postArray[1]['value'];
		$this->number_adults				= $postArray[2]['value'];
		$this->number_children				= $postArray[3]['value'];
		$this->accommodation_bedding_id		= $postArray[5]['value'];
		$this->extra_nights_pre				= $postArray[6]['value'];
		$this->extra_nights_post			= $postArray[7]['value'];
		$this->share_request_yn				= $postArray[8]['value'];
		$this->rollaway_or_cot				= $postArray[9]['value'];
	}
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public static function store_booking()
	{	

		$storeBooking = new self;

		if(isset($_GET['c'])) // if the code is set, get booking details based on code
		{
			$booking_code = $storeBooking->escapeValue($_GET['c']);
			// show remaining balance
			// booking id
			// Package Name:
			// No. of Adults:
			// Tours:
			
		}
		else
		{
			isset($_SESSION['booking_id']) ? $upsert = 'update' : $upsert = 'create';

			// need to store accommodation_bedding_id in booking table
			if(isset($_SESSION['accommodation_id']) && isset($_SESSION['bedding_id']))
			{
				$_SESSION['accommodation_bedding_id'] = $storeBooking->Accommodation_bedding->get_accommodation_bedding_id($_SESSION ['accommodation_id'], $_SESSION ['bedding_id'], $_SESSION['package_id']);
			}
			
			foreach($_SESSION['additional_information'] as $key => $value)
			{
				$_SESSION[$key] = $value;
			}

	
			if($upsert == 'create')
			{
			 	$_SESSION['booking_code'] 	= $storeBooking::generate_key();
			}

			
			$storeBooking = $storeBooking::populate($_SESSION);			
			$storeBooking->$upsert();
			$_SESSION['total_cost'] = Payment::total_cost($storeBooking->booking_id);

			$sql = "update bookings set total_cost ='" . $_SESSION['total_cost'] . "' where booking_id = " . $storeBooking->booking_id;
			$storeBooking->query($sql);




			// put the booking id into the session variable
			$upsert == 'update' ? $storeBooking->booking_id = $_SESSION['booking_id'] : $_SESSION['booking_id'] = $storeBooking->booking_id;
			
			$storeBooking->Booking_option_tours->delete_by_booking_id($_SESSION['booking_id']);
			$storeBooking->Booking_option_tours = $storeBooking->Booking_option_tours->populate_optional_tours($_SESSION['optional_tours']);
			for($i=0; $i<count($storeBooking->Booking_option_tours); $i++)
			{
				$storeBooking->Booking_option_tours[$i]->booking_id = $_SESSION['booking_id'];
				$storeBooking->Booking_option_tours[$i]->create();
			}

			$storeBooking->Guests->delete_by_booking_id($_SESSION['booking_id']);
			$storeBooking->Guests = $storeBooking->Guests->populate_guests($_SESSION, $_SESSION['number_guests']);
			for($i=0; $i<count($storeBooking->Guests); $i++)
			{	
				$storeBooking->Guests[$i]->booking_id = $_SESSION['booking_id'];
				$storeBooking->Guests[$i]->guest_no = ($i + 1);
				$storeBooking->Guests[$i]->create();
			}
			
			// $storeBooking->Accommodation = $storeBooking->Accommodation->populate(array("accommodation_id" => $_SESSION ['accommodation_id']));
			// $storeBooking->Bedding = $storeBooking->Bedding->populate(array("bedding_id" =>$_SESSION ['bedding_id']));
			// $storeBooking->Accommodation_bedding = $storeBooking->Accommodation_bedding->populate(array("accommodation_id" => $_SESSION ['accommodation_id'], 
		}																									// "bedding_id" =>$_SESSION ['bedding_id']));

	}

	// store public key and private key (in case user loses it) and send private key in URL to user
	public static function generate_key()
	{
			// $pubkey=$pubkey["key"];
		return md5(uniqid("BWSAOM5x5", true));
	}

	public function createForm2CC($amount)
	{
		if(!session_id()) session_start();

		$database = new self; // instance of database object

		$sql  = "INSERT INTO bookings (";
		$sql .= "total_cost, deposit, accommodation_bedding_id, accommodation_id, bedding_id, bedding_type_id, room_type_id, package_id, number_adults,"; 
		$sql .= "number_children, extra_nights_pre, extra_nights_post, inspirational_story, book_travel_partner_yn, travel_partner_id, how_hear_id, ack_tc_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($amount) . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("99") . "',"; // 99 signifies no accommodation
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("1") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("--") . "',";
		$sql .= "'". $database->escapeValue("N") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("Y") . "')";

		if($database->query($sql)) 
		{
			$this->booking_id = $database->insertId();
			
		} else {
			return false; 
		}


		$sql  = "INSERT INTO guests (";
		$sql .= "booking_id, primary_contact_yn, guest_no, firstname, lastname, prefered_name, email, child_yn, gender, dob, prefered_event_id,";
		$sql .= "phone";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->booking_id) . "',";
		$sql .= "'". $database->escapeValue("Y") . "',";
		$sql .= "'". $database->escapeValue("1") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["firstname"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["lastname"]) . "',";
		$sql .= "'". $database->escapeValue("--") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["email"]) . "',";
		$sql .= "'". $database->escapeValue("N") . "',";
		$sql .= "'". $database->escapeValue("-") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["dob"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["prefered_event_id"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["phone"]) . "')";

		if($database->query($sql)) 
		{
			$this->user_id = $database->insertId();
			return "S";
		} else {
			return false; 
		}
	}
	public function createForm2($amount)
	{
		if(!session_id()) session_start();

		$database = new self; // instance of database object

		$sql  = "INSERT INTO bookings (";
		$sql .= "total_cost, deposit, accommodation_bedding_id, accommodation_id, bedding_id, bedding_type_id, room_type_id, package_id, number_adults,"; 
		$sql .= "number_children, extra_nights_pre, extra_nights_post, inspirational_story, book_travel_partner_yn, travel_partner_id, how_hear_id, ack_tc_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($amount) . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("99") . "',"; // 99 signifies no accommodation
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("99") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("1") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("--") . "',";
		$sql .= "'". $database->escapeValue("N") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("0") . "',";
		$sql .= "'". $database->escapeValue("Y") . "')";

		if($database->query($sql)) 
		{
			$this->booking_id = $database->insertId();
			
		} else {
			return false; 
		}


		$sql  = "INSERT INTO guests (";
		$sql .= "booking_id, primary_contact_yn, guest_no, firstname, lastname, prefered_name, email, child_yn, gender, dob, prefered_event_id,";
		$sql .= "phone";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->booking_id) . "',";
		$sql .= "'". $database->escapeValue("Y") . "',";
		$sql .= "'". $database->escapeValue("1") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["firstname"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["lastname"]) . "',";
		$sql .= "'". $database->escapeValue("--") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["email"]) . "',";
		$sql .= "'". $database->escapeValue("N") . "',";
		$sql .= "'". $database->escapeValue("-") . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["dob"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["prefered_event_id"]) . "',";
		$sql .= "'". $database->escapeValue($_SESSION["form2"]["phone"]) . "')";


		if($database->query($sql)) 
		{
			$this->user_id = $database->insertId();
			return "S";
		} else {
			return false; 
		}
	}

	public function firstStepCreate() {
		$database = new self;
		$sql = "INSERT INTO bookings (package_id, user_id, step, completed, event_id, room_type_id)";
		$sql .= " VALUES (" . $database->escapeValue($this->package_id) . ", ";
        $sql .= $database->escapeValue($this->user_id) . ", ";
        $sql .= $database->escapeValue($this->step) . ", ";
        $sql .= "'" . $database->escapeValue($this->completed) . "', ";
        $sql .= $database->escapeValue($this->event_id) . ", ";
        $sql .= $database->escapeValue($this->room_type_id) . /*", "*/ ")";

//		if (empty($this->tshirt_id)) $this->tshirt_id = 'NULL';
//        $sql .= $database->escapeValue($this->tshirt_id) . ",";
//        $sql .= "'" . $database->escapeValue($this->tshirt_name) . "',";
//        $sql .= $database->escapeValue($this->tshirt_quantity) . ",";
//
//        if (empty($this->singlet_id)) $this->singlet_id = 'NULL';
//        $sql .= $database->escapeValue($this->singlet_id) . ",";
//        $sql .= $database->escapeValue($this->singlet_quantity) . ")";

//		 var_dump($sql);exit;

		if ($database->query($sql)) {
			return $database->insertId();
		} else {
			return false;
		}
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO bookings (";
		$sql .= "booking_code, total_cost, deposit, accommodation_bedding_id, accommodation_id, bedding_id, room_type_id, package_id, number_adults, number_children, extra_nights_pre, extra_nights_post, share_request_YN, rollaway_or_cot, travel_insurance, inspirational_story, book_travel_partner_yn, travel_partner_id, ec_firstname, ec_lastname, ec_phone, ec_email, how_hear_id, ack_tc_yn, extra_nights_post";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->booking_code) . "',";
		$sql .= "'". $database->escapeValue($this->total_cost) . "',";
		$sql .= "'". $database->escapeValue($this->deposit) . "',";
		$sql .= "'". $database->escapeValue($this->accommodation_bedding_id) . "',";
		$sql .= "'". $database->escapeValue($this->accommodation_id) . "',";
		$sql .= "'". $database->escapeValue($this->bedding_id) . "',";
		$sql .= "'". $database->escapeValue($this->room_type_id) . "',";
		$sql .= "'". $database->escapeValue($this->package_id) . "',";
		$sql .= "'". $database->escapeValue($this->number_adults) . "',";
		$sql .= "'". $database->escapeValue($this->number_children) . "',";
		$sql .= "'". $database->escapeValue($this->extra_nights_pre) . "',";
		$sql .= "'". $database->escapeValue($this->extra_nights_post) . "',";
		$sql .= "'". $database->escapeValue($this->share_request_yn) . "',";
		$sql .= "'". $database->escapeValue($this->rollaway_or_cot) . "',";
		$sql .= "'". $database->escapeValue($this->travel_insurance) . "',";
		$sql .= "'". $database->escapeValue($this->inspirational_story) . "',";
		$sql .= "'". $database->escapeValue($this->book_travel_partner_yn) . "',";
		$sql .= "'". $database->escapeValue($this->travel_partner_id) . "',";
		$sql .= "'". $database->escapeValue($this->ec_firstname) . "',";
		$sql .= "'". $database->escapeValue($this->ec_lastname) . "',";
		$sql .= "'". $database->escapeValue($this->ec_phone) . "',";
		$sql .= "'". $database->escapeValue($this->ec_email) . "',";
		$sql .= "'". $database->escapeValue($this->how_hear_id) . "',";
		$sql .= "'". $database->escapeValue($this->ack_tc_yn) . "',";
		$sql .= "'". $database->escapeValue($this->extra_nights_post) . "')";

		if($database->query($sql)) 
		{
			$this->booking_id = $database->insertId();
			return "S";
		} else {
			return false; 
		}
	}

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE bookings SET ";
		$sql .= "booking_code = '". $database->escapeValue($this->booking_code) . "',";
		$sql .= "total_cost = '". $database->escapeValue($this->total_cost) . "',";
		$sql .= "deposit = '". $database->escapeValue($this->deposit) . "',";
		$sql .= "accommodation_bedding_id = '". $database->escapeValue($this->accommodation_bedding_id) . "',";
		$sql .= "accommodation_id = '". $database->escapeValue($this->accommodation_id) . "',";
		$sql .= "bedding_id = '". $database->escapeValue($this->bedding_id) . "',";
		$sql .= "room_type_id = '". $database->escapeValue($this->room_type_id) . "',";
		$sql .= "package_id = '". $database->escapeValue($this->package_id) . "',";
		$sql .= "number_adults = '". $database->escapeValue($this->number_adults) . "',";
		$sql .= "number_children = '". $database->escapeValue($this->number_children) . "',";
		$sql .= "extra_nights_pre = '". $database->escapeValue($this->extra_nights_pre) . "',";
		$sql .= "share_request_YN = '". $database->escapeValue($this->share_request_yn) . "',";
		$sql .= "rollaway_or_cot = '". $database->escapeValue($this->rollaway_or_cot) . "',";
		$sql .= "travel_insurance = '". $database->escapeValue($this->travel_insurance) . "',";
		$sql .= "inspirational_story = '". $database->escapeValue($this->inspirational_story) . "',";
		$sql .= "book_travel_partner_yn = '". $database->escapeValue($this->book_travel_partner_yn) . "',";
		$sql .= "travel_partner_id = '". $database->escapeValue($this->travel_partner_id) . "',";
		$sql .= "ec_firstname = '". $database->escapeValue($this->ec_firstname) . "',";
		$sql .= "ec_lastname = '". $database->escapeValue($this->ec_lastname) . "',";
		$sql .= "ec_phone = '". $database->escapeValue($this->ec_phone) . "',";
		$sql .= "ec_email = '". $database->escapeValue($this->ec_email) . "',";
		$sql .= "how_hear_id = '". $database->escapeValue($this->how_hear_id) . "',";
		$sql .= "ack_tc_yn = '". $database->escapeValue($this->ack_tc_yn) . "',";
		$sql .= "extra_nights_post = '". $database->escapeValue($this->extra_nights_post) . "'";
		$sql .=  " WHERE booking_id = ". $database->escapeValue($this->booking_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

    public function updateBooking()
    {
        $database = new self; // instance of database object

        $sql  = "UPDATE bookings SET ";
        $sql .= "bedding_configuration_id = '". $database->escapeValue($this->bedding_id) . "',";
        $sql .= "extra_nights_pre = '". $database->escapeValue($this->extra_nights_pre) . "',";
        $sql .= "extra_nights_post = '". $database->escapeValue($this->extra_nights_post) . "',";
        $sql .= "share_request_YN = '". $database->escapeValue($this->share_request_yn) . "',";
        $sql .= "travel_insurance_option_id = '". $database->escapeValue($this->travel_insurance_option_id) . "',";
        $sql .= "how_hear_id = '". $database->escapeValue($this->how_hear_id) . "',";
        $sql .= "step = '". $database->escapeValue($this->step) . "',";
        $sql .= "active_living_membership = '". $database->escapeValue($this->active_living_membership) . "',";
        $sql .= "additional_information = '". $database->escapeValue($this->additional_information) . "',";
        $sql .= "completed = '". $database->escapeValue($this->completed) . "',";
        $sql .= "number_adult_guests = '". $database->escapeValue($this->number_adult_guests) . "',";
        $sql .= "number_children_guests = '". $database->escapeValue($this->number_children_guests) . "',";
        if (empty($this->total_cost)) $this->total_cost = 'NULL';

		$sql .= "total_cost = ". $database->escapeValue($this->total_cost) /*. ","*/;

//        if (empty($this->tshirt_id)) $this->tshirt_id = 'NULL';
//        $sql .= "tshirt_id = ". $database->escapeValue($this->tshirt_id) . ",";
//        $sql .= "tshirt_quantity = '". $database->escapeValue($this->tshirt_quantity) . "',";
//
//        if (empty($this->singlet_id)) $this->singlet_id = 'NULL';
//        $sql .= "singlet_id = ". $database->escapeValue($this->singlet_id) . ",";
//        $sql .= "singlet_quantity = '". $database->escapeValue($this->singlet_quantity) . "',";
//
//        $sql .= "tshirt_name = '". $database->escapeValue($this->tshirt_name) . "'";


        $sql .=  " WHERE booking_id = ". $database->escapeValue($this->booking_id);

//         var_dump($sql);exit;

//        var_dump($sql);exit;

        $database->query($sql);

        return ($database->affectedRows() == 1) ? true : false;
    }

    public function updateBookingPaidStatus()
    {
        $database = new self; // instance of database object

        $sql  = "UPDATE bookings SET ";
        $sql .= "paid = '". $database->escapeValue($this->paid) . "', ";
        $sql .= "total_paid = '". $database->escapeValue($this->total_paid) . "'";
        $sql .=  " WHERE booking_id = ". $database->escapeValue($this->booking_id);



        // var_dump($sql);exit;

//        var_dump($sql);exit;

        $database->query($sql);

        return ($database->affectedRows() == 1) ? true : false;
    }

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM Bookings WHERE  booking_id =" . $database->escapeValue($this->booking_id);

		$database->query($sql);
	}

	public static function getBookingForUser($booking_id, $user_id) {
		$database = new self;
		$sql = "SELECT * FROM bookings WHERE booking_id = " . $database->escapeValue($booking_id);
		$sql .= " AND user_id = " . $database->escapeValue($user_id);
		$result_set = $database->query($sql);
		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array = self::populate($row);
		}
		return $object_array;
	}

    public function updateStep()
    {
        $database = new self; // instance of database object
        $sql  = "UPDATE bookings SET ";
        $sql .= "step = '". $database->escapeValue($this->step) . "'";
        $sql .=  " WHERE booking_id = ". $database->escapeValue($this->booking_id);
        $database->query($sql);
        return ($database->affectedRows() == 1) ? true : false;
    }

    // Puts values from array $data to properties of Guests object
    public function setValues(Bookings $guest, $data) {
        foreach ($guest as $guestKey => $guestValue) {
            foreach ($data as $dataKey => $dataValue) {
                if ($guestKey == $dataKey) {
                    $guest->$guestKey = $dataValue;
                }
            }
        }
        return $guest;
    }

	public function checkBookForUser() {
		$database = new self;

		$sql = "SELECT b.booking_id FROM bookings b INNER JOIN guests g ";
        $sql .= "ON b.booking_id = g.booking_id AND ";
        $sql .= "b.user_id = g.user AND b.event_id = g.prefered_event_id ";
        $sql .= " AND b.event_id = " . $database->escapeValue($this->event_id);
        $sql .= " AND b.user_id = " . $database->escapeValue($this->user_id);
        $sql .= " AND b.package_id = " . $database->escapeValue($this->package_id);
        $sql .= " AND b.room_type_id = " . $database->escapeValue($this->room_type_id);
        $sql .= " AND g.primary_contact_YN = 'Y'";

		$result_set = $database->query($sql);
		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array = self::populate($row);
		}
		return (!empty($object_array)) ? $object_array->booking_id : null;
	}

    public static function getCompleted($user) {
        $database = new self;

        $sql = "SELECT p.name, b.booking_id FROM packages p INNER JOIN bookings b ";
        $sql .= "ON p.package_id = b.package_id WHERE b.user_id = " . $database->escapeValue($user) . " AND b.completed = 'Y'";


        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = $row;
        }
        return $object_array;
    }

    public static function getById($id, $user = null) {
        $database = new self;

        $sql = "SELECT * FROM bookings WHERE booking_id = " . $database->escapeValue($id);

//        var_dump($sql);
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetchArray($result_set)) {
            $object_array = self::populate($row);
        }
        return $object_array;
    }

	public static function getAllForUser($user) {
		$database = new self;

		$sql = "SELECT p.name, p.end_date, b.booking_id, b.paid, b.completed, b.package_id, b.room_type_id, b.event_id, archieved_yn, b.total_paid FROM packages p INNER JOIN bookings b ";
		$sql .= "ON p.package_id = b.package_id WHERE b.user_id = " . $database->escapeValue($user);
        $sql .= " AND archieved_yn = 'N'";
		$sql .= " AND YEAR(timestamp) BETWEEN YEAR(NOW()) -1 AND YEAR(NOW())";

//        var_dump($sql);


		$result_set = $database->query($sql);
		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = $row;
		}
		return $object_array;
	}

	public static function getBookingsForAdmin() {
		$database = new self;

		$sql = "SELECT b.booking_id, p.name as package_name, b.total_cost";
		$sql .= ", g.firstname as guest_firstname, g.lastname as guest_lastname"; 
		$sql .= " FROM bookings b NATURAL JOIN packages p ";
		$sql .= " INNER JOIN guests g ON g.booking_id = b.booking_id ";
		$sql .= " WHERE g.primary_contact_YN = 'Y'";

		$result_set = $database->query($sql);
		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = $row;
		}
		return $object_array;
	}

    public function archieveBooking() {
        $database = new self;
        $sql = "UPDATE bookings SET archieved_yn = '" . $database->escapeValue($this->archieved_yn) . "'";
        $sql .= " WHERE booking_id = " . $database->escapeValue($this->booking_id);
        $sql .= " AND user_id = " . $database->escapeValue($this->user_id);

//        var_dump($sql);exit;
        $database->query($sql);
//        var_dump($database->affectedRows());exit;
        return ($database->affectedRows() == 1) ? true : false;
    }

}

?>