<?php
class Users extends DatabaseObject 
{

	protected static $table_name="users";
		
	// Users table attributes
	public $user_id;
	public $access_level;
	public $username;
	public $firstname;
	public $lastname;


	// Metadata user attributes
	public $uniqueid;		// guid
	public $sn;				// surname
	public $givenname; 		// firstname
	public $displayname; 	// usually firstname + lastname
	public $gugender; 		// gender

	public $token;
	public $email;
	public $password;
	public $status;

	function __construct()
	{
		parent::__construct();
	}

	public static function isAdmin()
	{

		return true;//($_SESSION['generic_access_level'] == ADMIN); // force -- return true while setting up
	}

	public static function isEditor()
	{
		return true;//($_SESSION['generic_access_level'] == EDITOR) || ($_SESSION['generic_access_level'] == ADMIN); // force -- return true while setting up
	}

	public static function isGeneral()
	{
		return true;//($_SESSION['generic_access_level'] == GENERAL); // force -- return true while setting up
	}

	// Common Database Methods are in database.php


	public static function findById($id=0) 
	{
		$database = new self;
		$sql = "SELECT * FROM categories WHERE user_id={$id}";
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	/**
	* 	add's a staff member to the database if they have a valid guid and aren't currently added
	*  assumes properties are already populated
	* 
	*  @return  string
	*/
	public function addUser($guid=NULL, $newAccessLevel=NULL)
	{
		$database = new self;

		$this->username 	= $database->escapeValue($guid);
		$this->access_level 	= $database->escapeValue($newAccessLevel);
		

		
		if(!Users::isAdmin() && $newAccessLevel > EDITOR)
		{
			return "Sorry you must be an administrator to add admins";
		}

		if($this->isInPeopleSoft($this->username))
		{
			$accessLevel = self::isInUserTable($this->username); // returns an access level if user exists
			if(!$accessLevel)
			{
				if($this->createUser())
					return "Staff member {$this->firstname} {$this->lastname} has been added";
				else
					return "There was an error adding the user!";
			}
			else
			{
				if($newAccessLevel > $accessLevel)
				{
					if(Users::updateAccessLevel($newAccessLevel, $this->username))
					{
						return "{$this->firstname} {$this->lastname}'s access level has been upgraded";
					}
					else
					{
						return "{$this->firstname} {$this->lastname}'s access level could not be upgraded";
					}
				}

				return "{$this->firstname} {$this->lastname} already exists with an access level $accessLevel";
			}
		}
		else
		{
			return "{$this->username} is an invalid number";
		}
	}

	public function assignUser($guid, $documentId)
	{
		$database = new self;

		if($this->isInPeopleSoft($guid))
		{
			$user = self::getUserId($guid);
			if(isset($user[0]->access_level) && $user[0]->access_level == ADMIN)
			{
				return "The user is an administrator and doesn't need to be assigned.";
			}

			if(!isset($user[0]->user_id))
			{
				$this->access_level = EDITOR;
				$this->createUser();
			}
			else
			{
				$this->user_id = $user[0]->user_id;
			}

			// check if user is already assigned
			$sql = "select user_id from document_users where user_id = {$this->user_id} and document_id = {$documentId}";
			$checkUserAssigned = self::findBySql($sql);
			if(count($checkUserAssigned) > 0)
			{
				return "User is already assigned to this document.";
			}

			// assign the user
			if(self::assignUserToDocument($this->user_id, $documentId))
			{
				return "User assigned to document id {$documentId}";
			}
			else
			{
				return "Could not assign this user to document id {$documentId}";
			}
		}
		else
		{
			return "{$this->username} is an invalid number";
		}
	}

	public static function updateAccessLevel($accessLevel, $username)
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE users SET ";
		$sql .= "access_level='". $database->escapeValue($accessLevel) . "' "; 
		$sql .=  "WHERE username = '". $database->escapeValue($username) . "'";
		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;

	}
	 /**
	 * 	checks if a users is currently in the users table
	 * 
	 *  @param  string  $guid  
	 *  @return  boolean
	 */
	public static function isInUserTable($guid)
	{
		$user = self::findBySql("select * from users where username = '{$guid}'");
		if (count($user) == 1) 
		{
			return  $user[0]->access_level;
		}
		else
		{
			return false;
		}
	}

	 /**
	 * 	get a users user_id
	 * 
	 *  @param  string  $guid  
	 *  @return  boolean
	 */
	public static function getUserId($guid)
	{
		$user = self::findBySql("select * from users where username = '{$guid}'");
		if (count($user) > 0) 
		{
			return  $user;
		}
		else
		{
			return false;
		}
	}

	 /**
	 * 	checks if a users guid is valid
	 * 
	 *  @param  string  $guid  
	 *  @return  boolean
	 */
	public function isInPeopleSoft($guid)
	{
		$user = self::findBySql("select * from md_person@metadata mpb where mpb.uniqueid = '{$guid}'");
		if (count($user) == 1)
		{
			// assign metadata properties to user properties in case we want to add the user using createUser method
			//$this->username	 		= 	$user[0]->uniqueid;
			//$this->firstname 		= 	$user[0]->givenname;
			//$this->lastname 		= 	$user[0]->sn;
			//$this->access_level 	= 	ADMIN;
			//$this->ethics_approval	= 	0;

			$this->username = $user[0]->uniqueid;
			$this->firstname = $user[0]->givenname;
			$this->lastname = $user[0]->sn;
			//$this->ethics_approval = $_SESSION['generic_ethics_approval'] = $user->ethics_approval;
			//$_SESSION['generic_currentUser'] =  strtoupper($user[0]->givenname) . " " . strtoupper($user[0]->sn) . ", " . strtoupper($user[0]->uniqueid);
			
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function authenticate($username="",$password="") 
	{
		$database = new self;
		$username = $database->escapeValue($username);
		// $password = $database->escapeValue($password);

		$password = crypt($database->escapeValue($password), $database->escapeValue($username));


		
		$sql = "SELECT * FROM users 
				WHERE email = '{$username}' and password = '{$password}'";
				// echo $sql;

		$result_array = self::findBySql($sql);

		return !empty($result_array) ? array_shift($result_array) : false;
	}

	public static function isInSpecifiedClass($userId, $documentId) 
	{
		$database = new self;
		// select course from from documents and cross-query with metadata
		$sql = "
				 SELECT distinct mpb.uniqueid, mpb.sn, mpb.givenname
	             FROM md_class@metadata              mcc,
	                  md_class_uniquemember@metadata mcu,
	                  md_person@metadata             mpb,
	                  documents d
	             WHERE mcc.cn_class = mcu.cn_class
	             AND mcu.stdnt_uniquemember = mpb.uniqueid AND mpb.uniqueid =  trim('{$userId}')
	             AND mcc.gucataloguenumber = trim(d.course) AND d.document_id = " . $database->escapeValue($documentId) . "
				";
		$result_set = $database->query($sql);
		
		return $database->numRows($result_set) > 0 ? 1 : 0;
	}


	// sample function
	/**
	 *  Get 
	 *
	 *  @param  int  $id 
	 *  @return  object
	 */

	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;
		$sql = 	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	// sample process $_POST function
	/**
	 *  process post variable - e.g. select list with values prefixed with FSLIBBED@
	 *
	 *  @param  array  $postArray 
	 *  @return  true or false
	 */

	public function processPost($postArray)
	{

		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->createUser();
			}
		}
		return true;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private static function populate($record) 
	{
		$database = new self;
		// Could check that $record exists and is an array
		$object = new self;

		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  $attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
		  if($object->hasAttribute($attribute)) {
			  // check to see if attribute is a clob and load the value if it is
			  if(is_object($value))
			  {
				  $object->$attribute = stripslashes($value->read($value->size()));
			  }
			  else
			  {
			  	$object->$attribute = stripslashes($value);
			  }
		  }
		}
		return $object;
	}

	private function hasAttribute($attribute) 
	{
	  // get_object_vars returns an associative array with all attributes 
	  // (incl. private ones!) as the keys and their current values as the value
	  $objectVars = get_object_vars($this);
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $objectVars);
	}

	public static function assignUserToDocument($user_id, $document_id)
	{
		$database = new self;
		$sql = "insert into document_users (user_id, document_id) values (" . $user_id . ", " . $document_id . ")";
		if($database->query($sql)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function createUser()
	{
		$database = new self; // instance of database object

		// $sql  = "INSERT INTO users(";
		// $sql .= "access_level, username, firstname, lastname";
		// $sql .= ") VALUES (";	
		// $sql .= "'". $database->escapeValue($this->access_level)."',";
		// $sql .= "'". $database->escapeValue($this->username)."',";
		// $sql .= "'". $database->escapeValue($this->firstname)."',";
		// $sql .= "'". $database->escapeValue($this->lastname)."')";
	
		// // to return the id, use the following:
		
		// $sql .= " returning user_id into :user_id";

		$sql  = "INSERT INTO users(";
		$sql .= "access_level, email, password, token, status";
		$sql .= ") VALUES (";	
		$sql .= "'". $database->escapeValue($this->access_level)."',";
		$sql .= "'". $database->escapeValue($this->email)."',";
		$sql .= "'". $database->escapeValue($this->password)."',";
		$sql .= "'". $database->escapeValue($this->token)."',";
		$sql .= "'". $database->escapeValue($this->status)."')";
	
		// to return the id, use the following:
	
		if($database->query($sql)) {
			$this->user_id = $database->insertId();
			return $this->user_id;
		} else {
			return false; 
		}
	}


	public function updateUser() 
	{
		$database = new self; // instance of database object
		$sql  = "UPDATE Users SET ";
		$sql .= "access_level='". $database->escapeValue($this->access_level) . "' , "; 
		$sql .= "username='". $database->escapeValue($this->username) . "' , "; 
		$sql .= "firstname='". $database->escapeValue($this->firstname) . "' , "; 
		$sql .= "lastname='". $database->escapeValue($this->lastname) . "' ";
		$sql .=  "WHERE user_id= ". $database->escapeValue($this->user_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}


	public function restorePassword()
	{
		$database = new self; // instance of database object
		$sql  = "UPDATE users SET ";
		$sql .= "password='". $database->escapeValue($this->password) . "'";
		$sql .=  "WHERE email= '". $database->escapeValue($this->email) . "'";

//		var_dump($sql);exit;
		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public static function deleteByUserId($user_id) 
	{
		$database = new self; // instance of database object
		$sql = "delete from Users where user_id =" . $database->escapeValue($user_id);
		
		$database->query($sql);
	}

	public function deleteUser() 
	{
		$database = new self; // instance of database object
		$sql = "delete from Users where user_id =" . $database->escapeValue($this->user_id);

		$database->query($sql);
	}

	public static function findByEmail($email) 
	{
		$database = new self;
		$sql = "SELECT * FROM users WHERE email='{$email}'";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	public static function findByToken($token) 
	{
		$database = new self;
		$sql = "SELECT * FROM users WHERE token='{$token}'";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array = self::populate($row);
		}
		return $object_array;
	}

	public function activateUser() 
	{
		$database = new self; // instance of database object
		$sql  = "UPDATE users SET ";
		$sql .= "status='enabled', token=NULL "; 
		$sql .=  " WHERE user_id= ". $database->escapeValue($this->user_id);
		// var_dump($sql);exit;

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public static function findByFbId($fb_id) 
	{
		$database = new self;
		$sql = "SELECT * FROM users WHERE fb_id='{$fb_id}'";

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array = self::populate($row);
		}
		return $object_array;
	}

	public function addUserFromFb()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO users(";
		$sql .= "access_level, status, firstname, lastname, fb_id";
		$sql .= ") VALUES (";	
		$sql .= "'". $database->escapeValue($this->access_level)."',";
		$sql .= "'". $database->escapeValue($this->status)."',";
		$sql .= "'". $database->escapeValue($this->firstname)."',";
		$sql .= "'". $database->escapeValue($this->lastname)."',";
		$sql .= "'". $database->escapeValue($this->fb_id)."')";

		// var_dump($sql);exit;
		// to return the id, use the following:
		// var_dump($sql);exit;
		if($database->query($sql)) {
			$this->user_id = $database->insertId();
			return $this->user_id;
		} else {
			return false; 
		}
	}
}
?>