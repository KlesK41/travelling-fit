<?php

class Transactions extends DatabaseObject
{
	protected static $table_name="transactions";

	// transactions members
	public $transaction_id; // primary key
	public $cardholders_name;
	public $amount;
	public $amount_with_fee;
	public $payment_method;
	public $ts;
	public $merchant_trans_id;
	public $booking_id;
	public $funds_received_YN;
	public $comments;
	public $archived_yn;

	public $user_id;
	public $bpay_code;

	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;

		$sql =	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO transactions (";
		$sql .= "cardholders_name, amount, payment_method, merchant_trans_id, booking_id, funds_received_YN, comments, archived_YN, user_id ";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->cardholders_name) . "',";
		$sql .= "'". $database->escapeValue($this->amount) . "',";
		$sql .= "'". $database->escapeValue($this->payment_method) . "',";
		$sql .= "'". $database->escapeValue($this->merchant_trans_id) . "',";
		$sql .= "'". $database->escapeValue($this->booking_id) . "',";
		$sql .= "'". $database->escapeValue($this->funds_received_YN) . "',";
		$sql .= "'". $database->escapeValue($this->comments) . "',";
		$sql .= "'". $database->escapeValue($this->archived_YN) . "',";
		$sql .= "'". $database->escapeValue($this->user_id) . "')";

		if($database->query($sql)) 
		{
			$this->transaction_id = $database->insertId();
			return $this->transaction_id;
		} else {
			return false; 
		}
		
	}

	public static function createPayment($payment_method, $cardholders_name, $amount, $merchant_trans_id, $booking_id, $funds_received_YN)
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO transactions (";
		$sql .= "cardholders_name, amount, payment_method, merchant_trans_id, booking_id, funds_received_YN, comments";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($cardholders_name) . "',";
		$sql .= "'". $database->escapeValue($amount) . "',";
		$sql .= "'". $database->escapeValue($payment_method) . "',";
		$sql .= "'". $database->escapeValue($merchant_trans_id) . "',";
		$sql .= "'". $database->escapeValue($booking_id) . "',";
		$sql .= "'". $database->escapeValue($funds_received_YN) . "',";
		$sql .= "'". $database->escapeValue("local entrant - DD") . "')";


		if($database->query($sql)) 
		{
			$transaction_id = $database->insertId();
			return $transaction_id;
		} else {
			return false; 
		}
		
	}


	public static function updatePayment($transaction_id, $cardholders_name, $amount, $amount_with_fee, $merchant_trans_id, $booking_id, $funds_received_YN)
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE transactions SET ";
		$sql .= "cardholders_name = '". $database->escapeValue($cardholders_name) . "',";
		$sql .= "amount = '". $database->escapeValue($amount) . "',";
		$sql .= "amount_with_fee = '". $database->escapeValue($amount_with_fee) . "',";
		$sql .= "merchant_trans_id = '". $database->escapeValue($merchant_trans_id) . "',";
		$sql .= "booking_id = '". $database->escapeValue($booking_id) . "',";
		$sql .= "comments = '". $database->escapeValue("local entrant - CC") . "',";
		$sql .= "funds_received_YN = '". $database->escapeValue($funds_received_YN) . "' ";
		$sql .=  " WHERE transaction_id = ". $database->escapeValue($transaction_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public static function updatePaymentAdditionalPayment($transaction_id, $cardholders_name, $amount, $amount_with_fee, $merchant_trans_id, $booking_id, $funds_received_YN)
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE transactions SET ";
		$sql .= "cardholders_name = '". $database->escapeValue($cardholders_name) . "',";
		$sql .= "amount = '". $database->escapeValue($amount) . "',";
		$sql .= "amount_with_fee = '". $database->escapeValue($amount_with_fee) . "',";
		$sql .= "merchant_trans_id = '". $database->escapeValue($merchant_trans_id) . "',";
		$sql .= "booking_id = '". $database->escapeValue($booking_id) . "',";
		$sql .= "comments = '". $database->escapeValue("Additional Payment") . "',";
		$sql .= "funds_received_YN = '". $database->escapeValue($funds_received_YN) . "' ";
		$sql .=  " WHERE transaction_id = ". $database->escapeValue($transaction_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function updatePaymentMethod()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE transactions SET ";
		$sql .= "payment_method = '". $database->escapeValue($this->payment_method) . "',";
		$sql .= "booking_id = '". $database->escapeValue($this->booking_id) . "',";
		$sql .= "amount = '". $database->escapeValue($this->amount) . "',";
		$sql .= "funds_received_YN = '". $database->escapeValue($this->funds_received_YN) . "',";
		$sql .= "archived_YN = '". $database->escapeValue($this->archived_YN) . "'";
		$sql .=  " WHERE transaction_id = ". $database->escapeValue($this->transaction_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE transactions SET ";
		$sql .= "cardholders_name = '". $database->escapeValue($this->cardholders_name) . "',";
		$sql .= "amount = '". $database->escapeValue($this->amount) . "',";
		$sql .= "amount_with_fee = '". $database->escapeValue($this->amount_with_fee) . "',";
		$sql .= "payment_method = '". $database->escapeValue($this->payment_method) . "',";
		$sql .= "ts = '". $database->escapeValue($this->ts) . "',";
		$sql .= "merchant_trans_id = '". $database->escapeValue($this->merchant_trans_id) . "',";
		$sql .= "booking_id = '". $database->escapeValue($this->booking_id) . "',";
		$sql .= "funds_received_YN = '". $database->escapeValue($this->funds_received_YN) . "',";
		$sql .= "comments = '". $database->escapeValue($this->comments) . "',";
		$sql .= "archived_YN = '". $database->escapeValue($this->archived_YN) . "'";
		$sql .=  " WHERE transaction_id = ". $database->escapeValue($this->transaction_id);

		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM transactions WHERE  transaction_id =" . $database->escapeValue($this->transaction_id);

		$database->query($sql);
	}

	public function createBpayTransaction()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO transactions (";
		$sql .= "amount, payment_method, booking_id, funds_received_YN, user_id, bpay_code";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->amount) . "',";
		$sql .= "'". $database->escapeValue($this->payment_method) . "',";
		$sql .= "'". $database->escapeValue($this->booking_id) . "',";
		$sql .= "'". $database->escapeValue($this->funds_received_YN) . "',";
		$sql .= "'". $database->escapeValue($this->user_id) . "',";
		$sql .= "'". $database->escapeValue($this->bpay_code) . "')";

		if($database->query($sql))
		{
			$this->transaction_id = $database->insertId();
			return $this->transaction_id;
		} else {
			return false;
		}

	}

	public static function getForUser($user, $booking_id, $page = 1, $limit=null)
	{
		$database = new self;


        if ($limit !== null) {
            $limit = " LIMIT " . ($page - 1) * $limit . ',' . $limit;
        }

		$sql =	"SELECT * FROM transactions WHERE user_id = " . $database->escapeValue($user);
        $sql .= " AND booking_id = " . $database->escapeValue($booking_id);
        $sql .= " ORDER BY transaction_id DESC";
        $sql .= $limit;

//		var_dump($sql);

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

    public static function getCountForUser($user, $booking_id) {
        $database = new self;

        $sql =	"SELECT count(*) as cnt FROM transactions WHERE user_id = " . $database->escapeValue($user);
        $sql .= " AND booking_id = " . $database->escapeValue($booking_id);

//        var_dump($sql);exit;

        $result_set = $database->query($sql);
        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array = $row['cnt'];
        }

        return $object_array;
    }

	public function getPaymentMethod() {
		switch($this->payment_method){
			case 'CC':
				return 'Credit Card';
			break;
			case 'BP':
				return 'BPay';
			break;
            case 'DD':
                return 'Direct Deposit';
                break;
		}
	}

	public static function getPaidSum($booking_id) {
        $database = new self();

        $sql = "SELECT SUM(amount) as amount FROM transactions WHERE booking_id = " . $database->escapeValue($booking_id);
        $result_set = $database->query($sql);

        $object_array = array();

        while ($row = $database->fetchArray($result_set)) {
            $object_array = $row['amount'];
        }

        return $object_array;

    }

}

?>