<?php

class Guests extends DatabaseObject
{
	protected static $table_name="Guests";

	// Guests members
	public $user_id; // primary key
	public $booking_id;
	public $primary_contact_yn;
	public $guest_no;
	public $firstname;
	public $lastname;
	public $prefered_name;
	public $email;
	public $child_yn;
	public $gender;
	public $dob;
	public $prefered_event_id;
	public $nationality;
	public $address_same_primary_yn;
	public $address_1;
	public $city;
	public $state;
	public $postcode;
	public $country;
	public $phone;
	public $dietry_requirements;
	public $additional_info;
	public $archived_yn;

	public $tshirt_id;
	public $tshirt_quantity;
	public $tshirt_name;
	public $singlet_id;
	public $singlet_quantity;


	public $user;
	public $phone2_work_number_yn;
	public $phone2_alternate;
	public $phone1_work_number_yn;
	public $phone1_preferred;
	public $suburb;
	public $occupation;
	public $middle_name;
	public $partisipation;
	public $emergency_contact_name;
	public $emergency_contact_phone_number;
	public $title;
    public $as_primary = false;
	// public $guest_options_tours;


	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// sample function
	/**
	*  Get 
	*
	*  @param  int  $id 
	*  @return  object
	*/
	public static function getSomething($id1=NULL, $id2=NULL)
	{
		$database = new self;

		$sql =	'
				';

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}

		return $object_array;

	}

	// sample process $_POST function
	/**
	*  process post variable - e.g. select list with values prefixed with FSLIBBED@
	*
	*  @param  array  $postArray 
	*  @return  true or false
	*/
	public function processPostData($postArray){
		$this->address_2 			= $postArray[10]['value'];	
		$this->phone 				= $postArray[11]['value'];	
		$this->state 				= $postArray[13]['value'];
		$this->firstname 			= $postArray[1]['value'];
		$this->lastname 			= $postArray[2]['value'];
		$this->prefered_name 		= $postArray[3]['value'];	
		$this->email 				= $postArray[4]['value'];
		$this->gender 				= $postArray[5]['value'];
		$this->dob 					= $postArray[6]['value'];
		$this->prefered_event_id 	= $postArray[7]['value'];
		$this->shirt_size 			= $postArray[8]['value'];	
	}
	public function processPostCreate($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}

	public function processPostDelete($postArray)
	{
		// add new entries
		foreach ($postArray as $attribute=>$value){
			if(substr($value,0,9) == "FSLIBBED@")
			{
				$attributes = explode("_", substr($value, 9));

				$this->attribute1 = $attributes[0];
				$this->attribute2 = $attributes[1]; 
				$this->attribute3 = $attributes[2];
				$this->attribute4 = $attributes[3]; 

				$this->create();
			}
		}
		return true;
	}


	public static function populate_guests() {
		// Could check that $record exists and is an array
		
		$guests = array();

		for ($i=0; $i<($_SESSION['number_guests']); $i++)
		{
			$object = new self;
			
			foreach($_SESSION['guest_data'][$i + 1] as $attribute=>$value)
			{
				$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
				if($object->hasAttribute($attribute)) 
				{
					$object->$attribute = stripslashes($value);
				}
			}

			$guests [] = $object;
		}

		return $guests;
	}

	public static function populate($record) {
		// Could check that $record exists and is an array
		$object = new self;

		foreach($record as $attribute=>$value){
			$attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
			if($object->hasAttribute($attribute)) {
				$object->$attribute = stripslashes($value);
			}
		}
		return $object;
	}

	public static function findBySql($sql="") 
	{
		$database = new self;

		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private function hasAttribute($attribute) 
	{
		// get_object_vars returns an associative array with all attributes 
		// (incl. private ones!) as the keys and their current values as the value
		$objectVars = get_object_vars($this);
		// We don't care about the value, we just want to know if the key exists
		// Will return true or false
		return array_key_exists($attribute, $objectVars);
	}

	public function create()
	{
		$database = new self; // instance of database object

		$sql  = "INSERT INTO guests (";
		$sql .= "booking_id, primary_contact_yn, guest_no, firstname, lastname, prefered_name, email, child_yn, gender, dob, prefered_event_id, nationality, address_same_primary_yn, address_1, city, state, postcode, country, phone, additional_info, tshirt_id, archived_yn";
		$sql .= ") VALUES (";
		$sql .= "'". $database->escapeValue($this->booking_id) . "',";
		$sql .= "'". $database->escapeValue($this->primary_contact_yn) . "',";
		$sql .= "'". $database->escapeValue($this->guest_no) . "',";
		$sql .= "'". $database->escapeValue($this->firstname) . "',";
		$sql .= "'". $database->escapeValue($this->lastname) . "',";
		$sql .= "'". $database->escapeValue($this->prefered_name) . "',";
		$sql .= "'". $database->escapeValue($this->email) . "',";
		$sql .= "'". $database->escapeValue($this->child_yn) . "',";
		$sql .= "'". $database->escapeValue($this->gender) . "',";
		$sql .= "'". $database->escapeValue($this->dob) . "',";
		$sql .= "'". $database->escapeValue($this->prefered_event_id) . "',";
		$sql .= "'". $database->escapeValue($this->nationality) . "',";
		$sql .= "'". $database->escapeValue($this->address_same_primary_yn) . "',";
		$sql .= "'". $database->escapeValue($this->address_1) . "',";
		$sql .= "'". $database->escapeValue($this->city) . "',";
		$sql .= "'". $database->escapeValue($this->state) . "',";
		$sql .= "'". $database->escapeValue($this->postcode) . "',";
		$sql .= "'". $database->escapeValue($this->country) . "',";
		$sql .= "'". $database->escapeValue($this->phone) . "',";
		$sql .= "'". $database->escapeValue($this->additional_info) . "',";
		$sql .= "'". $database->escapeValue($this->tshirt_id) . "',";
		$sql .= "'". $database->escapeValue($this->archived_yn) . "')";

		if($database->query($sql)) 
		{
			$this->user_id = $database->insertId();
			return "S";
		} else {
			return false; 
		}
	}

	public function firstStepCreateGuest() {
		$database = new self;
		$sql = "INSERT INTO guests (";
		$sql .= "user, title, booking_id, firstname, lastname, middle_name, prefered_name, dob, gender, partisipation, occupation, address_1, ";
		$sql .= "suburb, state, postcode, country, phone1_preferred, phone1_work_number_YN, phone2_alternate, phone2_work_number_YN, ";
		$sql .= "email, emergency_contact_name, emergency_contact_phone_number, primary_contact_YN, prefered_event_id, dietry_requirements, tshirt_id, tshirt_name, tshirt_quantity, singlet_id, singlet_quantity)";
		$sql .= "VALUES (";
		$sql .= "'" . $database->escapeValue($this->user) . "',";
		$sql .= "'" . $database->escapeValue($this->title) . "',";
		$sql .= "'" . $database->escapeValue($this->booking_id) . "',";
		$sql .= "'" . $database->escapeValue($this->firstname) . "',";
		$sql .= "'" . $database->escapeValue($this->lastname) . "',";
		$sql .= "'" . $database->escapeValue($this->middle_name) . "',";
		$sql .= "'" . $database->escapeValue($this->prefered_name) . "',";
		$sql .= "'" . date('m/d/Y', strtotime($database->escapeValue($this->dob))) . "',";
		$sql .= "'" . $database->escapeValue($this->gender) . "',";
		$sql .= "'" . $database->escapeValue($this->partisipation) . "',";
		$sql .= "'" . $database->escapeValue($this->occupation) . "',";
		$sql .= "'" . $database->escapeValue($this->address_1) . "',";
		$sql .= "'" . $database->escapeValue($this->suburb) . "',";
		$sql .= "'" . $database->escapeValue($this->state) . "',";
		$sql .= "'" . $database->escapeValue($this->postcode) . "',";
		$sql .= "'" . $database->escapeValue($this->country) . "',";
		$sql .= "'" . $database->escapeValue($this->phone1_preferred) . "',";
		$sql .= "'" . $database->escapeValue($this->phone1_work_number_yn) . "',";
		$sql .= "'" . $database->escapeValue($this->phone2_alternate) . "',";
		$sql .= "'" . $database->escapeValue($this->phone2_work_number_yn) . "',";
		$sql .= "'" . $database->escapeValue($this->email) . "',";
		$sql .= "'" . $database->escapeValue($this->emergency_contact_name) . "',";
		$sql .= "'" . $database->escapeValue($this->emergency_contact_phone_number) . "',";
		$sql .= "'" . $database->escapeValue($this->primary_contact_yn) . "',";
		$sql .= "'" . $database->escapeValue($this->prefered_event_id) . "',";
		$sql .= "'" . $database->escapeValue($this->dietry_requirements) . /*"')"*/ "',";

        if (empty($this->tshirt_id)) $this->tshirt_id = 'NULL';
        $sql .= $database->escapeValue($this->tshirt_id) . ",";
        $sql .= "'" . $database->escapeValue($this->tshirt_name) . "',";
        $sql .= $database->escapeValue($this->tshirt_quantity) . ",";

        if (empty($this->singlet_id)) $this->singlet_id = 'NULL';
        $sql .= $database->escapeValue($this->singlet_id) . ",";
        $sql .= $database->escapeValue($this->singlet_quantity) . ")";
//		 echo $sql;exit;
		if ($database->query($sql)) {
//			var_dump(123);exit;
			return true;
		} else {
//			var_dump(321);exit;
			return false;
		}
	}

	public function secondStepCreateAddtitionalGuest(array $data, $event_id, $booking_id, $cnt) {

		$database = new self;
		$insertCnt = count($data) / $cnt;
		$i = 1;

//		$sql = "INSERT INTO guests (";
//		$sql .= "user, child_YN, title, firstname, middle_name, lastname, prefered_name, dob, gender, partisipation, occupation, address_1, ";
//		$sql .= "suburb, state, postcode, country, phone1_preferred, phone1_work_number_yn, phone2_alternate, phone2_work_number_yn, email, emergency_contact_name, emergency_contact_phone_number, ";
//		$sql .= "primary_contact_YN, prefered_event_id, booking_id)";
//		$sql .= "VALUES ";

		$sql = "INSERT INTO guests (";
		$sql .= "user, child_YN, title, firstname, middle_name, lastname, prefered_name, dob, gender, partisipation, dietry_requirements, ";
        $sql .= "tshirt_quantity, singlet_quantity, tshirt_id, singlet_id, tshirt_name, ";
		$sql .= "address_1, suburb, state, postcode, country, phone1_preferred, email, ";
		$sql .= "primary_contact_YN, prefered_event_id, booking_id)";
		$sql .= "VALUES ";

		foreach ($data as $key=>$value) {
			if ($i == 1) {
				$sql .= "(";
				$sql .= "'" . $database->escapeValue($_SESSION['generic_user_id']) . "'," ;
			}
			if (strpos($key, "Dob") !== false ) {
//				$value = date('d/m/Y', strtotime($value));
//				var_dump($value);exit;
			}
			if (strpos($key, "Tshirt_id") !== false ) {
				if (empty($value)) $value = "NULL";
				$sql .= $value . ", ";
			} elseif (strpos($key, "Singlet_id") !== false ) {
				if (empty($value)) $value = "NULL";
				$sql .= $value . ", ";
			} else {
				$sql .= "'" . $database->escapeValue($value) . "',";
			}
			if ($i == $insertCnt) {
				$sql .= "'N',";
				$sql .= $database->escapeValue($event_id) . ",";
				$sql .= $database->escapeValue($booking_id) . "),";
				$i = 0;
			}
			$i++;
		}
		$sql = substr($sql, 0, -1);

       // echo $sql;exit;
//		echo $sql; exit;

		if ($database->query($sql)) {
			return true;
		} else return false;
	}

    public function secondStepUpdateAddtitionalGuest(array $data, array $guestIds, $cnt) {

        $database = new self;

        $insertCnt = count($data) / $cnt;
        $i = 1;
        $j = 0;

        $set = '';
        $sql = "UPDATE guests g INNER JOIN (";
        foreach ($data as $key=>$value) {
            $key = lcfirst(str_replace('additionalGuest', '', $key));
            $key = preg_replace('/\d+$/', '', $key);
            if ($key == 'address_') $key = 'address_1';
            if ($i == 1) $sql .= "SELECT " . $database->escapeValue($guestIds[$j]) . " as id,";
            if (strpos($key, "dob") !== false ) {
                $value = date('m/d/Y', strtotime($value));
            }
            $sql .= "'" . $database->escapeValue($value) . "' as " . $key . ",";
            if ($key != 'id' && $key != 'child') {
                $set .= "g." . $key . "=ag." . $key . ",";
            }
            if ($i == $insertCnt) {
                $sql = substr($sql, 0, -1);
                $j++;
                if ($j != count($guestIds)) {
                    $sql .= " UNION ALL ";
                }
                $i = 0;
            }
            $i++;
        }
        if ($j == count($guestIds)) {
            $set = substr($set, 0, -1);
            $sql .= ") as ag ";
            $sql .= "ON ag.id = g.user_id SET ";
            $sql .= $set;
        }
//        echo $sql;exit;

        if ($database->query($sql)) {
            return true;
        } else return false;
    }

	public function update()
	{
		$database = new self; // instance of database object

		$sql  = "UPDATE guests SET ";
		$sql .= "title = '". $database->escapeValue($this->title) . "',";
		$sql .= "booking_id = '". $database->escapeValue($this->booking_id) . "',";
		$sql .= "firstname = '". $database->escapeValue($this->firstname) . "',";
		$sql .= "lastname = '". $database->escapeValue($this->lastname) . "',";
		$sql .= "middle_name = '". $database->escapeValue($this->middle_name) . "',";
		$sql .= "prefered_name = '". $database->escapeValue($this->prefered_name) . "',";
		$sql .= "dob = '". date('d/m/Y', strtotime($database->escapeValue($this->dob))) . "',";
		$sql .= "gender = '". $database->escapeValue($this->gender) . "',";
		$sql .= "partisipation = '". $database->escapeValue($this->partisipation) . "',";
		$sql .= "occupation = '". $database->escapeValue($this->occupation) . "',";
		$sql .= "address_1 = '". $database->escapeValue($this->address_1) . "',";
		$sql .= "suburb = '". $database->escapeValue($this->suburb) . "',";
		$sql .= "state = '". $database->escapeValue($this->state) . "',";
		$sql .= "postcode = '". $database->escapeValue($this->postcode) . "',";
		$sql .= "country = '". $database->escapeValue($this->country) . "',";
		$sql .= "phone1_preferred = '". $database->escapeValue($this->phone1_preferred) . "',";
		$sql .= "phone1_work_number_YN = '". $database->escapeValue($this->phone1_work_number_yn) . "',";
		$sql .= "phone2_alternate = '". $database->escapeValue($this->phone2_alternate) . "',";
		$sql .= "phone2_work_number_YN = '". $database->escapeValue($this->phone2_work_number_yn) . "',";
		$sql .= "email = '". $database->escapeValue($this->email) . "',";
		$sql .= "emergency_contact_name = '". $database->escapeValue($this->emergency_contact_name) . "',";
		$sql .= "emergency_contact_phone_number = '". $database->escapeValue($this->emergency_contact_phone_number) . "',";
        $sql .= "primary_contact_YN = '". $database->escapeValue($this->primary_contact_yn) . "',";
        $sql .= "prefered_event_id = '". $database->escapeValue($this->prefered_event_id) . "',";

		$sql .= "dietry_requirements = '". $database->escapeValue($this->dietry_requirements) . /*"'"*/ "',";

        if (empty($this->tshirt_id)) $this->tshirt_id = 'NULL';
        $sql .= "tshirt_id = ". $database->escapeValue($this->tshirt_id) . ",";
        $sql .= "tshirt_quantity = '". $database->escapeValue($this->tshirt_quantity) . "',";

        if (empty($this->singlet_id)) $this->singlet_id = 'NULL';
        $sql .= "singlet_id = ". $database->escapeValue($this->singlet_id) . ",";
        $sql .= "singlet_quantity = '". $database->escapeValue($this->singlet_quantity) . "',";

        $sql .= "tshirt_name = '". $database->escapeValue($this->tshirt_name) . "'";

		$sql .=  " WHERE user_id = ". $database->escapeValue($this->user_id);

//        echo $sql;exit;
		$database->query($sql);

		return ($database->affectedRows() == 1) ? true : false;
	}

	public function delete()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM guests WHERE  user_id =" . $database->escapeValue($this->user_id);

		$database->query($sql);
	}

	public function delete_by_booking_id($booking_id)
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM guests WHERE  booking_id =" . $database->escapeValue($booking_id);

		$database->query($sql);
	}


	// Puts values from array $data to properties of Guests object
	public function setValues(Guests $guest, $data) {
		foreach ($guest as $guestKey => $guestValue) {
			foreach ($data as $dataKey => $dataValue) {
				$dataKey = lcfirst(str_replace('primaryContact', '', $dataKey));
				if ($guestKey == $dataKey) {
					$guest->$guestKey = trim($dataValue);
				}
			}
		}
		return $guest;
	}

	public static function getGuestForBook($user_id, $booking_id, $primary_contact_YN = 'Y') {
		$database = new self;
		$sql = "SELECT * FROM guests WHERE booking_id = " . $database->escapeValue($booking_id);
		$sql .= " AND user = " . $database->escapeValue($user_id);
//		$sql .= " AND primary_contact_YN = '" . $database->escapeValue($primary_contact_YN) . "'";

//		var_dump($sql);
		$result_set = $database->query($sql);
		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = self::populate($row);
		}
		return $object_array;
	}

    public static function getPrimaryGuestForBook($user_id, $booking_id, $primary_contact_YN = 'Y') {
        $database = new self;
        $sql = "SELECT * FROM guests WHERE booking_id = " . $database->escapeValue($booking_id);
        $sql .= " AND user = " . $database->escapeValue($user_id);
		$sql .= " AND primary_contact_YN = '" . $database->escapeValue($primary_contact_YN) . "'";
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetchArray($result_set)) {
            $object_array = self::populate($row);
        }
        return $object_array;
    }

    public static function getNotPrimaryGuestForBook($user_id, $booking_id, $primary_contact_YN = 'Y') {
        $database = new self;
        $sql = "SELECT * FROM guests WHERE booking_id = " . $database->escapeValue($booking_id);
        $sql .= " AND user = " . $database->escapeValue($user_id);
		$sql .= " AND primary_contact_YN = '" . $database->escapeValue($primary_contact_YN) . "'";
        $result_set = $database->query($sql);

        $object_array = array();
        while ($row = $database->fetchArray($result_set)) {
            $object_array[] = self::populate($row);
        }

        return $object_array;
    }

	public function deleteByBookingForGuest()
	{
		$database = new self;// instance of database object

		$sql = "DELETE FROM guests WHERE  booking_id =" . $database->escapeValue($this->booking_id);
        $sql .= " AND user = " . $database->escapeValue($this->user);
        $sql .= " AND primary_contact_YN = 'N'";
        return ($database->query($sql)) ? true : false;
	}

	public static function getTitles() {
		$database = new self;
		$sql = "SELECT title_id, title FROM people_title WHERE available_yn = 'Y'";
		$result_set = $database->query($sql);

		$object_array = array();
		while ($row = $database->fetchArray($result_set)) {
			$object_array[] = $row;
		}
		return $object_array;
	}




}

?>