<?php
class Access_levels extends DatabaseObject 
{

	protected static $table_name="access_levels";
		
	// Users table attributes
	public $access_level;
	public $name;
	public $description;


	function __construct() {
		parent::__construct();
	}

	// Common Database Methods are in database.php


	public static function findById($id=0) 
	{
		$database = new self;
		$sql = "SELECT * FROM categories WHERE user_id={$id}";
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	 /**
	 * 	checks if a users is currently in the users table
	 * 
	 *  @param  string  $sNumber  
	 *  @return  boolean
	 */
	public static function isInUserTable($sNumber)
	{
		$user = self::findBySql("select * from users where username = '{$sNumber}'");
		if (count($user) == 1) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function findBySql($sql="") 
	{
		$database = new self;
		$result_set = $database->query($sql);
		$object_array = array();

		while ($row = $database->fetchArray($result_set)) {
		  $object_array[] = self::populate($row);
		}
		return $object_array;
	}

	private static function populate($record)
	{
		// Could check that $record exists and is an array
		$object = new self;
	
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  $attribute = strtolower($attribute); // Oracle put's attribute in upper case -- we want them to match our relevant members names
		  if($object->hasAttribute($attribute)) {
			  // check to see if attribute is a clob and load the value if it is
			  if(is_object($value))
			  {
				  $object->$attribute = $value->read($value->size());
			  }
			  else
			  {
			  	$object->$attribute = stripslashes($value);
			  }
		  }
		}
		return $object;
	}

	private function hasAttribute($attribute) 
	{
	  // get_object_vars returns an associative array with all attributes 
	  // (incl. private ones!) as the keys and their current values as the value
	  $objectVars = get_object_vars($this);
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $objectVars);
	}


}
?>
