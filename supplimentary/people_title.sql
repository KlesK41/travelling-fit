-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Лют 26 2016 р., 16:49
-- Версія сервера: 10.1.9-MariaDB
-- Версія PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `oldtemp`
--

-- --------------------------------------------------------

--
-- Структура таблиці `people_title`
--

CREATE TABLE `people_title` (
  `title_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `available_yn` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `people_title`
--

INSERT INTO `people_title` (`title_id`, `title`, `available_yn`) VALUES
(1, 'Mr', 'Y'),
(2, 'Mrs', 'Y'),
(3, 'Ms', 'Y'),
(4, 'Miss', 'Y'),
(5, 'Dr', 'Y'),
(6, 'Prof', 'Y'),
(7, 'Asoc Prof', 'Y'),
(8, 'Hon', 'Y'),
(9, 'Sir', 'Y');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `people_title`
--
ALTER TABLE `people_title`
  ADD PRIMARY KEY (`title_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `people_title`
--
ALTER TABLE `people_title`
  MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
