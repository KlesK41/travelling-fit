-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Час створення: Лют 18 2016 р., 09:25
-- Версія сервера: 5.6.29
-- Версія PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `travelli_oldtemp`
--

-- --------------------------------------------------------

--
-- Структура таблиці `accommodation`
--

CREATE TABLE IF NOT EXISTS `accommodation` (
  `accommodation_id` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_name` varchar(200) DEFAULT NULL COMMENT 'full package name as displayed on form',
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  PRIMARY KEY (`accommodation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп даних таблиці `accommodation`
--

INSERT INTO `accommodation` (`accommodation_id`, `accommodation_name`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'OUTBACK PIONEER BUDGET', 'Y', 1, 'N'),
(2, 'OUTBACK PIONEER STANDARD', 'Y', 2, 'N'),
(3, 'EMU WALK APARTMENTS 1 BED', 'Y', 3, 'N'),
(4, 'EMU WALK APARTMENTS 2 BED', 'Y', 4, 'N'),
(5, 'DESERT GARDENS STANDARD', 'Y', 5, 'N'),
(6, 'DESERT GARDENS DELUXE ROCK VIEW', 'Y', 6, 'N'),
(7, 'SAILS IN THE DESERT SUPERIOR', 'Y', 7, 'N'),
(8, 'SAILS IN THE DESERT - TERRACE', 'Y', 8, 'N'),
(9, 'SAILS IN THE DESERT DELUXE SUITE', 'N', 9, 'N'),
(10, 'NO ACCOMMODATION REQUIRED', 'Y', 10, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `accommodation_package`
--

CREATE TABLE IF NOT EXISTS `accommodation_package` (
  `package_accommodation_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) DEFAULT NULL,
  `accommodation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`package_accommodation_id`),
  KEY `package_id` (`package_id`),
  KEY `accommodation_id` (`accommodation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп даних таблиці `accommodation_package`
--

INSERT INTO `accommodation_package` (`package_accommodation_id`, `package_id`, `accommodation_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 2),
(4, 1, 2),
(5, 2, 3),
(6, 2, 3),
(7, 2, 4),
(8, 2, 4),
(9, 3, 5),
(10, 3, 5),
(11, 3, 6),
(12, 3, 6),
(13, 4, 7),
(14, 4, 7),
(15, 4, 8),
(16, 4, 8),
(17, 5, 9),
(18, 5, 9),
(19, 5, 10),
(20, 5, 10);

-- --------------------------------------------------------

--
-- Структура таблиці `bedding`
--

CREATE TABLE IF NOT EXISTS `bedding` (
  `bedding_id` int(11) NOT NULL AUTO_INCREMENT,
  `bedding_name` varchar(100) DEFAULT NULL,
  `max_people` int(11) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  PRIMARY KEY (`bedding_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `bedding`
--

INSERT INTO `bedding` (`bedding_id`, `bedding_name`, `max_people`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Single', 1, 'Y', 0, 'N'),
(2, 'Twin (2 beds)', 2, 'Y', 1, 'N'),
(3, 'Double (1 bed for two people)', 3, 'Y', 2, 'N'),
(4, 'Triple', 4, 'Y', 3, 'N'),
(5, 'Quad', 5, 'Y', 5, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `bedding_configuration`
--

CREATE TABLE IF NOT EXISTS `bedding_configuration` (
  `bedding_configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_id` int(11) DEFAULT NULL,
  `bedding_configuration_name` varchar(100) DEFAULT NULL,
  `bedding_cost` varchar(20) DEFAULT NULL,
  `min_people` int(11) DEFAULT '1',
  `max_people` int(11) DEFAULT '1',
  PRIMARY KEY (`bedding_configuration_id`),
  KEY `accommodation_id` (`accommodation_id`),
  KEY `min_people` (`min_people`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп даних таблиці `bedding_configuration`
--

INSERT INTO `bedding_configuration` (`bedding_configuration_id`, `accommodation_id`, `bedding_configuration_name`, `bedding_cost`, `min_people`, `max_people`) VALUES
(1, 1, 'Single', '140.00', 1, 1),
(2, 2, 'Twin (2 beds)', '240.00', 1, 2),
(3, 3, 'Double (1 bed for two people)', '240.00', 1, 3),
(4, 4, 'Triple', '340.00', 2, 3),
(5, 5, 'Quad', '340.00', 2, 4),
(6, 6, 'Bedding Configuration Name6', '440.00', 3, 5),
(7, 7, 'Bedding Configuration Name7', '440.00', 3, 5),
(8, 8, 'Bedding Configuration Name8', '540.00', 5, 6),
(9, 9, 'Bedding Configuration Name9', '640.00', 5, 7),
(10, 10, 'Bedding Configuration Name10', '640.00', 6, 7);

-- --------------------------------------------------------

--
-- Структура таблиці `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `booking_code` varchar(256) DEFAULT NULL,
  `total_cost` int(11) DEFAULT NULL,
  `deposit` decimal(6,2) DEFAULT NULL COMMENT 'The deposit is calculated by the number of adults and not children.',
  `bedding_configuration_id` varchar(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `number_adult_guests` int(11) DEFAULT NULL,
  `number_children_guests` int(11) DEFAULT NULL,
  `extra_nights_pre` varchar(11) DEFAULT NULL,
  `extra_nights_post` varchar(11) DEFAULT NULL,
  `share_request_YN` char(1) DEFAULT 'N',
  `travel_insurance_option_id` varchar(11) DEFAULT NULL,
  `ec_firstname` varchar(100) DEFAULT NULL COMMENT 'emergency contact firstname',
  `ec_lastname` varchar(100) DEFAULT NULL COMMENT 'emergency contact lastname',
  `ec_phone` varchar(40) DEFAULT NULL,
  `ec_email` varchar(400) DEFAULT NULL,
  `passport_number` varchar(400) DEFAULT NULL,
  `how_hear_id` varchar(11) DEFAULT NULL,
  `ack_tc_yn` char(1) DEFAULT 'N' COMMENT 'acknowledged terms and conditions',
  `airline_freq_flyer_number` varchar(200) DEFAULT NULL,
  `consultant` varchar(25) DEFAULT NULL,
  `passport_expiry` date DEFAULT NULL,
  `passport_nationality` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `step` int(11) DEFAULT NULL,
  `completed` enum('Y','N') NOT NULL DEFAULT 'N',
  `paid` enum('Y','N','P') DEFAULT NULL,
  `active_living_membership` varchar(11) DEFAULT NULL,
  `additional_information` text,
  `confirmed_yn` enum('Y','N') NOT NULL DEFAULT 'N',
  `total_paid` int(11) DEFAULT '0',
  `archieved_yn` enum('Y','N') NOT NULL DEFAULT 'N',
  `tshirt_id` int(11) DEFAULT NULL,
  `tshirt_quantity` int(11) DEFAULT NULL,
  `tshirt_name` varchar(255) DEFAULT NULL,
  `singlet_id` int(11) DEFAULT NULL,
  `singlet_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `bedding_configuration_id` (`bedding_configuration_id`),
  KEY `package_id` (`package_id`),
  KEY `how_hear_id` (`how_hear_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп даних таблиці `bookings`
--

INSERT INTO `bookings` (`booking_id`, `event_id`, `package_id`, `user_id`, `booking_code`, `total_cost`, `deposit`, `bedding_configuration_id`, `room_type_id`, `number_adult_guests`, `number_children_guests`, `extra_nights_pre`, `extra_nights_post`, `share_request_YN`, `travel_insurance_option_id`, `ec_firstname`, `ec_lastname`, `ec_phone`, `ec_email`, `passport_number`, `how_hear_id`, `ack_tc_yn`, `airline_freq_flyer_number`, `consultant`, `passport_expiry`, `passport_nationality`, `timestamp`, `step`, `completed`, `paid`, `active_living_membership`, `additional_information`, `confirmed_yn`, `total_paid`, `archieved_yn`, `tshirt_id`, `tshirt_quantity`, `tshirt_name`, `singlet_id`, `singlet_quantity`) VALUES
(24, 1102, 30, 26, NULL, NULL, NULL, NULL, 39, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, '2016-02-04 12:53:55', 1, 'N', NULL, NULL, NULL, 'N', 0, 'N', NULL, NULL, NULL, NULL, NULL),
(27, 1102, 33, 22, NULL, 839, NULL, '1', 47, 0, 0, '1', '1', 'N', '1', NULL, NULL, NULL, NULL, NULL, '2', 'N', NULL, NULL, NULL, NULL, '2016-02-08 21:00:58', 3, 'Y', 'Y', '', '', 'N', 839, 'N', 13, 1, 'EchoUA', NULL, NULL),
(28, 656, 32, 22, NULL, 100501, NULL, '', 43, 0, 0, '2', '0', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-10 09:29:20', 4, 'Y', 'P', '', '', 'N', 217, 'Y', NULL, 1, 'fdhdf', NULL, NULL),
(30, 1102, 33, 26, NULL, 2517, NULL, '1', 47, 2, 0, '8', '12', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2015-02-09 02:15:22', 3, 'Y', NULL, '112211', '', 'N', 0, 'Y', NULL, 1, 'shirt name', NULL, NULL),
(31, 1102, 33, 26, NULL, 839, NULL, '1', 47, 0, 0, '0', '0', 'N', '', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-11 10:49:48', 3, 'Y', 'Y', '', '', 'N', 839, 'N', NULL, 0, '', NULL, NULL),
(32, 631, 35, 22, NULL, 4431, NULL, '', 51, 1, 1, '1', '1', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-10 10:08:31', 4, 'Y', 'P', '', '', 'N', 400, 'N', NULL, 1, 'Test', NULL, NULL),
(33, 631, 35, 26, NULL, 2964, NULL, '', 51, 1, 0, '8', '12', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-15 13:49:15', 4, 'Y', 'P', '112211', '', 'N', 1300, 'N', NULL, 0, '', NULL, 0),
(34, 631, 35, 26, NULL, 4446, NULL, '', 51, 1, 1, '8', '12', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-11 12:12:15', 4, 'Y', 'Y', '112211', '', 'N', 4446, 'N', NULL, 2, 'Bond 007', NULL, NULL),
(35, 631, 35, 26, NULL, 4446, NULL, '', 52, 1, 1, '8', '12', 'N', '1', NULL, NULL, NULL, NULL, NULL, '2', 'N', NULL, NULL, NULL, NULL, '2016-02-11 12:33:21', 4, 'Y', 'Y', '112211', '', 'N', 4446, 'N', 15, 1, 'Stu 16', NULL, NULL),
(36, NULL, 36, NULL, NULL, 0, NULL, '3', NULL, 0, 0, NULL, NULL, 'N', '3', NULL, NULL, NULL, NULL, NULL, '7', 'N', NULL, NULL, NULL, NULL, '2016-02-11 16:36:30', NULL, 'N', NULL, NULL, NULL, 'N', 0, 'N', NULL, NULL, NULL, NULL, NULL),
(37, 1102, 34, 26, NULL, 1732, NULL, '1', 49, 1, 0, '0', '0', 'N', '1', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-16 00:29:02', 3, 'Y', 'P', '', '', 'N', 100, 'N', NULL, 0, '', NULL, 0),
(38, 1102, 33, 24, NULL, 1918, NULL, '', 47, 1, 0, '0', '0', 'N', '3', NULL, NULL, NULL, NULL, NULL, '8', 'N', NULL, NULL, NULL, NULL, '2016-02-16 14:49:47', 3, 'Y', 'P', '2', 'TEST INFORMATION', 'N', 18, 'N', 13, 2, '', 8, 1),
(39, 631, 35, 24, NULL, 4446, NULL, '', 52, 1, 1, '0', '0', 'N', '3', NULL, NULL, NULL, NULL, NULL, '8', 'N', NULL, NULL, NULL, NULL, '2016-02-15 16:27:29', 4, 'Y', 'P', '3', 'TEST INFORMATION', 'N', 12, 'N', NULL, 0, '', NULL, 0),
(40, 631, 35, 26, NULL, 3124, NULL, '', 51, 1, 0, '8', '12', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-16 00:41:55', 4, 'Y', 'P', '112211', '', 'N', 2000, 'N', 12, 1, '', 7, 1),
(41, 631, 35, 26, NULL, 3124, NULL, '3', 51, 1, 0, '8', '12', 'N', '2', NULL, NULL, NULL, NULL, NULL, '1', 'N', NULL, NULL, NULL, NULL, '2016-02-16 02:04:19', 4, 'Y', 'P', '112211', 'additional info - test', 'N', 1200, 'N', 12, 1, 'Bondie', 7, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `booking_option_tours`
--

CREATE TABLE IF NOT EXISTS `booking_option_tours` (
  `booking_option_tours_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `optional_tour_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`booking_option_tours_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп даних таблиці `booking_option_tours`
--

INSERT INTO `booking_option_tours` (`booking_option_tours_id`, `booking_id`, `optional_tour_id`) VALUES
(3, 36, 29),
(4, 36, 28),
(5, 36, 27),
(12, 39, 27),
(13, 39, 28),
(14, 40, 27),
(17, 41, 28),
(18, 41, 27);

-- --------------------------------------------------------

--
-- Структура таблиці `costs`
--

CREATE TABLE IF NOT EXISTS `costs` (
  `cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `cost_type` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`cost_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `costs`
--

INSERT INTO `costs` (`cost_id`, `cost_type`, `parent_id`, `amount`) VALUES
(1, NULL, 1, '5000.00'),
(2, NULL, 2, '6000.00'),
(3, NULL, 3, '7000.00'),
(4, NULL, 4, '8000.00'),
(5, NULL, 5, '9000.00');

-- --------------------------------------------------------

--
-- Структура таблиці `guests`
--

CREATE TABLE IF NOT EXISTS `guests` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL DEFAULT '1',
  `booking_id` int(11) DEFAULT NULL,
  `event_type_id` int(11) DEFAULT NULL,
  `primary_contact_YN` char(1) DEFAULT 'N',
  `guest_no` int(11) DEFAULT NULL COMMENT 'what guest number is the person? 1 (primary contact), 2, 3, 4, etc',
  `firstname` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `prefered_name` varchar(50) DEFAULT NULL,
  `dob` varchar(45) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `prefered_event_id` int(11) DEFAULT NULL,
  `occupation` varchar(200) DEFAULT NULL,
  `child_YN` char(1) DEFAULT 'N',
  `address_same_primary_YN` char(1) DEFAULT 'N',
  `address_1` varchar(500) DEFAULT NULL,
  `suburb` varchar(500) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL COMMENT 'free text - perhaps autocomplete based on country',
  `postcode` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `phone1_preferred` varchar(50) DEFAULT NULL,
  `phone1_work_number_YN` char(1) DEFAULT 'N',
  `phone2_alternate` varchar(50) DEFAULT NULL,
  `phone2_work_number_YN` char(1) DEFAULT 'N',
  `email` varchar(150) DEFAULT NULL,
  `passport_number` varchar(200) DEFAULT 'None',
  `passport_expiry` date DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  `airline_freq_flyer_number` varchar(200) DEFAULT NULL,
  `dietary_requirements` text,
  `passport_nationality` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `partisipation` varchar(255) DEFAULT NULL,
  `emergency_contact_name` varchar(255) DEFAULT NULL,
  `emergency_contact_phone_number` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vip_yn` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`user_id`),
  KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- Дамп даних таблиці `guests`
--

INSERT INTO `guests` (`user_id`, `user`, `booking_id`, `event_type_id`, `primary_contact_YN`, `guest_no`, `firstname`, `middle_name`, `lastname`, `prefered_name`, `dob`, `gender`, `prefered_event_id`, `occupation`, `child_YN`, `address_same_primary_YN`, `address_1`, `suburb`, `state`, `postcode`, `country`, `phone1_preferred`, `phone1_work_number_YN`, `phone2_alternate`, `phone2_work_number_YN`, `email`, `passport_number`, `passport_expiry`, `archived_YN`, `airline_freq_flyer_number`, `dietary_requirements`, `passport_nationality`, `timestamp`, `partisipation`, `emergency_contact_name`, `emergency_contact_phone_number`, `title`, `vip_yn`) VALUES
(38, 26, 24, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 1102, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', '', '6155221133', '', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-04 12:53:55', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(39, 19, 25, NULL, 'Y', NULL, 'TEST', '', '', '', '01/01/1970', 'M', 656, '', 'N', 'N', 'TEST', 'TEST', 'TEST', 'TEST', 'United Arab Emirates', '123456789', 'N', '', 'N', 'TEST@test.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-08 09:05:37', 'Half Marathon', 'TEST', '123456789', 'Mr', 'N'),
(41, 22, 27, NULL, 'Y', NULL, 'Serhii', '', 'Fedoryshyn', '', '02/06/1992', 'M', 1102, '', 'N', 'N', 'Lviv', 'Lviv', 'Lviv', '79053', 'Ukraine', '567898765', 'N', '', '', 'fedoryshyn.sergiy@gmail.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-08 20:56:01', 'Marathon', 'Test', '56789876545', 'Mr', 'N'),
(42, 22, 28, NULL, 'Y', NULL, 'dfhdf', 'dfhdfh', 'hdfh', 'fhdfhd', '01/01/1970', 'M', 656, 'fdgdfgdf', 'N', 'N', 'dfhdfh', 'dfhfdh', 'fhdfhd', '78756', 'Andorra', '43636346436', 'N', '', 'N', 'gsgs@sfsd.sdf', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-08 21:07:37', 'Marathon', '4747474545', '4363534633', 'Mr', 'N'),
(47, 26, 30, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 1102, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-09 01:48:35', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(49, 26, 30, NULL, 'N', NULL, 'Steb', '', 'Lanely', '', '02/09/1977', 'M', 1102, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-09 02:06:33', 'Marathon', NULL, NULL, 'Mr', 'N'),
(50, 26, 30, NULL, 'N', NULL, 'Peter', '', 'Pan', '', '02/09/1998', 'F', 1102, NULL, 'N', 'N', '474 lanely way', 'Brisbane', 'Queensland', '4000', 'Australia', '+614233112', 'N', NULL, 'N', '', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-09 02:06:33', 'Marathon', NULL, NULL, 'Mr', 'N'),
(51, 26, 31, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 1102, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-09 02:18:34', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(53, 22, 32, NULL, 'Y', NULL, 'Test', 'Test', 'Test', 'Test', '01/01/1970', 'M', 631, 'Test', 'N', 'N', 'Test', 'Test', 'Test', '56343', 'Ukraine', '76465465656', 'N', '55765654674', 'N', 'tfhgf@hchgc.jhj', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-10 09:07:07', 'Marathon', 'Test', '76447648648', '', 'N'),
(66, 26, 33, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 631, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 03:45:33', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(68, 1, 33, NULL, 'N', NULL, 'TEST', NULL, 'TEST', 'TEST', '11/01/99', 'M', NULL, NULL, 'N', 'N', 'efgsdfg', 'sdfgsdf', 'qgfda', '2332', 'au', '12412421312', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 11:18:35', NULL, NULL, NULL, NULL, 'Y'),
(70, 26, 34, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 631, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:08:56', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(71, 26, 34, NULL, 'N', NULL, 'TEST CHILD FNAME', 'TEST CHILD MNAME', 'TEST CHILD LNAME', 'TEST CHILD', '06/20/2008', 'M', 631, NULL, 'Y', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:11:05', 'Marathon', NULL, NULL, 'Mr', 'N'),
(72, 26, 34, NULL, 'N', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '06/17/1976', 'M', 631, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:11:05', 'Marathon', NULL, NULL, 'Mr', 'N'),
(73, 26, 35, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 631, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:28:52', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(74, 26, 35, NULL, 'N', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '02/09/2006', 'M', 631, NULL, 'Y', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:31:02', 'Half Marathon', NULL, NULL, 'Mr', 'N'),
(75, 26, 35, NULL, 'N', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '06/18/1988', 'F', 631, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 12:31:02', 'Marathon', NULL, NULL, 'Mrs', 'N'),
(77, 1, 36, NULL, 'N', NULL, 'fsef', NULL, 'gseg', 'gsegse', NULL, 'M', NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'N', NULL, 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-11 15:51:27', NULL, NULL, NULL, NULL, 'N'),
(78, 26, 33, NULL, 'N', NULL, 'Steb', '', 'Lanely', '', '02/15/1961', 'M', 631, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 13:47:18', 'Marathon', NULL, NULL, 'Mr', 'N'),
(79, 26, 37, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1971', 'M', 1102, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 14:09:36', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(81, 26, 37, NULL, 'N', NULL, 'Steb', '', 'Lanely', '', '02/16/2016', 'M', 1102, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 14:24:08', 'Marathon', NULL, NULL, 'Hon', 'N'),
(82, 24, 38, NULL, 'Y', NULL, 'Andrew', '', 'TEST', '', '06/10/1989', 'M', 1102, 'Web developer', 'N', 'N', 'TEST ADDRESS', 'TEST SUBURB', 'TEST STATE', 'TEST POSTCODE', 'Ukraine', '123456789', 'N', '987654321', 'N', 'TEST@email.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-16 14:49:47', 'Marathon', 'TEST CONTACT', '987456321', 'Sir', 'N'),
(83, 24, 38, NULL, 'N', NULL, 'TEST ', '', '', '', '01/23/1989', 'F', 1102, NULL, 'N', 'N', 'TEST ADDRESS', 'TEST SUBURB', 'TEST STATE', 'TEST POSTCODE', 'Ukraine', '123456789', 'N', NULL, 'N', 'TEST@email.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 15:03:54', 'Marathon', NULL, NULL, 'Miss', 'N'),
(84, 24, 39, NULL, 'Y', NULL, 'Andrew', 'TEST', 'S', '', '06/10/1989', 'M', 631, 'Web developer', 'N', 'N', 'TEST ADDRESS', 'TEST SUBURB', 'TEST STATE', 'TEST POSTCODE', 'Ukraine', '123456789', 'N', '987654321', 'N', 'TEST@email.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 15:36:10', 'Marathon', 'TEST CONTACT', '987456321', 'Sir', 'N'),
(85, 24, 39, NULL, 'N', NULL, 'TEST CHILD', '', '', '', '01/01/2015', 'F', 631, NULL, 'Y', 'N', 'TEST ADDRESS', 'TEST SUBURB', 'TEST STATE', 'TEST POSTCODE', 'Ukraine', '123456789', 'N', NULL, 'N', 'TEST@email.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 15:49:42', 'Marathon', NULL, NULL, 'Miss', 'N'),
(86, 24, 39, NULL, 'N', NULL, 'TEST ', '', '', '', '01/23/1989', 'F', 631, NULL, 'N', 'N', 'TEST ADDRESS', 'TEST SUBURB', 'TEST STATE', 'TEST POSTCODE', 'Ukraine', '123456789', 'N', NULL, 'N', 'TEST@email.com', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-15 15:49:42', 'Marathon', NULL, NULL, 'Miss', 'N'),
(87, 26, 40, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 631, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-16 00:39:57', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(88, 26, 40, NULL, 'N', NULL, 'Steb', '', 'Lanely', '', '02/15/1961', 'M', 631, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-16 00:40:22', 'Marathon', NULL, NULL, 'Mr', 'N'),
(89, 26, 41, NULL, 'Y', NULL, 'TEST', 'TEST', 'TEST', 'TEST TEST', '01/01/1970', 'M', 631, '', 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'Y', '6155221133', 'Y', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-16 01:27:23', 'Marathon', 'TEST TEST', '6155112233', 'Mr', 'N'),
(90, 26, 41, NULL, 'N', NULL, 'Steb', '', 'Lanely', NULL, '02/15/1961', 'M', 631, NULL, 'N', 'N', '123 TEST ST TEST TEST', 'TEST suburb', 'Queensland', '4000', 'Australia', '6155112233', 'N', NULL, 'N', 'test@simpleclick.com.au', 'None', NULL, 'N', NULL, NULL, NULL, '2016-02-16 02:06:47', 'Marathon', NULL, NULL, 'Mr', 'Y');

-- --------------------------------------------------------

--
-- Структура таблиці `how_hear`
--

CREATE TABLE IF NOT EXISTS `how_hear` (
  `how_hear_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` decimal(10,0) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  PRIMARY KEY (`how_hear_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп даних таблиці `how_hear`
--

INSERT INTO `how_hear` (`how_hear_id`, `name`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Australian Outback Marathon Website', 'Y', '0', 'N'),
(2, 'Travelling Fit Website', 'Y', '1', 'N'),
(3, 'Travelling Fit Newsletter', 'Y', '3', 'N'),
(4, 'International Travel Agent', 'Y', '4', 'N'),
(5, 'Existing Client', 'Y', '5', 'N'),
(6, 'Word of Mouth/Referral', 'Y', '6', 'N'),
(7, 'Advertising', 'Y', '7', 'N'),
(8, 'Google', 'Y', '8', 'N'),
(9, 'Other Marathon Event', 'Y', '9', 'N'),
(10, 'Other', 'Y', '10', 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `optional_tours`
--

CREATE TABLE IF NOT EXISTS `optional_tours` (
  `optional_tour_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `tour_date` date DEFAULT NULL COMMENT 'see if guest is 16 or older',
  `cost` int(11) DEFAULT NULL COMMENT 'Note: Cost''s will need to be copied across to booking section as they may change here.',
  `cost_extras` decimal(10,0) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `brief_description` varchar(50) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`optional_tour_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Дамп даних таблиці `optional_tours`
--

INSERT INTO `optional_tours` (`optional_tour_id`, `title`, `tour_date`, `cost`, `cost_extras`, `description`, `brief_description`, `available_YN`, `sort_order`, `archived_YN`, `event_id`) VALUES
(23, 'This is test optional tour 1', NULL, 2000, NULL, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 'Y', NULL, 'N', 1102),
(24, 'This is test optional tour 2', NULL, 2500, NULL, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 'Y', NULL, 'N', 1102),
(25, 'Optional tour for test package 2', NULL, 11, NULL, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 'Y', NULL, 'N', 1102),
(26, 'Optional tour for Stuart', NULL, 100500, NULL, '<p><strong><img src="uploads/source/Screenshot_1.png" alt="" width="886" height="574" data-mce-selected="1"><br><br>Lorem Ipsum:<br><br><br></strong><strong></strong>Lorem Ipsum<br>Lorem Ipsum <br>Lorem Ipsum<br>Lorem Ipsum<br><br><br>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br><br></p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 447.5px; top: 7.5px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 890.5px; top: 294.5px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 447.5px; top: 581.5px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 4.5px; top: 294.5px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 7.5px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 890.5px; top: 7.5px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 890.5px; top: 581.5px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 581.5px;"></div>', NULL, 'Y', NULL, 'N', 656),
(27, 'test1', NULL, 30, NULL, '<p>Test optional tour 1,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 1,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 1,&nbsp;text describing the opional tour, etc.Test optional tour 1,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 1,&nbsp;text describing the opional tour, etc.Test optional tour 1,&nbsp;text describing the opional tour, etc.</p>', NULL, 'Y', NULL, 'N', 631),
(28, 'TEST 2', NULL, 200, NULL, '<p>Test optional tour 2,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 2,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 2,&nbsp;text describing the opional tour, etc.&nbsp;Test optional tour 2,&nbsp;text describing the opional tour, etc.&nbsp;</p>', NULL, 'Y', NULL, 'N', 631),
(29, 'TEST', NULL, 100, NULL, '<p>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</p>', NULL, 'Y', NULL, 'N', 656),
(30, 'TEST3', NULL, 100, NULL, '', NULL, 'Y', NULL, 'N', 631);

-- --------------------------------------------------------

--
-- Структура таблиці `optional_tour_package`
--

CREATE TABLE IF NOT EXISTS `optional_tour_package` (
  `optional_tour_package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) DEFAULT NULL,
  `optional_tour_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`optional_tour_package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Дамп даних таблиці `optional_tour_package`
--

INSERT INTO `optional_tour_package` (`optional_tour_package_id`, `package_id`, `optional_tour_id`, `event_id`, `booking_id`, `user_id`) VALUES
(38, 35, 27, NULL, NULL, NULL),
(40, 35, 28, NULL, NULL, NULL),
(44, 35, 27, 631, 34, 26),
(45, 35, 27, 631, 35, 26),
(57, 32, 26, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL COMMENT 'full package name as displayed on form',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `available` varchar(20) DEFAULT NULL,
  `description` text,
  `display_order` varchar(11) DEFAULT '0',
  PRIMARY KEY (`package_id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп даних таблиці `packages`
--

INSERT INTO `packages` (`package_id`, `event_id`, `name`, `start_date`, `end_date`, `available`, `description`, `display_order`) VALUES
(32, 656, 'WELCOME TO THE WONDERFUL WORLD OF DISNEY!', '2016-02-11', '2016-02-23', 'Y', '<p><img src="../uploads/source/disney_main%20(1).jpg" width="650" height="187" data-mce-src="../uploads/source/disney_main%20(1).jpg" data-mce-selected="1"><br>The Walt Disney World Marathon presented by CIGNA is the final race of the weekend, where you will complete the most magical 26.2 miles on Earth. The course takes you through all four Walt Disney World Theme Parks, starting at Epcot, continuing on to the Magic Kingdom Park, Disneyâ€™s Animal Kingdom Park and Disneyâ€™s Hollywood Studios before an exhilarating finish back at Epcot.</p><p>In addition to the Marathon Event there is a Half Marathon, Walt Disney World 10K, Disney Family Fun Run 5K &amp; Kids races.</p><p>If you are feeling adventerous why not participate in&nbsp;Dopey Challenge or the Goofy Race and a Half Challenge! The Dopey Challenge is the 5K, 10K, Half Marathon &amp; Marathon run over 4 days or the Goofy &amp; a Half Challenge is the Half Marathon &amp; Marathon run over 2 days!</p><p>The Walt Disney World Marathon presented by CIGNA is the final race of the weekend, where you will complete the most magical 26.2 miles on Earth. The course takes you through all four Walt Disney World Theme Parks, starting at Epcot, continuing on to the Magic Kingdom Park, Disneyâ€™s Animal Kingdom Park and Disneyâ€™s Hollywood Studios before an exhilarating finish back at Epcot.</p><p>In addition to the Marathon Event there is a Half Marathon, Walt Disney World 10K, Disney Family Fun Run 5K &amp; Kids races.</p><p>If you are feeling adventerous why not participate in&nbsp;Dopey Challenge or the Goofy Race and a Half Challenge! The Dopey Challenge is the 5K, 10K, Half Marathon &amp; Marathon run over 4 days or the Goofy &amp; a Half Challenge is the Half Marathon &amp; Marathon run over 2 days!</p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 329.5px; top: 7.5px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 654.5px; top: 101px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 329.5px; top: 194.5px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 4.5px; top: 101px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 7.5px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 654.5px; top: 7.5px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 654.5px; top: 194.5px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 194.5px;"></div>', ''),
(33, 1102, '3 Night Package - Marathon or Half Marathon', '2016-01-11', '2016-02-20', 'Y', '<p><img src="uploads/source/disney_sports_3.jpg" alt="" width="296" height="174" data-mce-src="uploads/source/disney_sports_3.jpg"><img src="uploads/source/disney_sports_2.jpg" alt="" width="296" height="174" data-mce-src="uploads/source/disney_sports_2.jpg"><img src="uploads/source/disney_sports_1.jpg" alt="" width="296" height="174" data-mce-src="uploads/source/disney_sports_1.jpg"><br></p><h2>DISNEYâ€™S VALUE RESORT</h2><p>Hotel Location: Disney''s Pop Centry Resort</p><p>&nbsp;</p><h4>NON-RUNNER PRICES</h4><p><strong>TWIN SHARE:&nbsp;$251&nbsp;&nbsp;|&nbsp; SINGLE:&nbsp; $501</strong></p><h4>PACKAGE INCLUSIONS</h4><p>Guaranteed Race Entry to the 2016 Walt Disney World â€“&nbsp; Marathon<br>3 Nightâ€™s accommodation at&nbsp;one on the above Disney&nbsp;Value Resort''s&nbsp;<br>Return Airport to Hotel Transfers (operated by Disneyworld)<br>Facebook communication with other Travelling Fit Clients participating in the Walt Disney World Marathon Weekend<br>Coolmax Running Top<br>Transfer to Race Start<br>ChampionÂ® Unisex Long-Sleeved Tech Shirt<br>Finisher Medal<br>iGiftBag<br>Personalised Race Bib<br>Official Digital Event Guide<br>On course and post race refreshments<br>Chrono Track B-Tag timed race with live runner tracking sign up for friends &amp; family</p><p>&nbsp;<strong>Please note: The package prices are based on the Marathon and Half Marathon entries</strong></p><h2>DISNEYâ€™S VALUE RESORT</h2><p>Hotel Location: Disney''s Pop Centry Resort</p><p>&nbsp;</p><h4>NON-RUNNER PRICES</h4><p><strong>TWIN SHARE:&nbsp;$251&nbsp;&nbsp;|&nbsp; SINGLE:&nbsp; $501</strong></p><h4>PACKAGE INCLUSIONS</h4><p>Guaranteed Race Entry to the 2016 Walt Disney World â€“&nbsp; Marathon<br>3 Nightâ€™s accommodation at&nbsp;one on the above Disney&nbsp;Value Resort''s&nbsp;<br>Return Airport to Hotel Transfers (operated by Disneyworld)<br>Facebook communication with other Travelling Fit Clients participating in the Walt Disney World Marathon Weekend<br>Coolmax Running Top<br>Transfer to Race Start<br>ChampionÂ® Unisex Long-Sleeved Tech Shirt<br>Finisher Medal<br>iGiftBag<br>Personalised Race Bib<br>Official Digital Event Guide<br>On course and post race refreshments<br>Chrono Track B-Tag timed race with live runner tracking sign up for friends &amp; family</p><p>&nbsp;<strong>Please note: The package prices are based on the Marathon and Half Marathon entries</strong></p>', ''),
(34, 1102, '5 Night Package - Dopey Challenge', '2016-02-12', '2016-02-20', 'Y', '<h4><strong>Package Inclusions</strong></h4><p>Guaranteed Race Entry to the 2016 Walt Disney World â€“&nbsp; Dopey Challenge<br>5 Nightâ€™s accommodation at&nbsp;either the&nbsp;Disney&nbsp;Value Resort&nbsp;or Disney''s Moderate Resort<br>Return Airport to Hotel Transfers (operated by Disneyworld)<br>Facebook communication with other Travelling Fit Clients participating in the Walt Disney World Marathon Weekend<br>Coolmax Running Top<br>Transfer to Race Start<br>ChampionÂ® Unisex Long-Sleeved Tech Shirt<br>Finisher Medal<br>iGiftBag<br>Personalised Race Bib<br>Official Digital Event Guide<br>On course and post race refreshments<br>Chrono Track B-Tag timed race with live runner tracking sign up for friends &amp; family</p><p><strong>&nbsp;<strong>Please note: The package prices are based on the&nbsp;Dopey Challenge. &nbsp;</strong></strong></p><p>&nbsp;</p><p>There is a choice of 6 different Disney Event Distances that you may want to join in. They cater to every type of runner and it is such an enjoyable experience you would not want to miss it;</p><p>&nbsp;</p><ul><li>Dopey Challenge - Marathon, Half Marathon, 10K &amp; 5k run over 4 days</li><li>Goofy Race &amp; a Half Challenge - Marathon &amp; Half Marathon run over 2 days</li><li>Marathon</li><li>Half Marathon</li><li>Walt Disney World 10K</li><li>Disney Family Fun Run 5K</li><li>Kids Dashes - 100m, 200m &amp; 400m, 13 years old &amp; under - <strong>SUBJECT TO AVAILIBILITY AT TIME OF BOOKING</strong></li><li>Mickey Mile - 13 years old &amp; under - <strong>SUBJECT TO AVAILIBILITY AT TIME OF BOOKING&nbsp;</strong><h4><strong>Package Inclusions</strong></h4><p>Guaranteed Race Entry to the 2016 Walt Disney World â€“&nbsp; Dopey Challenge<br>5 Nightâ€™s accommodation at&nbsp;either the&nbsp;Disney&nbsp;Value Resort&nbsp;or Disney''s Moderate Resort<br>Return Airport to Hotel Transfers (operated by Disneyworld)<br>Facebook communication with other Travelling Fit Clients participating in the Walt Disney World Marathon Weekend<br>Coolmax Running Top<br>Transfer to Race Start<br>ChampionÂ® Unisex Long-Sleeved Tech Shirt<br>Finisher Medal<br>iGiftBag<br>Personalised Race Bib<br>Official Digital Event Guide<br>On course and post race refreshments<br>Chrono Track B-Tag timed race with live runner tracking sign up for friends &amp; family</p><p><strong>&nbsp;<strong>Please note: The package prices are based on the&nbsp;Dopey Challenge. &nbsp;</strong></strong></p><p>&nbsp;</p><p>There is a choice of 6 different Disney Event Distances that you may want to join in. They cater to every type of runner and it is such an enjoyable experience you would not want to miss it;</p><p>&nbsp;</p><ul><li>Dopey Challenge - Marathon, Half Marathon, 10K &amp; 5k run over 4 days</li><li>Goofy Race &amp; a Half Challenge - Marathon &amp; Half Marathon run over 2 days</li><li>Marathon</li><li>Half Marathon</li><li>Walt Disney World 10K</li><li>Disney Family Fun Run 5K</li><li>Kids Dashes - 100m, 200m &amp; 400m, 13 years old &amp; under - <strong>SUBJECT TO AVAILIBILITY AT TIME OF BOOKING</strong></li><li>Mickey Mile - 13 years old &amp; under - <strong>SUBJECT TO AVAILIBILITY AT TIME OF BOOKING&nbsp;</strong></li></ul></li></ul>', ''),
(35, 631, 'Sunroute Ariake (or similar) - 5 Days', '2016-02-28', '2016-02-29', 'Y', '<h2><img src="uploads/source/tokyo_sunroute_1.jpg" alt="" width="276" height="162"><img src="uploads/source/tokyo_sunroute_2.jpg" alt="" width="275" height="162" data-mce-selected="1"><img src="uploads/source/tokyo_sunroute_3.jpg" alt="" width="270" height="159"><br><br>SUNROUTE ARIAKE HOTEL</h2><p>Hotel Distance from Finish Line: 300 Metres</p><p>Situated in the bay area this hotel is close to many tourist sights. The Tokyo Marathon expo is nearby at the&nbsp;Tokyo Big Sight&nbsp;Exhibition Hall.</p><p>The hotel has&nbsp;a restaurant which&nbsp;offers a combination of both Japanese and Western&nbsp;food.</p><h4>NON-RUNNER PACKAGES</h4><p><strong>Twin Share $956 &nbsp;|&nbsp; Single Room $1020</strong></p><h4>INCLUSIONS</h4><p>Guaranteed Race Entry<br>4 nightsâ€™ accommodation - In 25 Feb - Out 29 Feb<br>Breakfast Daily (Breakfast box provided on race day)<br>Bus Transfer to Race Start<br>Service Charges &amp; Government Tax<br>Specially designed "Australia" Coolmax running top (exclusive to Travelling Fit runners only)</p><p><strong>NOTE</strong>:<br>The land arrangement is operated by KNT, Japan.<br>* If the Sunroute Ariake Hotel is not available at time of booking, a hotel in the same area and of similar standard will be offered.</p><h2>SUNROUTE ARIAKE HOTEL</h2><p>Hotel Distance from Finish Line: 300 Metres</p><p>Situated in the bay area this hotel is close to many tourist sights. The Tokyo Marathon expo is nearby at the&nbsp;Tokyo Big Sight&nbsp;Exhibition Hall.</p><p>The hotel has&nbsp;a restaurant which&nbsp;offers a combination of both Japanese and Western&nbsp;food.</p><h4>NON-RUNNER PACKAGES</h4><p><strong>Twin Share $956 &nbsp;|&nbsp; Single Room $1020</strong></p><h4>INCLUSIONS</h4><p>Guaranteed Race Entry<br>4 nightsâ€™ accommodation - In 25 Feb - Out 29 Feb<br>Breakfast Daily (Breakfast box provided on race day)<br>Bus Transfer to Race Start<br>Service Charges &amp; Government Tax<br>Specially designed "Australia" Coolmax running top (exclusive to Travelling Fit runners only)</p><p><strong>NOTE</strong>:<br>The land arrangement is operated by KNT, Japan.<br>* If the Sunroute Ariake Hotel is not available at time of booking, a hotel in the same area and of similar standard will be offered.</p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 417.5px; top: 10.1875px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 555px; top: 91.1875px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 417.5px; top: 172.188px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 280px; top: 91.1875px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 280px; top: 10.1875px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 555px; top: 10.1875px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 555px; top: 172.188px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 280px; top: 172.188px;"></div>', ''),
(36, 631, 'Keio Plaza Hotel - 5 Days', '2016-02-28', '2016-02-29', 'Y', '<h2><br><img src="uploads/source/tokyo_keio_2.jpg" width="296" height="173" data-mce-src="uploads/source/tokyo_keio_2.jpg"><img src="uploads/source/tokyo_keio_3.jpg" alt="" width="296" height="173"><img src="uploads/source/tokyo_keio_1.jpg" alt="" width="296" height="173" data-mce-selected="1"><br>KEIO PLAZA HOTEL</h2><p>Hotel Distance to Start Line: 200 Metres</p><p>Located in the heart of Tokyo, this hotel is within walking distance to Shinjuku Station, a huge variety of restaurants and bars, shopping areas and right across the road from the Marathon start line.</p><p>The hotel has a large choice of dining styles to experience</p><h4>NON-RUNNER PACKAGES</h4><p><strong>Twin Share $1496 &nbsp;|&nbsp; Single Room $2193</strong></p><h4>INCLUSIONS</h4><p>Guaranteed Race Entry<br>5 days'' accommodation - In 25 Feb - Out 29 Feb<br>Breakfast Daily<br>Bus Transfer from Finish Area<br>Tokyo Sightseeing Tour including Expo visit - 26 Feb (Keio Plaza guests only)<br>Service Charges &amp; Government Tax<br>Specially designed "Australia" Coolmax running top (exclusive to Travelling Fit runners only)<br><br><strong>NOTE</strong>:<br>The land arrangement is operated by KNT, Japan.</p><h2>KEIO PLAZA HOTEL</h2><p>Hotel Distance to Start Line: 200 Metres</p><p>Located in the heart of Tokyo, this hotel is within walking distance to Shinjuku Station, a huge variety of restaurants and bars, shopping areas and right across the road from the Marathon start line.</p><p>The hotel has a large choice of dining styles to experience</p><h4>NON-RUNNER PACKAGES</h4><p><strong>Twin Share $1496 &nbsp;|&nbsp; Single Room $2193</strong></p><h4>INCLUSIONS</h4><p>Guaranteed Race Entry<br>5 days'' accommodation - In 25 Feb - Out 29 Feb<br>Breakfast Daily<br>Bus Transfer from Finish Area<br>Tokyo Sightseeing Tour including Expo visit - 26 Feb (Keio Plaza guests only)<br>Service Charges &amp; Government Tax<br>Specially designed "Australia" Coolmax running top (exclusive to Travelling Fit runners only)<br><br><strong>NOTE</strong>:<br>The land arrangement is operated by KNT, Japan.</p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 152px; top: 205.688px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 300px; top: 292.188px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 152px; top: 378.688px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 4px; top: 292.188px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 4px; top: 205.688px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 300px; top: 205.688px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 300px; top: 378.688px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 4px; top: 378.688px;"></div>', ''),
(37, 1119, 'PROTEA BREAKWATER LODGE - Standard Room', '2016-03-24', '2016-04-02', 'Y', '<p><img src="includes/filemanager/dialog.php/~travellingfit/admin/includes/filemanager/dialog.phpuploads/source/two_oceans_breakwater_3.jpg" alt="">&nbsp;<img src="includes/filemanager/dialog.php/~travellingfit/admin/includes/filemanager/dialog.phpuploads/source/two_oceans_breakwater_2.jpg" alt="">&nbsp;<img src="includes/filemanager/dialog.php/~travellingfit/admin/includes/filemanager/dialog.phpuploads/source/two_oceans_breakwater_1.jpg" alt=""><br><br>Hotel Location: City</p><p>Just minutes away from restaurants, pubs, theatres and shops, the Protea Hotel Breakwater Lodge is premier Cape Town hotel accommodation, ideally positioned in the heart of Cape Town''s V &amp; A Waterfront development.</p><p>Once a 19th Century prison, the Protea Hotel Breakwater Lodge today provides comfortable and affordable accommodation in a spectacular setting that looks over the bay and up onto Table Mountain. Neighbouring Cape Town''s CBD, the Protea Hotel Breakwater Lodge lies within easy reach of the main routes to the beaches, mountain walks and city hotspots, not to mention the airport. So whether you''re in Cape Town for business reasons or for a leisurely and relaxing holiday, the Protea Hotel Breakwater Lodge will provide you with comfort and convenience during your stay.</p><p>Besides the hotel''s accommodation facilities, there are two restaurants within the hotel complex, as well as a bar which is perfect for both pre-dinner cocktails and late night liqueurs.</p><h4>NON-RUNNER PRICE</h4><p>Twin Share: $668 | Single: $918&nbsp;</p><h4>PACKAGE INCLUSIONS:</h4><p>Guaranteed Race Entry (runners only)<br>4 Nights'' Accommodation<br>- Check-In: 24 March 2016<br>- Check-Out: 28 March 2016<br>Breakfast Daily<br>Return Airport Transfers<br>Carbo Dinner on Friday 25th March 2016 (Vineyard Hotel only)<br>Bus Transfer To/From Race Venue (where applicable)<br>Specially designed Travelling Fit 2XU running top - Exclusive to Travelling Fit Clients<br>Access to Hospitality Marquee on Race Day providing hot &amp; cold drinks and snacks<br>Commemorative Gift<br>VIP Status - Exclusive to Travelling Fit Runners<br>Champion Chip (race timing device)<br>1% tourism levy &amp; 14% VAT</p><p>Hotel Location: City</p><p>Just minutes away from restaurants, pubs, theatres and shops, the Protea Hotel Breakwater Lodge is premier Cape Town hotel accommodation, ideally positioned in the heart of Cape Town''s V &amp; A Waterfront development.</p><p>Once a 19th Century prison, the Protea Hotel Breakwater Lodge today provides comfortable and affordable accommodation in a spectacular setting that looks over the bay and up onto Table Mountain. Neighbouring Cape Town''s CBD, the Protea Hotel Breakwater Lodge lies within easy reach of the main routes to the beaches, mountain walks and city hotspots, not to mention the airport. So whether you''re in Cape Town for business reasons or for a leisurely and relaxing holiday, the Protea Hotel Breakwater Lodge will provide you with comfort and convenience during your stay.</p><p>Besides the hotel''s accommodation facilities, there are two restaurants within the hotel complex, as well as a bar which is perfect for both pre-dinner cocktails and late night liqueurs.</p><h4>NON-RUNNER PRICE</h4><p>Twin Share: $668 | Single: $918&nbsp;</p><h4>PACKAGE INCLUSIONS:</h4><p>Guaranteed Race Entry (runners only)<br>4 Nights'' Accommodation<br>- Check-In: 24 March 2016<br>- Check-Out: 28 March 2016<br>Breakfast Daily<br>Return Airport Transfers<br>Carbo Dinner on Friday 25th March 2016 (Vineyard Hotel only)<br>Bus Transfer To/From Race Venue (where applicable)<br>Specially designed Travelling Fit 2XU running top - Exclusive to Travelling Fit Clients<br>Access to Hospitality Marquee on Race Day providing hot &amp; cold drinks and snacks<br>Commemorative Gift<br>VIP Status - Exclusive to Travelling Fit Runners<br>Champion Chip (race timing device)<br>1% tourism levy &amp; 14% VAT</p>', ''),
(38, 1120, 'HUDSON HOTEL - STANDARD QUEEN ROOM - 1 QUEEN BED', '2016-03-18', '2016-03-21', 'Y', '<p align="left"><br>356 West 58th Street, New York, NY 10019 USA</p><p><strong>Check In: </strong>3:00pm</p><p><strong>Check Out: </strong>12:00pm</p><h4>&nbsp;&nbsp;<img src="uploads/source/nyc_half_hudson_6.jpg" alt="" width="296" height="174" data-mce-selected="1"><br><br>SUPPORTER PRICES</h4><p><strong>Double&nbsp;Share: $598 &nbsp;|&nbsp; Single Room $1101</strong></p><p align="left"><strong>Property Description and Location:</strong></p><p>Close to Central Park and the theatre district, Hudson is a brilliant reflection of the boldness and diversity of the city, the quintessential New York hotel. The Philippe Starcks design includes chartreuse-lit escalators that sweep guests to a 40-foot high ivy-covered lobby, an enormous indoor and or outdoor private park and a ceiling fresco by world-famous painter Francesco Clemente. Hudson is the next generation of cheap chic stylish, democratic, affordable, young at heart and utterly cool. Hudson Hotel also includes a rooftop solarium and conservatory, cafeteria, library and concierge.</p><p>Hotel Location: The Hudson Hotel is located near the Half Marathon starting line, close to Central Park and the theatre district.</p><p><strong>Package Inclusions</strong></p><p>Guaranteed Race Entry for 2016 New York City Half Marathon<br>3 nightâ€™s accommodation at the Hudson Hotel New York<br>Check-in: Fri 18 March 2016 Check-out: Mon 21 March 2016<br>Tax and service charge<br>Travelling Fit 2XU running top - exclusive to Travelling Fit runners<br>Invitation to Travelling Fits VIP Facebook Group - exclusive to Travelling Fit runners<br>2016 New York City Half Marathon finishers certificate</p><p align="left">356 West 58th Street, New York, NY 10019 USA</p><p><strong>Check In: </strong>3:00pm</p><p><strong>Check Out: </strong>12:00pm</p><h4>SUPPORTER PRICES</h4><p><strong>Double&nbsp;Share: $598 &nbsp;|&nbsp; Single Room $1101</strong></p><p align="left"><strong>Property Description and Location:</strong></p><p>Close to Central Park and the theatre district, Hudson is a brilliant reflection of the boldness and diversity of the city, the quintessential New York hotel. The Philippe Starcks design includes chartreuse-lit escalators that sweep guests to a 40-foot high ivy-covered lobby, an enormous indoor and or outdoor private park and a ceiling fresco by world-famous painter Francesco Clemente. Hudson is the next generation of cheap chic stylish, democratic, affordable, young at heart and utterly cool. Hudson Hotel also includes a rooftop solarium and conservatory, cafeteria, library and concierge.</p><p>Hotel Location: The Hudson Hotel is located near the Half Marathon starting line, close to Central Park and the theatre district.</p><p><strong>Package Inclusions</strong></p><p>Guaranteed Race Entry for 2016 New York City Half Marathon<br>3 nightâ€™s accommodation at the Hudson Hotel New York<br>Check-in: Fri 18 March 2016 Check-out: Mon 21 March 2016<br>Tax and service charge<br>Travelling Fit 2XU running top - exclusive to Travelling Fit runners<br>Invitation to Travelling Fits VIP Facebook Group - exclusive to Travelling Fit runners<br>2016 New York City Half Marathon finishers certificate</p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 160.5px; top: 96.125px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 308.5px; top: 183.125px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 160.5px; top: 270.125px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 12.5px; top: 183.125px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 12.5px; top: 96.125px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 308.5px; top: 96.125px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 308.5px; top: 270.125px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 12.5px; top: 270.125px;"></div>', ''),
(39, 1120, 'HUDSON HOTEL - 3 Nights Deluxe Queen Room', '2016-03-18', '2016-03-21', 'Y', '<p align="left"><img src="uploads/source/nyc_half_hudson_2.jpg" data-mce-src="uploads/source/nyc_half_hudson_2.jpg" width="296" height="174">&nbsp;<img src="uploads/source/nyc_half_hudson_3%20(1).jpg" data-mce-src="uploads/source/nyc_half_hudson_3%20(1).jpg" width="296" height="174">&nbsp;<img src="uploads/source/tokyo_keio_3.jpg" data-mce-src="uploads/source/tokyo_keio_3.jpg" width="296" height="174" data-mce-selected="1"><br><br>356 West 58th Street, New York, NY 10019 USA</p><p><strong>Check In: </strong>3:00pm</p><p><strong>Check Out: </strong>12:00pm</p><h4>SUPPORTER PRICES</h4><p><strong>Double&nbsp;Share: $764&nbsp; | Single Room $1432</strong></p><p align="left"><strong>Property Description and Location:</strong></p><p>Close to Central Park and the theatre district, Hudson is a brilliant reflection of the boldness and diversity of the city, the quintessential New York hotel. The Philippe Starcks design includes chartreuse-lit escalators that sweep guests to a 40-foot high ivy-covered lobby, an enormous indoor and or outdoor private park and a ceiling fresco by world-famous painter Francesco Clemente. Hudson is the next generation of cheap chic stylish, democratic, affordable, young at heart and utterly cool. Hudson Hotel also includes a rooftop solarium and conservatory, cafeteria, library and concierge.</p><p>Hotel Location: The Hudson Hotel is located near the Half Marathon starting line, close to Central Park and the theatre district.</p><p><strong>Package Inclusions</strong></p><p>Guaranteed Race Entry for 2016 New York City Half Marathon<br>3 nightâ€™s accommodation at the Hudson Hotel New York<br>Check-in: Fri 18 March 2016 Check-out: Mon 21 March 2016<br>Tax and service charge<br>Travelling Fit 2XU running top - exclusive to Travelling Fit runners<br>Invitation to Travelling Fits VIP Facebook Group - exclusive to Travelling Fit runners<br>2016 New York City Half Marathon finishers certificate</p><p align="left">356 West 58th Street, New York, NY 10019 USA</p><p><strong>Check In: </strong>3:00pm</p><p><strong>Check Out: </strong>12:00pm</p><h4>SUPPORTER PRICES</h4><p><strong>Double&nbsp;Share: $764&nbsp; | Single Room $1432</strong></p><p align="left"><strong>Property Description and Location:</strong></p><p>Close to Central Park and the theatre district, Hudson is a brilliant reflection of the boldness and diversity of the city, the quintessential New York hotel. The Philippe Starcks design includes chartreuse-lit escalators that sweep guests to a 40-foot high ivy-covered lobby, an enormous indoor and or outdoor private park and a ceiling fresco by world-famous painter Francesco Clemente. Hudson is the next generation of cheap chic stylish, democratic, affordable, young at heart and utterly cool. Hudson Hotel also includes a rooftop solarium and conservatory, cafeteria, library and concierge.</p><p>Hotel Location: The Hudson Hotel is located near the Half Marathon starting line, close to Central Park and the theatre district.</p><p><strong>Package Inclusions</strong></p><p>Guaranteed Race Entry for 2016 New York City Half Marathon<br>3 nightâ€™s accommodation at the Hudson Hotel New York<br>Check-in: Fri 18 March 2016 Check-out: Mon 21 March 2016<br>Tax and service charge<br>Travelling Fit 2XU running top - exclusive to Travelling Fit runners<br>Invitation to Travelling Fits VIP Facebook Group - exclusive to Travelling Fit runners<br>2016 New York City Half Marathon finishers certificate</p><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:n-resize; margin:0; padding:0" style="cursor: n-resize; margin: 0px; padding: 0px; left: 152.5px; top: 183.5px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:e-resize; margin:0; padding:0" style="cursor: e-resize; margin: 0px; padding: 0px; left: 300.5px; top: 270.5px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:s-resize; margin:0; padding:0" style="cursor: s-resize; margin: 0px; padding: 0px; left: 152.5px; top: 357.5px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:w-resize; margin:0; padding:0" style="cursor: w-resize; margin: 0px; padding: 0px; left: 4.5px; top: 270.5px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:nw-resize; margin:0; padding:0" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 183.5px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:ne-resize; margin:0; padding:0" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 300.5px; top: 183.5px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:se-resize; margin:0; padding:0" style="cursor: se-resize; margin: 0px; padding: 0px; left: 300.5px; top: 357.5px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" data-mce-style="cursor:sw-resize; margin:0; padding:0" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 4.5px; top: 357.5px;"></div>', ''),
(40, 393, 'Athens Hotel (or similar) - 5 Days', '2016-04-01', '2016-04-07', 'Y', '<p>inclusion 1<br data-mce-bogus="1"></p><p>Inclusion 2<br data-mce-bogus="1"></p><p>inclusion 1</p><p>Inclusion 2<br></p><p>inclusion 1</p><p>Inclusion 2<br data-mce-bogus="1"></p>', '');

-- --------------------------------------------------------

--
-- Структура таблиці `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `room_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Y',
  `hotel_name` varchar(255) NOT NULL,
  `event_id` int(11) NOT NULL,
  `optional_tour` enum('Y','N') NOT NULL DEFAULT 'N',
  `package_id` int(11) DEFAULT NULL,
  `optional_tour_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_type_id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Дамп даних таблиці `room_type`
--

INSERT INTO `room_type` (`room_type_id`, `name`, `price`, `capacity`, `status`, `hotel_name`, `event_id`, `optional_tour`, `package_id`, `optional_tour_id`) VALUES
(43, 'Cheap', 1, 20, 'Y', 'The best hotel', 656, 'N', 32, NULL),
(44, 'expensive', 100, 20, 'Y', 'The best hotel', 656, 'N', 32, NULL),
(45, 'Cheap', 20, 60, 'Y', 'Not the best Hotel', 656, 'N', 32, NULL),
(46, 'expensive', 300, 4, 'Y', 'Not the best Hotel', 656, 'N', 32, NULL),
(47, 'Twin Share', 839, 20, 'Y', 'DISNEYâ€™S VALUE RESORT - 3 NIGHTS', 1102, 'N', 33, NULL),
(48, 'Single Room', 866, 20, 'Y', 'DISNEYâ€™S VALUE RESORT - 3 NIGHTS', 1102, 'N', 34, NULL),
(49, 'Twin Share', 1140, 20, 'Y', 'Hotel package 2', 1102, 'N', 34, NULL),
(50, 'Twin Share', 1141, 20, 'Y', 'Hotel package 2', 1102, 'N', 34, NULL),
(51, 'Twin', 1452, 20, 'Y', 'ARIAKE HOTEL', 631, 'N', 35, NULL),
(52, 'Single', 1517, 25, 'Y', 'ARIAKE HOTEL', 631, 'N', 35, NULL),
(55, 'Twin', 1990, 13, 'Y', 'Keio Plaza Hotel', 631, 'N', 36, NULL),
(56, 'Single', 2689, 14, 'Y', 'Keio Plaza Hotel', 631, 'N', 36, NULL),
(57, 'Twin Share', 815, 50, 'Y', 'PROTEA BREAKWATER LODGE', 1119, 'N', 37, NULL),
(58, 'Single Room', 1065, 50, 'Y', 'PROTEA BREAKWATER LODGE', 1119, 'N', 37, NULL),
(59, 'Twin Share', 950, 47, 'Y', 'HUDSON HOTEL', 1120, 'N', 38, NULL),
(60, 'Single Room', 1453, 46, 'Y', 'HUDSON HOTEL', 1120, 'N', 38, NULL),
(61, 'Twin Share', 1115, 23, 'Y', 'HUDSON HOTEL', 1120, 'N', 39, NULL),
(62, 'Single Room', 1784, 42, 'Y', 'HUDSON HOTEL', 1120, 'N', 39, NULL),
(63, 'Twin Share', 1234, 4, 'Y', 'Executive Deluxe', 393, 'N', 40, NULL),
(64, 'Queen Room', 1111, 2, 'Y', 'Standard Twin', 393, 'N', 40, NULL),
(65, 'Single', 111111111, 1, 'Y', 'Test Hotel', 393, 'N', 40, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `singlets`
--

CREATE TABLE IF NOT EXISTS `singlets` (
  `singlet_id` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(100) DEFAULT NULL,
  `type` enum('M','F') DEFAULT NULL,
  `available_YN` enum('Y','N') DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`singlet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп даних таблиці `singlets`
--

INSERT INTO `singlets` (`singlet_id`, `size`, `type`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Female Size 8', 'F', 'Y', NULL, 'N'),
(2, 'Female Size 10', 'F', 'Y', NULL, 'N'),
(3, 'Female Size 12', 'F', 'Y', NULL, 'N'),
(4, 'Female Size 14', 'F', 'Y', NULL, 'N'),
(5, 'Female Size 16', 'F', 'Y', NULL, 'N'),
(6, 'Female Size 18', 'F', 'Y', NULL, 'N'),
(7, 'Male Small - (S)', 'M', 'Y', NULL, 'N'),
(8, 'Male Medium - (M)', 'M', 'Y', NULL, 'N'),
(9, 'Male Large - (L)', 'M', 'Y', NULL, 'N'),
(10, 'Male Extra Large - (XL)', 'M', 'Y', NULL, 'N'),
(11, 'Male Extra Extra Large - (XXL)', 'M', 'Y', NULL, 'N'),
(12, 'Male Extra Extra Extra Large - (XXXL)', 'M', 'Y', NULL, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primrary contacts firstname initial and surname to be sent as a reference to the merchant',
  `cardholders_name` varchar(100) DEFAULT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `amount_with_fee` varchar(45) DEFAULT NULL,
  `payment_method` varchar(2) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `merchant_trans_id` varchar(500) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `funds_received_YN` char(1) DEFAULT 'N',
  `comments` varchar(1000) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  `bpay_code` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Дамп даних таблиці `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `cardholders_name`, `amount`, `amount_with_fee`, `payment_method`, `ts`, `merchant_trans_id`, `booking_id`, `funds_received_YN`, `comments`, `archived_YN`, `bpay_code`, `user_id`) VALUES
(27, NULL, '839', NULL, 'BP', '2016-02-08 21:00:58', NULL, 27, 'N', NULL, 'N', '272211027', 22),
(28, 'sdgsdgsdg', '20', NULL, 'CC', '2016-02-08 21:51:15', 'CC', 28, '', '', 'N', NULL, 22),
(29, NULL, '47', NULL, 'BP', '2016-02-08 21:51:30', NULL, 28, 'N', NULL, 'N', '28226564', 22),
(30, NULL, '150', NULL, 'DD', '2016-02-08 21:52:25', NULL, 28, 'N', NULL, 'N', '', 22),
(31, 'test name', '99', NULL, 'BP', '2016-02-09 02:33:16', '232432423', 24, 'N', NULL, 'N', '23423432234', 21),
(32, NULL, '200', NULL, 'BP', '2016-02-10 09:26:10', NULL, 32, 'N', NULL, 'N', '32226316', 22),
(33, NULL, '200', NULL, 'BP', '2016-02-10 09:26:59', NULL, 32, 'N', NULL, 'N', '32226316', 22),
(35, NULL, '839', NULL, 'BP', '2016-02-11 10:49:48', NULL, 31, 'N', NULL, 'N', '312611022', 26),
(37, NULL, '4446', NULL, 'BP', '2016-02-11 12:12:15', NULL, 34, 'N', NULL, 'N', '34266316', 26),
(38, NULL, '4446', NULL, 'BP', '2016-02-11 12:33:21', NULL, 35, 'N', NULL, 'N', '35266318', 26),
(39, NULL, '882', NULL, 'BP', '2016-02-11 12:35:42', NULL, 33, 'N', NULL, 'N', '33266314', 26),
(40, NULL, '118', NULL, 'BP', '2016-02-11 12:36:11', NULL, 33, 'N', NULL, 'N', '33266314', 26),
(41, NULL, '200', NULL, 'BP', '2016-02-11 12:37:32', NULL, 33, 'N', NULL, 'N', '33266314', 26),
(42, NULL, '0.00', NULL, 'CC', '2016-02-11 15:52:36', NULL, 36, 'N', NULL, 'N', NULL, NULL),
(44, NULL, '100', NULL, 'BP', '2016-02-15 13:49:15', NULL, 33, 'N', NULL, 'N', '33266314', 66),
(45, NULL, '100.00', NULL, 'DD', '2016-02-15 14:06:19', NULL, 37, 'N', NULL, 'N', NULL, 79),
(46, NULL, '18', NULL, 'BP', '2016-02-15 15:17:57', NULL, 38, 'N', NULL, 'N', '382411028', 82),
(47, NULL, '100', NULL, 'DD', '2016-02-15 15:24:52', NULL, 38, 'N', NULL, 'N', NULL, 82),
(61, 'TEST', '12', NULL, 'CC', '2016-02-15 16:27:29', 'CC', 39, '', '', 'N', NULL, 84),
(62, NULL, '2000', NULL, 'BP', '2016-02-16 00:41:55', NULL, 40, 'N', NULL, 'N', '40266319', 87),
(63, NULL, '1000', NULL, 'BP', '2016-02-16 01:36:58', NULL, 41, 'N', NULL, 'N', '41266311', 89),
(64, NULL, '200', NULL, 'DD', '2016-02-16 01:37:33', NULL, 41, 'N', NULL, 'N', '', 89);

-- --------------------------------------------------------

--
-- Структура таблиці `travel_insurance`
--

CREATE TABLE IF NOT EXISTS `travel_insurance` (
  `travel_insurance` int(11) NOT NULL AUTO_INCREMENT,
  `travel_insurance_name` varchar(4000) DEFAULT NULL,
  `brief_description` varchar(25) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`travel_insurance`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `travel_insurance`
--

INSERT INTO `travel_insurance` (`travel_insurance`, `travel_insurance_name`, `brief_description`, `order`) VALUES
(1, 'I will take out travel insurance online via the Travelling Fit website and earn 25% discount - <a href="http://www.suresave.net.au/home.php?affid=2971" target=_"blank">Click here to book your insurance</a>', 'Online via Website', 1),
(2, 'I would like to take out Travel Insurance but would prefer Travelling Fit to issue my policy at the retail price. ', 'Will Contact Office', 2),
(3, 'I do not wish to take out travel insurance through Travelling Fit - Warning: please refer clause 13.2 & 13.3 of the Booking Terms & Conditions before accepting this option', 'No Thanks', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `tshirts`
--

CREATE TABLE IF NOT EXISTS `tshirts` (
  `tshirt_id` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(100) DEFAULT NULL,
  `type` char(1) DEFAULT NULL COMMENT 'M - male',
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  PRIMARY KEY (`tshirt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп даних таблиці `tshirts`
--

INSERT INTO `tshirts` (`tshirt_id`, `size`, `type`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Child Size 6', 'U', 'Y', 0, 'N'),
(2, 'Child Size 8', 'U', 'Y', 1, 'N'),
(3, 'Child Size 10', 'U', 'Y', 2, 'N'),
(4, 'Child Size 12', 'U', 'Y', 3, 'N'),
(5, 'Child Size 14', 'U', 'Y', 4, 'N'),
(6, 'Female Size 8', 'F', 'Y', 5, 'N'),
(7, 'Female Size 10', 'F', 'Y', 6, 'N'),
(8, 'Female Size 12', 'F', 'Y', 7, 'N'),
(9, 'Female Size 14', 'F', 'Y', 8, 'N'),
(10, 'Female Size 16', 'F', 'Y', 9, 'N'),
(11, 'Female Size 18', 'F', 'Y', 10, 'N'),
(12, 'Male Small - (S)', 'M', 'Y', 11, 'N'),
(13, 'Male Medium - (M)', 'M', 'Y', 13, 'N'),
(14, 'Male Large - (L)', 'M', 'Y', 14, 'N'),
(15, 'Male Extra Large - (XL)', 'M', 'Y', 15, 'N'),
(16, 'Male Extra Extra Large - (XXL)', 'M', 'Y', 16, 'N'),
(17, 'Male Extra Extra Extra Large - (XXXL)', 'M', 'Y', 17, 'N'),
(18, 'Singlet - M', 'M', 'Y', NULL, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level` int(11) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` enum('enabled','disabled') NOT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `google_id` (`google_id`),
  UNIQUE KEY `fb_id` (`fb_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`user_id`, `access_level`, `username`, `firstname`, `lastname`, `password`, `token`, `email`, `status`, `fb_id`, `google_id`) VALUES
(1, 1, 'admin', 'Michael', 'Walton', 'zen#2014', '', 'admin@admin.com', 'enabled', '0', '0'),
(19, 2, NULL, NULL, NULL, 'an33424jHQQEY', NULL, 'andriystefak@gmail.com', 'enabled', NULL, NULL),
(20, 2, NULL, NULL, NULL, 'sthyVTKO97aYQ', 'b2575fda37e250c893238be9a4eaf1aca6387e61', 'stuart@simpleclick.com.au', 'disabled', NULL, NULL),
(21, 2, NULL, NULL, NULL, 'maA9OpRMtpISw', NULL, 'manifestinfotech@gmail.com', 'enabled', NULL, NULL),
(22, 2, NULL, NULL, NULL, 'fey35wiLjpMyE', NULL, 'fedoryshyn.sergiy@gmail.com', 'enabled', NULL, NULL),
(24, 2, NULL, 'Andriy', 'Stefak', NULL, NULL, NULL, 'enabled', '437089536466217', NULL),
(25, 2, NULL, 'Serge', 'Fedoryshyn', NULL, NULL, NULL, 'enabled', '10207191923988410', NULL),
(26, 2, NULL, 'Stuart', 'Bond', NULL, NULL, NULL, 'enabled', '1174002492627740', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
