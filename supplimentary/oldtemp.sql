-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Січ 17 2016 р., 15:09
-- Версія сервера: 5.6.24
-- Версія PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `oldtemp`
--

-- --------------------------------------------------------

--
-- Структура таблиці `accommodation`
--

CREATE TABLE IF NOT EXISTS `accommodation` (
  `accommodation_id` int(11) NOT NULL,
  `accommodation_name` varchar(200) DEFAULT NULL COMMENT 'full package name as displayed on form',
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `accommodation`
--

INSERT INTO `accommodation` (`accommodation_id`, `accommodation_name`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'OUTBACK PIONEER BUDGET', 'Y', 1, 'N'),
(2, 'OUTBACK PIONEER STANDARD', 'Y', 2, 'N'),
(3, 'EMU WALK APARTMENTS 1 BED', 'Y', 3, 'N'),
(4, 'EMU WALK APARTMENTS 2 BED', 'Y', 4, 'N'),
(5, 'DESERT GARDENS STANDARD', 'Y', 5, 'N'),
(6, 'DESERT GARDENS DELUXE ROCK VIEW', 'Y', 6, 'N'),
(7, 'SAILS IN THE DESERT SUPERIOR', 'Y', 7, 'N'),
(8, 'SAILS IN THE DESERT - TERRACE', 'Y', 8, 'N'),
(9, 'SAILS IN THE DESERT DELUXE SUITE', 'N', 9, 'N'),
(10, 'NO ACCOMMODATION REQUIRED', 'Y', 10, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `accommodation_package`
--

CREATE TABLE IF NOT EXISTS `accommodation_package` (
  `package_accommodation_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `accommodation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `accommodation_package`
--

INSERT INTO `accommodation_package` (`package_accommodation_id`, `package_id`, `accommodation_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 2),
(4, 1, 2),
(5, 2, 3),
(6, 2, 3),
(7, 2, 4),
(8, 2, 4),
(9, 3, 5),
(10, 3, 5),
(11, 3, 6),
(12, 3, 6),
(13, 4, 7),
(14, 4, 7),
(15, 4, 8),
(16, 4, 8),
(17, 5, 9),
(18, 5, 9),
(19, 5, 10),
(20, 5, 10);

-- --------------------------------------------------------

--
-- Структура таблиці `bedding`
--

CREATE TABLE IF NOT EXISTS `bedding` (
  `bedding_id` int(11) NOT NULL,
  `bedding_name` varchar(100) DEFAULT NULL,
  `max_people` int(11) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bedding`
--

INSERT INTO `bedding` (`bedding_id`, `bedding_name`, `max_people`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Single', 1, 'Y', 0, 'N'),
(2, 'Twin (2 beds)', 2, 'Y', 1, 'N'),
(3, 'Double (1 bed for two people)', 3, 'Y', 2, 'N'),
(4, 'Triple', 4, 'Y', 3, 'N'),
(5, 'Quad', 5, 'Y', 5, 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `bedding_configuration`
--

CREATE TABLE IF NOT EXISTS `bedding_configuration` (
  `bedding_configuration_id` int(11) NOT NULL,
  `accommodation_id` int(11) DEFAULT NULL,
  `bedding_configuration_name` varchar(100) DEFAULT NULL,
  `bedding_cost` varchar(20) DEFAULT NULL,
  `min_people` int(11) DEFAULT '1',
  `max_people` int(11) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bedding_configuration`
--

INSERT INTO `bedding_configuration` (`bedding_configuration_id`, `accommodation_id`, `bedding_configuration_name`, `bedding_cost`, `min_people`, `max_people`) VALUES
(1, 1, 'Single', '140.00', 1, 1),
(2, 2, 'Twin (2 beds)', '240.00', 1, 2),
(3, 3, 'Double (1 bed for two people)', '240.00', 1, 3),
(4, 4, 'Triple', '340.00', 2, 3),
(5, 5, 'Quad', '340.00', 2, 4),
(6, 6, 'Bedding Configuration Name6', '440.00', 3, 5),
(7, 7, 'Bedding Configuration Name7', '440.00', 3, 5),
(8, 8, 'Bedding Configuration Name8', '540.00', 5, 6),
(9, 9, 'Bedding Configuration Name9', '640.00', 5, 7),
(10, 10, 'Bedding Configuration Name10', '640.00', 6, 7);

-- --------------------------------------------------------

--
-- Структура таблиці `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `booking_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `booking_code` varchar(256) DEFAULT NULL,
  `total_cost` decimal(15,2) DEFAULT NULL,
  `deposit` decimal(6,2) DEFAULT NULL COMMENT 'The deposit is calculated by the number of adults and not children.',
  `bedding_configuration_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `number_adult_guests` int(11) DEFAULT NULL,
  `number_children_guests` int(11) DEFAULT NULL,
  `extra_nights_pre` int(11) DEFAULT NULL,
  `extra_nights_post` int(11) DEFAULT NULL,
  `share_request_YN` char(1) DEFAULT 'N',
  `travel_insurance_option_id` int(11) DEFAULT NULL,
  `ec_firstname` varchar(100) DEFAULT NULL COMMENT 'emergency contact firstname',
  `ec_lastname` varchar(100) DEFAULT NULL COMMENT 'emergency contact lastname',
  `ec_phone` varchar(40) DEFAULT NULL,
  `ec_email` varchar(400) DEFAULT NULL,
  `passport_number` varchar(400) DEFAULT 'None',
  `how_hear_id` int(11) DEFAULT NULL,
  `ack_tc_yn` char(1) DEFAULT 'N' COMMENT 'acknowledged terms and conditions',
  `airline_freq_flyer_number` varchar(200) DEFAULT NULL,
  `consultant` varchar(25) NOT NULL,
  `passport_expiry` date NOT NULL,
  `passport_nationality` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `step` int(11) DEFAULT NULL,
  `completed` enum('Y','N') NOT NULL,
  `paid` enum('Y','N') DEFAULT NULL,
  `active_living_membership` varchar(11) NOT NULL,
  `additional_information` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bookings`
--

INSERT INTO `bookings` (`booking_id`, `event_id`, `package_id`, `user_id`, `booking_code`, `total_cost`, `deposit`, `bedding_configuration_id`, `room_type_id`, `number_adult_guests`, `number_children_guests`, `extra_nights_pre`, `extra_nights_post`, `share_request_YN`, `travel_insurance_option_id`, `ec_firstname`, `ec_lastname`, `ec_phone`, `ec_email`, `passport_number`, `how_hear_id`, `ack_tc_yn`, `airline_freq_flyer_number`, `consultant`, `passport_expiry`, `passport_nationality`, `timestamp`, `step`, `completed`, `paid`, `active_living_membership`, `additional_information`) VALUES
(1, 0, 1, 1, 'de3e4cf55b1cee839ead430cb205b16e', '5165.00', '500.00', 1, 1, 2, 0, 0, 0, 'N', 1, 'Susan', 'Rudy', '+447851212082', 'test@test.com', 'N845843857847', 6, 'Y', '8456', '', '2016-07-28', 'Polish', '2016-01-16 15:49:56', 0, 'N', NULL, '0', ''),
(2, 0, 2, 0, '96bb4d7e9d6f8f142619d9736dd28093', '5702.00', '500.00', 2, 2, 3, 0, 0, 0, 'N', 1, 'Bob', 'Dyhlan', '+446851212082', 'test2@test.com', 'M845843857847', 5, 'Y', '7456', '', '2016-07-28', 'Polish', '2015-09-09 16:15:50', 0, 'Y', NULL, '0', ''),
(3, 0, 3, 1, '11fe36ca16cc25cd0d238dd979b91146', '6540.00', '0.00', 3, 3, 4, 0, 0, 0, 'N', 1, 'Michael', 'Nuvo', '+448851212082', 'test3@test.com', 'L845843857847', 4, 'Y', '9456', '', '2016-07-28', 'Polish', '2016-01-16 15:46:28', 0, 'N', NULL, '0', ''),
(4, 0, 4, 0, 'a8199d795b195fc674895fa26cec7101', '7540.00', '0.00', 4, 4, 5, 0, 0, 0, 'N', 1, 'Henri', 'LAPLANCHE', '+444851212082', 'test4@test.com', 'K845843857847', 3, 'Y', '1456', '', '2016-07-28', 'Polish', '2016-01-16 13:33:36', 0, 'N', NULL, '0', ''),
(5, 0, 5, 0, 'c4c8799ea6fd30966a878eeade2364f2', '8540.00', '0.00', 5, 5, 6, 0, 0, 0, 'N', 1, 'Roman', 'Boyko', '+442851212082', 'test5@test.com', 'V845843857847', 2, 'Y', '2456', '', '2016-07-28', 'Polish', '2015-10-10 12:15:50', 0, 'Y', NULL, '0', ''),
(6, 1, 1, 1, NULL, NULL, NULL, 0, 34, 1, 1, 5, 8, 'N', 3, NULL, NULL, NULL, NULL, 'None', 5, 'N', NULL, '', '0000-00-00', NULL, '2016-01-16 15:50:18', 4, 'Y', NULL, '3', 'hello world!'),
(9, 1, 1, 1, NULL, NULL, NULL, 1, 20, 1, 1, 5, 8, 'Y', 2, NULL, NULL, NULL, NULL, 'None', 9, 'N', NULL, '', '0000-00-00', NULL, '2016-01-16 18:21:24', 3, 'N', NULL, '3', 'hello world!');

-- --------------------------------------------------------

--
-- Структура таблиці `costs`
--

CREATE TABLE IF NOT EXISTS `costs` (
  `cost_id` int(11) NOT NULL,
  `cost_type` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `costs`
--

INSERT INTO `costs` (`cost_id`, `cost_type`, `parent_id`, `amount`) VALUES
(1, NULL, 1, '5000.00'),
(2, NULL, 2, '6000.00'),
(3, NULL, 3, '7000.00'),
(4, NULL, 4, '8000.00'),
(5, NULL, 5, '9000.00');

-- --------------------------------------------------------

--
-- Структура таблиці `guests`
--

CREATE TABLE IF NOT EXISTS `guests` (
  `user_id` int(11) NOT NULL,
  `user` int(11) NOT NULL DEFAULT '1',
  `booking_id` int(11) DEFAULT NULL,
  `event_type_id` int(11) DEFAULT NULL,
  `primary_contact_YN` char(1) DEFAULT 'N',
  `guest_no` int(11) DEFAULT NULL COMMENT 'what guest number is the person? 1 (primary contact), 2, 3, 4, etc',
  `firstname` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `prefered_name` varchar(50) DEFAULT NULL,
  `dob` varchar(45) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `prefered_event_id` int(11) DEFAULT NULL,
  `occupation` varchar(200) DEFAULT NULL,
  `child_YN` char(1) DEFAULT 'N',
  `address_same_primary_YN` char(1) DEFAULT 'N',
  `address_1` varchar(500) DEFAULT NULL,
  `suburb` varchar(500) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL COMMENT 'free text - perhaps autocomplete based on country',
  `postcode` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `phone1_preferred` varchar(50) DEFAULT NULL,
  `phone1_work_number_YN` char(1) DEFAULT 'N',
  `phone2_alternate` varchar(50) DEFAULT NULL,
  `phone2_work_number_YN` char(1) DEFAULT 'N',
  `email` varchar(150) DEFAULT NULL,
  `passport_number` varchar(200) DEFAULT 'None',
  `passport_expiry` date NOT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  `airline_freq_flyer_number` varchar(200) DEFAULT NULL,
  `dietary_requirements` text,
  `passport_nationality` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `partisipation` varchar(255) NOT NULL,
  `emergency_contact_name` varchar(255) NOT NULL,
  `emergency_contact_phone_number` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `guests`
--

INSERT INTO `guests` (`user_id`, `user`, `booking_id`, `event_type_id`, `primary_contact_YN`, `guest_no`, `firstname`, `middle_name`, `lastname`, `prefered_name`, `dob`, `gender`, `prefered_event_id`, `occupation`, `child_YN`, `address_same_primary_YN`, `address_1`, `suburb`, `state`, `postcode`, `country`, `phone1_preferred`, `phone1_work_number_YN`, `phone2_alternate`, `phone2_work_number_YN`, `email`, `passport_number`, `passport_expiry`, `archived_YN`, `airline_freq_flyer_number`, `dietary_requirements`, `passport_nationality`, `timestamp`, `partisipation`, `emergency_contact_name`, `emergency_contact_phone_number`, `title`) VALUES
(1, 1, 1, 1, 'N', 2, 'Bob', 'Daves', 'Dyhlan', 'Bob', '09/07/1984', 'M', 1, 'lawer', 'Y', 'Y', '', '', '', '', 'Poland', '5511223344', 'Y', '+446851212082', 'N', 'test2@test.com', 'M845843857847', '2016-07-28', '', '7456', 'None', 'Poland', '2016-01-13 08:59:48', '', '', '', ''),
(2, 1, 2, 2, 'Y', 1, 'Susan', 'Susan', 'Rudy', 'Susan', '09/07/1985', 'W', 2, 'dentist', 'Y', 'Y', '', '', '', '', 'Poland', '+447851212082', 'Y', '5521223344', 'N', 'test@test.com', 'N845843857847', '2016-07-28', '', '8456', 'None', 'Poland', '2016-01-13 08:59:48', '', '', '', ''),
(3, 1, 3, 3, 'Y', 1, 'Michael', 'Michael', 'Nuvo', 'Michael', '09/07/1983', 'M', 1, 'programmer', 'Y', 'Y', '', '', '', '', 'Poland', '+448851212082', 'Y', '5521223344', 'N', 'test3@test.com', 'L845843857847', '2016-07-28', '', '9456', 'None', 'Poland', '2016-01-13 08:59:48', '', '', '', ''),
(4, 1, 4, 4, 'Y', 1, 'Henri', 'Henri', 'LAPLANCHE', 'Henri', '09/07/1986', 'M', 3, 'miner', 'Y', 'Y', '', '', '', '', 'Poland', '+444851212082', 'Y', '5521223344', 'N', 'test4@test.com', 'K845843857847', '2016-07-28', '', '1456', 'None', 'Poland', '2016-01-13 08:59:48', '', '', '', ''),
(5, 1, 5, 5, 'N', 2, 'Roman', 'Roman', 'Boyko', 'Roman', '09/07/1982', 'M', 1, 'driver', 'Y', 'Y', '', '', '', '', 'Poland', '+442851212082', 'Y', '5521223344', 'N', 'test5@test.com', 'V845843857847', '2016-07-28', '', '2456', 'None', 'Poland', '2016-01-13 08:59:48', '', '', '', ''),
(6, 1, 6, NULL, 'Y', NULL, 'Andriy', '', '', 'KlesK', '10/06/1989', 'M', 1, '', 'N', 'N', 'Lysenka st. 6', 'Kolomyya', 'Ukraine', '78200', 'Ukraine', '0952059023', 'Y', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-15 16:07:10', 'event', 'gomer', '102', ''),
(9, 1, 6, NULL, 'N', NULL, 'agfname1', 'agmiddle_name1', '', '', '06/07/1989', NULL, 1, '', 'N', 'N', 'agaddress1', 'agsuburb1', 'agstate1', 'agpostcode1', 'agcountry1', '1001', 'N', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 13:28:47', 'agevent1', 'agcontact1', '1002', 'agtitle1'),
(10, 1, 6, NULL, 'N', NULL, 'agfname2', '', 'aglastname2', '', '06/08/1989', NULL, 1, '', 'Y', 'N', 'agaddress2', 'agsuburb2', 'agstate2', 'agpostcode2', 'agcountry2', '2001', 'N', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 17:19:45', 'agevent2', 'agcontact2', '2002', 'agtitle2'),
(11, 1, 7, NULL, 'Y', NULL, 'Andriy', '', '', '', '01/01/1970', 'M', 1, '', 'N', 'N', 'Lysenka st. 6', 'Kolomyya', 'Ukraine', '78200', 'grdgrd', '0952059023', '', '', '', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 14:41:03', 'htfhtf', 'htfh', '2001', ''),
(13, 1, 9, NULL, 'Y', NULL, 'Andriy', '', '', 'KlesK', '10/06/1989', 'M', 1, '', 'N', 'N', 'Lysenka st. 6', 'Kolomyya', 'Ukraine', '78200', 'Ukraine', '0952059023', 'Y', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 16:56:27', 'event', 'gomer', '102', ''),
(14, 1, 9, NULL, 'N', NULL, 'agfname1', 'agmiddle_name1', '', '', '06/07/1989', NULL, 1, '', 'N', 'N', 'agaddress1', 'agsuburb1', 'agstate1', 'agpostcode1', 'agcountry1', '1001', 'N', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 18:12:42', 'agevent1', 'agcontact1', '1002', 'agtitle1'),
(15, 1, 9, NULL, 'N', NULL, 'agfname2', '', 'aglastname2', '', '06/08/1989', NULL, 1, '', 'Y', 'N', 'agaddress2', 'agsuburb2', 'agstate2', 'agpostcode2', 'agcountry2', '2001', 'N', '', 'N', '', 'None', '0000-00-00', 'N', NULL, NULL, NULL, '2016-01-16 18:12:42', 'agevent2', 'agcontact2', '2002', 'agtitle2');

-- --------------------------------------------------------

--
-- Структура таблиці `how_hear`
--

CREATE TABLE IF NOT EXISTS `how_hear` (
  `how_hear_id` int(11) NOT NULL,
  `name` varchar(400) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` decimal(10,0) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `how_hear`
--

INSERT INTO `how_hear` (`how_hear_id`, `name`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'Australian Outback Marathon Website', 'Y', '0', 'N'),
(2, 'Travelling Fit Website', 'Y', '1', 'N'),
(3, 'Travelling Fit Newsletter', 'Y', '3', 'N'),
(4, 'International Travel Agent', 'Y', '4', 'N'),
(5, 'Existing Client', 'Y', '5', 'N'),
(6, 'Word of Mouth/Referral', 'Y', '6', 'N'),
(7, 'Advertising', 'Y', '7', 'N'),
(8, 'Google', 'Y', '8', 'N'),
(9, 'Other Marathon Event', 'Y', '9', 'N'),
(10, 'Other', 'Y', '10', 'N');

-- --------------------------------------------------------

--
-- Структура таблиці `optional_tours`
--

CREATE TABLE IF NOT EXISTS `optional_tours` (
  `optional_tour_id` int(11) NOT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `tour_date` date DEFAULT NULL COMMENT 'see if guest is 16 or older',
  `cost` decimal(10,0) DEFAULT NULL COMMENT 'Note: Cost''s will need to be copied across to booking section as they may change here.',
  `cost_extras` decimal(10,0) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `brief_description` varchar(50) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N',
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `optional_tours`
--

INSERT INTO `optional_tours` (`optional_tour_id`, `title`, `tour_date`, `cost`, `cost_extras`, `description`, `brief_description`, `available_YN`, `sort_order`, `archived_YN`, `event_id`) VALUES
(1, 'Sat 25th July - Uluru Sunset Coach Tour (+$26 park pass if required)', '2015-07-25', '55', '26', 'Depart from your hotel at approx 5:20pm to the Uluru sunset viewing area. Enjoy complimentary canapes and sparkling wine as the sun sets over the western horizon. Returns to your hotel at approximately 19:20h.', '25th July Sunset Coach', 'Y', 0, 'N', 1),
(2, 'Sat 25th July - Sunset Camel Ride', '2015-07-25', '129', '0', 'Begin your peaceful 1 hour camel ride over the big red sand dunes at sunset. A refreshing glass of sparkling wine or beer and tasty snacks wait at the end of the trail.', '25th July Sunset Camel', 'Y', 1, 'N', 1),
(3, 'Sat 25th July - Helicopter Flight - Uluru - 15 Min flight', '2015-07-25', '130', '0', 'Bookings Essential - You won''t fully appreciate the awesome spectacle of Uluru until you see it from the air.', '25th July 15 min Helicopter', 'Y', 2, 'N', 2),
(4, 'Sat 25th July - Helicopter Flight - Uluru and Kata Tjuta - 30 Min flight', '2015-07-25', '245', '0', 'Bookings Essential -  Showcasing Uluru and the 36 conglomerate rock domes of Kata Tjuta', '25th July 30 min Helicopter', 'Y', 3, 'N', 3),
(5, 'Sat 25th July - Helicopter Flight - Grand Tour (recommended) - 36 Min flight', '2015-07-25', '280', '0', 'Bookings Essential -  Showcasing Uluru and the 36 conglomerate rock domes of Kata Tjuta  including the Western Side and Walpa Gorge - Highly recommended.', '25th July 36 min Helicopter', 'Y', 4, 'N', 4),
(6, 'optional tour ', NULL, NULL, NULL, 'diawhdoiawh ihawdoihwaiohdawh oiahwdaw\nfefseoihfoiusehfiohseiofhseifhseio sehfise hofhesoi', NULL, 'Y', NULL, 'N', 4),
(8, 'gdghdrgdrg', NULL, NULL, NULL, 'gdrgdrgdrgdrgdrgdrgdr', NULL, 'Y', NULL, 'N', 4),
(9, 'fsrgdhft tfh fthft fhesohfiousehoifhseoihfes2143214234', NULL, NULL, NULL, 'h tfhtfh fthft ht', NULL, 'Y', NULL, 'N', 5),
(10, 'btdgd rdjkg podrjpogjodr ', NULL, NULL, NULL, 'gdrg dr[gk[dr gjdrp gj[drjg [dr', NULL, 'Y', NULL, 'N', 5);

-- --------------------------------------------------------

--
-- Структура таблиці `optional_tour_package`
--

CREATE TABLE IF NOT EXISTS `optional_tour_package` (
  `optional_tour_package_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `optional_tour_id` int(11) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `optional_tour_package`
--

INSERT INTO `optional_tour_package` (`optional_tour_package_id`, `package_id`, `optional_tour_id`, `event_id`, `booking_id`, `user_id`) VALUES
(1, 1, 1, 0, 0, 0),
(2, 2, 2, 0, 0, 0),
(3, 3, 3, 0, 0, 0),
(4, 4, 4, 0, 0, 0),
(5, 5, 5, 0, 0, 0),
(27, 1, 1, 1, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `package_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL COMMENT 'full package name as displayed on form',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `available` varchar(20) DEFAULT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `packages`
--

INSERT INTO `packages` (`package_id`, `event_id`, `name`, `start_date`, `end_date`, `available`, `description`, `display_order`) VALUES
(1, 1, 'Platinum Package - 7 Days / 6 Nights', '2016-07-28', '2016-08-04', 'Y', 'WOW!', 0),
(2, 2, 'Platinum Package - 6 Days / 5 Nights', '2016-07-28', '2016-08-03', 'Y', '2WOW!', 0),
(3, 3, 'Platinum Package - 10 Days / 9 Nights', '2016-07-28', '2016-08-06', 'Y', '3WOW!', 0),
(4, 4, 'Platinum Package - 6 Days / 5 Nights', '2016-08-28', '2016-09-04', 'Y', '4WOW!', 0),
(5, 5, 'Platinum Package - 6 Days / 5 Nights', '2016-09-28', '2016-10-04', 'Y', '5WOW!', 0),
(7, 1, 'update test 2398046 test', '2008-02-13', '2015-12-17', 'Y', 'update test description ', 0),
(10, 5, 'event 5 ddoaw jpo', '2010-12-12', '2014-04-06', 'y', 'djpaeowjfpoawjpo fjapojfpo ajwpfdjawp\nawkfpawjpfowa', 0),
(11, 4, 'event 4 package 2', '2015-12-25', '2015-12-30', 'y', 'doapodjawodj poawjdpojaw jdawjd iajid', 0),
(13, 5, 'dawfawf', '2010-12-23', '2011-03-04', 'y', 'dwadwafaw', 0),
(16, 3, 'dawpodoiwa jp', '2010-11-23', '2011-12-12', 'y', 'jpo powhd ipoawhd[o ahdoaw', 0),
(19, 2, 'ofpsepojsefposej opp 12834612', '2011-11-11', '2012-12-12', 'Y', 'fpjespfposepofj po', 0),
(21, 2, 'sgseg', '2010-10-10', '2011-11-11', 'Y', 'segesgdrfhtf', 0),
(23, 4, 'daw[fk[f', '2002-03-03', '2004-04-04', 'y', 'kf[sekf[pksefkp[', 0),
(24, 5, 'event 5 package new update', '2015-12-28', '2015-12-29', 'Y', 'new package', 0),
(25, 5, 'grdsghrdhrd dhdr hrd ', '2015-12-29', '2015-12-30', 'Y', 'fgsegsegesg', 0),
(26, 3, 'fesg', '2015-12-15', '2015-12-23', 'Y', 'gsegsef', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `room_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Y',
  `hotel_name` varchar(255) NOT NULL,
  `event_id` int(11) NOT NULL,
  `optional_tour` enum('Y','N') NOT NULL DEFAULT 'N',
  `package_id` int(11) DEFAULT NULL,
  `optional_tour_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `room_type`
--

INSERT INTO `room_type` (`room_type_id`, `name`, `price`, `capacity`, `status`, `hotel_name`, `event_id`, `optional_tour`, `package_id`, `optional_tour_id`) VALUES
(20, 'room 12312412', 30, 5, 'Y', 'hotel', 1, 'N', 1, NULL),
(21, 'room test2', 50, 1, 'Y', 'hotel', 1, 'N', 7, NULL),
(25, 'room2 updated', 700, 2, 'Y', '', 1, 'Y', NULL, NULL),
(26, 'room3 updated', 1000, 3, 'Y', '', 1, 'Y', NULL, NULL),
(27, 'room4 ', 1200, 2, 'Y', '', 1, 'Y', NULL, NULL),
(28, 'room1', 1000, 2, 'Y', '', 2, 'Y', NULL, NULL),
(30, 'room 1', 1000, 2, 'Y', '', 3, 'Y', NULL, NULL),
(31, 'room2', 1500, 3, 'Y', '', 2, 'Y', NULL, NULL),
(32, 'room1', 1000, 4, 'Y', 'hotel_1', 5, 'N', 10, NULL),
(34, 'gdrgrdh', 1000, 2, 'Y', 'dafsegsergg', 1, 'N', 1, NULL),
(35, 'grhdrh', 2000, 4, 'Y', 'hftjtfjtfjtfj', 1, 'N', 7, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `access_level` int(11) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`user_id`, `access_level`, `username`, `firstname`, `lastname`, `password`) VALUES
(1, 1, 'admin', 'Michael', 'Walton', 'zen#2014');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `accommodation`
--
ALTER TABLE `accommodation`
  ADD PRIMARY KEY (`accommodation_id`);

--
-- Індекси таблиці `accommodation_package`
--
ALTER TABLE `accommodation_package`
  ADD PRIMARY KEY (`package_accommodation_id`), ADD KEY `package_id` (`package_id`), ADD KEY `accommodation_id` (`accommodation_id`);

--
-- Індекси таблиці `bedding`
--
ALTER TABLE `bedding`
  ADD PRIMARY KEY (`bedding_id`);

--
-- Індекси таблиці `bedding_configuration`
--
ALTER TABLE `bedding_configuration`
  ADD PRIMARY KEY (`bedding_configuration_id`), ADD KEY `accommodation_id` (`accommodation_id`), ADD KEY `min_people` (`min_people`);

--
-- Індекси таблиці `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`), ADD KEY `bedding_configuration_id` (`bedding_configuration_id`), ADD KEY `package_id` (`package_id`), ADD KEY `how_hear_id` (`how_hear_id`);

--
-- Індекси таблиці `costs`
--
ALTER TABLE `costs`
  ADD PRIMARY KEY (`cost_id`), ADD KEY `parent_id` (`parent_id`);

--
-- Індекси таблиці `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`user_id`), ADD KEY `booking_id` (`booking_id`);

--
-- Індекси таблиці `how_hear`
--
ALTER TABLE `how_hear`
  ADD PRIMARY KEY (`how_hear_id`);

--
-- Індекси таблиці `optional_tours`
--
ALTER TABLE `optional_tours`
  ADD PRIMARY KEY (`optional_tour_id`);

--
-- Індекси таблиці `optional_tour_package`
--
ALTER TABLE `optional_tour_package`
  ADD PRIMARY KEY (`optional_tour_package_id`);

--
-- Індекси таблиці `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`), ADD KEY `event_id` (`event_id`);

--
-- Індекси таблиці `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`room_type_id`), ADD KEY `package_id` (`package_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `accommodation`
--
ALTER TABLE `accommodation`
  MODIFY `accommodation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `accommodation_package`
--
ALTER TABLE `accommodation_package`
  MODIFY `package_accommodation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблиці `bedding`
--
ALTER TABLE `bedding`
  MODIFY `bedding_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `bedding_configuration`
--
ALTER TABLE `bedding_configuration`
  MODIFY `bedding_configuration_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблиці `costs`
--
ALTER TABLE `costs`
  MODIFY `cost_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `guests`
--
ALTER TABLE `guests`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблиці `how_hear`
--
ALTER TABLE `how_hear`
  MODIFY `how_hear_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `optional_tours`
--
ALTER TABLE `optional_tours`
  MODIFY `optional_tour_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `optional_tour_package`
--
ALTER TABLE `optional_tour_package`
  MODIFY `optional_tour_package_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблиці `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблиці `room_type`
--
ALTER TABLE `room_type`
  MODIFY `room_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
