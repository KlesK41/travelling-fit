-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Січ 14 2016 р., 15:33
-- Версія сервера: 10.1.9-MariaDB
-- Версія PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `oldtemp2`
--

-- --------------------------------------------------------

--
-- Структура таблиці `bedding`
--

CREATE TABLE `bedding` (
  `bedding_id` int(11) NOT NULL,
  `bedding_name` varchar(100) DEFAULT NULL,
  `max_people` int(11) DEFAULT NULL,
  `available_YN` char(1) DEFAULT 'Y',
  `sort_order` int(11) DEFAULT NULL,
  `archived_YN` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bedding`
--

INSERT INTO `bedding` (`bedding_id`, `bedding_name`, `max_people`, `available_YN`, `sort_order`, `archived_YN`) VALUES
(1, 'SINGLE', 1, 'Y', 0, 'N'),
(2, 'DOUBLE', 2, 'Y', 1, 'N'),
(3, 'TRIPLE', 3, 'Y', 2, 'N'),
(4, 'QUAD', 4, 'Y', 3, 'N'),
(5, 'FIVE', 5, 'Y', 5, 'N'),
(6, 'SIX', 6, 'Y', 6, 'N'),
(7, 'NOT REQUIRED', 7, 'Y', 7, 'N');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `bedding`
--
ALTER TABLE `bedding`
  ADD PRIMARY KEY (`bedding_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `bedding`
--
ALTER TABLE `bedding`
  MODIFY `bedding_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
