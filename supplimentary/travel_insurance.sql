-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Січ 14 2016 р., 16:11
-- Версія сервера: 10.1.9-MariaDB
-- Версія PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `oldtemp2`
--

-- --------------------------------------------------------

--
-- Структура таблиці `travel_insurance`
--

CREATE TABLE `travel_insurance` (
  `travel_insurance` int(11) NOT NULL,
  `travel_insurance_name` varchar(4000) DEFAULT NULL,
  `brief_description` varchar(25) NOT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `travel_insurance`
--

INSERT INTO `travel_insurance` (`travel_insurance`, `travel_insurance_name`, `brief_description`, `order`) VALUES
(1, 'I will take out travel insurance online via the Travelling Fit website and earn 25% discount - <a href="http://www.suresave.net.au/home.php?affid=2971" target=_"blank">Click here to book your insurance</a>', 'Online via Website', 1),
(2, 'I would like to take out Travel Insurance but would prefer Travelling Fit to issue my policy at the retail price. ', 'Will Contact Office', 2),
(3, 'I do not wish to take out travel insurance through Travelling Fit - Warning: please refer clause 13.2 & 13.3 of the Booking Terms & Conditions before accepting this option', 'No Thanks', 3);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `travel_insurance`
--
ALTER TABLE `travel_insurance`
  ADD PRIMARY KEY (`travel_insurance`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `travel_insurance`
--
ALTER TABLE `travel_insurance`
  MODIFY `travel_insurance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
