<?php
/***********************************************************
* definitions.php
* 
* Defines constants to be used through the app.
* 
*
************************************************************/

//define base path
defined('BASEPATH')                         ? null : define ('BASEPATH',    'http://67.225.210.86/~travellingfit');

// define access levels for admin and general users
defined('ADMIN') 							? null : define ('ADMIN', 		'16');
defined('GENERAL') 							? null : define ('GENERAL', 	 '2');

// database type - MySQL or Oracle
defined('DATABASE_TYPE')					? null : define ('DATABASE_TYPE', 'MySQLDatabase'); 

// prevent direct access - if this const has no value in a child web page, then direct access has been attempted
defined('DIRECT_ACCESS_PREVENT') ? null : define ('DIRECT_ACCESS_PREVENT', true);

defined('DEPOSIT_AMOUNT') ? null : define ('DEPOSIT_AMOUNT', '0');


// directory separator
defined('DS') 					? null : define ('DS', DIRECTORY_SEPARATOR);

// set a variable to determine if we're on a DEVELOPMENT server
switch (preg_replace("/:.*$/", "", strtolower($_SERVER['HTTP_HOST']))) 
{
	// PHP5 dev servers
	case 'app-ps.griffith.edu.au':
	case 'app-ps.secure.griffith.edu.au':
	case 'app-stage.griffith.edu.au':
	case 'app-stage.devsecure.griffith.edu.au':
	case 'app-dev.griffith.edu.au':
	case 'app-dev.devsecure.griffith.edu.au':
	case 'tempest-0.itc.griffith.edu.au':
	case 'tempest-1.itc.griffith.edu.au':
	case 'tempest-2.itc.griffith.edu.au':
	case 'poc-app-dev.devsecure.griffith.edu.au':
	case 'poc-app-test.secure.griffith.edu.au':
	case 'localhost':
	case '132.234.148.140':
		defined('IS_DEVELOPMENT_SERVER')	? null : define ('IS_DEVELOPMENT_SERVER',			TRUE);
	break;

	// PHP5 production servers
	case 'app.griffith.edu.au':
	case 'app.secure.griffith.edu.au':
	default:
		defined('IS_DEVELOPMENT_SERVER')	? null : define ('IS_DEVELOPMENT_SERVER',			FALSE);
	break;
}
?>