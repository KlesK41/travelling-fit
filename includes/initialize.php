<?php
// ini_set('display_errors', 'strerr');
/***********************************************************
* initialise.php
* 
* Loads necessary prerequisite objects, libraries, etc
* 
*
************************************************************/


// contains constant variables, such as user access levels and database type
require_once("definitions.php");


// define end of line termination by operating system - Windows or Mac or Linux
if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
  $eol="\r\n";
} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) {
  $eol="\r";
} else {
  $eol="\n";
}

defined('EOL') ? null : define ('EOL', $eol);

date_default_timezone_set("Australia/Queensland"); 

// load database connection credentials
require_once(dirname(__FILE__) . "/config.php");

// load core objects
// require_once("SessionHandlerInterface.php");
require_once("SessionHandler.php");  // custom session handler, also starts the session and regnerates a new session id every 30 mins
require_once("session.php");
require_once("database.php");
require_once("databaseobject.php");

// load basic functions next so that everything after can use them
require_once("functions.php");


// include all value objects
foreach(glob("../model/VO/*.php") as $filename) require_once $filename;		

// include business objects
foreach(glob("../model/business/*.php") as $filename) require_once $filename;		



// force login for local development
switch(preg_replace("/:.*$/", "", strtolower($_SERVER['HTTP_HOST'])))
{
	case 'localhost':
		$_SESSION['generic_userdata']['userid'] 	= 1;
		$_SESSION['generic_access_level'] 			= ADMIN;
	break;
}

// turn off all error reporting
error_reporting(-1);


// Allow people want test this app and not be displayed the no bookings message:
$_SESSION['tester'] = "false";

if(isset($_GET['s']) && $_GET['s'] == '99')
{
	$_SESSION['tester'] = "true"; 
}


?>