<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Login extends DatabaseObject{
	
	protected static $table_name="login";
	public $login_id;
	public $user_id;
	public $job_id;
	
	// Common Database Methods
	public static function find_all() {
		return self::find_by_sql("SELECT * FROM login");
  }
  
  public static function find_by_id($id=0) {
    $result_array = self::find_by_sql("SELECT * FROM login WHERE login_id = '{$id}' LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
  }
	
	public static function populate($postArray)
	{
		$object = new self;
		foreach ($postArray as $attribute=>$value){
			$object->$attribute = stripslashes($value);
		}
		return $object;
	}
	
  public static function find_by_sql($sql="") {
    global $database;
    $result_set = $database->query($sql);
    $object_array = array();
    while ($row = $database->fetch_array($result_set)) {
      $object_array[] = self::instantiate($row);
    }
    return $object_array;
  }

	private static function instantiate($record) {
		// Could check that $record exists and is an array
    $object = new self;
		// Simple, long-form approach:
		// $object->id 				= $record['id'];
		// $object->username 	= $record['username'];
		// $object->password 	= $record['password'];
		// $object->first_name = $record['first_name'];
		// $object->last_name 	= $record['last_name'];
		
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  if($object->has_attribute($attribute)) {
		    $object->$attribute = stripslashes($value);
		  }
		}
		return $object;
	}
	
	private function has_attribute($attribute) {
	  // get_object_vars returns an associative array with all attributes 
	  // (incl. private ones!) as the keys and their current values as the value
	  $object_vars = get_object_vars($this);
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $object_vars);
	}
	
	public function create() {
		global $database; // instance of database object
		
		$sql  = "INSERT INTO login (";
		$sql .= "user_id, job_id";
		$sql .= ") VALUES ('";
		$sql .= $database->escape_value($this->user_id) . "', '";
		$sql .= $database->escape_value($this->job_id) . "')"; // make active by default
		
		if($database->query($sql)) {
			$this->login_id = $database->insert_id();
		} else {
			return false; 
		}
	}
	
	public function update() {
		global $database; // instance of database object
		$sql  = "UPDATE login SET ";
		$sql .= "user_id=". $database->escape_value($this->user_id) . ", ";
		$sql .= "job_id='". $database->escape_value($this->job_id) . "'";
		$sql .= " WHERE login_id=". $database->escape_value($this->login_id);
		
		$database->query($sql);
		return ($database->affectedRows() == 1) ? true : false;
		
	}
	
	public function delete() {
		// $sql = "delete from login where login_id =" . $database->escape_value($this->login_id);
	}
	
	
}

?>