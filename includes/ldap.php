<?php

	function is_pass_ldap($txt_uid, $txt_pass) {
	
		// Setup the Variables
		$pass 		= 	1;
		$fail 		= 	0;
		$noconnect 	= 	2;
		$server 	= 	"ldap-auth.griffith.edu.au";
		$port 		= 	389;

		// basic sequence with LDAP is connect, bind, search, interpret search
		// result, close connection
		//echo "validating access ... <BR>";
		$ds = ldap_connect($server,$port);	// must be a valid LDAP server!
		
		if ($ds) {
				$r=ldap_bind($ds);		 // this is an "anonymous" bind, typically
										 // read-only access
				//$sr=ldap_search($ds,"o=poseidon.itc.gu.edu.au", 'cn=' .'*'. $user .'*' );
				//$sr=ldap_search($ds,"o=poseidon.itc.gu.edu.au", 'uid=' . $user	);
				$sr=ldap_search($ds,"", 'uid=' . $txt_uid  );
				$info = ldap_get_entries($ds, $sr);
				$dn = $info[0]["dn"];
		
			if(!$txt_pass) {
				// generate a bogus password to pass if the user doesn't give us one
				// this gets around systems that are anonymous search enabled
				$pass = crypt(microtime());
			}
		
			$r=@ldap_bind($ds,$dn,$txt_pass);  //bind with auth
			//echo "Bind result is ".$r."<p>";
			//echo "Number of entires returned is ".ldap_count_entries($ds,$sr)."<p>";
			$entries = ldap_count_entries($ds,$sr);
		
			if($r==1 and $entries==1) { //user is valid
				$result = $pass;
			} else {
				//echo "wrong user and password combination! <BR>";
				//echo '<a href="index.php">try again</a> <BR>';
				$result = $fail;
			}
		
			ldap_close($ds);
		
		} else {
			//echo "<h4>Unable to connect to Authentication server to verify your account details </h4><BR>";
			//echo '<a href="index.php">try again</a> <BR>';
			$result = $noconnect;
		}
		
		return $result;
		
	} // End of the Is_Pass_Ldap Function

	// -------------------------------------------------------------------		
		
	function refresh_left($page){
		
		echo "<script language='JavaScript'\n>";
		echo "<!--\n";
		echo "self.parent.nav.location = '" . $page . "'\n";
		echo " //-->\n";
		echo "</script>\n";
		
	}  // End of Refresh_Left Function
		
	// -------------------------------------------------------------------
		
	function refresh_main(){
		
		//echo "login successful <BR>";
		//echo "generating workspace screen for staff member $txt_uid  <BR>";
		echo '<script LANGUAGE="JavaScript">
		window.location.replace("menu_history.php")
		</script>';
		
	} // End of Refresh_Main Function

	// -------------------------------------------------------------------
		
	function load_bad_login(){
		
		//echo "bad login <BR>";
		echo '<script LANGUAGE="JavaScript">
		window.location.replace("bad_login.php")
		</script>';
		
	} // End of Load_Bad_Login

	// -------------------------------------------------------------------
?>