<?php
/**
 *	SSO
 *
 *	Check if the user is signed in. If not, perfom a SSO request.
 */
if (preg_replace("/:.*$/", "", strtolower($_SERVER['HTTP_HOST'])) != 'localhost') 
{
	// URL to current page
	$appURL = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$appName = 'Hitbase Portal';
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/../security key/.pingsinglesignon.php');
	list($ok,$result) = singleSignOn(1,TRUE);
	
	if ($result <> '-1' and $ok !== FALSE) {
		$_SESSION['sso']=true;
		$_POST['username'] = strtolower($result['userid']);

	} else {

		singleSignonRedirect(1,$appURL,$appName);

		exit; 
	} 
	
} else { 
	/**
	 *	we're in test so can't use sso, so fabricate something like what it returns for testing ...
	 *	Some business test students:
	 *
	 *	s282475		Scott	Douglas
	 *	s1141119	Ivan	Simic
	 *	s1287602	Michael	Spiller
	 *	s58184		Judith	Spence
	 *	s1048296	Carl 	Schwede (in 1001ICT)
	 */
	$result = array (
		"userid" => "s2594557", 
		"name" => "Stuart Bond" ,
		"emplid" => "s2594557",
		"email" => "summantha.radcliffe@griffithuni.edu.au",
		"roles" => array ( 
			0 => "WorkingOnLocalhost",
			1 => "GU_STUDENT",
			2 => "Griffith User",
			3 => "PAPP_USER"
		)
	);
}
?>
