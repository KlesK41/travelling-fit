<?php
function dump($var)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function start_session()
{
	$handler = new AOMSessionHandler();
	session_set_save_handler($handler, true);
	if(!session_id()) session_start();
}

function check_logged_in()
{
	start_session();
	if(!isset($_SESSION['generic_is_admin']))
	{
		$location = "Location: index.php?v=login&i=1";
	    ob_start();
	    header($location);
    	ob_flush();
		die();
	}

}

function credit_card_type($CardNo)
{
	/*
	'*CARD TYPES            *PREFIX           *WIDTH
	'American Express       34, 37            15
	'Diners Club            300 to 305, 36    14
	'Carte Blanche          38                14
	'Discover               6011              16
	'EnRoute                2014, 2149        15
	'JCB                    3                 16
	'JCB                    2131, 1800        15
	'Master Card            51 to 55          16
	'Visa                   4                 13, 16
	*/    
	//Just in case nothing is found
	$CreditCardType = "Unknown";

	//Remove all spaces and dashes from the passed string
	$CardNo = str_replace("-", "",str_replace(" ", "",$CardNo));

	//Check that the minimum length of the string isn't less
	//than fourteen characters and -is- numeric
	If(strlen($CardNo) < 14 || !is_numeric($CardNo))
	    return false;

	//Check the first two digits first
	switch(substr($CardNo,0, 2))
	{
	    Case 34: Case 37:
	        $CreditCardType = "American Express";
	        break;
	    Case 36:
	        $CreditCardType = "Diners Club";
	        break;
	    Case 38:
	        $CreditCardType = "Carte Blanche";
	        break;
	    Case 51: Case 52: Case 53: Case 54: Case 55:
	        $CreditCardType = "Master Card";
	        break;
	}

	//None of the above - so check the
	if($CreditCardType == "Unknown")
	{
	    //first four digits collectively
	    switch(substr($CardNo,0, 4))
	    {
	        Case 2014:Case 2149:
	            $CreditCardType = "EnRoute";
	            break;
	        Case 2131:Case  1800:
	            $CreditCardType = "JCB";
	            break;
	        Case 6011:
	            $CreditCardType = "Discover";
	            break;
	    }
	}

	//None of the above - so check the
	if($CreditCardType == "Unknown")
	{
	    //first three digits collectively
	    if(substr($CardNo,0, 3) >= 300 && substr($CardNo,0, 3) <= 305)
	    {
	        $CreditCardType = "American Diners Club";
	    }
	}

	//None of the above -
	if($CreditCardType == "Unknown")
	{
	    //So simply check the first digit
	    switch(substr($CardNo,0, 1))
	    {
	        Case 3:
	            $CreditCardType = "JCB";
	            break;
	        Case 4:
	            $CreditCardType = "Visa";
	            break;
	    }
	}

	return $CreditCardType;
}

class functions {

public static function escapeValue( $value ) {
	$magicQuotesActive = get_magic_quotes_gpc();
	$realEscapeStringExists = function_exists( "mysql_real_escape_string" );

		if( $realEscapeStringExists ) { // PHP v4.3.0 or higher
		// undo any magic quote effects so mysql_real_escape_string can do the work
		if( $magicQuotesActive ) { $value = stripslashes( $value ); }
		// *** find an oracle replacement for the below...
		//$value = mysql_real_escape_string( $value );
	} else { // before PHP v4.3.0
		// if magic quotes aren't already on then add slashes manually
		if( !$magicQuotesActive ) { $value = addslashes( $value ); }
		// if magic quotes are active, then the slashes already exist
	}
	return $value;
}

public static function calculateAge($birthday){
   
	 // need to calculate age at time of event
	 $delta = strtotime(DATE_OF_EVENT) - strtotime(date('j F Y'));

	 if ($delta > 0)
    return floor((time() + $delta - strtotime($birthday))/31556926);
   else
	 	return floor((time() - strtotime($birthday))/31556926);
}

public static function strip_zeros_from_date( $marked_string="" ) {
  // first remove the marked zeros
  $no_zeros = str_replace('*0', '', $marked_string);
  // then remove any remaining marks
  $cleaned_string = str_replace('*', '', $no_zeros);
  return $cleaned_string;
}

public static function formatDateForMySQL ($date) {

	$e = explode('/', $date);
	
	return "{$e[2]}/{$e[1]}/{$e[0]}";

}

/**
 *  Display a drop down box
 *
 *  @param  object data, str name, str value, str display, str display2, str display3
 *  @return  str
 */

public static function addSelectList($data, $firstline='', $name,  $value, $display, $display2='', $display3='')
{
	$selectList = '<select name="'. $name .'" id="' .$name .'">';
	$firstline != '' ? $selectList .= '<option value="">' . $firstline . '</option>' : $firstline .= '';
	for ($i=0; $i<count($data); $i++)
	{
		$text = $data[$i]->$display;
		$display2 != '' ? $text .= ' - ' . $data[$i]->$display2 : $text .= '';
		$display3 != '' ? $text .= ' - ' . $data[$i]->$display3 : $text .= '';
		$selectList .= '<option value="' . $data[$i]->$value. '">' . substr($text,0,64) .'</option>';
	}
	$selectList .= '</select>';
	
	return $selectList;
}

/**
 *  Display a multiple select list
 *
 *  @param  object data, str name, str value, str display, str display2, str display3
 *  @return  str
 */

public static function addMultipleSelectList($data, $firstline='', $name,  $value, $display, $display2='', $display3='')
{
	$selectList = '<select name="'. $name .'[]" id="' .$name .' multiple="multiple">';
	$firstline != '' ? $selectList .= '<option value="">' . $firstline . '</option>' : $firstline .= '';
	for ($i=0; $i<count($data); $i++)
	{
		$text = $data[$i]->$display;
		$display2 != '' ? $text .= ' - ' . $data[$i]->$display2 : $text .= '';
		$display3 != '' ? $text .= ' - ' . $data[$i]->$display3 : $text .= '';
		$selectList .= '<option value="' . $data[$i]->$value. '">' . substr($text,0,64) .'</option>';
	}
	$selectList .= '</select>';
	
	return $selectList;
}

public static function getIpAddress() {

    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;

}

public static function convertToJSON($result)
{
	$json = "{";
	foreach($result as $attribute => $value)
		$json .= "{$attribute}: '{$value}', ";
	$json = substr($json, 0, strlen($json) - 2);
	$json .= "}";

	return $json;
}

public static function formatDateForHTML ($date) {

	$e = explode('-', $date);
	
	return "{$e[2]}/{$e[1]}/{$e[0]}";

} 

// Description: adds days to given date and returns a date
// Paramerters: days, date
// Preconditions: --
// Postcondition: returns a date

public static function addDays($days, $date)
{
    $date = date('Y-m-d', strtotime('+' . $days . ' days', strtotime($date)));
    return $date;
} 

#
/**
 * Calculating the difference between two dates
 * @author: Elliott White
 * @author: Jonathan D Eisenhamer.
 * @link: http://www.quepublishing.com/articles/article.asp?p=664657&rl=1
 * @since: Dec 1, 2006.
 */
// Will return the number of days between the two dates passed in
public static function countDays( $a, $b )
{
	// First we need to break these dates into their constituent parts:
	$gd_a = getdate( $a );
	$gd_b = getdate( $b );
	
	// Now recreate these timestamps, based upon noon on each day
	// The specific time doesn't matter but it must be the same each day
	$a_new = mktime( 12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year'] );
	$b_new = mktime( 12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year'] );
	
	// Subtract these two numbers and divide by the number of seconds in a
	// day. Round the result since crossing over a daylight savings time
	// barrier will cause this time to be off by an hour or two.
	return round( abs( $a_new - $b_new ) / 86400 );
}

// Description: checks whether a date is valid or not
// Paramerters: day, month, year
// Preconditions: --
// Postcondition: return true if valid, false if not


public static function dateCheck($day, $month, $year)
{
    if ($day!="" && $month!="" && $year!="")
    {
    if (is_numeric($year) && is_numeric($month) && is_numeric($day))
        {
            return checkdate($month,$day,$year);
        }
    }  
    return false;

}

public static function redirectTo( $location = NULL ) {
  if ($location != NULL) {
    header("Location:{$location}");
		echo "redirect not working...";
    exit;
  }
}

// create the option components of the select control with a selected value of $selectedValue
public static function workshopComboBox($text, $selectedValue, $registrant_id, $session, $preference_number) {

	echo "<select name='workshop_{$registrant_id}_{$preference_number}_{$session}' id='workshop_{$registrant_id}_{$preference_number}_{$session}' class='required'>";

	for ($i = 0; $i < count($text); $i++)
	{
		echo "<option";
		if ($text[$i] != "") echo " value='FSLIBBED@{$registrant_id}_{$preference_number}_{$session}_{$text[$i]}'";
		if ($selectedValue == $text[$i]) echo " selected='selected'";
		echo "'>" . $text[$i] . "</option>";
	}
	echo "</select>";
}


public static function checked($value)
{
	if ($value=='1')
		echo "checked";
}

public static function outputMessage($message="") {
  if (!empty($message)) { 
    return "<p class=\"message\">{$message}</p>";
  } else {
    return "";
  }
}

public static function dmyDate($date)
{
	$arr = explode("-",$date);
	
	return "{$arr[2]}/{$arr[1]}/{$arr[0]}";
	
}

public static function includeLayoutTemplate($template="")
{
	include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}


// create email
public static function prepareEmail() {
	ob_start();
	?>
	<html>
	<head></head>
  <body>
  <?php
	// echo body
	?>
  </body>
  </html>
  <?php
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
}

?>