<?php

class Template
{
    private static $template = '';


    // Packages start
    public static function renderPackagesList($packages) {
        if (!empty($packages)) {
            self::clearTemplate();
            foreach ($packages as $package) {
                self::templateForPackage($package);
            }
        }
    }
    public static function renderAddedPackage($package, $event = null) {
        self::clearTemplate();
        self::templateForPackage($package, $event);
    }
    private static function templateForPackage($package, $event = null) {
        if ($event !== 'update' ) {
            self::$template .= '<tr role="row" class="odd" id="package-tr-' . $package->package_id . '">';
        }
        self::$template .= '<td class="">' . $package->name . '</td>';
        self::$template .= '<td>' . $package->package_id . '</td>';
        self::$template .= '<td class="center">' . ucfirst($package->available) . '</td>';
        self::$template .= '<td><div class="form-actions"><button type="button" class="btn default" onclick="editPackage(' . $package->package_id . ')"> Edit </button>';
        self::$template .= '<button type="button" class="btn default" onclick="deletePackage(' . $package->package_id . ')"> Delete </button></div></td>';
        if ($event !== 'update' ) {
            self::$template .= '</tr>';
        }
    }
    // Packages end

    // Room types start
    public static function renderAddedRoomType($roomType, $event = null) {
        self::clearTemplate();
        self::templateForRoomType($roomType, $event);
    }
    public static function renderRoomTypesList($roomTypes, $optional_tour = 'N') {
        self::clearTemplate();
        if (!empty($roomTypes)) {
            foreach ($roomTypes as $roomType) {
                if ($roomType->optional_tour == $optional_tour) {
                    self::templateForRoomType($roomType);
                }
            }
        }
    }
    private static function templateForRoomType($roomType, $event = null) {
        if ($event !== 'update') {
            self::$template .= '<tr role="row" class="even" id="room-type-tr-' . $roomType->room_type_id . '" package_id="' . $roomType->package_id . '">';
        }
        self::$template .= '<td class="">' . $roomType->name . '</td>';
        self::$template .= '<td class="">' . $roomType->hotel_name . '</td>';
        self::$template .= '<td>' . $roomType->price . '</td>';
        self::$template .= '<td class="center">' . $roomType->capacity . '</td>';
        self::$template .= '<td>' . $roomType->status . '</td>';
        self::$template .= '<td><div class="form-actions"><button type="button" class="btn default" onclick="editRoomType(' . $roomType->room_type_id . ')"> Edit</button>';
        self::$template .= '<button type="button" class="btn default" onclick="deleteRoomType(' . $roomType->room_type_id . ')">Delete</button></div></td>';
        if ($event !== 'update') {
            self::$template .= '</tr>';
        }

    }
    // Room types end

    // Optional tours start
    public static function renderAddedOptionalTour($optionalTour, $event = null) {
        self::clearTemplate();
        self::templateForOptionalTour($optionalTour, $event);
    }
    public static function renderOptionalTourList($optionalTours) {
        self::clearTemplate();
        if (!empty($optionalTours)) {
            foreach ($optionalTours as $optionalTour) {
                self::templateForOptionalTour($optionalTour);
            }
        }
    }
    private static function templateForOptionalTour($optionalTour, $event = null) {
        if ($event !== 'update') {
            self::$template .= '<tr role="row" class="odd" id="optional-tour-tr-' . $optionalTour->optional_tour_id . '">';
        }
        self::$template .= '<td class="">' . $optionalTour->title . '</td>';
        self::$template .= '<td>' . $optionalTour->optional_tour_id . '</td>';
        self::$template .= '<td>' . $optionalTour->cost . '</td>';
        self::$template .= '<td class="center">' . $optionalTour->available_yn . '</td>';
        self::$template .= '<td><div class="form-actions"><button type="button" class="btn default" onclick="editOptionalTour(' . $optionalTour->optional_tour_id . ')"> Edit</button>';
        self::$template .= '<button type="button" class="btn default" onclick="deleteOptionalTour(' . $optionalTour->optional_tour_id . ', ' . $optionalTour->package_id . ')"> Delete</button></div></td>';
        if ($event !== 'update') {
            self::$template .= '</tr>';
        }
    }
    // Optional tours end

    // Room types for optional tours start
    public static function renderAddedRoomTypeForOptionalTours($roomType, $event = null) {
        self::clearTemplate();
        self::templateForRoomTypeForOptionalTours($roomType, $event);
    }
    public static function renderRoomTypesForOptionalToursList($roomTypes, $optional_tour = 'Y') {
        self::clearTemplate();
        if (!empty($roomTypes)) {
            foreach ($roomTypes as $roomType) {
                if ($roomType->optional_tour == $optional_tour) {
                    self::templateForRoomTypeForOptionalTours($roomType);
                }
            }
        }
    }
    private static function templateForRoomTypeForOptionalTours($roomType, $event = null) {
        if ($event !== 'update') {
            self::$template .= '<tr role="row" class="even" id="room-type-for-optional-tours-tr-' . $roomType->room_type_id . '">';
        }
        self::$template .= '<td class="">' . $roomType->name . '</td>';
        self::$template .= '<td>' . $roomType->price . '</td>';
        self::$template .= '<td class="center">' . $roomType->capacity . '</td>';
        self::$template .= '<td>' . $roomType->status . '</td>';
        self::$template .= '<td><div class="form-actions"><button type="button" class="btn default" onclick="editRoomTypeForOptionalTours(' . $roomType->room_type_id . ')"> Edit</button>';
        self::$template .= '<button type="button" class="btn default" onclick="deleteRoomTypeForOptionalTours(' . $roomType->room_type_id . ')">Delete</button></div></td>';
        if ($event !== 'update') {
            self::$template .= '</tr>';
        }
    }
    // Room types for optional tours end

    // Packages for event preview start
    public static function templateForPackagesForEventPreview($packages, $roomTypes) {
        self::clearTemplate();
        if (!empty($packages)) {
            foreach ($packages as $package) { 
                self::$template .= '<div class="row margtopline"><div class="border-wrapp">';
                self::$template .= '<div class=""><div class="col-md-8">';
                self::$template .= '<div class="height-quality">' . $package->name . '</div>';
                self::$template .= '</div><div class="col-md-4"><div class="form-actions">';
                self::$template .= '<a href="' . $_SERVER["SCRIPT_NAME"] . '?v=package_preview&id=' . $package->package_id . '">';
                self::$template .= '<button type="button" class="btn gray default"> More Details </button></a>';
                self::$template .= '</div></div></div></div></div>';

                if (!empty($roomTypes)) {
                    foreach ($roomTypes as $roomType) {
                        if ($roomType->package_id == $package->package_id) {

                            self::$template .= '<div class="row"><div class="border-wrapp"><div class="col-md-4">';
                            self::$template .= '<div class="">' . $roomType->name . '</div></div>';
                            self::$template .= '<div class="col-md-4"><div class="">';
                            self::$template .= '$ ' . $roomType->price . ' per Person</div></div>';
                            self::$template .= '<div class="col-md-4"><div class="form-actions">';
                            self::$template .= '<a href="' . str_replace('/control/CRUD.php', '/index.php', $_SERVER["SCRIPT_NAME"]) . '?v=book_package&package=' . $package->package_id . '&room=' . $roomType->room_type_id . '&event=' . $package->event_id . '">';
                            self::$template .= '<button type="button" class="btn green"> Book Now </button>';
                            self::$template .= '</a></div></div></div></div>';
                        } 
                    } 
                }
                self::$template .= '<div class="border-separate"></div>';
            }
        } 
            
    }
    // Packages for event preview end

    // Transactions template start
    public static function getTemplateForTransactions($transactions) {
        self::clearTemplate();
        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                self::$template .= '<tr>';
                self::$template .= '<td>' . date('d/m/Y', strtotime($transaction->ts)) . '</td>';
                self::$template .= '<td>' . $transaction->transaction_id . '</td>';
                self::$template .= '<td>' . $transaction->getPaymentMethod() . '</td>';
                self::$template .= '<td>$ ' . $transaction->amount . '</td></tr>';
            }
        }
    }
    // Transactions template end

    public static function getTemplate() {
        return self::$template;
    }

    private static function clearTemplate() {
        self::$template = '';
    }

}