<?php
require_once('database.php');

switch(DATABASE_TYPE)
{
	case "oracleDatabase":
		class DatabaseObject extends oracleDatabase{ }
	break;

	case "MySQLDatabase":
		class DatabaseObject extends MySQLDatabase{	}
	break;

	default:
		die("please provide a database type in includes/definitions.php");
	break;
}
