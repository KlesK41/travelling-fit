<?php
/***********************************************************
* database.php
* 
* Defines mySQL and Oracle database class.
* See databaseobject.php for where the appropriate class is
* selected.
*
************************************************************/

require_once("config.php");

/*
	Oracle
*/

error_reporting(-1);
/*
	MySQL
*/

class MySQLDatabase {
	
	private $connection;
	public	$lastQuery;
	private $magicQuotesActive;
	private $realEscapeStringExists;
	
  	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	public function openConnection() {
		// 		echo DB_NAME;
		// echo DB_SERVER;
		// echo DB_USER;

		$this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);


		if (!$this->connection) {
			if(mysql_error() == 1203)
			{
				echo "Too many connections";
			}
			die("Database connection failed: " . mysql_error());
		} else {

			$dbSelect = mysqli_select_db($this->connection, DB_NAME);
			if (!$dbSelect) {
				die("Database selection failed: " . mysql_error());
			}
		}
	}

	public function closeConnection() {
		if(isset($this->connection)) {
			mysqli_close($this->connection);
			unset($this->connection);
		}
	}

	public function query($sql) {
		$this->lastQuery = $sql;
		$result = mysqli_query($this->connection, $sql);
		$this->confirmQuery($result);
		return $result;
	}
	
	public function escapeValue( $value ) {
		if( $this->realEscapeStringExists ) { // PHP v4.3.0 or higher
			// undo any magic quote effects so mysql_real_escape_string can do the work
			if( $this->magicQuotesActive ) { $value = stripslashes( $value ); }
			$value = mysqli_real_escape_string( $this->connection, $value );
		} else { // before PHP v4.3.0
			// if magic quotes aren't already on then add slashes manually
			if( !$this->magicQuotesActive ) { $value = addslashes( $value ); }
			// if magic quotes are active, then the slashes already exist
		}
		return $value;
	}
	
	// "database-neutral" methods
	public function fetchArray($result_set) {

		return mysqli_fetch_array($result_set);
	}

	public function numRows($result_set) {
		return mysqli_num_rows($result_set);
	}

	public function insertId() {
		// get the last id inserted over the current db connection
		return mysqli_insert_id ($this->connection);
	}

	public function affectedRows() {
		return mysqli_affected_rows($this->connection);
	}

	private function confirmQuery($result) {
		if (!$result) {
			$output = "Database query failed: " . mysqli_error($this->connection). "<br /><br />";
	    //$output .= "Last SQL query: " . $this->lastQuery;
	    // die( $output );
		}
	}
	
}


class oracleDatabase {
	
	private $connection;
	public 	$lastQuery;
	private $magicQuotesActive;
	private $realEscapeStringExists;
	private $numRows;
	private $cnt;
	private $resource;
	private $returnId;
	public $database="Oracle";

  	function __construct() {
		$this->closeConnection();
		$this->openConnection(DB_USER, DB_PASS, DB_SERVER);
		$this->magicQuotesActive = get_magic_quotes_gpc();
		$this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
	}

	// Common Database Methods
	public static function findAll() 
	{
		$sql = "SELECT * FROM ".self::$table_name;
		return self::findBySql($sql);
	}

	public function openConnection($dbUser, $dbPass, $dbServer) {
		$this->connection = @oci_connect($dbUser, $dbPass, $dbServer);
		if (!$this->connection) {
			$e = oci_error();
			die(htmlentities($e['message'], ENT_QUOTES));
		}
	}

	public function closeConnection() {
		if(isset($this->connection)) {
			oci_close($this->connection);
			unset($this->connection);
		}
	}

	public function getConnection()
	{
		return $this->connection;
	}

	public function query($sql, $return_id=NULL) {

		$this->result_set = oci_parse($this->connection, $sql);
		if($return_id != NULL) {
			oci_bind_by_name($this->result_set,":".$return_id,$this->returnId,32);
		}

		if(!oci_execute($this->result_set))
		{
			//echo "<b>$sql</b>";	
		}

		return $this->result_set;
	}

	public function escapeValue( $value ) {
		if( $this->realEscapeStringExists ) { // PHP v4.3.0 or higher
			// undo any magic quote effects so mysql_real_escape_string can do the work
			if( $this->magicQuotesActive ) { $value = stripslashes( $value ); }
			// *** find an oracle replacement for the below...
			//$value = mysql_real_escape_string( $value );
			$value = str_replace("'","''",stripslashes($value));
		} else { // before PHP v4.3.0
			// if magic quotes aren't already on then add slashes manually
			if( !$this->magicQuotesActive ) { $value = addslashes( $value ); }
			// if magic quotes are active, then the slashes already exist
		}
		return $value;
	}

	// "database-neutral" methods
	public function fetchArray($result_set) {
		return oci_fetch_array($result_set, OCI_BOTH);
	}
	
	public function numRows($result_set) {
		return oci_fetch_all($result_set, $result);
	}

	public function insertId() {
		// return the last id inserted over the current db connection
		return $this->returnId;
	}

	public function affectedRows() {
		return oci_num_rows($this->result_set);
	}

	private function confirmQuery($result) {
		if (!$result) {
			$error = oci_error($this->result_set);
			$output = "Database query failed: " . mysql_error() . "<br /><br />";
		    $output .= "Last SQL query: " . $this->lastQuery;
			
			//die( $output );
		}
	}

}

?>