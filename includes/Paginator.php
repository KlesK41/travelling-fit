<?php

/**
 * Created by PhpStorm.
 * User: A
 * Date: 02.02.2016
 * Time: 15:04
 */
class Paginator
{
    private $_limit = 3;
    private $_count;
    private $_page;

    public function __construct($count, $page = 1) {
        $this->_count = $count;
        $this->_page = $page;
    }

    public function render() {
        $prevPage = $lastPage = '';


        $last = ceil($this->_count/$this->_limit);

        $prevPageNum = 1;
        $nextPageNum = $last;

        $template = '<p id="dynamic_pager_demo2">';
        $template .= '<ul class="pagination bootpag" style="float: right">';
        if ($this->_page == 1) {
            $prevPage = 'disabled';
        } else {
            $prevPageNum = $this->_page - 1;
        }
        if ($last == 1 || $last == $this->_page) {
            $lastPage = 'disabled';
            $nextPageNum = $last;
        } else {
            $nextPageNum = $this->_page + 1;
        }
        $template .= '<li data-lp="1" class="prev ' . $prevPage . '"><a href="javascript:;" onclick="goToPage(' . $prevPageNum . ', ' . $_SESSION['generic_user_id'] . ', ' . $_SESSION['bpay_booking_id'] . ')"><i class="fa fa-angle-left"></i></a></li>';
        for ($i = 0; $i < $last; $i++) {
            $disabled = '';
            if ($i + 1 == $this->_page) {
                $disabled = 'disabled';
            }
            $template .= '<li class="' . $disabled . '"><a href="javascript:;" page-number="' . ($i + 1) . '" onclick="goToPage(' . ($i + 1) . ', ' . $_SESSION['generic_user_id'] . ', ' . $_SESSION['bpay_booking_id'] . ')">' . ($i + 1) . '</a></li>';
        }
        $template .= '<li class="next ' . $lastPage . '"><a href="javascript:;" onclick="goToPage(' . $nextPageNum . ', ' . $_SESSION['generic_user_id'] . ', ' . $_SESSION['bpay_booking_id'] . ')"><i class="fa fa-angle-right"></i></a></li>';
        $template .= '</ul></p>';

        return $template;
    }

    public function getLimit() {
        return $this->_limit;
    }


}