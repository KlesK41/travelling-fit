<?php
// A class to help work with Sessions
// In our case, primarily to manage logging users in and out
// Keep in mind when working with sessions that it is generally 
// inadvisable to store DB-related objects in sessions
class Session {
	private $logged_in=false; // force
	private $adminLoggedIn = false; // force
	private $generalLoggedIn = false;
	public $user_id;
	public $access_level;
	public $ethics_approval;
	public $snumber;
	public $firstName;
	public $lastName;

	function __construct() {
		if(!session_id()) session_start();
		$this->check_login();
    if($this->logged_in) {
		
      // actions to take right away if user is logged in
    } else {
      // actions to take right away if user is not logged in
    }
	}

	//public function is_logged_in() {
  //  return $this->logged_in;
  //}
  public function isLoggedIn() {
    return $this->logged_in;
  }

	public function login($user) {
	// database should find user based on snumber/password
	if($user){
	  		$this->user_id = $_SESSION['generic_user_id'] = $user->user_id;
			$this->access_level = $_SESSION['generic_access_level'] = $user->access_level;
			//$this->snumber = $_SESSION['generic_snumber'] = $user->snumber;
			$this->firstname = $_SESSION['generic_firstname'] = $user->firstname;
			$this->lastname = $_SESSION['generic_lastname'] = $user->lastname;
			//$this->ethics_approval = $_SESSION['generic_ethics_approval'] = $user->ethics_approval;
			$_SESSION['generic_currentUser'] =  strtoupper($user->firstname) . " " . strtoupper($user->lastname);
			if ($user->access_level  == '1')
				$_SESSION['generic_is_admin'] = true;
			
			if ($user->access_level  == '1' || $user->access_level  == '2')
			{
				$this->logged_in = true;
			}
    }
  }

	public function set($key, $value) {
		// put variables into session variables
		$_SESSION[$key] = $value;
	}


	private static function instantiate($record) {
		// Could check that $record exists and is an array
    $object = new self;
		// Simple, long-form approach:
		// $object->id 				= $record['id'];
		// $object->snumber 	= $record['snumber'];
		// $object->password 	= $record['password'];
		// $object->first_name = $record['first_name'];
		// $object->last_name 	= $record['last_name'];
		
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  if($object->has_attribute($attribute)) {
		    $object->$attribute = stripslashes($value);
		  }
		}
		return $object;
	}
	
  public function logout() {
	unset($_SESSION['generic_user_id']);
	unset($_SESSION['generic_currentUser']);
    unset($_SESSION['generic_access_level']);
	unset($_SESSION['generic_firstname']);
	unset($_SESSION['generic_lastname']);
	unset($_SESSION['generic_ethics_approval']);
	unset($_SESSION['generic_snumber']);

    unset($this->user_id);
    $this->logged_in = false;
	$this->adminLoggedIn = false;
	$this->generalLoggedIn = false;
	session_destroy();
	}

	private function check_login() {
    if(isset($_SESSION['generic_user_id'])) 
    {
        $this->user_id = $_SESSION['generic_user_id'];
	    $this->logged_in = true;
    } 
    else 
    {
      unset($this->user_id);
      $this->logged_in = false;
    }
  }
}

$session = new Session();

?>