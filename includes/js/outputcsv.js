$(document).ready(function(){
  outputCSV.initialise();
})

// store information in local storage
$('body').on('click', '#storeData_button',  function(event){
  Util.safe_log('=== store form data in local storage ===');
  outputCSV.storeFormData("exportcsv_form");
})

/*
    *** Set up on click actions for buttons ***
*/
// export localstorage to csv
$('body').on('click', '#printData_button',  function(event){
  Util.safe_log('=== print local storage ===');
  outputCSV.displayLocalStorage('record_', parseInt(localStorage[outputCSV.recordCountVar]));
})

// clear local storage
$('body').on('click', '#clearData_button',  function(event){
  Util.safe_log('=== clear local storage ===');
  outputCSV.clearLocalStorage();
})

// prepare CSV data
$('body').on('click', '#prepareCSV',  function(event)
{
  outputCSV.generateCSV();
})

var outputCSV = {
  keyPrefix: 'record_',
  recordCountVar: 'instance',
  /**
  *  @purpose initialise the app
  */
  initialise: function()
  {
    if(!Util.supportsHtml5Storage)
    {
      alert("this browser doesn't support localStorage");
    }

    if(localStorage[outputCSV.recordCountVar] == undefined)
    {
      localStorage[outputCSV.recordCountVar] = 1;
    }
  },
  /**
  *  @purpose clear local storage
  */
  clearLocalStorage: function()
  {
    localStorage.clear();
    localStorage[outputCSV.recordCountVar] = 1;
  },
  /**
  *  @purpose display the contents of localstorage records from 1 to numberRecords
  *           keyPrefix and numberRecords form the key of the localStorage array
  *  @param   keyPrefix
  *  @param   numberRecords
  *  @return
  */
  displayLocalStorage : function()
  {
    for(i=1; i<localStorage[outputCSV.recordCountVar]; i++)  
    {
      Util.safe_log(outputCSV.keyPrefix + i);
      Util.safe_log(localStorage[outputCSV.keyPrefix + i]);
    }
  },
  storeFormData : function(formId)
  {
    var formData = $("#" + formId).serializeArray();
    var newRecord = {};
    $.each(formData, function(i, obj){
        newRecord[obj.name] = obj.value;
    });

    localStorage[outputCSV.keyPrefix + localStorage[outputCSV.recordCountVar]] = JSON.stringify(newRecord);
    localStorage[outputCSV.recordCountVar] = parseInt(localStorage[outputCSV.recordCountVar]) + 1;
  },
  /**
  *  @purpose get headings (key values) and make csv friendly
  *
  *  @param  array
  *  @return string
  */
  getHeadings : function(arr)
  {
    var csv = '';
    var csvHeadings = '';
    var objectSize = Util.getObjectSize(arr);
    var i = 0;
    $.each(arr, function(k, v) {
        csvHeadings += k;
        i++;

        //csv += csv_row.join(";");
        if (i == objectSize)
        {
          csvHeadings +='\n';
        }
        else
        {
          csvHeadings +=',';
        }

    });
    return csvHeadings;
  },
  /**
  *  @purpose get values of specified array and make csv friendly
  *
  *  @param  array
  *  @return string
  */
  getValues : function(arr)
  {
    Util.safe_log('=== getValues(arr) ===');
    var csv = '';
    var csvData = '';
    var objectSize = Util.getObjectSize(arr);
    var i = 0;
    $.each(arr, function(k, v) 
    {
        Util.safe_log(v);
        if(csvData != undefined)
        {
          csvData += v;
        }
        i++;

        //csv += csv_row.join(";");
        if (i == objectSize)
        {
          csvData +='\n';
        }
        else
        {
          csvData +=',';
        }

    });
    return csvData;
  },
  /**
  *  @purpose generate and export a csv file from the contents of localstorage records from 1 to numberRecords
  *           keyPrefix and numberRecords form the key of the localStorage array
  *  @param   keyPrefix
  *  @param   numberRecords
  *  @return  output to file data.csv
  */
  generateCSV : function()
  {
    if (typeof(window.localStorage[outputCSV.keyPrefix + '1']) !== "undefined")
    {
        Util.safe_log('=== Generate CSV ===');
        var data = '';
        var arr = JSON.parse( window.localStorage[outputCSV.keyPrefix + '1'] );
        var csvHeadings = outputCSV.getHeadings(arr);

        for (i=1; i<localStorage[outputCSV.recordCountVar]; i++)
        {
          var arr = JSON.parse( localStorage[outputCSV.keyPrefix + i] );
          data += outputCSV.getValues(arr);
        }

        Util.safe_log('Export to csv');
        Util.safe_log('=== csv headings === ');
        Util.safe_log(csvHeadings);
        Util.safe_log('=== csv data === ');
        Util.safe_log(data);

        var csv = csvHeadings + data;

        //http://cheateinstein.com/category-javascript/client-side-export-to-csv/
       $('.download').attr("href", "data:text/csv;base64,"+btoa(csv));
       $('.download').click();
    }
  }
}

