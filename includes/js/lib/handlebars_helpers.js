
Handlebars.registerHelper('list', function(ctx, opts) {
  var res = ''
  ctx.forEach(function(obj){
    var key = Object.keys(obj);
    var values = [];
    // Util.safe_log(key);
    key.forEach(function(attribute){
    	// Util.safe_log(attribute);
    	//Util.safe_log(obj[attribute] + "* ");
    	values.push(obj[attribute])
    });
    var data = {key: key, val: values}
    Util.safe_log($.type(key));
    res += opts.fn(obj, { data: data });
  });
  return res;
});