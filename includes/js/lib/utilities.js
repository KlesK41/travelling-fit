var Util	= 
	{
		safe_log:		function(a)
			{
				console&&console.log(a)
			},
		getUrlVars:		function getUrlVars()
			{
				var vars = [], hash;
				var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
				for(var i = 0; i < hashes.length; i++)
				{
					hash = hashes[i].split('=');
					vars.push(hash[0]);
					vars[hash[0]] = hash[1];
				}
				return vars;
			},
		supportsHtml5Storage: function () {
			try {
		    		return 'localStorage' in window && window['localStorage'] !== null;
		  		} catch (e) 
		  		{
		    		return false;
		  		}
			},
		getObjectSize: function(obj) {
			    var len = 0, key;
			    for (key in obj) {
			        if (obj.hasOwnProperty(key)) len++;
			    }
			    return len;
			},
		ajaxData: function(data, callback) {
			$.each(data, function(key, element) {
				//Utility.safe_log('key: ' + key + '\n' + 'value: ' + element);
			});
			var returnData;	// return json
			var rawData;	// return raw data
			$.ajax({
					type: "POST",
					async: true,
					url: "../control/CRUD.php", //window.modelurl
					timeout: 10000,
					data: 	data,
					error: function(x, t, m)
					{
						if(t==="timeout")
						{
								//Utility.safe_log('ajax request has timed out');
						}
						else
						{
								//Utility.safe_log('an error has occured with the ajax response: ' + t);
						}
					},
					success: function(data) {
						//console.log(data);
						try {
							rawData = data;
							returnData = jQuery.parseJSON(data);

						} catch (err) {
							Util.safe_log('-------');
							Util.safe_log('ERROR:');
							Util.safe_log(err);
							Util.safe_log('DATA:');
							Util.safe_log(data);
							Util.safe_log('-------');
							
							returnData = {};
						}
						callback(returnData);
					}
			});

			return returnData;
		}
	};
