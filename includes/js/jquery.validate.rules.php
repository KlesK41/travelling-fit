<?php
$date = $_SESSION['hbportal_DOB_AT_TIME_OF_EVENT'];
$dobAtTimeOfEvent = substr($date, 0, 4) . ", " . substr($date, 5, 2) . ", " . substr($date, 8, 2);
?>
$().ready(function() {
// this function checks the selected date, returning true if older than 18 years
  $.validator.addMethod("Age", function() {
    //only compare date if all items have a value
    if($("#DOBd").val() != "" && $("#DOBm").val() != "" && $("#DOBy").val() != "") {
      //get selected date
      var DOB = new Date($("#dob_year").val(), $("#dob_month").val(), $("#dob_day").val());
      var eday = new Date(<?php echo $dobAtTimeOfEvent ?>); 
      //if older than 18
      if(DOB < eday) {
				$("#row1").remove();
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, 'The registrant must be older that 18 at the time of the event');

  $.validator.addMethod("AgeMember", function() {
    //only compare date if all items have a value
    if($("#DOBd").val() != "" && $("#DOBm").val() != "" && $("#DOBy").val() != "") {
      //get selected date
      var DOB = new Date($("#dob_member_year").val(), $("#dob_member_month").val(), $("#dob_member_day").val());
      var eday = new Date(<?php echo $dobAtTimeOfEvent ?> );
      if(DOB < eday) {
				$("#row1").remove();
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, 'The registrant must be older that 18 at the time of the event');


  jQuery.validator.addMethod("phoneAU", function(phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, ""); 
    return this.optional(element) || phone_number.length > 7 &&
      phone_number.match(/^[(]?\d{2}[)]?-?\d{3}-?\d{1}-?\d{1}-?\d{1}-?-?$/);
  }, "Please specify a valid phone number with area code");

  jQuery.validator.addMethod("mobileAU", function(phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, ""); 
    return this.optional(element) || phone_number.length > 9 &&
      phone_number.match(/^[(]?\d{2}[)]?-?\d{3}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?-?$/);
  }, "Please specify a valid phone number with area code");
  
  
   $.validator.addMethod("phoneAndArea", function() {
    if ($("#home_phone").val() == "" && $("#home_phone_area").val() == "")
    	return true;
    if ($("#home_phone").val() != "" && $("#home_phone_area").val() != "")
    	return true;
  }, 'Please specify an area code and phone number');

jQuery.validator.addMethod("notEqual", function(value, element, param) {
		 alert("here");
		 return this.optional(element) || value != param;
		}, "Please specify a different (non-default) value");

	// validate signup form on keyup and submit
	$("#reg").validate({
		rules: {
			firstname: {
				required: true,
				minlength: 2
			},
			lastname: {
				required: true,
				minlength: 2
			},
			username: {
				required: true,
				minlength: 2
			},
      post_code:
      {	
      	required: true,
      	number: true,
        minlength: 4
      },
			dob_day: {
				required: true,
				range: [1, 31]
			},
			dob_month: {
				required: true,
				range: [1, 12]
			},
			dob_year: {
				Age: true,
				required: true,
				range: [1900, 2011]
			},
			dob_child_day: {
				AgeChild: true,
				required: true,
				range: [1, 31]
			},
			dob_child_month: {
				AgeChild: true,
				required: true,
				range: [1, 12]
			},
			dob_child_year: {
	      AgeChildUnder18: true,
				AgeChild: true,
				required: true,
				range: [1900, 2011]
			},
			dob_member_day: {
				required: true,
				range: [1, 31]
			},
			dob_member_month: {
				required: true,
				range: [1, 12]
			},
			dob_member_year: {
				AgeMember: true,
				required: true,
				range: [1900, 2011]
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			email_repeat: {
				required: true,
				equalTo: "#email"
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
      SN_allergies_detail:
      {
      	required:'#SN_allergies:checked'
      },
      SN_dietary_detail:
      {
      	required:'#SN_dietary:checked'
      },
      SN_other_detail:
      {
      	required:'#SN_other:checked'
      },
      how_hear_other:
      {
      	required:'#how_hear:checked'
      },
      home_phone:
      {
      	required: false,
        phoneAU: true,
        phoneAndArea: true
      },
      home_phone_area:
      {
      	required: false,
				minlength: 2,
        number: true
      },
      mobile_phone:
      {
      	required: true,
        mobileAU: true
      },
      card_type: "required",
      payment: "required",
			gender: "required",
			stream: "required",
			how_hear: "required",
			agree: "required",
			spouse: "required",
			child: "required",
			select: "required"
			
		},
		messages: {
			firstname: {
				required: "Please enter your firstname.",
				minlength: "Your firstname must consist of at least 2 characters"
			},
			lastname: {
				required: "Please enter your lastname.",
				minlength: "Your lastname must consist of at least 2 characters"
			},			
			dateofbirth: "Please enter your date of birth",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			email_repeat: "Please re-type the above email address",
			agree: "Please accept our policy"
		}
	});
});