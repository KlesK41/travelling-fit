<?php
session_start();
define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/facebook-sdk-v5/');
require_once __DIR__ . '/facebook-sdk-v5/autoload.php';

//echo 'Start<br>';
$fb = new Facebook\Facebook([
    'app_id' => '572973799518112',
    'app_secret' => '6e6002d3b8a154e5558296edc08c6f71',
    'default_graph_version' => 'v2.5',
    //'default_access_token' => '644988485583207|8Wn_JWx_VefAVwOdKTK-_ZtN2Yo'
]);
//echo 'Start<br>';
$helper = $fb->getRedirectLoginHelper();


try {
    $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
//echo 'Start<br>';
if (isset($accessToken)) {
    // Logged in!
    $_SESSION['facebook_access_token'] = (string) $accessToken;

    // Now you can redirect to another page and use the
    // access token from $_SESSION['facebook_access_token']

    // $location = "Location: index.php?v=package&s=99";
    // header($location);
}

// $accessToken = $helper->getAccessToken();
try {
    // Get the Facebook\GraphNodes\GraphUser object for the current user.
    // If you provided a 'default_access_token', the '{access-token}' is optional.
    $response = $fb->get('/me?fields=id,name,email, first_name, last_name', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}


$me = $response->getGraphUser();


if ($me) {
    require_once("../includes/initialize.php");

    $user = Users::findByFbId($me->getId());

    // var_dump($user);

    if (!$user) {
        // var_dump(1);
        $user = new Users();
        $user->firstname = $me->getFirstName();
        $user->lastname = $me->getLastName();
        $user->fb_id = $me->getId(); 
        $user->status = 'enabled';
        $user->access_level = 2;
        $id = $user->addUserFromFb();
        // var_dump($id);
    }

    $session->login($user);
    if (isset($_SESSION['redirect'])) {
            $location = "Location: ". $_SESSION['redirect'];
            unset($_SESSION['redirect']);
    } else {
            //$location = "Location: index.php?v=package&s=99";
        $location = "Location: index.php?v=my_booking";
        
    }

    header($location);
}
// echo 'Logged in as ' . $me->getName();


?>