<?php

// echo phpinfo();exit();
/***********************************************************
* index.php - front controller
* 
* This web app requires a .htaccess file to prevent direct
* access to files other than this file.
*
************************************************************/
date_default_timezone_set('Australia/Queensland');
define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/facebook-sdk-v5/');
require_once __DIR__ . '/facebook-sdk-v5/autoload.php';

// if(!session_id()) session_start();

// include relevant objects, like database object, value objects, functions
require_once("../includes/initialize.php");


// if (!$session->isLoggedIn()) 
// { 
// 	$view="login"; 
// }

// main is the view controller


require_once("view/main.php");

?>
