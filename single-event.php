<?php

/*

*	Post type Event

*/

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );



remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );



// Header

add_action('genesis_entry_content', function(){

	$title = get_the_title();

	$date = get_field('date');

	$continent = get_field('continent');

	$location = get_field('location');

	$logo = get_field('logo');

	?>

	<div class="internal-header-container event">

		<table>

			<tbody>

				<tr class="r-1">

					<td class="event-details">

						<table>

							<tbody>

								<tr class="r-1-1">

									<td colspan="3" class="title-container"><h4 class="title"><?php echo $title; ?></h4></td>

								</tr>

								

								<tr class="r-1-2">

									<td class="date-container"><span class="date with-icon"><?php echo ($date) ? $date : 'N/A'; ?></span></td>

									<td class="continent-container">

										<span class="continent with-icon">

											<?php echo ($continent) ? '<a href="/events/?event-location='. $continent .'">' . $continent . '</a>' : 'N/A'; ?>

										</span>

									</td>

									<td class="location-container">

										<span class="location with-icon">

											<?php echo ($location) ? '<a href="/events/?event-country='. $location .'">' . $location . '</a>' : 'N/A'; ?>

										</span>

									</td>

								</tr>

							</tbody>

						</table>

					</td>

					<?php if( $logo ) { ?>

						<td class="logo-container">

							<img src="<?php echo $logo['url']; ?>" alt="<?php the_title() ?> logo" width="<?php echo $logo['width']; ?>" height="<?php echo $logo['height']; ?>">

						</td>

					<?php } // if logo ?>

				</tr>

			</tbody>

		</table>

	</div>

	<?php

}, 10);



// Pop-ups

add_action('genesis_entry_content', function(){

	// Top Right Buttons

	echo '<div class="top-buttons">';

		echo '<a href="#event-enquiry-form" class="btn fancybox-inline">Enquire</a>';

		echo '<a href="#become-priority-form" class="btn fancybox-inline become-priority">Become a priority client</a>';

	echo '</div>';

	

	// Enquiry Popup Form

	echo '<div class="popup-form-container" style="display:none;">';

		echo '<div id="event-enquiry-form">';

			echo '<div class="popup-form-wrapper popup-form-wrapper-1">';

				echo do_shortcode('[gravityform id="8" title="false" description="false" tabindex="600" ajax="true"]');

			echo '</div>';

		echo '</div>';

	echo '</div>';

	

	// Become Priority Popup Form

	echo '<div class="popup-form-container" style="display:none;">';

		echo '<div id="become-priority-form">';

			echo '<div class="popup-form-wrapper popup-form-wrapper-2">';

				echo do_shortcode('[gravityform id="9" title="false" description="false" tabindex="700" ajax="true"]');

			echo '</div>';

		echo '</div>';

	echo '</div>';

}, 20);



// Tabs Nav

add_action('genesis_entry_content', function(){

	$id = get_the_ID();

	echo '<div class="tab_wrap custom-tabs event-tabs event">';

		echo '<ul class="nav nav-tabs">';

			echo '<li class="active" id="course-info-trigger"><a data-toggle="tab" href="#course-'. $id .'">The Course</a></li>';

			echo '<li class="" id="race-info-trigger"><a data-toggle="tab" href="#race-info-'. $id .'">Race Info</a></li>';

			echo '<li class="" id="race-reviews-trigger"><a data-toggle="tab" href="#reviews-'. $id .'">Reviews</a></li>';

			echo '<li class="" id="gallery-trigger"><a data-toggle="tab" href="#gallery-'. $id .'">Gallery</a></li>';

			echo '<li class="" id="packages-trigger"><a data-toggle="tab" href="#packages-'. $id .'">Package Inclusions</a></li>';

			echo '<li class="" id="deposit-payment-trigger"><a data-toggle="tab" href="#deposit-payment-'. $id .'">Deposit and Payment Structure</a></li>';

		echo '</ul>';

}, 20);



// Header Image

/* add_action('genesis_entry_content', function(){

	$header_banner = get_field('header_banner');

	if( $header_banner ) {

		echo '<div class="header-banner"><img src="'. $header_banner['url'] .'" alt="'. $header_banner['title'] .'" width="'. $header_banner['width'] .'" height="'. $header_banner['width'] .'"></div>';

	}

}, 30); */



// Tabs Content

add_action('genesis_entry_content', function(){

	$id = get_the_ID();

		echo '<div class="tab-content">';

		

			// The Course

			echo '<div class="tab-pane fade active in course-tab" id="course-'. $id .'">';
			
				$header_banner = get_field('header_banner');

				if( $header_banner ) {

					echo '<div class="header-banner"><img src="'. $header_banner['url'] .'" alt="'. $header_banner['title'] .'" width="'. $header_banner['width'] .'" height="'. $header_banner['width'] .'"></div>';

				}

				echo '<h3 class="heading-style-1">The Course</h3>';

				$course_content = get_field('course_content');

				

				echo '<div class="content-container">';

					echo ( $course_content ) ? $course_content : 'No course info...';

				echo '</div>';

			echo '</div>';

			

			// Race Info

			echo '<div class="tab-pane fade race-info-tab" id="race-info-'. $id .'">';

				echo '<h3 class="heading-style-1">Race Info</h3>';

				$race_info_content = get_field('race_info_content');

				echo '<div class="content-container">';

					echo ($race_info_content) ? $race_info_content : 'No race info...';

				echo '</div>';

			echo '</div>';

			

			// Reviews

			echo '<div class="tab-pane fade reviews-tab" id="reviews-'. $id .'">';

				echo '<h3 class="heading-style-1">Reviews</h3>';

				echo '<div class="content-container">';

					

					/* $reviews_content = get_field('reviews_content');

					echo ($reviews_content) ? $reviews_content : 'No reviews...'; */

					// Query Race Reviews

					$args = array(

						'post_type' => 'race-review',

						'show_posts' => -1,

						'meta_query' => array(
							'relation' => 'OR',
							array(

								'key'     => 'event_post',

								'value'   => get_the_ID(),

								'compare' => '=',

							),
							array(

								'key'     => 'duplicate_review_post',

								'value'   => get_the_ID(),

								'compare' => '=',

							),

						),

					);

					$the_query = new WP_Query( $args );

					if ( $the_query->have_posts() ) {

						echo '<div class="testimonials event-page race-reviews">';

						while ( $the_query->have_posts() ) { $the_query->the_post();

							$author = get_field('author');

							echo '<blockquote class="testimonial-box">';

								echo '<div class="testimonial-content-page">';

									echo '<h5 class="heading"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h5>';

										echo '<a href="'. get_permalink() .'">' . fbf_custom_excerpt(200) . '</a>';

									echo ($author) ? '<div class="testimonial-name-bottom"><a href="'. get_permalink() .'">Written By '. $author .'</a></div>' : '';	

								echo '</div>';

							echo '</blockquote>';

						}

						echo '</div>';

					} else {

						echo 'No Reviews for this event';

					}

					wp_reset_postdata();

					// End Race Review Query

		

				echo '</div>';

			echo '</div>';

			

			// Gallery

			echo '<div class="tab-pane fade gallery-tab" id="gallery-'. $id .'">';

				echo '<h3 class="heading-style-1">Gallery</h3>';

				$gallery_content = get_field('gallery_content');

				echo '<div class="content-container">';

					echo ($gallery_content) ? $gallery_content : 'No gallery...';

				echo '</div>';

			echo '</div>';

			

			// Package Inclusions

			echo '<div class="tab-pane fade packages-tab" id="packages-'. $id .'">';

				echo '<h3 class="heading-style-1">Packages</h3>';

				$packages_content = get_field('packages_content');

				$packages_include = get_field('packages_include');

				$package_brochures = get_field('package_brochures');

				echo '<div class="content-container">';

				

					echo '<div class="row">';

						$col_class_1 = ( $packages_include ) ? ' col-sm-6' : ' col-sm-12';

						echo '<div class="col-xs-12'. $col_class_1 .' package-content">';

							echo ($packages_content) ? $packages_content : 'No packages';

							

							if( $package_brochures ) {

								echo '<div class="package-brochures-container">';

									echo '<h5>Package Brochures</h5>';

									echo '<div class="package-brochures">';

										echo '<div class="row">';

										foreach ( $package_brochures as $package_brochure ) {

											echo '<div class="col-xs-12 col-sm-6 package-brochure">';

												echo '<a href="'. $package_brochure['pdf_file']['url'] .'" target="_blank">'. $package_brochure['name'] .'</a>';

											echo '</div>';

										}

										echo '</div>';

									echo '</div>';

								echo '</div>';

							}

							

						echo '</div>';

						

						if( $packages_include ) {

							echo '<div class="col-xs-12'. $col_class_1 .' package_inclusions">';

								echo '<h5>Packages Includes</h5>';

								echo '<ul class="list-group packages-list">';

									foreach( $packages_include as $p ) {

										echo '<li class="list-group-item">'. $p['package'] .'</li>';

									}

								echo '</ul>';

							echo '</div>';

						}

						

						// Hotels

						echo '<div class="col-xs-12 package-hotels" id="package-hotels"></div>';







						// $hotels = get_field('hotels');

						// if( $hotels ) {

						// 	echo '<div class="col-xs-12 package-hotels">';

						// 		$hotel_index = 0;

						// 		$a = ''; // Package Details Accordion buffer

						// 		foreach( $hotels as $hotel ) { $hotel_index++;

						// 			$hotel_name = $hotel['hotel_name'];

						// 			$package_accordion_class = 'package-details-accordion-'. $hotel_index;

									

						// 			echo '<div class="package-hotel data-display-1">';

						// 				// Heading

						// 				if( $hotel_name ) {

						// 					echo '<table class="hotel-room heading data-group-1">';

						// 						echo '<tbody>';

						// 							echo '<tr>';

						// 								echo '<td class="cell cell-1">';

						// 									echo '<span class="heading-name">'. $hotel_name .'</span>';

						// 								echo '</td>';

														

						// 								echo '<td class="cell cell-3">';

						// 									echo '<span class="btn package-details-btn btn-show-content-2" rel="'. $package_accordion_class .'">Package Details</span>';

						// 								echo '</td>';

						// 							echo '</tr>';

						// 						echo '</tbody>';

						// 					echo '</table>';

						// 				}

										

						// 				foreach( $hotel['rooms'] as $room ) {

						// 					$type = $room['type'];

						// 					$price = $room['price'];

						// 					$book_link = $room['book_link'];

						// 					echo '<table class="hotel-room data-group-1">';

	

						// 						echo '<tr>';

						// 							echo '<td class="cell cell-1">';

						// 								echo '<span>'. $type .'</span>';

						// 							echo '</td>';

													

						// 							echo '<td class="cell cell-2">';

						// 								echo '<span>'. $price . '</span>';

						// 							echo '</td>';

													

						// 							echo '<td class="cell cell-3">';

						// 								if( strtolower($book_link) == 'sold out' ) {

						// 									echo '<span class="btn btn-2 sold-out">Sold out</span>';

						// 								} else {

						// 									echo '<a class="btn btn-2 book-now-btn" href="'. $book_link .'">Book Now</a>';

						// 								}

						// 							echo '</td>';

						// 						echo '</tr>';

						// 					echo '</table>';

						// 				} // rooms

										

						// 				// Accordion Content - Package Details

						// 				$a = '<div class="package-details-accordion show-content-2 show-content-'. $package_accordion_class .'">';

						// 					$a .= '<div class="inner">';



						// 						$a .= '<div class="row">';

						// 							$a .= '<div class="col-xs-12 col-sm-6 col-md-6">';

						// 								$package_details_sheading = $hotel['heading'];

						// 								$package_details_left_content = $hotel['left_content'];

						// 								$a .= '<h3 class="package-details-heading">'. $package_details_sheading .'</h3>';

						// 								$a .= $package_details_left_content;

						// 							$a .= '</div>';

													

						// 							$a .= '<div class="col-xs-12 col-sm-6 col-md-6">';

						// 								$rooms_pricing_heading = $hotel['rooms_pricing_heading'];

						// 								$rooms_pricing = $hotel['rooms_pricing'];

														

						// 								// Rooms & Pricing

						// 								if( $rooms_pricing ) {

						// 									$a .= '<table class="table-style-1 room-pricing">';

						// 										if( $rooms_pricing_heading ) {

						// 											$a .= '<tr class="heading">';

						// 												$a .= '<td colspan="2">';

						// 													$a .= $rooms_pricing_heading;

						// 												$a .= '</td>';

						// 											$a .= '</tr>';

						// 										}

						// 										foreach( $rooms_pricing as $room_pricing ) {

						// 											$a .= '<tr>';

						// 												$a .= '<td class="room-name">';

						// 													$a .= $room_pricing['name'];

						// 												$a .= '</td>';

						// 												$a .= '<td class="room-price">';

						// 													$a .= $room_pricing['price'];

						// 												$a .= '</td>';

						// 											$a .= '</tr>';

						// 										}

						// 									$a .= '</table>';

						// 								}

						// 							$a .= '</div>';

						// 						$a .= '</div>';

												

						// 						// Days

						// 						$days = $hotel['days'];

						// 						$a .= '<div class="package-details-days custom-accordion accordion-style-2">';

						// 							$i = 0;

						// 							foreach( $hotel['days'] as $day ) { $i++;

						// 								$a .= '<div class="accordion-item">';

						// 									$a .= '<span class="title accordion-toggle">'. $day['heading'] .'</span>';

						// 									$a .= '<div class="accordion-body">'. $day['content'] .'</div>';

						// 								$a .= '</div>';

						// 							}

						// 						$a .= '</div>';

												

						// 					$a .= '</div>';

						// 				$a .= '</div>';

						// 				// END Accordion Content

										

						// 			echo '</div>';

						// 		}

						// 	echo '</div>';

						// }

						

						// // Display Package Details Accordion

						// echo '<div class="col-xs-12">';

						// 	echo $a;

						// echo '</div>';

						

					echo '</div>';

					

				echo '</div>';

			echo '</div>'; // Package Inclusions

			

			// Deposit and Payment Structure

			echo '<div class="tab-pane fade packages-tab" id="deposit-payment-'. $id .'">';

				echo '<h3 class="heading-style-1">Deposit and Payment Structure</h3>';

				echo '<div class="content-container">';

					echo '[DEPOSIT AND PAYMENT TABLE HERE]';

				echo '</div>';

			echo '</div>'; // Deposit and Payment Structure ?>

			

		<?php echo '</div><!--tab-content-->';

		

	echo '</div><!--tab_wrap-->';

	

	// Query Testimonials

	global $post;

	$event_slug = $post->post_name;

	$testimonial_args = array(

		'post_type' => 'tf-testimonial',

		'showposts' => 3,

		'tax_query' => array(

			array(

				'taxonomy' => 'testimonial-category',

				'field'    => 'slug',

				'terms'    => $event_slug

			),

		),

	);

	

	$testimonial_query = new WP_Query( $testimonial_args );

	echo '<div class="testimonials event-page">';

		if ( $testimonial_query->have_posts() ) {

			while($testimonial_query->have_posts()) { $testimonial_query->the_post();

				echo '<blockquote class="testimonial-box">';

					echo '<div class="testimonial-content-page">';

						//$marathon = get_field('marathon');

						//echo ($marathon) ? '<h5 class="heading">'. $marathon .'</h5>' : '';

						echo get_the_content();

						echo '<div class="testimonial-name-bottom">'. get_the_title() .'</div>';	

					echo '</div>';

				echo '</blockquote>';

			}

		}

		wp_reset_postdata();

	echo '</div>';

	}, 40);

/*

*	Content Area

*/

function before_content_wrapper(){

	echo '<div class="post-content-wrapper">';

}

add_action('genesis_entry_content', 'before_content_wrapper', 1);





function after_content_wrapper(){

	echo '</div>';

}

add_action('genesis_entry_footer', 'after_content_wrapper', 10);



// Event Dropdown JS

add_action('wp_footer', function(){

?>

<script>





// Added by Andrew

(function($){

	var event_id = '<?php echo get_the_ID(); ?>';

  // console.log(event_id);

  $.post('/~travellingfit/app/control/CRUD.php', {

  	method: 'getPackagesForEventPreview',

  	event_id: event_id

  }, function(result) {

  	// debugger;

  	result = jQuery.parseJSON(result);

  	console.log(result);

  	if (result.packages.length > 0) {

  		var template = '';

  		for (var i in result.packages) {

  			template += '<div class="package-hotel data-display-1 package-details-' + result.packages[i].package_id + '" id="package-details-' + result.packages[i].package_id + '">';
  			template += '<h2 style="display:none" id="package-title-' + result.packages[i].package_id + '">' + result.packages[i].name + '</h2>'

  			template += '<table class="hotel-room heading data-group-1"><tbody><tr>';

  			if (result.rooms.length > 0) {
  				for (var k in result.rooms) {
  					if (result.packages[i].package_id == result.rooms[k].package_id) {
  						var name = result.rooms[k].hotel_name
  					}
  				}
  			}

  			template += '<td class="cell cell-1"><span class="heading-name">' + name + '</span>';

  			template += '</td><td class="cell cell-3">';

  			template += '<a href="javascript:void(0)">' ;

  			// template += '<span class="btn package-details-btn btn-show-content-2" onclick="jQuery(\'#package-info-' + result.packages[i].package_id + '\').slideToggle();">Package Details</span></a>';
  			template += '<span class="btn package-details-btn btn-show-content-2" id="show-package-button-' + result.packages[i].package_id + '" onclick="showPackage(' + result.packages[i].package_id + ')">Package Details</span>';
  			template += '<span class="btn package-details-btn btn-show-content-2" id="hide-package-button-' + result.packages[i].package_id + '" style="display:none" onclick="hidePackage(' + result.packages[i].package_id + ')">Back To Packages</span>';

  			template += '</a></td></tr></tbody></table>';

  			if (result.rooms.length > 0) {

  				for (var j in result.rooms) {

  					if (result.packages[i].package_id == result.rooms[j].package_id) {

  						template += '<table class="hotel-room data-group-1"><tr>';

  						template += '<td class="cell cell-1">';

  						template += '<span>' + result.rooms[j].hotel_name + '</span></td>';

  						template += '<td class="cell cell-1">';

  						template += '<span>' + result.rooms[j].name + '</span></td>';

  						template += '<td class="cell cell-2">';

  						template += '<span>$ ' + result.rooms[j].price + ' per Person</span></td>';

  						template += '<td class="cell cell-3">';

  						template += '<a class="btn btn-2 book-now-btn" href="';
  						if (result.rooms[j].status == 'Y') {
  							template += '/~travellingfit/app/client/index.php?v=book_package&package=' + result.packages[i].package_id + '&room=' + result.rooms[j].room_type_id + '&event=' + result.packages[i].event_id + '">Book Now</a>';
  						} else {
  							template += 'javascript:;">Sold Out</a>';	
  						}
  						

  						template += '</td></tr></table>';

  					}

  				}

  			}

  			template += '<div id="package-info-' + result.packages[i].package_id + '" style="display:none; margin-top:25px">';
  			template += '<div id="package-imgs-' + result.packages[i].package_id + '" class="col-md-12" style="padding:0!important; margin-bottom:20px;"></div>'
  			// template += '<div id="package-info-' + result.packages[i].package_id + '">';

  			var pattern = /\.\.\/uploads\/source\//g;
  			result.packages[i].description = result.packages[i].description.replace(pattern, '../../app/uploads/source/');	

  			template += '<div class="describing">' + result.packages[i].description + '</div>'

  			if (result.opt_tours.length > 0) {

  				template += '<div class="groups"><div class="panel-group accordion" id="accordion3">';

  				for (var k in result.opt_tours) {

  					if (result.packages[i].package_id == result.opt_tours[k].package_id) {

	  					template += '<div class="panel panel-default"><div class="panel-heading">';

	  					template += '<h4 class="panel-title">';

	  					template += '<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse' + result.opt_tours[k].optional_tour_id + '" aria-expanded="false">' + result.opt_tours[k].title + '</a></h4></div>';

	  					template += '<div id="collapse' + result.opt_tours[k].optional_tour_id + '" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">';

	  					result.opt_tours[k].description = result.opt_tours[k].description.replace(pattern, '../..app//uploads/source/');
	  					template += '<div class="panel-body" style="word-break: break-all;">' + result.opt_tours[k].description + '</div></div></div>';

  					}

  				}

  				template += '</div></div>';

  			}



  			template += '</div></div>';

  		}

  	}

  	$('.package-hotels').html(template);

  	// debugger;

	var elements = $('[id^=package-info-]');
	if (elements.length > 0) {
		for (var i = 0; i < elements.length; i++) {
			var id = jQuery(elements[i]).attr("id");
			jQuery(elements[i]).find('img').wrap("<div class='img-wrap' style='display:table;margin:auto'></div>")
			jQuery(elements[i]).find('.img-wrap').wrap("<div class='col-md-4 img-wrap-col'></div>")
			var imgs = jQuery(elements[i]).find('.img-wrap-col').clone();
			// console.log(imgs)
			jQuery(elements[i]).find('.img-wrap-col').remove();
			id = id.replace('package-info-', 'package-imgs-');
			// console.log(id);
			jQuery("#" + id).html(imgs);
			// jQuery(elements[i]).find('img').css("margin-right", "25px")
			// console.log(jQuery(elements[i]));
		}
	}

  });





	

//   // data = {};

//   // data.event_id = event_id;

//   // data.method = 'getPackagesForEventPreview';

//   // Util.ajaxData(data, function(returnData){

//   //   console.log(returnData);

//   //   $('#packagesForEvent').html(returnData);

    

//   // });

	$(function(){

		var clicked = false;

		$('.become-priority').click(function(){

			if( !clicked ) {

				clicked = true;

				$('.event-posts-dropdown option[value*="DISABLE"]').prop('disabled', true).addClass('disabled');

			}

		});

		

		// Open Tab

		if(window.location.hash){

			var target = $('#'+ window.location.hash.split("#")[1] + '-trigger a');

			if( target.length ){

				target.click();

				/* $('html,body').animate({

					scrollTop: target.offset().top - 35

				}, 700); */

			}

		}





		

	});

})(jQuery);

	function showPackage(package_id, obj) {

		var el = jQuery(".package-details-" + package_id);
		el.siblings().hide();
		jQuery('.heading-name').css('font-size', '24px');
		jQuery('.heading-name').css('font-weight', 600)

		jQuery("#package-info-" + package_id).show();
		// jQuery("#package-title-" + package_id).show();
		jQuery("#show-package-button-" + package_id).hide();
		jQuery("#hide-package-button-" + package_id).show();

		jQuery(".package-content").hide();
		document.location = "#package-details-" + package_id
		jQuery(".package_inclusions").hide();
		document.location = "#package-details-" + package_id


		
		
		// jQuery.scrollTo('#hide-package-button-' + package_id);

	}

	function hidePackage(package_id, obj) {
		var el = jQuery(".package-details-" + package_id);
		el.siblings().show();

		jQuery('.heading-name').css('font-size', '17px');
		jQuery('.heading-name').css('font-weight', 300)

		jQuery("#package-info-" + package_id).hide();
		// jQuery("#package-title-" + package_id).hide();
		jQuery("#show-package-button-" + package_id).show();
		jQuery("#hide-package-button-" + package_id).hide();
		jQuery(".package-content").show();
		document.location = "#package-hotels"
		jQuery(".package_inclusions").show();
		document.location = "#package-hotels"


		// jQuery(".package-details-" + package_id).ScrollTo();
	}



</script>

<?php

});



genesis();