<?php ob_start();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (isset($_GET['token']) && !empty($_GET['token'])) {
    $user = Users::findByToken($_GET['token']);
    if (!empty($user)) {
      $activated = $user->activateUser();
    }
  }
}

?>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1058044937972-s6sjs4sh5rg6qr8a7ckkp4kurhlvatit.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>


    <title>Home</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
 <div class="container">

      <form class="form-signin" role="form" action="index.php?v=login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="input" class="form-control space" placeholder="Username" name="username" required="" autofocus="" autocomplete="off" >
        <div class="space-top space">
            <input type="password" class="form-control" placeholder="Password" name="password" required="" autocomplete="off" >
        </div>
<!--         <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> -->
        <div class="space">
            <input type="submit" class="btn btn-lg btn-primary btn-block" type="submit" name='login' value="Sign in"></button>
        </div>
        <?php if (isset($activated) && $activated) { ?>
          <p>Your account has been activated</p>
        <?php } ?>
        
          <?php
              $fb = new Facebook\Facebook([
                  'app_id' => '572973799518112',
                  'app_secret' => '6e6002d3b8a154e5558296edc08c6f71',
                  'default_graph_version' => 'v2.5',
                  //'default_access_token' => '644988485583207|8Wn_JWx_VefAVwOdKTK-_ZtN2Yo'
              ]);

          $helper = $fb->getRedirectLoginHelper();
          $permissions = ['email']; // optional
          $path = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
          $loginUrl = $helper->getLoginUrl('http://' . $_SERVER['HTTP_HOST'] . $path . 'login-callback.php', $permissions);
          // var_dump($loginUrl);
          echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
          ?>
          
          <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
          <script>
              function onSignIn(googleUser) {
                  // Useful data for your client-side scripts:
                  var profile = googleUser.getBasicProfile();
                  console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                  console.log("Name: " + profile.getName());
                  console.log("Image URL: " + profile.getImageUrl());
                  console.log("Email: " + profile.getEmail());

                  // The ID token you need to pass to your backend:
                  var id_token = googleUser.getAuthResponse().id_token;
                  //console.log("ID Token: " + id_token);

                  // if (id_token != "" && id_token != false && id_token != undefined){
                  //     window.location = "http://li610-125.members.linode.com/booking/index.php?v=package&s=99";
                  // }
              };
          </script>

          <div class="space"><?php
if(isset($_GET['i']))
echo "Incorrect username and / or password.";
?>
        </div>
      </form>

    </div>

</body>
</html>

<?php

if(isset($_POST['login']))
{

	$foundUser = Users::authenticate($_POST['username'], $_POST['password']);

  // var_dump($foundUser);exit;
	// User already enroled in system
	if ($foundUser && $foundUser->status == 'enabled') 
  {

		$session->login($foundUser);
    // var_dump($_SESSION);exit();
        if (isset($_SESSION['redirect'])) {
            $location = "Location: ". $_SESSION['redirect'];
            unset($_SESSION['redirect']);
        } else {
            //$location = "Location: index.php?v=package&s=99";
          if ($_SESSION['generic_is_admin']) {
            $location = "Location: index.php?v=admin_packages";
          } else {
            $location = "Location: index.php?v=my_booking";
          }
            
        }
	}
    else
    {
        // User not in system or incorrect Password
        $location = "Location: index.php?v=login&i=1";
    }

  header($location);

}

ob_flush();

?>
