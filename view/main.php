<?php

// User authentication is checked on each page.
// User authentication and access control to information is primarily done via oracle procedures.
// In the authentication check, if a user is required to be logged in, they will be redirected to the login screen
$view = isset($_GET['v']) ? $_GET['v'] : null;
switch($view)
{				
	case 'login':
		$name 			= 	'login';
		$directory		= 	'view';
	break;

	case 'logout':
		$name 			= 	'logout';
		$directory		= 	'view';
	break;

	case 'sign_up':
		$name 			=	'sign_up';
		$directory		= 	'view';
		break;
// --------------- Start of Booking Form ---------------
	case 'package':

		if($_SESSION['tester'] == "true")
		{
			$name			= 	'package';
		}
		else
		{
			$name			= 	'package.soldout'; // sold out!!!
		}
		$directory		=	'general';
	break;

	case 'admin_packages':


		$name			= 	'admin_packages';

		$directory		=	'control';
		break;

	case 'personal_details':
		$name			= 	'personal_details';
		$directory		=	'general';
	break;
	
	case 'guests':
		$name			= 	'guests';
		$directory		=	'general';
	break;
	
	case 'optional_tours':
		$name			= 	'optional_tours';
		$directory		=	'general';
	break;

	case 'additional_information':
		$name			= 	'additional_information';
		$directory		=	'general';
	break;

	
	case 'payment_method':
		$name			= 	'payment_method';
		$directory		=	'general';
	break;


	case 'payment_method_2':
		$name			= 	'payment_method_2';
		$directory		=	'general';
	break;


	// -- FORM 1 -- Local Entrants - Volunteer Registration
	case 'volunteer':
		$name			= 	'volunteer';

		$name			= 	'local.soldout'; // sold out!!!
		$directory		=	'general';
	break;

	// -- FORM 2 -- Local Entrants - Additional Payments
	case 'payment_method_manual':
		$name			= 	'payment_method_manual';

		$name			= 	'local.soldout'; // sold out!!!
		$directory		=	'general';
	break;

	// -- FORM 3 -- Local Entrants - Choose and pay for event
	case 'payment_method_manual_2':
		$name			= 	'payment_method_manual_2';

		$name			= 	'local.soldout'; // sold out!!!
		$directory		=	'general';
	break;

	case 'payment_confirmation_CC':
		$name			= 	'payment_confirmation_CC';
		$directory		=	'general';
	break;

	case 'payment_confirmation_CC_manual':
		$name			= 	'payment_confirmation_CC_manual';
		$directory		=	'general';
	break;	

	case 'payment_confirmation_CC_manual_2':
		$name			= 	'payment_confirmation_CC_manual_2';
		$directory		=	'general';
	break;

	case 'payment_confirmation_DD_manual_2':
		$name			= 	'payment_confirmation_DD_manual_2';
		$directory		=	'general';
	break;

	case 'payment_confirmation_CC_DD_manual':
		$name			= 	'payment_confirmation_CC_DD_manual';
		$directory		=	'general';
	break;

	case 'payment_confirmation_DD':
		$name			= 	'payment_confirmation_DD';
		$directory		=	'general';
	break;

	case 'book_package':
		$name 			=	'book_package';
//		$directory		=	'general';
		$directory		=	'control';
	break;

    case 'event_preview':
        $name           = 'event_preview';
        $directory      = 'control';
    break;

    case 'terms_and_conditions':
        $name           = 'terms_and_conditions';
        $directory      = 'general';
        break;

    case 'my_booking':
        $name           = 'my_booking';
        $directory      = 'control';
        break;

	case 'delete_booking':
		$name           = 'delete_booking';
		$directory      = 'control';
		break;

	case 'client_payment':
		$name           = 'client_payment';
		$directory      = 'control';
		break;
// --------------- End of Booking Form ---------------

// --------------- Start of Admin System ---------------
	case 'admin_bookings':
		$name 			= 	'admin_bookings';
		$directory		=	'control';
		break;

	case 'admin_transactions':
		$name 			= 	'admin_transactions';
		$directory		=	'control';
		break;


    case 'admin_form_values':
        $name 			= 	'admin_form_values';
        $directory		=	'control';
        break;

    case 'admin_guests':
        $name 			= 	'admin_guests';
        $directory		=	'control';
        break;


	case 'bookings':
		$name			= 	'bookings';
		$directory		=	'admin';
	break;

	case 'guests':
		$name			= 	'guests';
		$directory		=	'admin';
	break;

	case 'accom_bedding':
		$name			= 	'accom_bedding';
		$directory		=	'admin';
	break;

	case 'form_values':
		$name			= 	'form_values';
		$directory		=	'admin';
	break;

	case 'transactions':
		$name			= 	'transactions';
		$directory		=	'admin';
	break;


	case 'error':
		$name			= 	'invalid_url';
		$directory		=	'general';
	break;

// --------------- End of Admin System ---------------

	case 'home':	// Home View
		$name 			= 	'home';
		$directory		= 	'general';
	break;

	default:
		$name 			= 	'admin_bookings';
		$directory		=	'control';
	break;
}


require_once($directory . DS . $name . ".php");

?>