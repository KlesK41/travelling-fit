$(document).ready(function() {
	$("div#internationalTravelPartners").hide()
	getHearAboutUs(function(){
		getInternationalTravelPartners(function(){
			getAdditionalInformationFormData(function(e)
			{
				$("#how_hear_id").val(e.how_hear_id);
				Util.safe_log($("input[name='book_travel_partner_YN']:checked").val());
				if($("input[name='book_travel_partner_YN']:checked").val() == 'Y')
				{
					$("div#internationalTravelPartners").show()
				}
				else
				{
					$("div#internationalTravelPartners").hide()
				}
			});
		});
	});

  /*$("#users_datagrid").dataTable( 
  {
//      "aoColumnDefs": [ { "bSearchable": false, "aTargets": [ 5 ] } ],
//      "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 5 ] } ],
      "iDisplayLength": 50,
      "bServerSide": true,
      "sAjaxSource": "control/datagrids/users_dg.php"
  }
  );
  $('.instructive-text-home').hide();*/
});

function getInternationalTravelPartners(callback)
{
	data        = {};
    data.method     = 'getInternationalTravelPartners';
    Util.ajaxData(data, function(returnData) {
    	$("div#internationalTravelPartners").append("<label>Please select*</label><br>");
	  for(var i = 0; i < returnData.length; i++)
      {
		var travelPartner = '<label for="travel_partner_id_' + returnData[i].travel_partner_id + '"><input type="radio" name="travel_partner_id" id="travel_partner_id_' + returnData[i].travel_partner_id + '" value="' + returnData[i].travel_partner_id + '" required> ' + returnData[i].name + '</label><br>';

		$("div#internationalTravelPartners").append(travelPartner);
      }
      $("div#internationalTravelPartners").append('<label for="travel_partner_id" class="error">Required</label>');
    if (jQuery.type(callback) === "function")
    {
    	callback(returnData);
    }
  });
}

$("input[name='book_travel_partner_YN']").change(function(){

	if($("input[name='book_travel_partner_YN']:checked").val() == 'Y')
	{
		$("div#internationalTravelPartners").show();
	}
	else
	{
		$("div#internationalTravelPartners").hide();
	}
})

function getHearAboutUs(callback)
{
    data        = {};
    data.method     = 'getHearAboutUs';
    Util.ajaxData(data, function(returnData){	
	$("select#how_hear_id option[value!='']").remove();
	  for(var i = 0; i < returnData.length; i++)
      {
        $('#how_hear_id')
         .append($("<option></option>")
         .attr("value",returnData[i].how_hear_id)
         .text(returnData[i].name)); 
      }
        if (jQuery.type(callback) === "function")
	    {
	    	callback(returnData);
	    }
    });
}

function getAdditionalInformationFormData(callback)
{
	data        		= {};
    data.method     	= 'getSavedAdditionalInformationForm';
    Util.ajaxData(data, function(returnData){
    	$("#additionalInformationForm").values(returnData);
    	if (typeof callback == "function") callback(returnData);
    });
}

function saveAdditionalInformationForm(url_action){
	data        = {};
    data.method     = 'saveAdditionalInformationForm';
    data.post_data = $("#additionalInformationForm").values();
    Util.ajaxData(data, function(returnData){
        if(returnData == "S")
		{
			Util.safe_log(data.post_data);

			window.location.href = url_action;
		}
		 else
		{
			alert('Sorry, we are currently unable to continue this booking. Please try again.')
		}
    });
}

$("#next_button").click(function(){

	var url_action = '../~australianoutbac/booking/index.php?v=payment_method';
	url_action = 'http://australianoutbackmarathon.com/booking/index.php?v=payment_method';
	url_action = 'index.php?v=payment_method';  
	// url_action = window.location.protocol+"//"+window.location.host + "/booking/" + 'index.php?v=payment_method';  // force
	if($("#additionalInformationForm").valid())
	{

		saveAdditionalInformationForm(url_action);
	}
	else
	{
		$("form").find(":input.error:first").focus();
	}

});


$("#prev_button").click(function(){

	var url_action = '../~australianoutbac/booking/index.php?v=optional_tours';
	url_action = window.location.protocol+"//"+window.location.host + "/booking/" + 'index.php?v=optional_tours';  // force
	saveAdditionalInformationForm(url_action);

});
