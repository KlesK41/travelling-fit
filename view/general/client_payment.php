<?php
$path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>TravellingFit - Payment</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet"
          id="style_components" type="text/css"/>
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet"
          type="text/css" id="style_color"/>
    <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/styles/bpay.css" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        #direct .space {
            margin-bottom: 15px;
        }

        #direct .label_name {
            font-weight: 700;
        }

        .make-payment {
            margin-bottom: -38px;
        }

        @media (max-width: 768px) {
            .make-payment {
                margin-bottom: 0px;
            }
        }

        @media (min-width: 768px) {
            #direct .label_name {
                text-align: right;
            }
        }
        #creditCardWrapper {
            margin-top: 50px;
        }
        #creditCardWrapper .paragraph {
            margin-bottom: 15px;
        }
    </style>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
    <!-- BEGIN HEADER INNER -->
    <div class="container">
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    Travelling Fit
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?v=logout" class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="container">
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo BASEPATH; ?>">Travelling Fit</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?v=my_booking">My Booking</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span>Continue with Booking</span>
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="panel panel-success align-fihca ">
                        <div class="panel-heading">
                            <h3 class="panel-title">Make a Payment</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Event:
                                </label>
                                <div class="col-md-7">
                                    <?php echo $event->post_title; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Booking ID:
                                </label>

                                <div class="col-md-7">
                                    <?php echo $booking->booking_id; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Package:
                                </label>

                                <div class="col-md-7">
                                    <?php echo $package->name; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Pre-itinerary nights:
                                </label>

                                <div class="col-md-7">
                                    <?php echo $booking->extra_nights_pre; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Post-itinerary nights:
                                </label>

                                <div class="col-md-7">
                                    <?php echo $booking->extra_nights_post; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Guests:
                                </label>

                                <div class="col-md-7">
                                    <?php echo($booking->number_adult_guests + $booking->number_children_guests + 1); ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Adult Runners:
                                </label>

                                <div class="col-md-7">
                                    <?php echo($booking->number_children_guests + 1); ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Deposit per adult runner:
                                </label>

                                <div class="col-md-7">
                                    $<?php echo $roomType->price; ?>
                                </div>
                            </div>
                            <?php if (!empty($optionalTours)) { ?>
                                <div class="form-group col-md-12">
                                    <label class="col-md-4 control-right">
                                        Tours:
                                    </label>

                                    <div class="col-md-7">
                                        <?php foreach ($optionalTours as $tour) { ?>
                                            <?php echo $tour['title']; ?>
                                            <br/>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Total Coast:
                                </label>

                                <div class="col-md-7" name="total_cost">
                                    $ <?php echo $totalCost; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Total Paid:
                                </label>

                                <div class="col-md-7" name="total_paid">
                                    $ <?php echo $booking->total_paid; ?>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-4 control-right">
                                    Amount Due:
                                </label>

                                <div class="col-md-7" name="amount_due">
                                    $ <?php echo($totalCost - $booking->total_paid); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <form id="paymentDetailsValidate"
                              action="<?php echo $_SERVER['SCRIPT_NAME']; ?>?v=payment_confirmation_DD"
                              method="post" class="main">
                            <div class="form-body form-body-payment">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Paymnt Amount:</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-lg" placeholder=""
                                               name="payment_amount">

                                        <div class="form-group" id="payment_options">
                                            <div class="radio-list">
                                                <label>
                                                    <div class="radio" id="uniform-optionsRadios23"><span><input
                                                                type="radio" name="optionsRadios" id="dd_method"
                                                                value="dd"></span></div>
                                                    Direct Deposit
                                                </label>
                                                <label>
                                                    <div class="radio" id="uniform-optionsRadios22">
                                                            <span>
                                                                <input
                                                                    type="radio"
                                                                    name="optionsRadios"
                                                                    id="cc_method"
                                                                    value="cc">
                                                            </span>
                                                    </div>
                                                    Credit Card
                                                </label>
                                                <label>
                                                    <div class="radio" id="uniform-optionsRadios23">
                                                            <span>
                                                                <input
                                                                    type="radio" name="optionsRadios"
                                                                    id="bpay_method"
                                                                    value="bpay">
                                                            </span>
                                                    </div>
                                                    BPay
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <div class="col-md-12" id="credit_card" style="display: none;">
                            <div id="centre-creditCardWrapper">
                                <form class="cmxform" id="reg" method="post"
                                      action="https://transact.nab.com.au/test/directpost/authorise">
                                <div id="creditCardFingerPrint">
                                    <div class="portlet-title well pay-action">
                                        <div class="caption">
                                        <span class=" sbold uppercase">
                                            Credit Card Payment
                                        </span>
                                        </div>
                                    </div>

                                    <!--                            <a href="javascript:;" class="thumbnail">-->
                                    <!--                                                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNTciIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTU3IiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijc4LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTU3eDE4MDwvdGV4dD48L3N2Zz4=" alt="100%x180" style="height: 180px; width: 100%; display: block;" data-src="../assets/global/plugins/holder.js/100%x180"> -->
                                    <div class="wrapper">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Credit card*:</label>

                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <label>
                                                                <input type="radio" name="EPS_CARDTYPE"
                                                                       value="visa">
                                                                VISA
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="EPS_CARDTYPE"
                                                                       value="mastercard">
                                                                MASTERCARD </label>
                                                            <label>
                                                                <input type="radio" name="EPS_CARDTYPE"
                                                                       value="amex">
                                                                AMEX
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="EPS_CARDTYPE"
                                                                       value="diners">
                                                                DINERS
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Card Holders Name*:</label>

                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg"
                                                                   name="cardholders_name" id="cardholders_name"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="display:none">
                                                    <label class="col-md-3 control-label">Amount I would like to
                                                        pay*:</label>

                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg"
                                                                   id="client_payment_amount_enter" disabled>
                                                            <input type="hidden" id="deposit_paid_YN" value="N">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Fee*:</label>

                                                    <div class="col-md-9">
                                                        <label class="control-label">
                                                            <br/>
                                                        </label>
                                                    </div>
                                                </div>


                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Plus credit card bank
                                                            fee*:</label>

                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg"
                                                                       id="client_payment_amount" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                    <!--                            </a>-->
                                </div>
                                <div id="creditCardWrapper" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Payment Amount:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control input-lg" id="display_amount_due" readonly="readonly">
                                         </div>
                                    </div>
                                    <h4>ENTER CREDIT CARD DETAILS:</h4>
                                    <div class="form-group">
                                        <!--Card Holders Name:<br /> !-->
                                        <!-- <input type="text" name="cardholders_name" size="32" class="required">!--><br/>
                                        <label class="col-md-3 control-label">Card Numbers*:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control input-lg" id="EPS_CARDNUMBER" name="EPS_CARDNUMBER" size="32" title="Required" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Expiry Date: (mm/yyyy)*</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="EPS_EXPIRYMONTH" id="EPS_EXPIRYMONTH" title="Required" required>
                                                        <?php
                                                        echo "<option value=''></option>";
                                                        for ($i = 1; $i < 13; $i++)
                                                            echo "<option value='" . $i . "'>" . $i . "</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="EPS_EXPIRYYEAR" id="EPS_EXPIRYYEAR" title="Required" required>
                                                        <?php
                                                        echo "<option value=''></option>";
                                                        for ($i = date('Y'); $i < date('Y') + 6; $i++)
                                                            echo "<option value='" . $i . "'>" . $i . "</option>";

                                                        $timestamp = gmdate('YmdHis', time());
                                                        ?>
                                                    </select><br/>

                                                    <input type="hidden" name="primary_guest_last_name" id="primary_guest_last_name"
                                                           value="<?php echo $primary_guest_last_name ?>"/>
                                                    <!-- credit card variables neccessary to process transaction with NAB direct post !-->
                                                    <input type="hidden" name="EPS_MERCHANT" value="YNX0010"/>
                                                    <input type="hidden" name="EPS_REFERENCEID"/>
                                                    <input type="hidden" name="EPS_AMOUNT"/>
                                                    <input type="hidden" name="EPS_TIMESTAMP"/>
                                                    <input type="hidden" name="EPS_FINGERPRINT"/>
<!--                                                    <input type="hidden" name="EPS_RESULTURL" value="http://webapps.synology.me/aom/response_manual"/>-->
                                                    <input type="hidden" name="EPS_RESULTURL" value="http://<?php echo $_SERVER['HTTP_HOST']?>/<?php echo $_SERVER['SCRIPT_NAME']?>?v=response"/>
                                                    <!-- test !-->
                                                    <!-- <input type="hidden" name="EPS_RESULTURL" value="https://australianoutbackmarathon.com/aom/response_manual" /> <!-- live !-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="EPS_REDIRECT" value="TRUE"/>
                                        <label class="col-md-3 control-label">CVV:</label>
                                        <div class="col-md-9">
                                            <input class="form-control input-lg"type="text" name="EPS_CCV" maxlength="3" size="3" class="required">
                                            <p>Your CVV number is the 3 digits on the back of your card.</p>
                                        </div>
                                    </div>
                                    <h4 class="paragraph">Cancellation & Refund Policy:</h4>
                                    <p class="paragraph">Cancellation must be made in writing to ... Requests received will receive a refund less a $50
                                    administration fee.</p>
                                    <input type="checkbox" name="policy_agree" value="1" class="required" required>I have read and agree to the
                                    cancellation policy<br/>
                                    <label for="policy_agree" class="error" style="display:none;">To proceed you need to read and agree to the
                                        cancelation policy</label>
                                    <div class="paragraph clearfix">
                                        <input class="btn green pull-right" type='submit' name='payment_submit' id='payment_submit' value='PROCESS PAYMENT'><br/>
                                        <input class="btn dafault pull-left" type='button' name='cancel' id='cancel' value='CANCEL'>
                                    </div>
                                    <p class="paragraph">REGISTRANTS PLEASE NOTE: While your request is being processed, be patient and do not try to resubmit the
                                    request.</p>
                                    <p class="paragraph">Processing can take up to 150 seconds.</p>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-12" id="bpay" style="display: none">
                            <div class="portlet-title well pay-action">
                                <div class="caption">
                                        <span class=" sbold uppercase">
                                            BPay
                                        </span>
                                </div>
                            </div>

                            <!--                            <a href="javascript:;" class="thumbnail">-->
                            <!--                                                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNTciIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTU3IiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijc4LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTU3eDE4MDwvdGV4dD48L3N2Zz4=" alt="100%x180" style="height: 180px; width: 100%; display: block;" data-src="../assets/global/plugins/holder.js/100%x180"> -->
                            <div class="wrapper">
                                <div class="bpay-block">
                                    <div class="logo">
                                        <img src="../includes/img/bpay_logo2edited.jpg">
                                    </div>
                                    <div class="code-input">
                                        <label for="BillerCode" style="font-weight: 800">Biller Code: </label><input
                                            id="BillerCode" type="text" placeholder="<XXXX>" size="6">
                                        <br/>
                                        <label for="Ref" style="font-weight: 800">Ref: </label><input id="Ref"
                                                                                                      type="text"
                                                                                                      placeholder="<XXXX XXXX XXXX>"
                                                                                                      size="15">
                                    </div>
                                    <div class="info">
                                        <p><b>Telephone &amp; Internet Banking Bpay</b></p>

                                        <p>Contact yout bank or financial institution to make this payment from your
                                            cheque, savings, debit, credit card or transaction account.</p>

                                        <p>More info: <a href="www.bpa.com.au">www.bpa.com.au</a></p>
                                    </div>
                                </div>
                            </div>
                            <!--                            </a>-->
                        </div>
                        <div class="col-md-12" id="direct" style="display: none">
                            <div class="portlet-title well pay-action">
                                <div class="caption">
                                        <span class=" sbold uppercase">
                                            Direct Deposit Information
                                        </span>
                                </div>
                            </div>

                            <!--                            <a href="javascript:;" class="thumbnail">-->
                            <!--                                                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNTciIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTU3IiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijc4LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTU3eDE4MDwvdGV4dD48L3N2Zz4=" alt="100%x180" style="height: 180px; width: 100%; display: block;" data-src="../assets/global/plugins/holder.js/100%x180"> -->
                            <div class="wrapper">
                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Account Name:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        Travelling Fit
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Bank:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        National Australia Bank
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">BSB:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        082-574
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Account Number:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        83-544-8081
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-12 field-label">Overseas Telegraphic or Wire Transfer
                                        *
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Bank:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        National Australia Bank
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Address:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        Shop 21, 148 The Entrance Road, Erina, NSW, 2250, Australia
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Account Name:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        Travelling Fit
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Account Number:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        83-544-8006
                                    </div>
                                </div>

                                <div class="row space">
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="label_name">Swift Code:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        NATAAU3302S
                                    </div>
                                </div>

                                <div class="space">
                                    * Note: Payment from an overseas bank account (outside of Australia) will incur a
                                    bank fee of $25 per transaction. Please remember to add this fee to the total when
                                    making payment.
                                </div>
                            </div>
                            <!--                            </a>-->
                        </div>

                        <div class="col-md-12">
                            <div class="form-actions pay-action">
                                <div class="buttons-wrapp" style="text-align: center">
                                    <a href="javascript:;" class="btn green make-payment"
                                       onclick="makePayment('<?php echo $booking->booking_id ?>', '<?php echo $_SESSION["generic_user_id"] ?>', '<?php echo $booking->event_id ?>')">
                                        Make Payment</a>
                                </div>
                                <?php if ($booking->total_paid == 0) {?>
                                <button type="button" id="back-btn" class="btn dafault btn-outline"
                                        onclick="document.location = '<?php echo $_SERVER["SCRIPT_NAME"] ?>?v=book_package&package=<?php echo $booking->package_id; ?>&room=<?php echo $booking->room_type_id; ?>&event=<?php echo $booking->event_id; ?>&booking=<?php echo $booking->booking_id; ?>'">
                                    << Back to Booking Form
                                </button>
                                <?php } ?>
                                <button type="submit" class="btn green pull-right"
                                        onclick="document.location = '<?php echo $_SERVER["SCRIPT_NAME"]; ?>?v=my_booking'">
                                    Dashboard
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title well">
                            <div class="caption">
                            <span class=" sbold uppercase">
                                Payment History
                            </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-hover table-light">
                                    <thead>
                                    <tr class="uppercase">
                                        <th> Data</th>
                                        <th> Transaction ID</th>
                                        <th> Payment Type</th>
                                        <th> Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody id="transactionsTable">
                                    <?php if (!empty($transactions)) {
                                        foreach ($transactions as $transaction) { ?>
                                            <tr>
                                                <td> <?php echo date('d/m/Y', strtotime($transaction->ts)); ?></td>
                                                <td> <?php echo $transaction->transaction_id; ?></td>
                                                <td> <?php echo $transaction->getPaymentMethod(); ?></td>
                                                <td> $<?php echo $transaction->amount; ?></td>
                                            </tr>
                                        <?php }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="paginator">
                        <?php echo $paginator;?>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <!-- <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
            <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
               title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase
                Metronic!</a>
        </div> -->
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>
<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
<script
    src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
    type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js"
        type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="../includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="../includes/assets/layouts/global/scripts/quick-sidebar.min.js"></script>
<script src="../view/general/client_payment.js" type="text/javascript"></script>
<script src="../includes/js/lib/utilities.js" type="text/javascript"></script>

<script src="../view/general/payment_method.js"></script>
<script src="../includes/js/cc/jquery.creditCardValidator.js"></script>

<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
