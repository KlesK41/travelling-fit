$(document).ready(function() {
  $('#personalDetailsValidate').validate();
  
  getEvents();

  $( "#dob" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0"
  });
});

function getEvents()
{
    data        = {};
    data.method     = 'getEvents';
    data.child_or_adult = 'C';
    Util.ajaxData(data, function(returnData){
      Util.safe_log(returnData);
      
      for(var i = 0; i < returnData.length; i++)
      {
        Util.safe_log(returnData[i].prefered_events);
        var event_id = returnData[i].event_id;
         $("div#preferedEvents").append("<input type='radio' id='prefered_event_id" + event_id + "' name='prefered_event_id' value='" + event_id + "' required>" +
                                        "<label for='prefered_event_id_" + event_id + "'>" + 
                                        returnData[i].event_name + 
                                        "</label>" + 
                                        "<br>"
                                        );
              

        // return returnData;
      }
      $("div#preferedEvents").append('<label for="prefered_event_id" class="error">Required</label>');
    });
}

function getAccommodation(number_guests)
{

    data        = {};
    data.method     = 'getAccommodation';
    data.number_guests = number_guests;
    Util.ajaxData(data, function(returnData){
      $("select#accommodation_id option[value!='']").remove();
      for(var i = 0; i < returnData.length; i++)
      {
        $('#accommodation_id')
         .append($("<option></option>")
         .attr("value",returnData[i].accommodation_id)
         .text(returnData[i].accommodation_name)); 
        Util.safe_log(returnData[i].accommodation_id);
        Util.safe_log(returnData[i].accommodation_name);
      }
      if($("#accommodation_id option:selected").val() != "")
      {
        getBedding();  
      }
    });
}

function getBedding()
{
    data        = {};
    data.method     = 'getBedding';
    data.accommodation_id = $("#accommodation_id option:selected").val();
    Util.ajaxData(data, function(returnData){
      $("select#bedding_id option[value!='']").remove();
      for(var i = 0; i < returnData.length; i++)
      {
        $('#bedding_id')
         .append($("<option></option>")
         .attr("value",returnData[i].bedding_id)
         .text(returnData[i].bedding_name)); 
        Util.safe_log(returnData[i].bedding_id);
        Util.safe_log(returnData[i].bedding_name);
      }
    });

}
$('#accommodation_id').change(function(){
  getBedding();
})
$("#number_adults, #number_children").change(function(){
  numberGuests();
})

function numberGuests()
{
  var number_guests = parseInt($("#number_adults").val()) + parseInt($("#number_children").val());
  $("div#number_guests").html(number_guests);
  getAccommodation(number_guests);
}



// $("#packageValidate").submit(function() {
//     if($("#packageValidate").validate().checkForm())
//     {

//         $.post("includes/CRUD.php", $("#packageValidate").serialize() ,
//         function(data){
//             if(data == "S")
//             {

//             }
//             else
//             {
//                 alert('Sorry, we are currently unable to store this booking. Please try Submitting it again.')
//             }
//         }, "json");
//     }
//     return false;
// });
