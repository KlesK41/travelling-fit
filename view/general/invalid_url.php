<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="entry-content" itemprop="text"><p>Bookings for the 2016 Australian Outback Marathon will be opening soon</p>
<p><a title="Contact Us" href="http://australianoutbackmarathon.com/contact/">Contact us today </a>to receive pre-notification of when bookings go live and be amongst the first to reserve your place in Australia’s most Iconic running event.</p>
</div>

</body>
</html>

<?php
