<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>TravellingFit - My Bookings</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar">
        <!-- BEGIN HEADER INNER -->
        <div class="container">
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                          Travelling Fit  
                      </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- END TODO DROPDOWN -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-quick-sidebar-toggler">
                            <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=logout';?>" class="dropdown-toggle">
                                <i class="icon-logout"></i>
                            </a>
                        </li>
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER INNER -->
    </div>
    <div class="container">
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <ul class="page-breadcrumb">
<!--                                <li>-->
<!--                                    <i class="icon-home"></i>-->
<!--                                    <a href="index.html">Home</a>-->
<!--                                    <i class="fa fa-angle-right"></i>-->
<!--                                </li>-->
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="<?php echo BASEPATH;?>">Travelling Fit</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <span>My Booking</span>
                                </li>
                            </ul>
                    </div>
                    <h3 class="page-title"> 
                                          Welcome back <span class="name"></span>
                                      </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_0" data-toggle="tab">Current Bookings</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1" data-toggle="tab"> Past Bookings  </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <?php if (isset($currentBookings)) {
                                            foreach ($currentBookings as $current) { ?>
                                            <div>
                                                <div class="group-tab" style="margin-bottom: 20px;">
                                                    <div class="take-desc">
                                                        <?php echo $current['name']; ?>
                                                    </div>
                                                    <div class="buttons-wrapp" style="float: right">
                                                        <?php if ($current['total_paid'] == 0) { ?>
                                                            <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=book_package&package=' . $current["package_id"] . '&room=' . $current["room_type_id"] . '&event=' . $current["event_id"] . '&booking=' . $current["booking_id"]?>"
                                                               class="btn green">Edit booking</a>
                                                        <?php } ?>
                                                        <?php if ($current['completed'] === 'Y') { ?>   
                                                        <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=client_payment&booking_id=' . $current['booking_id']; ?>" class="btn green">Make Payment</a>
                                                        <?php } ?>
                                                        <?php if ($current['archieved_yn'] === 'N') { ?>
                                                            <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=delete_booking&booking_id=' . $current['booking_id']; ?>" class="btn green">Delete Booking</a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } } ?>
                                    </div>
                                    <div class="tab-pane" id="tab_1">
                                        <?php if (isset($pastBookings)) {
                                            foreach ($pastBookings as $past) { ?>
                                                <div>
                                                    <div class="group-tab" style="margin-bottom: 20px;">
                                                        <div class="take-desc">
                                                            <?php echo $past['name']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <!-- <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
                <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
            </div> -->
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->
    <!--[if lt IE 9]>
<script src="includes/assets/global/plugins/respond.min.js"></script>
<script src="includes/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="../includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="../includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="../includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="../includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="../includes/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
