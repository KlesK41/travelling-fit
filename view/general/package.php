<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title> 
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<input type="hidden" id="package_id" value="<?php echo isset($_GET['package_id']) ? $_GET['package_id'] : '' ?>">
<input type="hidden" id="package" value="<?php echo isset($_GET['package']) ? $_GET['package'] : '' ?>">
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
        </div>
      </div>
    </div>
	
    <form id="packageValidate" action="../~australianoutbac/booking/index.php?v=guests" method="post" class="main">
    <div id="page-wrapper" class="container marginTop external-form">
      <fieldset>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li id="formName">Packages</li> 
       </ol>
      <!-- information panel !-->

      <!-- panel 1 !-->
      <div class="panel panel-default">
      <div id="packagesWrapper">
        <input type="hidden" name="method" value="storePackageForm" />
          <div class="bs-package">
            <div class="row">
              <h2>Itinerary Information</h2>
              <!-- <p><strong>Please note:</strong> This booking form does not generate a quote.<br> 
              Once completed one of our Australian Outback Marathon specialists will be in touch with final payment information.</p> !-->
            </div>
            <div class="row">
              <h4>Which package would you like to book?*</h2>
            </div>
            <div class="row">
              <div id="packages" class="col-xs-12 col-sm-12"></div>
            </div>            
            <div class="row">
              <h4>Accommodation</h2>
            </div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-4 field-label">Number of adults*</div>
				<div class="col-xs-12 col-sm-8">
					<select name="number_adults" id="number_adults" title="Required" required>
						<option value="">Please select</option>
            <option name="1">1</option>
						<option name="2">2</option>
						<option name="3">3</option>
						<option name="4">4</option>
						<option name="5">5</option>
						<option name="6">6</option>
					</select>
				</div>
			</div>
			
            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label">
              Number of children<br>
              <span class="sub-text">(a child is considered 12 years or under at the time of check-in)</span>
              </div>
              <div class="col-xs-12 col-sm-8">
              <select name="number_children" id="number_children">
                <option name="0">0</option>
                <option name="1">1</option>
                <option name="2">2</option>
                <option name="3">3</option>
                <option name="4">4</option>
                <option name="5">5</option>
              </select>
              </div>
            </div>
			
            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label">Total number of guests</div>
              <div class="col-xs-12 col-sm-8">
				<div id="number_guests" class="single-value"></div>
              </div>
            </div>
			      <div class="row">
              <div class="col-xs-12 col-sm-12 field-label notice">
              Please Note: All accommodation is now on a request basis and will be confirmed once your booking has been received
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label required">
              Accommodation Type*
              </div>
              <div class="col-xs-12 col-sm-8">
              <select name="accommodation_id" id="accommodation_id" title="Required" required>
                <option value="">Please select</option>
              </select>
              </div>
            </div>
			
<!--             <div class="row">
              <div class="col-xs-12 col-sm-4 field-label">
              Bedding
              </div>
              <div class="col-xs-12 col-sm-8">
              
      <select name="bedding_id" id="bedding_id" title="Required" required>
                <option value="">Please select</option>
              </select> 
              </div>
            </div> -->
			     <!-- force this based on number of adults -->
            <input type="hidden" id="bedding_id" name="bedding_id" value="">

            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label required">
              Bedding Configuration*
              </div>
              <div class="col-xs-12 col-sm-8">
              <select name="room_type_id" id="room_type_id" title="Required" required>
                <option value="">Please select</option>
              </select>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label required">
              Extra Night(s) - Pre Itinerary
              </div>
              <div class="col-xs-12 col-sm-8">
              <select name="extra_nights_pre" id="extra_nights_pre" title="Required" required>
                <option value="0">No Thanks</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
              </div>
            </div>
			
            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label">
              Extra Night(s) - Post Itinerary
              </div>
              <div class="col-xs-12 col-sm-8">
              <select name="extra_nights_post" id="extra_nights_post">
                <option value="0">No Thanks</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
              </div>
            </div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-4 field-label required">
					Share Request
				</div>
				
				<div class="col-xs-12 col-sm-8 input-el-inline">
					<input type="radio" name="share_request_YN" id="share_request_N" value="N" required checked="checked"><label for="share_request_N">No</label>
					<input type="radio" name="share_request_YN" id="share_request_Y" value="Y" required><label for="share_request_Y">Yes</label>
					<label for="share_request_YN" class="error">Required</label>
				</div>
			</div>
		
            <div class="row">
              <div class="col-xs-12 col-sm-4 field-label required">
              Do you require a rollaway or cot?
              </div>
              <div class="col-xs-12 col-sm-8 input-el-inline">
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_N" value="N" required checked="checked"><label for="rollaway_or_cot_N">No</label>
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_R" value="R" required><label for="rollaway_or_cot_R">Rollaway</label>
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_C" value="C" required><label for="rollaway_or_cot_C">Cot</label>
                <label for="rollaway_or_cot" class="error">Required</label>
              </div>
            </div>
		
			<div class="row buttons-container">
				<div class="col-xs-12 col-sm-8"></div>
				<div class="col-xs-6 col-sm-4"><input  class="submit" type="submit" value="Next"/><span class='glyphicon glyphicon-circle-arrow-right'></span></div>
			</div>
	  
		</div><!--bs-package-->
	</div>
	</div>
    </fieldset>
	</div>
  </form>


<script src="includes/js/jquery.min.js"></script>
<script src="includes/js/validate/jquery.validate.min.js"></script>  
<script src="includes/js/jquery.dataTables.min.js"></script>
<script src="includes/js/bootstrap/bootstrap.min.js"></script>
<script src="includes/js/lib/utilities.js"></script>
<script src="view/general/package.js"></script>
</body>
</html>