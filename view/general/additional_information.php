<?php 
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
			</div>
		</div>
	</div>
	
	<form id="additionalInformationForm" action="index.php?v=payment_method" method="post" class="main">
	<?php
		$number_guests    = $_SESSION['number_guests'];
		echo "<input type='hidden' name='number_guests' id='number_guests' value='" . $_SESSION['number_guests'] . "'>";
	?>
    <div id="page-wrapper" class='container marginTop external-form'>
	
		<!-- breadcrumbs !-->
		<ol class="breadcrumb"><li>Additional Information</li></ol>
	  
		<!-- information panel !-->
		<div class="panel panel-default">
			<div id="personalDetailsWrapper">
				<div class="bs-additional-information">
		
					<div class="row space more-space">
						<div class="col-xs-12 col-sm-12">
							<h4>Travel Insurance<h4>
						</div>
						
						<div class="col-xs-12 col-sm-12">
							<p class="space">Travel Insurance is not included in the price of the package. Please read clause 13 of the Booking Terms & Conditions before making your decision.</p>
							<label for="travel_insurance_1"><input type="radio" value="1" id="travel_insurance_1" name="travel_insurance" required checked="checked">
							I will contact Travelling Fit at <a href="mailto:sales@australianoutbackmarathon.com?Subject=Travel%20Insurance" target="_top">
sales@australianoutbackmarathon.com</a> to arrange my insurance cover</label><br><br>
							<label for="travel_insurance_2"><input type="radio" value="2" id="travel_insurance_2" name="travel_insurance" required>
							I would like to take out Travel Insurance but would prefer Travelling Fit to issue my policy at the retail price. Please send me all the details</label><br><br>
							<label for="travel_insurance_3"><input type="radio" value="3" id="travel_insurance_3" name="travel_insurance" required>
							I do not wish to take out travel insurance through Travelling Fit - Warning: please refer clause 13.2 & 13.3 of the Booking Terms & Conditions before accepting this option</label>
						</div>
					</div>

					<div class="row more-space">
					<div class="col-xs-12 col-sm-6">
							<h4>Inspirational Story</h4>
					</div>
						<div class="col-xs-12 col-sm-12">
							<p class="field-label space">Does anyone on the booking have an inspirational story that you would like to share?</p>
							<textarea class="deftext large-input" id="inspirational_story" name="inspirational_story" cols="80" rows="5" title="None" placeholder="None"></textarea>
						</div>
					</div>
		
					<div class="row space">
						<div class="col-xs-12 col-sm-12 field-label small-space">Have you booked through one of our International Travel Partners?</div>
						<div class="col-xs-12 col-sm-12">
							<label for="book_travel_partner_Y"><input type="radio" value="Y" name="book_travel_partner_YN" id="book_travel_partner_Y">
							Yes</label>
							<label for="book_travel_partner_N"><input type="radio" value="N" name="book_travel_partner_YN" id="book_travel_partner_N" checked>
							No</label>
						</div>
					</div>
		
					<div class="row space">
						<div class="col-xs-12 col-sm-10">
							<div id="internationalTravelPartners">
								<br>
							</div>
						</div>
					</div>

					<div class="row space">
						<div class="col-xs-12 col-sm-6">
							<h4>Emergency Contact</h4>
						</div>
						<div class="col-xs-12 col-sm-6"></div>
					</div>

					<div class="row space">
						<div class="col-xs-12 col-sm-3 field-label">First Name*</div>
						<div class="col-xs-12 col-sm-9">
							<input id="ec_firstname" type="text" name="ec_firstname" autocomplete="OFF" title="Required" required>		 
						</div>
					</div>

					<div class="row space">
						<div class="col-xs-12 col-sm-3 field-label">Last Name*</div>
						<div class="col-xs-12 col-sm-9">	
							<input id="ec_lastname" type="text" name="ec_lastname" autocomplete="OFF" title="Required" required>		 
						</div>
					</div>
		
					<div class="row space">
						<div class="col-xs-12 col-sm-3 field-label">Phone*</div>
						<div class="col-xs-12 col-sm-9">
							<input id="ec_phone" type="text" name="ec_phone" autocomplete="OFF" title="Required" required>		 
						</div>
					</div>
		
					<div class="row space">
						<div class="col-xs-12 col-sm-3 field-label">Email Address*</div>
						<div class="col-xs-12 col-sm-9">	
							<input id="ec_email" type="text" name="ec_email" autocomplete="OFF" title="Required" required>		 
						</div>
					</div>
		
					<div class="row space">
						<div class="col-xs-12 col-sm-3 field-label">How did you hear about us?*</div>
						<div class="col-xs-12 col-sm-9">	
							<select id="how_hear_id" name="how_hear_id" title="Required" required>
								<option selected="selected" value="Not Selected">Please Select...</option>
							</select>		 
						</div>
					</div>

					<div class="row space">
						<div class="col-xs-12 col-sm-3"></div>
						<div class="col-xs-12 col-sm-8">
						<input type="checkbox" value="Y" id="ack_tc_yn" name="ack_tc_yn" title="Required" required>
							By ticking this box, I acknowledge that I have read, understood and accept all
							<a href="http://australianoutbackmarathon.com/terms-conditions/" target="_blank">Terms and Conditions</a>
							and that I am authorised to accept these
							<a href="http://australianoutbackmarathon.com/terms-conditions/" target="_blank">Terms and Conditions</a>
							for all persons named as part of this booking<br>
							<label for="ack_tc_yn" class="error required" >Required</label>
						</div>

					</div>
		
					<div class="row space buttons-container">
						<div class="col-xs-12 col-sm-8 btn-left">
							<span class='glyphicon glyphicon-circle-arrow-left'></span>
							<input type="button" id="prev_button" value="Previous">
						</div>
						<div class="col-xs-6 col-sm-4 btn-right">
							<input type="button" id="next_button" value="Next">
							<span class='glyphicon glyphicon-circle-arrow-right'></span>
						</div>  
					</div>
		
				</div>
			</div>
		</div>
	</form>
	
<script src="includes/js/jquery.min.js"></script>
<script src="includes/js/validate/jquery.validate.min.js"></script>  
<script src="includes/js/ui/minified/jquery-ui.custom.min.js"></script>  
<script src="includes/js/jquery.dataTables.min.js"></script>
<script src="includes/js/bootstrap/bootstrap.min.js"></script>
<script src="includes/js/validate/jquery.validate.min.js"></script>  
<script src="includes/js/lib/utilities.js"></script>
<script src="includes/js/lib/values.js"></script>
<script src="view/general/additional_information.js"></script>
</body>
</html>