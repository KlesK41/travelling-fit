$(document).ready(function(){
    $('[name="payment_amount"]').on('keyup', function(e) {
        //if ($('#cc_method').parent().hasClass("checked")) {
            var val = $(e.target).val();
            $('#client_payment_amount_enter').val(val).keyup();
        //}
    })


    $('#BillerCode').keyup( function() {
        var $this = $(this);
        if($this.val().length > 4)
            $this.val($this.val().substr(0, 4));
    });
    $('#Ref').keyup( function() {
        var $this = $(this);
        if($this.val().length > 12)
            $this.val($this.val().substr(0, 12));
    });


    $('[name="optionsRadios"]').on("change", function(e) {
        //console.log(e.target);
        if ($(e.target).val() == 'bpay') {
            $('#credit_card').hide();
            $('#direct').hide();
            $('#bpay').show();
        }
        if ($(e.target).val() == 'cc') {
            $('#credit_card').show();
            $('#direct').hide();
            $('#bpay').hide();
        }
        if ($(e.target).val() == 'dd') {
            $('#credit_card').hide();
            $('#direct').show();
            $('#bpay').hide();
        }
    })
});

function goToPage(page, user, booking) {
    var data = {};
    data.page = page;
    data.user = user;
    data.booking = booking;
    data.method = 'getTransactionsForPage';
    Util.ajaxData(data, function(returnData) {
        $('#transactionsTable').html(returnData.template);
        $('#paginator').html(returnData.paginator);
        //Util.safe_log(returnData.message);
    });
}


function makePayment(booking_id, user_id, event_id) {


    var payment_amount = $("[name='payment_amount']").val();
    payment_amount = parseFloat(payment_amount);

    //console.log(payment_amount);
    if (!isNaN(payment_amount) && payment_amount > 0) {

        var amount_due = $('[name="amount_due"]').html();
        var total_cost = $('[name="total_cost"]').html();
        var total_paid = $('[name="total_paid"]').html();
        total_cost = total_cost.replace(/[\D\s]+/gm, "");
        amount_due = amount_due.replace(/[\D\s]+/gm, "");
        total_paid = total_paid.replace(/[\D\s]+/gm, "");

        if ($('#bpay_method').parent().hasClass('checked')) {

            if (!isNaN(parseFloat(amount_due)) && !isNaN(parseFloat(total_cost))) {
                if (parseFloat(payment_amount) <= parseFloat(amount_due)) {
                    data = {};
                    data.method = 'getBpayRef';
                    data.booking_id = booking_id;
                    data.user_id = user_id;
                    data.event_id = event_id;
                    data.amount_due = amount_due;
                    data.total_cost = total_cost;
                    data.payment_amount = payment_amount;
                    Util.ajaxData(data, function (returnData) {

                        document.location = returnData.returnUrl;
                        console.log(returnData);
                        $("#Ref").val(returnData.bpay_code);
                        $('[name="amount_due"]').html('$ ' + returnData.amount_due);
                        $('[name="total_paid"]').html('$ ' + returnData.paid);
                        $("[name='payment_amount']").val("");
                        var template = '<tr>';
                        template += '<td>' + returnData.transaction.date + '</td>';
                        template += '<td>' + returnData.transaction.id + '</td>';
                        template += '<td>' + returnData.transaction.type + '</td>';
                        template += '<td>$' + returnData.transaction.amount + '</td>';
                        template += '</tr>';
                        reloadPaginator(booking_id, user_id);
                        //$('#transactionsTable').append(template);
                        $("#back-btn").hide()
                    });
                } else {
                    alert("Payment amount must be less or equals " + amount_due);
                }
            }
        }
        if ($('#dd_method').parent().hasClass('checked')) {
            //alert(123);
            if (!isNaN(parseFloat(amount_due)) && !isNaN(parseFloat(total_cost))) {
                if (parseFloat(payment_amount) <= parseFloat(amount_due)) {
                    data = {};
                    data.method = 'makeDDPayment';
                    data.booking_id = booking_id;
                    data.user_id = user_id;
                    data.event_id = event_id;
                    data.amount_due = amount_due;
                    data.total_cost = total_cost;
                    data.payment_amount = payment_amount;
                    Util.ajaxData(data, function (returnData) {

                        document.location = returnData.returnUrl;

                        console.log(returnData);
                        $('[name="amount_due"]').html('$ ' + returnData.amount_due);
                        $('[name="total_paid"]').html('$ ' + returnData.paid);
                        $("[name='payment_amount']").val("");
                        var template = '<tr>';
                        template += '<td>' + returnData.transaction.date + '</td>';
                        template += '<td>' + returnData.transaction.id + '</td>';
                        template += '<td>' + returnData.transaction.type + '</td>';
                        template += '<td>$' + returnData.transaction.amount + '</td>';
                        template += '</tr>';
                        reloadPaginator(booking_id, user_id);
                        //$('#transactionsTable').append(template);
                        $("#back-btn").hide()
                    });
                } else {
                    alert("Payment amount must be less or equals " + amount_due);
                }
            }
        }

        if ($('#cc_method').parent().hasClass('checked')) {
            if($("#cardholders_name").val() !== "" && $("#client_payment_amount_enter").val() !== "")
            {
                data        = {};
                data.method     = 'getFingerPrint';
                data.amount     = $("#client_payment_amount").val();
                data.cardholders_name = $("#cardholders_name").val();
                data.client_payment_amount_enter = $("#client_payment_amount_enter").val();
                data.amount_due = amount_due;
                data.payment_amount = payment_amount;
                Util.ajaxData(data, function(returnData){
                    Util.safe_log(returnData.message);
                    if(returnData.message == "S")
                    {
                        $("input[name='EPS_REFERENCEID']").val(returnData.transaction_id);
                        $("input[name='EPS_AMOUNT']").val(returnData.amount);
                        $("input[name='EPS_TIMESTAMP']").val(returnData.timestamp);
                        $("input[name='EPS_FINGERPRINT']").val(returnData.finger_print);



                        // great! We have a fingerprint.
                        $("#payment_total_with_fee").show();
                        $("#creditCardWrapper").show();
                        $("#creditCardFingerPrint").hide();
                        $("#display_amount_due").val('$' + $("#client_payment_amount").val());

                    }
                    else if (returnData == "NOT_ENOUGH_FOR_DEPOSIT")
                    {
                        alert('Sorry, you need specify a bigger amount. If you\'re paying a deposit, please enter the deposit amount');
                        return false;
                    }
                    else
                    {

                        alert('Sorry, we are currently having trouble with this transaction. Please try again.')
                        $("#creditCardWrapper").hide();
                        $("#creditCardWrapper").hide();
                        $("#payment_total_with_fee").hide();
                        $("#prev_book_buttons").show();
                        $("#directDebitWrapper").hide();
                        $("#payment_method_CC").attr('checked', false);

                        console.log(returnData);
                    }

                    var t_paid = parseFloat(total_paid) + parseFloat(payment_amount);

                    $('[name="amount_due"]').html('$ ' + (amount_due - payment_amount));
                    $('[name="total_paid"]').html('$ ' + t_paid);
                    $("[name='payment_amount']").val("");
                    var template = '<tr>';
                    template += '<td>' + returnData.date + '</td>';
                    template += '<td>' + returnData.tranactionId + '</td>';
                    template += '<td>Credit Card</td>';
                    template += '<td>$' + payment_amount + '</td>';
                    template += '</tr>';
                    reloadPaginator(returnData.booking, returnData.user);
                    //$('#transactionsTable').append(template);
                    $("#back-btn").hide()

                });
            }
            else
            {
                alert('Please enter all required fields');
            }
        }

    } else {
        alert("Payment amount can not be empty and must be greater than 0");
    }
}

function reloadPaginator(booking, user) {
    var first = $("[page-number='1']");
    if (first.length > 0) {
        first.click();
    } else {
        goToPage(1, user, booking)
    }
}