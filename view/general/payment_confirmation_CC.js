$(document).ready(function() {

  $("#creditCardWrapper").hide();
  $("#directDebitWrapper").hide();
  $("#creditCardFingerPrint").hide();
  if($("#booking_code").length > 0)
  {
    $("#directDebitWrapper").hide();
    $("#payment_options").hide();
    $("#depositWrapper").hide();
    $("#prev_book_buttons").hide();
    $("#creditCardWrapper").show();
    $("#continue_payment").show();
    $("input#cancel").hide();
    
  }
});

$("[name=payment_method]").change(function(){

  if($("#payment_method_DD").is(':checked')) {
    $("#creditCardWrapper").hide();
    $("#creditCardFingerPrint").hide();
    $("#prev_book_buttons").show();
    $("#directDebitWrapper").show();
  }

  if($("#payment_method_CC").is(':checked')) {
    $("#directDebitWrapper").hide();
    $("#prev_book_buttons").hide();
    $("#creditCardFingerPrint").show();
  }
  
});


$("input#cancel").click(function(){
    $("#creditCardWrapper").hide();
    $("#creditCardWrapper").hide();
    $("#prev_book_buttons").show();
    $("#directDebitWrapper").hide();
    $("#payment_method_CC").attr('checked', false);
});

// --------------------------------------------------------------------------- HERE
$("#proceedToCCPayment").click(function(){
  // getFingerPrint
  /*
  data        = {};
  data.method     = 'getFingerPrint';
  Util.ajaxData(data, function(returnData){
  if(returnData == "S")
  {
    // url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
    // window.location.href = url_redirect;
  }
  else
  {
    alert('Sorry, the credit card facility is currently experiencing problems. Please try again.')
  }
  */
  $("#creditCardWrapper").show();
  $("#creditCardFingerPrint").hide();
});



function getTransactionInformation()
{
    data = {};
    data.method = 'getTransactionInformation';
    Util.ajaxData(data, function(returnData){

    });
}

function processPaymentInputForm(url_action){
    data        = {};
    data.method     = 'processPayment';
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      // url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      // window.location.href = url_redirect;
    }
    else
    {
      alert('Sorry, we are currently unable to continue this booking. Please try again.')
    }
    });
}

function savePaymentMethodFormInputs(url_action){
    data        = {};
    data.method     = 'savePaymentMethod';
    data.post_data  = $("#paymentDetailsValidate").values();
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      var url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      window.location.href = url_redirect;
    }
    else
    {
      alert('Sorry, we are currently unable to continue this booking. Please try again.')
    }
    });
}

$("#reset_button").click(function(){
  // clear session variable
    data        = {};
    data.method     = 'clearSession';
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      var url_action = '../~australianoutbac/booking/index.php?v=package';
      var url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      window.location.href = url_redirect;
    }
  });
});

$("#next_button").click(function(){

  var url_action = '../~australianoutbac/booking/index.php?v=payment_method&dd=1';
  url_action = 'index.php?v=payment_method&dd=1';
  if($("#paymentDetailsValidate").valid())
  {
    // process NAB details AND / OR send emails
    savePaymentMethodFormInputs(url_action);
  }
});

$("#prev_button").click(function(){
    var url_action = '../~australianoutbac/booking/index.php?v=additional_information';
    url_action = 'index.php?v=additional_information';
    savePaymentMethodFormInputs(url_action)
});

/*$("#button1").on("click", function(){
  alert('button1');
});

$("#call_ajax").on("click", function(){
  getSomething(1);
});


$('a.helpLink').on("click", function(){
	Util.safe_log($(this).attr('id'));
	$('div#' + $(this).attr('id') + 'Section').toggle();
	return false;
});

/*
*	getSomething(id)
* 	For demonstration purposes
*	An ajax call
*
*	@param {something_id} - id of an entity
*/

/*function getSomething(something_id)
{
		data 				= {};
		data.method 		= 'getSomething'
		data.something_id	= something_id;
		Util.ajaxData(data, function(returnData){
      Util.safe_log(returnData);
      alert(returnData[0].something_id + " " + returnData[0].attribute1 + " " + returnData[0].attribute2);
		});
}*/

