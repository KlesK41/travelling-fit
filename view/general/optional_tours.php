<?php
// print_r($_SESSION);
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
			</div>
		</div>
	</div>
				
	<form id="optionalToursForm" action="index.php?v=additional_information" method="post" class="main">
		
		<input type='hidden' name='number_guests' id='number_guests' value='<?php echo $_SESSION['number_guests'] ?>'>
		<div id="page-wrapper" class='container marginTop external-form'>
			<!-- breadcrumbs !-->
			<ol class="breadcrumb"> 
				<li>Optional Tours</li> 
			</ol>

			<div class="panel panel-default">
				<div class="bs-optional-tours">
					<div class="row margins">
						<h2>Which Optional Tours would you like to book?</h2>
						<div id="divtouroptions"></div>
						
						<div class="row space buttons-container">
							<div class="col-xs-12 col-sm-8 btn-left">
								<span class='glyphicon glyphicon-circle-arrow-left'></span>
								<input type="button" id="prev_button" value="Previous">
							</div>
							
							<div class="col-xs-6 col-sm-4 btn-right">
								<input type="button" id="next_button" value="Next">
								<span class='glyphicon glyphicon-circle-arrow-right'></span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </form>
	
<script src="includes/js/jquery.min.js"></script>
<script src="includes/js/jquery.dataTables.min.js"></script>
<script src="includes/js/bootstrap/bootstrap.min.js"></script>
<script src="includes/js/lib/utilities.js"></script>
<script src="includes/js/validate/jquery.validate.min.js"></script>
<script src="includes/js/lib/values.js"></script>
<script src="view/general/optional_tours.js"></script>
</body>
</html>