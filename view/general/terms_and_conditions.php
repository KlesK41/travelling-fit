<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Metronic | Form Layouts</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <style type="text/css">
        .text_term h4 {
            font-weight: 600;
        }
    </style>
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar">
        <!-- BEGIN HEADER INNER -->
        <div class="container">
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                          Travelling Fit  
                      </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- END TODO DROPDOWN -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="includes/assets/layouts/layout/img/avatar3_small.jpg" />
                                <span class="username username-hide-on-mobile"> Nick </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="page_user_profile_1.html">
                                        <i class="icon-user"></i> My Profile </a>
                                </li>
                                <li>
                                    <a href="app_calendar.html">
                                        <i class="icon-calendar"></i> My Calendar </a>
                                </li>
                                <li>
                                    <a href="app_inbox.html">
                                        <i class="icon-envelope-open"></i> My Inbox
                                        <span class="badge badge-danger"> 3 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="app_todo.html">
                                        <i class="icon-rocket"></i> My Tasks
                                        <span class="badge badge-success"> 7 </span>
                                    </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="page_user_lock_1.html">
                                        <i class="icon-lock"></i> Lock Screen </a>
                                </li>
                                <li>
                                    <a href="page_user_login_1.html">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-quick-sidebar-toggler">
                            <a href="javascript:;" class="dropdown-toggle">
                                <i class="icon-logout"></i>
                            </a>
                        </li>
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER INNER -->
    </div>
    <div class="container">
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <ul class="page-breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="index.html">Travelling Fit</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <span>My Booking</span>
                                </li>
                            </ul>
                    </div>
<div class="mega-text-wrapp text_term">
<div class="row">
    <div class="col-md-6">
                                    
                            <h4>
    1. How to book</h4>
<p>
    To reserve your place on one of the event packages, you must complete the Booking Form and pay the non-refundable initial deposit. Please refer to Deposit and Final Payment (clause 5 below) for full details</p>
<h4>
    2. Prices</h4>
<p>
    Prices are quoted at today's rate and are subject to any changes in airfares, tariffs and conditions imposed by airlines, wholesalers or other service providers. All costs are subject to currency fluctuations and/or price increases until paid in full.</p>
<h4>
    3. Package Requirement</h4>
<p>
    The sale of guaranteed race entry alone is not possible and must be purchased in conjunction with a&nbsp;Travelling Fit&nbsp;package.</p>
<h4>
    4. Airfares and Airport/Security Taxes</h4>
<p>
    We strongly recommend your airfares be booked through Travelling Fit to ensure all your travel arrangements coincide and your trip runs as smoothly as possible. Please note that Airport and Security Taxes are charged when paying for airfares but there may be additional taxes at some international/domestic airports upon departure.</p>
<h4>
    5. Deposit and Final Payment</h4>
<p>
    Each booking is subject to the following non-refundable initial deposits</p>
<ul>
    <li>
        A non-refundable deposit will be required at time of booking for the Event Package. Please refer to your travel specialist for the exact deposit amount; and</li>
    <li>
        Non-refundable $250 per person per airfare (if applicable).</li>
</ul>
<p>
    Deposits are due within 24 hours of acceptance.</p>
<p>
    Bookings cannot be confirmed until initial deposits are received in full and Travelling Fit Terms and Conditions are read and signed.</p>
<p>
    Note: Some bookings are subject to a second non-refundable deposit. Please refer to your travel specialist for further details</p>
<p>
    Final payment is due 90 days prior to departure unless otherwise specified by your travel specialist.</p>
<h4>
    6. Default Clause</h4>
<p>
    6.1. Interest on overdue invoices shall accrue daily from the date when payment becomes due, until the date of payment, at a rate of two and one half percent (2.5%) per calendar month (and at the Agent's sole discretion such interest shall compound monthly at such a rate) after as well as before any judgment.</p>
<p>
    6.2. If the Customer defaults in payment of any invoice when due, the Customer shall indemnify the Agent from and against all the Agent's costs and disbursements including on a solicitor and own client basis and in addition all of the Agent's nominees costs of collection.</p>
<h4>
    7. Payment Terms</h4>
<p>
    <strong>a) Cheque </strong>or <strong>Money Order</strong> made payable to TRAVELLING FIT.</p>
<p>
    <strong>b) </strong>For <strong>electronic transfers</strong> (via internet only), bank details are as follows:</p>
<table border="0" cellpadding="0" cellspacing="0" style="width: 400px; margin-left: 30px;">
    <tbody>
        <tr>
            <td style="width: 200px;">
                <em>Bank:</em></td>
            <td style="width: 200px;">
                <em>National Australia Bank</em></td>
        </tr>
        <tr>
            <td>
                <em>Account Name:</em></td>
            <td>
                <em>Travelling Fit</em></td>
        </tr>
        <tr>
            <td>
                <em>BSB Number:</em></td>
            <td>
                <em>082-574</em></td>
        </tr>
        <tr>
            <td>
                <em>Account Number:</em></td>
            <td>
                <em>41-846-7265</em></td>
        </tr>
    </tbody>
</table>
<p>
    &nbsp;</p>
<p>
    <strong>c)</strong> For <strong>direct deposits</strong> at a <strong>National Australia Bank</strong> (walk in), details as follows:</p>
<table border="0" cellpadding="0" cellspacing="0" style="width: 400px; margin-left: 30px;">
    <tbody>
        <tr>
            <td style="width: 200px;">
                <em>Account Name:</em></td>
            <td style="width: 200px;">
                <em>Travelling Fit</em></td>
        </tr>
        <tr>
            <td>
                <em>BSB Number:</em></td>
            <td>
                <em>082-574</em></td>
        </tr>
        <tr>
            <td>
                <em>Account Number:</em></td>
            <td>
                <i>41-846-7265</i></td>
        </tr>
    </tbody>
</table>
<p>
    &nbsp;</p>
<p>
    <em>Please use your surname and event in the transaction description for easy reference</em></p>
<p>
    If you wish to make payment from an overseas bank account by telegraphic/wire transfer*, our bank account details are as follows:</p>
<table border="0" cellpadding="0" cellspacing="0" style="width: 400px; margin-left: 30px;">
    <tbody>
        <tr>
            <td style="width: 200px;">
                <em>Bank:</em></td>
            <td style="width: 200px;">
                <em>National Australia Bank</em></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                <em>Address:</em></td>
            <td style="vertical-align: top;">
                <em>Shop 21, 148 The Entrance Road,<br>
                &#8203;Erina, NSW, 2250<br>
                Australia</em></td>
        </tr>
        <tr>
            <td>
                <em>Swift Code:</em></td>
            <td>
                <em>NATAAU3302S</em></td>
        </tr>
        <tr>
            <td>
                <em>Account Name:</em></td>
            <td>
                <em>Travelling Fit</em></td>
        </tr>
        <tr>
            <td>
                <em>BSB Number:</em></td>
            <td>
                <em>082-574</em></td>
        </tr>
        <tr>
            <td>
                <em>Account Number:</em></td>
            <td>
                <i>41-846-7265</i></td>
        </tr>
    </tbody>
</table>
<p>
    &nbsp;</p>
<p>
    *Note: Payment from an overseas bank account (outside of Australia) will incur a bank fee of AU$30 per transaction. Please let Travelling Fit know if this is your preferred method of payment.</p>
<p>
    <strong>e)</strong> Payment by <strong>Credit Card</strong>** will incur a transaction fee as follows:</p>
<ul>
    <li>
        Visa/MasterCard: 1.9%; or</li>
    <li>
        International Visa/Mastercard credit cards 2.5%; or</li>
    <li>
        American Express Cards: 3.18%; or</li>
    <li>
        Diners Card: 2.53%.</li>
</ul>
<p>
    **Note: Credit Card transaction fees may vary or in some instances may be waived by certain airlines, wholesalers and other service providers.</p>
<p>
    <strong>8. Cancellation Fees</strong></p>
<p>
    Each booking is subject to the following cancellation fees (variations apply with certain events – please refer to the list of variations further down):</p>
<ul>
    <li>
        25% of the amount received to date plus the non-refundable deposit if cancelled more than 90 days prior to the date of departure;</li>
    <li>
        50% of the amount received to date plus the non-refundable deposit if cancelled 60-90 days prior to the date of departure;</li>
    <li>
        75% of the amount received to date plus the non-refundable deposit if cancelled 30-59 days prior to the date of departure; or</li>
    <li>
        if cancelled within 29 days or less of the date of departure no refund will be given.</li>
</ul>
<p>
    <strong>Variations to the above apply as follows:</strong></p>
<ul>
    <li>
        Refunds are not available at any time for the TCS New York City Marathon, the Virgin Money London Marathon, the BMW Berlin Marathon&nbsp;or the Antarctica Marathon.</li>
    <li>
        Refunds are not available for the Marathon des Sables if cancelled on/after 01 February 2016.</li>
    <li>
        Refunds are not available for the Polar Circle Marathon if cancelled on/after 01 August 2015.</li>
</ul>
<p>
    <strong>NOTE 1:</strong> Certain airfares/land packages are non-refundable from the time they are purchased. Please contact Travelling Fit for further details.</p>
<p>
    <strong>NOTE 2:</strong> Race entries are STRICTLY non-transferrable.</p>

    </div>
    <div class="col-md-6">
                                    <h4>
    9. Fees</h4>
<p>
    A $55 late payment fee is applicable for any payment received after its due date. If any payment is not received within 5 days of becoming due, you will be deemed to have cancelled your booking and the cancellation policy under clause 8 will apply.</p>
<p>
    A $55 amendment fee per person per change made once reservations have been confirmed (plus any additional charges incurred by airlines, wholesalers and other service providers); except if the amendment is made within a month of departure a $110 fee will apply per person per change (plus any additional charges incurred by airlines, wholesalers and other service providers).</p>
<p>
    A $110 late booking fee may apply for reservations made within 2 months from the date of departure.</p>
<h4>
    10. Event Cancellation</h4>
<p>
    In the event that a race is cancelled for any reason the cancellation policy under clause 8 will apply subject to Travelling Fit refunding any race entry fees, if any, refunded by race organizers.</p>
<h4>
    11. Force Majeure</h4>
<p>
    If Travelling Fit is prevented (directly or indirectly) from performing any of its obligations under this agreement by reason of act of God, strikes, trade disputes, fire, breakdowns, interruption of transport, government or political action, acts of war or terrorism, acts of omissions of a third party or for any other cause whatsoever outside Travelling Fit's reasonable control, Travelling Fit will be under no liability whatsoever to you and may, at its option, by written notice to you to cancel the tour.</p>
<h4>
    12. Travel Documents</h4>
<p>
    Visas may be required. Please contact Travelling Fit for further details. Australian passport holders must make sure their passport is valid for at least 6 months from their date of return to Australia. Each individual is responsible for ensuring that all necessary travel documents are current and valid.</p>
<h4>
    13. Travel Insurance</h4>
<p>
    Personal travel insurance is not included in the package price. It is a condition of booking a package with Travelling Fit that you have travel insurance, and it is your responsibility to ensure that you are adequately insured for the full duration of the trip in respect of illness, injury, death, loss of baggage and personal items and cancellation/interruption and curtailment, as well as all of the activities you expect to participate in.</p>
<p>
    13.1. Participants can save 25% on travel insurance simply by going to the Travelling Fit website and applying online. Alternatively you can <a href="https://secure.covermore.com.au/agent/home.aspx?AlphaCode=HAN0157" target="_blank">click here</a> to purchase your travel insurance immediately</p>
<p>
    13.2. Travelling Fit is unable to provide this service if you live outside Australia and are participating in an event outside Australia. In these circumstances Travelling Fit recommends you contact insurers in your country of origin</p>
<p>
    13.3. If you do not purchase travel insurance through Travelling Fit you must provide evidence to Travelling Fit that you have obtained personal travel insurance. Without evidence Travelling Fit reserves the right to withhold your travel documents until such time as evidence is produced.</p>
<p>
    13.4. If you do not purchase travel insurance through Travelling Fit and you need to make a claim for whatever reason, a $55 minimum fee will be charged to process the relevant paperwork for your insurer.</p>
<h4>
    14. Release and Waiver of Liability</h4>
<p>
    In consideration of Travelling Fit accepting your application:</p>
<ul>
    <li>
        You release Travelling Fit and its officers, employees, agents and other representatives and recommended charity beneficiaries (hereafter "Travelling Fit and its personnel") from all cost, liability, loss or damage incurred or suffered by you directly or indirectly during the course of your travel and resulting from your personal injury, illness or death or damage to or loss of your property unless caused by the willful negligence or wrongful act of Travelling Fit and its personnel; and</li>
    <li>
        You waive any claims you have, or may at any time have, against Travelling Fit and its personnel and you agree, by accepting the inherent dangers and risks associated with any travel, not to make any claim against or seek any compensation from Travelling Fit and its personnel in respect of any personal injury, illness or death suffered by you or damage to or loss of property sustained by you as a result of your participation in an event.</li>
</ul>
<p>
    To the extent permitted by law, section 74 of the Trade Practices Act 1974 does not apply to this agreement.</p>
<h4>
    15. Disclaimer</h4>
<p>
    Travelling Fit reserves the right to refuse an application to register for any reason. Should an entrant's application be refused, the full registration fee (including the non-refundable initial deposit) will be refunded to the applicant. Your registration for an event takes effect once you complete the Booking Form, accept the accompanying Booking Terms and Conditions and the initial deposit has been received in full by Travelling Fit. Places are limited for the events and are allocated on a first come first served basis.</p>
<h4>
    16. Privacy Act</h4>
<p>
    16.1 The Customer agrees for the Agent to obtain from a credit-reporting agency a credit report containing personal credit information about the Customer in relation to credit provided by the Agent.</p>
<p>
    16.2 The Customer agrees that the Agent may exchange information about Customer with those credit providers named in the Application for Credit account or named in a consumer credit report issued by a reporting agency for the following purposes:</p>
<p style="margin-left: 40px;">
    a) to assess an application by Customer;<br>
    b) to notify other credit providers of a default by the Customer;<br>
    c) to exchange information with other credit providers as to the status of this credit account, where the Customer is in default with other credit providers; and<br>
    d) to assess the credit worthiness of Customer.</p>
<p>
    16.3 The Customer consents to the Agent being given a consumer credit report to collect overdue payment on commercial credit (Section 18K(1)(h) Privacy Act 1988).</p>
<p>
    16.4 The Customer agrees that Personal Data provided may be used and retained by the Agent for the following purposes and for other purposes as shall be agreed between the Customer and Agent or required by law from time to time:</p>
<p style="margin-left: 40px;">
    a) provision of Goods and/or Services; (b)marketing of Goods and/or Services by the Agent, its agents or distributors in relation to the Goods and/or Services;<br>
    b) analysing, verifying and/or checking the Customer's credit, payment and/or status in relation to the provision of Goods and/or Services;<br>
    c) processing of any payment instructions, direct debit facilities and/or credit facilities requested by Customer; and<br>
    d) enabling the daily operation of Customer's account and/or the collection of amounts outstanding in the Customer's account in relation to the Goods and/or Services. 16.5. The Agent may give, information about the Customer to a credit reporting agency for the following purposes:<br>
    e) to obtain a consumer credit report about the Customer; and or allow the credit reporting agency to create or maintain a credit information file containing information about the Customer.</p>

    </div>
</div>
<h2>Booking Terms and Conditions</h2>
   <p> 1. How to book <br>
To reserve your place on one of the event packages, you must complete the Booking Form and pay the non-refundable initial deposit. Please refer to Deposit and Final Payment (clause 5 below) for full details</p>

<p>2. Prices <br>
Prices are quoted at today's rate and are subject to any changes in airfares, tariffs and conditions imposed by airlines, wholesalers or other service providers. All costs are subject to currency fluctuations and/or price increases until paid in full.</p>

<p>3. Package Requirement <br>
The sale of guaranteed race entry alone is not possible and must be purchased in conjunction with an accommodation package.</p>

<p>4. Airfares and Airport/Security Taxes <br>
We strongly recommend your airfares be booked through Travelling Fit to ensure all your travel arrangements coincide and your trip runs as smoothly as possible. Please note that Airport and Security Taxes are charged when paying for airfares but there may be additional taxes at some international/domestic airports upon departure.</p>

<p>5. Deposit and Final Payment <br>
Each booking is subject to the following non-refundable initial deposits
</p>
<p>A non-refundable deposit will be required at time of booking for the Event Package. Please refer to your travel specialist for the exact deposit amount; and
Non-refundable $250 per person per airfare (if applicable).
</p>
<p>Deposits are due within 24 hours of acceptance.
</p>
<p>Bookings cannot be confirmed until initial deposits are received in full and Travelling Fit Terms and Conditions are read and signed.
</p>
<p>Note: Some bookings are subject to a second non-refundable deposit. Please refer to your travel specialist for further details
</p>
<p>Final payment is due 90 days prior to departure unless otherwise specified by your travel specialist.
</p>
<p>6. Default Clause <br>
6.1. Interest on overdue invoices shall accrue daily from the date when payment becomes due, until the date of payment, at a rate of two and one half percent (2.5%) per calendar month (and at the Agent's sole discretion such interest shall compound monthly at such a rate) after as well as before any judgment.
</p>
<p>6.2. If the Customer defaults in payment of any invoice when due, the Customer shall indemnify the Agent from and against all the Agent's costs and disbursements including on a solicitor and own client basis and in addition all of the Agent's nominees costs of collection.
</p>
<p>7. Payment Terms <br>
a) Cheque or Money Order made payable to TRAVELLING FIT.
</p>
<p>b) For electronic transfers (via internet only), bank details are as follows:

Bank: <br>
National Australia Bank <br>
Account Name: <br>
Travelling Fit <br>
BSB Number: <br>
082-574 <br>
Account Number: <br>
83-544-8081 <br>
 
</p>
<p>c) For direct deposits at a National Australia Bank (walk in), details as follows:

Account Name: <br>
Travelling Fit <br>
BSB Number: <br>
082-574 <br>
Account Number: <br>
83-544-8006 <br>
 
</p>
<p>Please use your surname and event in the transaction description for easy reference
</p>
</div>

                                                     <div class="form-actions pay-action">
                <a href="<?php echo $_SERVER['HTTP_REFERER'];?>"><button type="button" id="back-btn" class="btn dafault btn-outline"><< Back </button></a>
            </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
                <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->
    <!--[if lt IE 9]>
<script src="includes/assets/global/plugins/respond.min.js"></script>
<script src="includes/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="includes/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
