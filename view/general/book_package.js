$(document).ready(function () {

    $('#primaryContactDob').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });

    $(".tools a").on("click", function(e) {
            var el = e.target;
            if ($(el).attr("class") == 'expand') {
                $(".collapse").attr("class", "expand")
                var bodyElCurr = $(el).parent().parent().siblings().first()
                var bodyElements = $(".portlet-body");
                //console.log(bodyElCurr[0].firstElementChild)
                if (bodyElements.length > 0) {
                    for (var i = 0; i < bodyElements.length; i++) {
                        if ($(bodyElements[i])[0].firstElementChild != bodyElCurr[0].firstElementChild && $($(bodyElements[i])[0].firstElementChild).attr("id") != 'additionalGuestsForm') {
                            $(bodyElements[i]).slideUp()
                        }
                    }
                    document.location = "#" + $(bodyElCurr[0].firstElementChild).attr("id")
                }
            }
    })

    var buttons = $("[type='submit']").parent().on("click", function(e) {
        $(e.target).find("[type='submit']").click();
    });

    $.validator.addMethod("phone",
        function (phone_number, element) {
            if (phone_number == '') return true;
            phone_number = phone_number.replace(/\s+/g, "");
            return phone_number.match(/^\+*[0-9]{9,14}$/);
        }, "Please specify a valid phone number");

    $.validator.addMethod("isEmptyName",
        function (value, element, params) {
            var count = params.val();
            if (count == 0) {
                return true
            } else {
                if (value != '') return true;
            }
            return false;
        }, 'Name is required');

    var primaryContactFormValidator = $('#primaryContactDetailsForm').validate({
        rules: {
            primaryContactFirstname: "required",
            primaryContactDob: "required",
            primaryContactGender: "required",
            primaryContactPartisipation: "required",
            primaryContactAddress_1: "required",
            primaryContactSuburb: "required",
            primaryContactState: "required",
            primaryContactPostcode: "required",
            primaryContactCountry: "required",
            primaryContactPhone1_preferred: {
                required: true,
                //phone: $("#primaryContactPhone1_preferred")
            },
            primaryContactPhone2_alternate: {
                //phone: $("#primaryContactPhone2_alternate")
            },
            primaryContactEmergency_contact_name: "required",
            primaryContactEmergency_contact_phone_number: "required",
            primaryContactEmail: {
                email: true
            },
            //primaryContactTshirt_quantity: {
            //    number: true
            //},
            //primaryContactTshirt_name: {
            //    isEmptyName: $("#primaryContactTshirt_quantity")
            //}
        }
    });

    var form = $("#additionalGuestsForm");
    if (form.length > 0) {
        var fields = form.find(".required");
        var rules = {}
        for (var i = 0; i < fields.length; i++) {
            var name = $(fields[i]).attr("name");
            if (name.indexOf("Phone1_preferred") != -1) {
                rules[name] = {
                    required: true,
                    //phone: $(fields[i])
                }
            } else {
                if (name.indexOf("Phone1_preferred") != -1) {
                    rules[name] = {
                        //phone: $(fields[i])
                    }
                } else {
                    //rules[name] = "required";
                }
            }
            if (name.indexOf("additionalGuestDob") != -1) {
                $('[name="' + name + '"]').datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                });
            }
        }
        $("#additionalGuestsForm").validate({
            rules: rules
        });
    }

    $.validator.addMethod("isEmpty",
        function (value, element, params) {
            if (params.css("display") == "none") {
                return true;
            } else {
                if (params.css("display") == "block") {
                    return (value > 5) ? true : false;
                }
            }
            return false;
        }, 'Must be greater than 5');


    $('#additional_request_form').validate({
        rules: {
            extra_nigths_post_other: {
                number: true,
                isEmpty: $('#extra_nigths_post_other')
            },
            extra_nigths_pre_other: {
                number: true,
                isEmpty: $('#extra_nigths_pre_other')
            },
            active_living_membership: {
                number: true
            }

        }
    })

    $('#extra_nights_pre').on("change", function (e) {
        var val = $(e.target).val();
        if (val == '>5') {
            $('#extra_nigths_pre_other').show();
        } else {
            $('#extra_nigths_pre_other').hide();
        }
    })

    $('#extra_nights_post').on("change", function (e) {
        var val = $(e.target).val();
        if (val == '>5') {
            $('#extra_nigths_post_other').show();
        } else {
            $('#extra_nigths_post_other').hide();
        }
    })

    var primaryContact = {}
    primaryContact.additionalGuestAddress_1 = $("#primaryContactAddress_1").val();
    primaryContact.additionalGuestSuburb = $("#primaryContactSuburb").val();
    primaryContact.additionalGuestState = $("#primaryContactState").val();
    primaryContact.additionalGuestPostcode = $("#primaryContactPostcode").val();
    primaryContact.additionalGuestCountry = $("#primaryContactCountry").val();
    primaryContact.additionalGuestPhone1_preferred = $("#primaryContactPhone1_preferred").val();
    primaryContact.additionalGuestEmail = $("#primaryContactEmail").val();

    var additionalGuest = {};


    var checkboxes = $(".checkbox-inline input:checkbox");
    if (checkboxes.length > 0) {
        for (var i = 0; i < checkboxes.length; i++) {
            $(checkboxes[i]).on("change", function (e) {
                var parent = $(e.target).parent();
                var id = $(e.target).attr("id").replace("adressDetails", "");
                if (parent.hasClass("checked")) {
                    additionalGuest[id] = {};
                    //console.log(additionalGuest);
                    additionalGuest[id]['additionalGuestAddress_1' + id] = $("[name='additionalGuestAddress_1" + id + "']").val();
                    additionalGuest[id]['additionalGuestSuburb' + id] = $("[name='additionalGuestSuburb" + id + "']").val();
                    additionalGuest[id]['additionalGuestState' + id] = $("[name='additionalGuestState" + id + "']").val();
                    additionalGuest[id]['additionalGuestPostcode' + id] = $("[name='additionalGuestPostcode" + id + "'").val();
                    additionalGuest[id]['additionalGuestCountry' + id] = $("[name='additionalGuestCountry" + id + "']").val();
                    additionalGuest[id]['additionalGuestPhone1_preferred' + id] = $("[name='additionalGuestPhone1_preferred" + id + "']").val();
                    additionalGuest[id]['additionalGuestEmail' + id] = $("[name='additionalGuestEmail" + id + "']").val();
                    for (var key in primaryContact) {
                        //console.log($("#" + key + id));
                        $("[name='" + key + id + "']").val(primaryContact[key]);
                    }
                    $("#additionalGuestContactInfo" + id).hide();
                } else {
                    for (var key in additionalGuest[id]) {
                        $("[name='" + key + "']").val(additionalGuest[id][key]);
                    }
                    $("#additionalGuestContactInfo" + id).show();
                }
            })
        }
    }


    $("#primaryContactTshirt_quantity").on("change", function (e) {
        var val = $(e.target).val();
        if (val > 0) {
            if ($("#primaryContactTshirt_id").val() == '') {
                var el = $("#tshirt_size [selected]");
                el = (el.length > 0) ? el : $("#tshirt_size:first");
                $("#primaryContactTshirt_id").val(el.val())
            }
            $("#tshirt_size_block").show();
            $("#tshirt_name_block").show();
        } else {
            if (val == 0) {
                $("#tshirt_size_block").hide();
                $("#primaryContactTshirt_id").val(null);
                if ($("#primaryContactSinglet_quantity").val() == 0) {
                    $("#primaryContactTshirt_name").val("");
                    $("#tshirt_name_block").hide();
                }

            }
        }
    })

    $("#primaryContactSinglet_quantity").on("change", function (e) {
        var val = $(e.target).val();
        if (val > 0) {
            if ($("#primaryContactSinglet_id").val() == '') {
                var el = $("#singlet_size [selected]");
                el = (el.length > 0) ? el : $("#singlet_size:first");
                $("#primaryContactSinglet_id").val(el.val())
            }
            $("#singlet_size_block").show();
            $("#tshirt_name_block").show();
        } else {
            if (val == 0) {
                $("#singlet_size_block").hide();
                $("#primaryContactSinglet_id").val(null);
                if ($("#primaryContactTshirt_quantity").val() == 0) {
                    $("#primaryContactTshirt_name").val("");
                    $("#tshirt_name_block").hide();
                }
            }
        }
    })

    $("#tshirt_size").on("change", function (e) {
        var val = $(e.target).val();
        $("#primaryContactTshirt_id").val(val);
    })

    $("#singlet_size").on("change", function (e) {
        var val = $(e.target).val();
        $("#primaryContactSinglet_id").val(val);
    })

    $("[name='primaryContactGender']").on("change", function (e) {
        var el = $(e.target).val();
        var tTemplate = '';
        var sTemplate = '';
        if (el === 'M') {
            var singlets = maleSinglets
            var tshirts = maleTshirts;
        }
        if (el === 'F') {
            var singlets = femaleSinglets
            var tshirts = femaleTshirts;
        }
        if (tshirts.length > 0) {
            for (var i = 0; i < tshirts.length; i++) {
                var cnt = 1;
                for (var j in tshirts[i]) {
                    if (i == 0 && cnt == 1 && $("#primaryContactTshirt_quantity").val() > 0) {
                        $("#primaryContactTshirt_id").val(j);
                    }
                    tTemplate += '<option value="' + j + '">' + tshirts[i][j] + '</option>';
                    cnt++;
                }
            }
        }
        if (singlets.length > 0) {
            for (var i = 0; i < singlets.length; i++) {
                var cnt = 1;
                for (var j in singlets[i]) {
                    if (i == 0 && cnt == 1 && $("#primaryContactSinglet_quantity").val() > 0) {
                        $("#primaryContactSinglet_id").val(j);
                    }
                    sTemplate += '<option value="' + j + '">' + singlets[i][j] + '</option>';
                    cnt++;
                }
            }
        }
        $("#tshirt_size").html(tTemplate);
        $("#singlet_size").html(sTemplate);

    })

    agTshirtQuantityChange();
    agSingletQuantityChange();
    agTshirtSizeChange();
    agSingletSizeChange();
    agGenderChange();


    $("[name='number_adult_guests']").on("change", function () {
        addAdditionalGuestsAdultForm();
    })

    $("[name='number_children_guests']").on("change", function () {
        addAdditionalGuestsChildrenForm();
    })

    $("#additionalGuestsFormSubmit").on("click", function (e) {
        //debugger;
        e.preventDefault();
        var adults = $("[name='number_adult_guests']").val();
        var children = $("[name='number_children_guests']").val();
        // console.log(adults.length)
        // console.log(children.length)
        var error = false;
        if ((window.additionalGuestsFormValidator !== undefined)) {
            if (!window.additionalGuestsFormValidator.form()) {
                error = true;
            } 
        } else {
            error = false;
        }
        if (!error) {
            data = {}
            data.method = 'updateAdditionalGuestsCount'
            data.booking_id = booking_id
            data.adults = adults
            data.children = children
            Util.ajaxData(data, function (returnData) {
                $("#additionalGuestsForm").submit();
            });
        }
    })
})

function agGender(el) {
    if ($(el).attr("checked") !== 'checked') {
        var name = $(el).attr("name");
        var value = $(el).attr("value");
        var parent = $(el).parent();
        value = (value == 'M') ? 'F' : 'M';
        var secondEl = $("[name='" + name + "'][value='" + value + "']");
        $(secondEl).removeAttr("checked");
        $(secondEl).parent().removeClass("checked");
        parent.addClass("checked");
        $(el).attr("checked", "checked");
    }
}


additionalGuests = {};

function agAddressDetails(el) {
    //debugger;
    var primaryContact = {}
    primaryContact.additionalGuestAddress_1 = $("#primaryContactAddress_1").val();
    primaryContact.additionalGuestSuburb = $("#primaryContactSuburb").val();
    primaryContact.additionalGuestState = $("#primaryContactState").val();
    primaryContact.additionalGuestPostcode = $("#primaryContactPostcode").val();
    primaryContact.additionalGuestCountry = $("#primaryContactCountry").val();
    primaryContact.additionalGuestPhone1_preferred = $("#primaryContactPhone1_preferred").val();
    primaryContact.additionalGuestEmail = $("#primaryContactEmail").val();


    var parent = $(el).parent();
    var id = $(el).attr("id").replace("adressDetails", "");
    if (parent.hasClass("checked")) {
        parent.removeClass("checked");
        for (var key in additionalGuests[id]) {
            $("[name='" + key + "']").val(additionalGuests[id][key]);
        }
        $("#additionalGuestContactInfo" + id).show();
    } else {
        parent.addClass("checked");
        additionalGuests[id] = {};
        //console.log(additionalGuest);
        additionalGuests[id]['additionalGuestAddress_1' + id] = $("[name='additionalGuestAddress_1" + id + "']").val();
        additionalGuests[id]['additionalGuestSuburb' + id] = $("[name='additionalGuestSuburb" + id + "']").val();
        additionalGuests[id]['additionalGuestState' + id] = $("[name='additionalGuestState" + id + "']").val();
        additionalGuests[id]['additionalGuestPostcode' + id] = $("[name='additionalGuestPostcode" + id + "'").val();
        additionalGuests[id]['additionalGuestCountry' + id] = $("[name='additionalGuestCountry" + id + "']").val();
        additionalGuests[id]['additionalGuestPhone1_preferred' + id] = $("[name='additionalGuestPhone1_preferred" + id + "']").val();
        additionalGuests[id]['additionalGuestEmail' + id] = $("[name='additionalGuestEmail" + id + "']").val();
        for (var key in primaryContact) {
            //console.log($("#" + key + id));
            $("[name='" + key + id + "']").val(primaryContact[key]);
        }
        $("#additionalGuestContactInfo" + id).hide();
    }
}


function additionalGuestsFormValidate() {
    var form = $("#additionalGuestsForm");
    if (form.length > 0) {
        var fields = form.find(".required");
        var rules = {}
        for (var i = 0; i < fields.length; i++) {
            var name = $(fields[i]).attr("name");
            //console.log(name);
            if (name.indexOf("Phone1_preferred") != -1) {
                //console.log(1);
                rules[name] = {
                    required: true,
                    //phone: $(fields[i])
                }
            } else {
                if (name.indexOf("Phone1_preferred") != -1) {
                    rules[name] = {
                        //phone: $(fields[i])
                    }
                } else {
                    rules[name] = "required";
                }
            }
            if (name.indexOf("additionalGuestDob") != -1) {
                $('[name="' + name + '"]').datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                });
            }
        }
        additionalGuestsFormValidator = $("#additionalGuestsForm").validate({
            rules: rules
        });
    }
}

function addAdditionalGuestsAdultForm() {
    //debugger;
    var adults = $("[name='number_adult_guests']").val();
    var children = $("[name='number_children_guests']").val();

    var childrenBlocks = $("[guest-type='child']");
    var childrenBlocksLen = childrenBlocks.length;
    var adultBlocks = $("[guest-type='adult']");
    var adultBlocksLen = adultBlocks.length;

    if (adults == adultBlocksLen && children == childrenBlocksLen) {
        //console.log("Here must be a AJAX query to update number of guests of current booking");

    } else {

        if (adults < adultBlocksLen) {
            var diff = adultBlocksLen - adults;
            for (var i = 0; i < diff; i++) {
                adultBlocks[adultBlocksLen - 1 - i].remove();
                delete adultBlocks[adultBlocksLen - 1 - i];
            }
            var adultsCount = $("[guest-type='adult']");
            var childrenCount = $("[guest-type='child']");
        }
        if (adults > adultBlocksLen) {
            var diff = adults - adultBlocksLen;
            var template = '';
            for (var i = 1; i <= diff; i++) {
                template += '<div guest-type="adult">';
                template += '<p id="dynamic_pager_content1" class="well">Additional Guest ' + (adultBlocksLen * 1 + i * 1) + ' - Adult:</p>';
                template += '<input type="hidden" name="additionalGuestId[]" value="">';
                template += '<input type="hidden" name="child' + (adultBlocksLen * 1 + i * 1) + '" value="N">';
                template += '<div class="form-body"><div class="form-group"><label class="col-md-3 control-label">Title:</label>';
                template += '<div class="col-md-9">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTitle' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestTitle' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '<option value=" ">Please Select</option>';
                template += '<option>Mr</option><option>Mrs</option><option>Ms</option><option>Miss</option>';
                template += '<option>Dr</option><option>Proff</option><option>Asoc Proff</option><option>Hon</option><option>Sir</option>';
                template += '</select></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">First Name*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required"  value="" placeholder="First Name" name="additionalGuestFirstname' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Middle Name:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg"  value="" placeholder="Middle Name" name="additionalGuestMiddle_name' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Last Name:</label>';
                template += '<div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Last Name" name="additionalGuestLastname' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Known as (Preferred Name):</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Known as (Preferred Name)" name="additionalGuestPrefered_name' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Date of Birth*:</label>'
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">'
                template += '<input type="text" class="form-control input-lg required" value="" name="additionalGuestDob' + (adultBlocksLen * 1 + i * 1) + '"><br>'
                template += '</div></div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Gender:</label>'
                template += '<div class="col-md-9">'
                template += '<div class="radio-list"><label class="radio-inline"><div class="radio" id="uniform-optionsRadios25"><span>'
                template += '<div class="radio" id="uniform-optionsRadios25"><span class="checked"><input onclick="agGender(this)" type="radio" name="additionalGuestGender' + (adultBlocksLen * 1 + i * 1) + '"id="optionsRadios25" value="M" checked>'
                template += '</span></div></span></div>Male </label><label class="radio-inline"><div class="radio" id="uniform-optionsRadios26">';
                template += '<span><div class="radio" id="uniform-optionsRadios26"><span class=""><input onclick="agGender(this)" type="radio" name="additionalGuestGender' + (adultBlocksLen * 1 + i * 1) + '" id="optionsRadios26" value="F">'
                template += '</span></div></span></div>Female </label></div></div>';
                template += '</div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Event / Partisipation*:</label><div class="col-md-9"><div class="input-group">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestPartisipation' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestPartisipation' + (adultBlocksLen * 1 + i * 1) + '">'
                template += '<option>Marathon</option><option>Half Marathon</option><option>Ultra Marathon</option><option>Multi Stage</option>'
                template += '<option>Non-Runner</option></select></div></div></div>';

                template += '<div class="form-group"><label class="col-md-3 control-label">Dietry /Medical requirements:</label>';
                template += '<div class="col-md-9"><textarea class="form-control" rows="3" id="additionalGuestDietry_requirements' + (adultBlocksLen * 1 + i * 1) + '"name="additionalGuestDietry_requirements' + (adultBlocksLen * 1 + i * 1) + '"></textarea></div></div>'

                // tshirts and singlets template for additional guests start
                template += '<div class="form-group"><label class="col-md-3 control-label"></label><div class="col-md-9">'
                template += '<a href="http://www.travellingfit.com/SiteFiles/travellingfitcomau/pdf/TF_tshirt_size_chart.pdf" target="_blank">Please Click Here for Size Chart and Designs</a></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Tshirt quantity:</label><div class="col-md-3"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTshirt_quantity' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestTshirt_quantity' + (adultBlocksLen * 1 + i * 1) + '">'
                for (var tshirt_quantity = 0; tshirt_quantity <= 7; tshirt_quantity++) {
                    template += '<option>' + tshirt_quantity + '</option>'
                }
                template += '</select></div></div><div id="tshirt_size_block' + (adultBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-2 control-label">Tshirt size:</label><div class="col-md-4"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="tshirt_size' + (adultBlocksLen * 1 + i * 1) + '">'
                for (var tshirt = 0; tshirt < maleTshirts.length; tshirt++) {
                    for (var key in maleTshirts[tshirt]) {
                        template += '<option value="' + key + '">' + maleTshirts[tshirt][key] + '</option>'
                    }
                }
                template += '</select></div></div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Singlet quantity:</label><div class="col-md-3">'
                template += '<div class="input-group"><select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestSinglet_quantity' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestSinglet_quantity' + (adultBlocksLen * 1 + i * 1) + '">'
                for (var singlet_quantity = 0; singlet_quantity <= 7; singlet_quantity++) {
                    template += '<option>' + singlet_quantity + '</option>'
                }
                template += '</select></div></div><div id="singlet_size_block' + (adultBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-2 control-label">Singlet size:</label>'
                template += '<div class="col-md-4"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="singlet_size' + (adultBlocksLen * 1 + i * 1) + '"  >'
                for (var singlet = 0; singlet < maleSinglets.length; singlet++) {
                    for (var key in maleSinglets[singlet]) {
                        template += '<option value="' + key + '">' + maleSinglets[singlet][key] + '</option>'
                    }
                }
                template += '</select></div></div></div></div>'
                template += '<input type="hidden" name="additionalGuestTshirt_id' + (adultBlocksLen * 1 + i * 1) + '" id="additionalGuestTshirt_id' + (adultBlocksLen * 1 + i * 1) + '" value="">'
                template += '<input type="hidden" name="additionalGuestSinglet_id' + (adultBlocksLen * 1 + i * 1) + '" id="additionalGuestSinglet_id' + (adultBlocksLen * 1 + i * 1) + '" value="">'
                template += '<div class="form-group" id="tshirt_name_block' + (adultBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-3 control-label">Optional name on t-shirt:</label><div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" placeholder="Tshirt name" id="additionalGuestTshirt_name' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestTshirt_name' + (adultBlocksLen * 1 + i * 1) + '" value="">'
                template += '</div></div></div>'
                // tshirts and singlets template for additional guests end

                template += '<div class="form-group"><label class="col-md-3 control-label" for="adressDetails">Adress and Details same as primary contact? </label>';
                template += '<div class="col-md-9"><div class="checkbox-list"><label class="checkbox-inline">';
                template += '<div class="checker" id="uniform-adressDetails' + (adultBlocksLen * 1 + i * 1) + '"><span class=""><input onclick="agAddressDetails(this)" type="checkbox" id="adressDetails' + (adultBlocksLen * 1 + i * 1) + '" value="option1">'
                template += '</span></div></label></div></div></div>';
                template += '<div id="additionalGuestContactInfo' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '<div class="form-group"><label class="col-md-3 control-label">Address*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Address" name="additionalGuestAddress_1' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Suburb*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Suburb" name="additionalGuestSuburb' + (adultBlocksLen * 1 + i * 1) + '">'
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">State / Province*:</label><div class="col-md-9">'
                template += ' <div class="input-group"><input type="text" class="form-control input-lg required" value="" placeholder="State / Province" name="additionalGuestState' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Postcode*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Postcode" name="additionalGuestPostcode' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Country*:</label><div class="col-md-9"><div class="input-group">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestCountry' + (adultBlocksLen * 1 + i * 1) + '" name="additionalGuestCountry' + (adultBlocksLen * 1 + i * 1) + '">'
                for (var j = 0; j < countries.length; j++) {
                    template += '<option>' + countries[j] + '</option>';
                }
                template += '</select></div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Preferred Phone Number*:</label>';
                template += '<div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg required" value="" placeholder=" Phone Number" name="additionalGuestPhone1_preferred' + (adultBlocksLen * 1 + i * 1) + '">'
                template += '</div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Email Address:</label><div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Email Address" name="additionalGuestEmail' + (adultBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div></div></div></div>';
            }
            var lastAdult = $("[guest-type='adult']:last");
            if (lastAdult.length > 0) {
                //console.log('a1')
                lastAdult.after(template);
            } else {

                var lastChild = $("[guest-type='child']:last");
                if (lastChild.length > 0) {
                    //console.log('a2')
                    lastChild.after(template);
                } else {
                    //console.log('a3')
                    $("#additionalGuestsForm").prepend(template);
                }
            }
            var adultsCount = $("[guest-type='adult']");
            var childrenCount = $("[guest-type='child']");
        }


        if ((children < childrenBlocksLen && adults == adultBlocksLen) || (children > childrenBlocksLen && adults == adultBlocksLen) || (adults > adultBlocksLen)) {
            for (var i = 0; i < adultsCount.length; i++) {
                var elements = $(adultsCount[i]).find('[name^=additionalGuest]');
                var index = (childrenCount.length * 1 + i * 1 + 1)
                var child = $(adultsCount[i]).find('[name^=child]:first');
                if (child.length > 0) {
                    var pattern = /child[\d]/;
                    var childName = $(child).attr("name")
                    childName = childName.replace(pattern, 'child' + index);
                    $(child).attr("name", childName);
                }
                for (j = 0; j < elements.length; j++) {
                    var name = $(elements[j]).attr("name");

                    var id = $(elements[j]).attr("id");
                    var pattern = /additionalGuest(\D+1?[\D]*_?1?)[\d]{1,2}/mg
                    name = name.replace(pattern, 'additionalGuest' + '$1' + index);
                    $(elements[j]).attr("name", name);

                    if (name.indexOf('additionalGuestGender') != -1) {
                        if ($(elements[j]).parent().hasClass("checked")) {

                            $(elements[j]).click();
                            $(elements[j]).parent().addClass("checked")
                        }
                    }

                    if (id != undefined) {
                        id = id.replace(pattern, 'additionalGuest' + '$1' + index);
                        $(elements[j]).attr("id", id);
                    }
                }

                renameAdditionalGuestLabel(adultsCount[i], index);
                renameAdditionalGuestContactInfo(adultsCount[i], index)
                renameAddressDetails(adultsCount[i], index);
                renameTshirtSize(adultsCount[i], index);
                renameSingletSize(adultsCount[i], index);
            }
        }
        additionalGuestsFormValidate();
        agTshirtQuantityChange();
        agTshirtSizeChange();
        agSingletQuantityChange();
        agSingletSizeChange();
        agGenderChange();

        $("[name='cnt']").val(adultsCount.length * 1 + childrenCount.length * 1 + 1);

    }

}

function addAdditionalGuestsChildrenForm() {
    var adults = $("[name='number_adult_guests']").val();
    var children = $("[name='number_children_guests']").val();

    var childrenBlocks = $("[guest-type='child']");
    var childrenBlocksLen = childrenBlocks.length;
    var adultBlocks = $("[guest-type='adult']");
    var adultBlocksLen = adultBlocks.length;

    if (adults == adultBlocksLen && children == childrenBlocksLen) {
        //console.log("Here must be a AJAX query to update number of guests of current booking");

    } else {

        if (children < childrenBlocksLen) {
            var diff = childrenBlocksLen - children;
            for (var i = 0; i < diff; i++) {
                childrenBlocks[childrenBlocksLen - 1 - i].remove();
                delete childrenBlocks[childrenBlocksLen - 1 - i];
            }
            var adultsCount = $("[guest-type='adult']");
            var childrenCount = $("[guest-type='child']");
        }

        if (children > childrenBlocksLen) {
            var diff = children - childrenBlocksLen;
            var template = '';
            for (var i = 1; i <= diff; i++) {
                template += '<div guest-type="child">';
                template += '<p id="dynamic_pager_content1" class="well">Additional Guest ' + (childrenBlocksLen * 1 + i * 1) + ' - Child:</p>';
                template += '<input type="hidden" name="additionalGuestId[]" value="">';
                template += '<input type="hidden" name="child' + (childrenBlocksLen * 1 + i * 1) + '" value="Y">';
                template += '<div class="form-body"><div class="form-group"><label class="col-md-3 control-label">Title:</label>';
                template += '<div class="col-md-9">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTitle' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestTitle' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '<option value=" ">Please Select</option>';
                template += '<option>Mr</option><option>Mrs</option><option>Ms</option><option>Miss</option>';
                template += '<option>Dr</option><option>Proff</option><option>Asoc Proff</option><option>Hon</option><option>Sir</option>';
                template += '</select></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">First Name*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required"  value="" placeholder="First Name" name="additionalGuestFirstname' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Middle Name:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg"  value="" placeholder="Middle Name" name="additionalGuestMiddle_name' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Last Name:</label>';
                template += '<div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Last Name" name="additionalGuestLastname' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Known as (Preferred Name):</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Known as (Preferred Name)" name="additionalGuestPrefered_name' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Date of Birth*:</label>'
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">'
                template += '<input type="text" class="form-control input-lg required" value="" name="additionalGuestDob' + (childrenBlocksLen * 1 + i * 1) + '"><br>'
                template += '</div></div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Gender:</label>'
                template += '<div class="col-md-9">'
                template += '<div class="radio-list"><label class="radio-inline"><div class="radio" id="uniform-optionsRadios25"><span>'
                template += '<div class="radio" id="uniform-optionsRadios25"><span class="checked"><input onclick="agGender(this)" type="radio" name="additionalGuestGender' + (childrenBlocksLen * 1 + i * 1) + '"id="optionsRadios25" value="M" checked>'
                template += '</span></div></span></div>Male </label><label class="radio-inline"><div class="radio" id="uniform-optionsRadios26">';
                template += '<span><div class="radio" id="uniform-optionsRadios26"><span class=""><input onclick="agGender(this)" type="radio" name="additionalGuestGender' + (childrenBlocksLen * 1 + i * 1) + '" id="optionsRadios26" value="F">'
                template += '</span></div></span></div>Female </label></div></div>';
                template += '</div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Event / Partisipation*:</label><div class="col-md-9"><div class="input-group">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestPartisipation' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestPartisipation' + (childrenBlocksLen * 1 + i * 1) + '">'
                template += '<option>Marathon</option><option>Half Marathon</option><option>Ultra Marathon</option><option>Multi Stage</option>'
                template += '<option>Non-Runner</option></select></div></div></div>';

                template += '<div class="form-group"><label class="col-md-3 control-label">Dietry /Medical requirements:</label>';
                template += '<div class="col-md-9"><textarea class="form-control" rows="3" id="additionalGuestDietry_requirements' + (childrenBlocksLen * 1 + i * 1) + '"name="additionalGuestDietry_requirements' + (childrenBlocksLen * 1 + i * 1) + '"></textarea></div></div>'

                // tshirts and singlets template for additional guests start
                template += '<div class="form-group"><label class="col-md-3 control-label"></label><div class="col-md-9">'
                template += '<a href="http://www.travellingfit.com/SiteFiles/travellingfitcomau/pdf/TF_tshirt_size_chart.pdf" target="_blank">Please Click Here for Size Chart and Designs</a></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Tshirt quantity:</label><div class="col-md-3"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTshirt_quantity' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestTshirt_quantity' + (childrenBlocksLen * 1 + i * 1) + '">'
                for (var tshirt_quantity = 0; tshirt_quantity <= 7; tshirt_quantity++) {
                    template += '<option>' + tshirt_quantity + '</option>'
                }
                template += '</select></div></div><div id="tshirt_size_block' + (childrenBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-2 control-label">Tshirt size:</label><div class="col-md-4"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="tshirt_size' + (childrenBlocksLen * 1 + i * 1) + '">'
                for (var tshirt = 0; tshirt < maleTshirts.length; tshirt++) {
                    for (var key in maleTshirts[tshirt]) {
                        template += '<option value="' + key + '">' + maleTshirts[tshirt][key] + '</option>'
                    }
                }
                template += '</select></div></div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Singlet quantity:</label><div class="col-md-3">'
                template += '<div class="input-group"><select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestSinglet_quantity' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestSinglet_quantity' + (childrenBlocksLen * 1 + i * 1) + '">'
                for (var singlet_quantity = 0; singlet_quantity <= 7; singlet_quantity++) {
                    template += '<option>' + singlet_quantity + '</option>'
                }
                template += '</select></div></div><div id="singlet_size_block' + (childrenBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-2 control-label">Singlet size:</label>'
                template += '<div class="col-md-4"><div class="input-group">'
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="singlet_size' + (childrenBlocksLen * 1 + i * 1) + '"  >'
                for (var singlet = 0; singlet < maleSinglets.length; singlet++) {
                    for (var key in maleSinglets[singlet]) {
                        template += '<option value="' + key + '">' + maleSinglets[singlet][key] + '</option>'
                    }
                }
                template += '</select></div></div></div></div>'
                template += '<input type="hidden" name="additionalGuestTshirt_id' + (childrenBlocksLen * 1 + i * 1) + '" id="additionalGuestTshirt_id' + (childrenBlocksLen * 1 + i * 1) + '" value="">'
                template += '<input type="hidden" name="additionalGuestSinglet_id' + (childrenBlocksLen * 1 + i * 1) + '" id="additionalGuestSinglet_id' + (childrenBlocksLen * 1 + i * 1) + '" value="">'
                template += '<div class="form-group" id="tshirt_name_block' + (childrenBlocksLen * 1 + i * 1) + '" style="display: none">'
                template += '<label class="col-md-3 control-label">Optional name on t-shirt:</label><div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" placeholder="Tshirt name" id="additionalGuestTshirt_name' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestTshirt_name' + (childrenBlocksLen * 1 + i * 1) + '" value="">'
                template += '</div></div></div>'
                // tshirts and singlets template for additional guests end

                template += '<div class="form-group"><label class="col-md-3 control-label" for="adressDetails">Adress and Details same as primary contact? </label>';
                template += '<div class="col-md-9"><div class="checkbox-list"><label class="checkbox-inline">';
                template += '<div class="checker" id="uniform-adressDetails' + (childrenBlocksLen * 1 + i * 1) + '"><span class=""><input onclick="agAddressDetails(this)" type="checkbox" id="adressDetails' + (childrenBlocksLen * 1 + i * 1) + '" value="option1">'
                template += '</span></div></label></div></div></div>';
                template += '<div id="additionalGuestContactInfo' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '<div class="form-group"><label class="col-md-3 control-label">Address*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Address" name="additionalGuestAddress_1' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Suburb*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Suburb" name="additionalGuestSuburb' + (childrenBlocksLen * 1 + i * 1) + '">'
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">State / Province*:</label><div class="col-md-9">'
                template += ' <div class="input-group"><input type="text" class="form-control input-lg required" value="" placeholder="State / Province" name="additionalGuestState' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Postcode*:</label>';
                template += '<div class="col-md-9"><div class="input-group">';
                template += '<input type="text" class="form-control input-lg required" value="" placeholder="Postcode" name="additionalGuestPostcode' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Country*:</label><div class="col-md-9"><div class="input-group">';
                template += '<select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestCountry' + (childrenBlocksLen * 1 + i * 1) + '" name="additionalGuestCountry' + (childrenBlocksLen * 1 + i * 1) + '">'
                for (var j = 0; j < countries.length; j++) {
                    template += '<option>' + countries[j] + '</option>';
                }
                template += '</select></div></div></div>';
                template += '<div class="form-group"><label class="col-md-3 control-label">Preferred Phone Number*:</label>';
                template += '<div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg required" value="" placeholder=" Phone Number" name="additionalGuestPhone1_preferred' + (childrenBlocksLen * 1 + i * 1) + '">'
                template += '</div></div></div>'
                template += '<div class="form-group"><label class="col-md-3 control-label">Email Address:</label><div class="col-md-9"><div class="input-group">'
                template += '<input type="text" class="form-control input-lg" value="" placeholder="Email Address" name="additionalGuestEmail' + (childrenBlocksLen * 1 + i * 1) + '">';
                template += '</div></div></div></div></div></div>';
            }

            var lastChild = $("[guest-type='child']:last");
            if (lastChild.length > 0) {
                //console.log('c1')
                lastChild.after(template);
            } else {
                var firstAdult = $("[guest-type='adult']:first");
                if (firstAdult.length > 0) {
                    //console.log('c2')
                    firstAdult.before(template);
                } else {
                    //console.log('c3')
                    $("#additionalGuestsForm").prepend(template);
                }
            }

            var adultsCount = $("[guest-type='adult']");
            var childrenCount = $("[guest-type='child']");


        }


        if ((children < childrenBlocksLen && adults == adultBlocksLen) || (children > childrenBlocksLen && adults == adultBlocksLen) || (adults > adultBlocksLen)) {
            for (var i = 0; i < adultsCount.length; i++) {


                var index = (childrenCount.length * 1 + i * 1 + 1)

                var child = $(adultsCount[i]).find('[name^=child]:first');
                if (child.length > 0) {
                    var pattern = /child[\d]/;
                    var childName = $(child).attr("name")
                    childName = childName.replace(pattern, 'child' + index);
                    $(child).attr("name", childName);
                }

                var elements = $(adultsCount[i]).find('[name^=additionalGuest]');
                for (j = 0; j < elements.length; j++) {
                    var name = $(elements[j]).attr("name");



                    var id = $(elements[j]).attr("id");
                    var pattern = /additionalGuest(\D+1?[\D]*_?1?)[\d]{1,2}/mg
                    name = name.replace(pattern, 'additionalGuest' + '$1' + index);
                    $(elements[j]).attr("name", name);

                    if (name.indexOf('additionalGuestGender') != -1) {
                        if ($(elements[j]).parent().hasClass("checked")) {
                            $(elements[j]).click();
                            $(elements[j]).parent().addClass("checked")
                        }
                    }

                    if (id != undefined) {
                        id = id.replace(pattern, 'additionalGuest' + '$1' + index);
                        $(elements[j]).attr("id", id);
                    }
                }
                renameAdditionalGuestLabel(adultsCount[i], index);
                renameAdditionalGuestContactInfo(adultsCount[i], index);
                renameAddressDetails(adultsCount[i], index);
                renameTshirtSize(adultsCount[i], index);
                renameSingletSize(adultsCount[i], index);
            }
        }
        additionalGuestsFormValidate();
        agTshirtQuantityChange();
        agTshirtSizeChange();
        agSingletQuantityChange();
        agSingletSizeChange();
        agGenderChange();

        $("[name='cnt']").val(adultsCount.length * 1 + childrenCount.length * 1 + 1);

    }
}

function agTshirtQuantityChange() {
    $("[id^=additionalGuestTshirt_quantity]").on("change", function (e) {
        var id = $(e.target).attr("id").replace("additionalGuestTshirt_quantity", "");
        var val = $(e.target).val();
        if (val > 0) {
            if ($("#additionalGuestTshirt_id" + id).val() == '') {
                var el = $("#tshirt_size" + id + "[selected]");
                el = (el.length > 0) ? el : $("#tshirt_size" + id + ":first");
                $("#additionalGuestTshirt_id" + id).val(el.val())
            }
            $("#tshirt_size_block" + id).show();
            $("#tshirt_name_block" + id).show();
        } else {
            if (val == 0) {
                $("#tshirt_size_block" + id).hide();
                $("#additionalGuestTshirt_id" +id).val(null);
                if ($("#additionalGuestSinglet_quantity" + id).val() == 0) {
                    $("#additionalGuestTshirt_name" + id).val("");
                    $("#tshirt_name_block" + id).hide();
                }
            }
        }
    })
}

function agSingletQuantityChange() {
    $("[id^=additionalGuestSinglet_quantity]").on("change", function (e) {
        var id = $(e.target).attr("id").replace("additionalGuestSinglet_quantity", "")
        var val = $(e.target).val();
        if (val > 0) {
            if ($("#additionalGuestSinglet_id" + id).val() == '') {
                var el = $("#singlet_size" + id + " [selected]");
                //console.log(el);
                el = (el.length >  0) ? el : $("#singlet_size" + id + ":first");
                $("#additionalGuestSinglet_id" + id).val(el.val())

            }
            $("#singlet_size_block" + id).show();
            $("#tshirt_name_block" + id).show();
        } else {
            if (val == 0) {
                $("#singlet_size_block" + id).hide();
                $("#additionalGuestSinglet_id" + id).val(null);
                if ($("#additionalGuestTshirt_quantity" + id).val() == 0) {
                    $("#additionalGuestTshirt_name" + id).val("");
                    $("#tshirt_name_block" + id).hide();
                }
            }
        }
    })
}

function agTshirtSizeChange() {
    $("[id^=tshirt_size]").on("change", function (e) {
        var id = $(e.target).attr("id").replace("tshirt_size", "")
        var val = $(e.target).val();
        $("#additionalGuestTshirt_id" + id).val(val);
    })
}

function agSingletSizeChange() {
    $("[id^=singlet_size]").on("change", function (e) {
        var id = $(e.target).attr("id").replace("singlet_size", "")
        var val = $(e.target).val();
        $("#additionalGuestSinglet_id" + id).val(val);
    })
}

function agGenderChange() {
    $("[name^='additionalGuestGender']").on("change", function (e) {
        //debugger;
        var id = $(e.target).attr("name").replace("additionalGuestGender", "")
        var el = $(e.target).val();
        var tTemplate = '';
        var sTemplate = '';
        if (el === 'M') {
            var singlets = maleSinglets
            var tshirts = maleTshirts;
        }
        if (el === 'F') {
            var singlets = femaleSinglets
            var tshirts = femaleTshirts;
        }
        if (tshirts.length > 0) {
            for (var i = 0; i < tshirts.length; i++) {
                var cnt = 1;
                for (var j in tshirts[i]) {
                    if (i == 0 && cnt == 1 && $("#additionalGuestTshirt_quantity" + id).val() > 0) {
                        $("#additionalGuestTshirt_id" + id).val(j);
                    }
                    tTemplate += '<option value="' + j + '">' + tshirts[i][j] + '</option>';
                    cnt++;
                }
            }
        }
        if (singlets.length > 0) {
            for (var i = 0; i < singlets.length; i++) {
                var cnt = 1;
                for (var j in singlets[i]) {
                    if (i == 0 && cnt == 1 && $("#additionalGuestSinglet_quantity" + id).val() > 0) {
                        $("#additionalGuestSinglet_id" + id).val(j);
                    }
                    sTemplate += '<option value="' + j + '">' + singlets[i][j] + '</option>';
                    cnt++;
                }
            }
        }
        $("#tshirt_size" + id).html(tTemplate);
        $("#singlet_size" + id).html(sTemplate);

    })
}

function renameAdditionalGuestLabel(el, index) {
    var titleEl = $(el).find('.well');
    if (titleEl.length > 0) {
        var pattern = /Additional Guest ([\d])/m
        var title = $(titleEl).html()
        title = title.replace(pattern, 'Additional Guest ' + index)
        $(titleEl).html(title);
    }
}

function renameAdditionalGuestContactInfo(el, index) {
    var contacts = $(el).find("[id^=additionalGuestContactInfo]");
    var pattern = /additionalGuestContactInfo[\d]/m
    var contactsId = $(contacts).attr("id");
    contactsId = contactsId.replace(pattern, 'additionalGuestContactInfo' + index)
    $(contacts).attr("id", contactsId);
}

function renameAddressDetails(el, index) {
    var addressDetails = $(adultsCount[i]).find("[id^=adressDetails]");
    var pattern = /adressDetails[\d]/m
    var addressDetailsId = $(addressDetails).attr("id");
    addressDetailsId = addressDetailsId.replace(pattern, 'adressDetails' + index)
    $(addressDetails).attr("id", addressDetailsId);
}

function renameTshirtSize(el, index) {
    var tshirtSize = $(el).find("[id^=tshirt_size][name!=tshirt_size]");
    if (tshirtSize.length > 0) {
        for (var j = 0; j < tshirtSize.length; j++) {
            var tshirtSizeId = $(tshirtSize[j]).attr("id");
            if (tshirtSizeId != 'tshirt_size_block') {
                if (tshirtSizeId.indexOf('tshirt_size_block') != -1) {
                    var pattern = /tshirt_size_block[\d]/m
                    tshirtSizeId = tshirtSizeId.replace(pattern, 'tshirt_size_block' + index)
                } else {
                    var pattern = /tshirt_size[\d]/m
                    tshirtSizeId = tshirtSizeId.replace(pattern, 'tshirt_size' + index)
                }
                $(tshirtSize[j]).attr("id", tshirtSizeId);
            }
        }
    }
}

function renameSingletSize(el, index) {
    var singletSize = $(el).find("[id^=singlet_size][name!=singlet_size]");
    if (singletSize.length > 0) {
        for (var j = 0; j < singletSize.length; j++) {
            var singletSizeId = $(singletSize[j]).attr("id");
            if (singletSizeId != 'singlet_size_block') {
                if (singletSizeId.indexOf('singlet_size_block') != -1) {
                    var pattern = /singlet_size_block[\d]/m
                    singletSizeId = singletSizeId.replace(pattern, 'singlet_size_block' + index)
                } else {
                    var pattern = /singlet_size[\d]/m
                    singletSizeId = singletSizeId.replace(pattern, 'singlet_size' + index)
                }
                $(singletSize[j]).attr("id", singletSizeId);
            }
        }
    }
}











