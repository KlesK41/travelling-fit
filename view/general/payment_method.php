<?php
error_reporting(1);

if(isset($_GET['c'])) // if the code is set, the customer is returning to make payment
{
	Bookings::put_booking_id_into_session(urldecode($_GET['c']));
	// get amount owing - bookings->total - transactions
}
else
{
	Bookings::store_booking();
}
Payment::is_desosit_paid();
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
        </div>
      </div>
    </div>
    <div id="page-wrapper" class='container marginTop external-form'>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li>Payment method</li> 
       </ol>
       <?php if(isset($_GET['c']))
       {
       	echo "<input type='hidden' id='booking_code' name='booking_code' value='" . functions::escapeValue($_GET['c']) . "'>";
       }
       ?>
		<?php 
			// get primary guest for merchant transact record
			$primary_guest = Payment::primary_guest($_SESSION['booking_id']);
			$primary_guest_last_name = $primary_guest[0]['lastname'];
			$invoice_accom = Payment::accommodation($_SESSION['booking_id']);
			// *** TEST CONFIRMATION EMAIL *** 
			//$conf = Payment::get_email_confirmation($_SESSION['booking_id'], 'CC');
			//dump($conf);
		?>
<!--        <div id="continue_payment">
			<div class="panel panel-default margins">
				<div class="space">
					For booking ID <? //$_SESSION['booking_id'] ?> you have paid ... 
				</div>
				<div class="row space">
					<div class="col-xs-12 col-sm-2">
						Outstanding Balance: ....
					</div>
					<div class="col-xs-12 col-sm-8">		 
					</div>
				</div>
								<div class="row space">
					<div class="col-xs-12 col-sm-2">
						Package Name: <?php //echo $invoice_accom[0]['package_name']; ?>
					</div>
					<div class="col-xs-12 col-sm-8">		 
					</div>
				</div>
				<div class="row space">
				 	<div class="col-xs-12 col-sm-2">
					No. of Adults: 
				 	</div>
				 	<div class="col-xs-12 col-sm-8">		 
				 	</div>
				</div>
				<div class="row space">
				 	<div class="col-xs-12 col-sm-2">
					Tours:
				 	</div>
				 	<div class="col-xs-12 col-sm-8">		 
				 	</div>
				</div>
	       </div>
       </div> -->
      	<!-- information panel !-->
      	<!-- panel 1 !-->
		
      	<div id="depositWrapper">
	      <div class="panel panel-default margins">
				<div class="space">
					<strong>A deposit of $<?php echo Payment::deposit($_SESSION['booking_id']) ?> is required to secure this booking.</strong> 
				</div>

				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Package Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">
						<?php echo $invoice_accom[0]['package_name']; ?>
					</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>No. of Adults:</strong>
				 	</div>
				 	<div class="col-xs-12 col-sm-9">
						<?php echo Payment::get_number_adults($_SESSION['booking_id']) ?>
				 	</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>Tours:</strong>
				 	</div>
					<div class="col-xs-12 col-sm-9">
						<?php 
						$tours = Payment::tours_park_titles($_SESSION['booking_id']);
						for($i=0; $i<count($tours); $i++) {
							echo $tours[$i]['title'] . "<br>";
						}
						?>
				 	</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>Deposit Amount:</strong> 
				 	</div>
				 	<div class="col-xs-12 col-sm-9">
						$<?php echo Payment::deposit($_SESSION['booking_id']) ?>
				 	</div>
				 	<div id="totalAmountWrapper">
					 	<div class="col-xs-12 col-sm-3">
							<strong>Total Package Cost:</strong> 
					 	</div>
					 	<div class="col-xs-12 col-sm-9">
							$<?php echo Payment::total_cost($_SESSION['booking_id']) ?>
					 	</div>
					 	<div class="col-xs-12 col-sm-3">
							<strong>Total Amount Owing:</strong> 
					 	</div>
					 	<div class="col-xs-12 col-sm-9">
							$<?php echo Payment::get_amount_due($_SESSION['booking_id']) ?>
					 	</div>

					 	<div class="col-xs-12 col-sm-9">
					 		&nbsp
					 	</div>
					 	
			<?php
			// if($_SESSION['book_travel_partner_YN'] != 'Y')
			// {
			?>
					<div class="col-xs-12 col-sm-9">
					 	<i>Please Note: Direct Deposits will be deducted from the amount due once processed.</i>
					 	</div>
					 </div>
				</div>
				<div class="row space">
				 	<div class="col-xs-12 col-sm-10">
					<!-- <strong>You have paid</strong> -->
				 	</div>
				</div>
			<?php
			// }


			if(isset($_SESSION['travel_partner_id']) && $_SESSION['book_travel_partner_YN'] == 'Y')
			{
			?>
							<div class="space margins">
									<p>You have chosen to book with a travel partner.</p>
									<p>Please press "Book" to send an invoice to your travel partner.</p>
							</div>
			<?php
			}
			?>
			<form id="paymentDetailsValidate" action="index.php?v=payment_confirmation_DD" method="post" class="main">
			<?php
			if($_SESSION['book_travel_partner_YN'] != 'Y')
			{
			?>
				<div class="row space" id="chose_pay_method">
				 	<div class="col-xs-12 col-sm-10">
					<h4>Please chose a payment method*:</h4>
				 	</div>
				</div>
			
				<div class="row space inline-radio-buttons-styling" id="payment_options">
				 	<div class="col-xs-12 col-sm-3">
				 	<label for="payment_method_DD">
						<input type="radio" id="payment_method_DD" name="payment_method" value="DD" required> 
						Direct Debit
					</label>
					</div>
					<div class="col-xs-12 col-sm-3">
					<label for="payment_method_CC">
					<!-- Sorry, the credit card payments facility is done. It will be available soon. !-->
					<!-- Comment out proceeding the two lines and text between centre-creditCardWrapper div to disable credit card payments !-->
						<input type="radio" id="payment_method_CC" name="payment_method" value="CC" required>
						Credit Card 
					</label>

					</div>
				</div>
				<div class="row">
					<label for="payment_method" class="error" >Required</label><br>
				</div>

				<?php
			}
				?>
			</form>
			<?php
			if($_SESSION['book_travel_partner_YN'] != 'Y')
			{
			?>
			</div>
		</div>
		<div id="centre-creditCardWrapper"> <!-- remove this div in the event no credit card payments are being accepted !-->
			<div id="creditCardFingerPrint">
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Card Holders Name*:</strong> 

					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" id="cardholders_name" name="cardholders_name" size="32" class="required"><br />
					</div>
				</div>
				<div class="row space">
				<div class="col-xs-12 col-sm-3">
					<strong>Amount I would like to pay*:</strong> 
				</div>

				<div class="col-xs-12 col-sm-9">
					<input type="text" id="client_payment_amount_enter" value="<?php echo $_SESSION['deposit_paid_YN'] == 'N' ?  Payment::deposit($_SESSION['booking_id']) : '' ?>">
				 	<?php Payment::is_desosit_paid(); ?>
							<?php echo $_SESSION['deposit_paid_YN'] == 'N' ?  "<input type='hidden' id='deposit_paid_YN' value='N'>" : "<input type='hidden' id='deposit_paid_YN' value='Y'>" ?>
				</div>


				<!-- LIVE URL !-->
				<!-- <form class="cmxform" id="reg" method="post" action="https://transact.nab.com.au/live/directpost/authorise">   -->

				<!-- TEST URL !-->
				<form class="cmxform" id="reg" method="post" action="https://transact.nab.com.au/test/directpost/authorise">
				<!-- <form class="cmxform" id="reg" method="post" action="http://192.168.0.11/booking/index.php?v=payment_confirmation_CC"> -->


				<div class="col-xs-12 col-sm-3" id="payment_total_with_fee">
					Fee: <div id="credit_card_fee"></div>
					<strong>Plus credit card bank fee</strong> 
					<input type="text" id="client_payment_amount" value="<?php echo $_SESSION['deposit_paid_YN'] == 'N' ?  Payment::deposit($_SESSION['booking_id']) : '' ?>" readonly="readonly">

				</div>



				<div class="col-xs-12 col-sm-10">
					<input type="radio" name="EPS_CARDTYPE" value="visa" required>VISA    <input type="radio" name="EPS_CARDTYPE" value="mastercard" required>MASTERCARD   <input type="radio" name="EPS_CARDTYPE" value="amex" required>AMEX   <input type="radio" name="EPS_CARDTYPE" value="diners" required>DINERS
					<label for="card_type" class="error" style="display:none;">Please select a card type*:</label><br />
					<br>
					<input type="button" id="proceedToCCPayment" value="Proceed To Credit Card Payment >>">
				</div>
				</div>
			</div>
			<div id="creditCardWrapper">
				Payment Amount: <br> <input type="text" id="display_amount_due" readonly="readonly">

				<h4>ENTER CREDIT CARD DETAILS:</h4>

				<!--Card Holders Name:<br /> !-->
				<!-- <input type="text" name="cardholders_name" size="32" class="required">!--><br />
				Card Numbers*:<br />
				<input type="text" id="EPS_CARDNUMBER" name="EPS_CARDNUMBER" size="32"  title="Required" required><br />
				Expiry Date: (mm/yyyy)*<br />

				<select name="EPS_EXPIRYMONTH" id="EPS_EXPIRYMONTH"  title="Required" required>
				<?php
				echo "<option value=''></option>";
				for ($i = 1; $i < 13; $i++)
						echo "<option value='" . $i . "'>" . $i . "</option>";
				?>
				</select>
				<select name="EPS_EXPIRYYEAR" id="EPS_EXPIRYYEAR"  title="Required" required>
				<?php
				echo "<option value=''></option>";
				for ($i = date('Y'); $i < date('Y') + 6; $i++)
						echo "<option value='" . $i . "'>" . $i . "</option>";
						
				$timestamp=gmdate('YmdHis', time());
				?>
				</select><br />
					
				<input type="hidden" name="primary_guest_last_name" id="primary_guest_last_name" value="<?php echo $primary_guest_last_name ?>" />
				<!-- credit card variables neccessary to process transaction with NAB direct post !-->
				<input type="hidden" name="EPS_MERCHANT" value="YNX0010" />
				<input type="hidden" name="EPS_REFERENCEID"  />
				<input type="hidden" name="EPS_AMOUNT"  />
				<input type="hidden" name="EPS_TIMESTAMP"  />
				<input type="hidden" name="EPS_FINGERPRINT"  />
				<input type="hidden" name="EPS_RESULTURL" value="http://webapps.synology.me/aom/response_manual" /> <!-- test !--> 
				 <!-- <input type="hidden" name="EPS_RESULTURL" value="https://australianoutbackmarathon.com/aom/response_manual" /> <!-- live !-->
				<input type="hidden" name="EPS_REDIRECT" value="TRUE" />
				CVV:<br />
				<input type="text" name="EPS_CCV" maxlength="3" size="3"  class="required">Your CVV number is the 3 digits on the back of your card.</a><br />
				<h4>Cancellation & Refund Policy:</h4>
				Cancellation must be made in writing to ... Requests received will receive a refund less a $50 administration fee.<br />
				<br />
				<input type="checkbox" name="policy_agree" value="1"  class="required" required>I have read and agree to the cancellation policy<br />
				<label for="policy_agree" class="error" style="display:none;">To proceed you need to read and agree to the cancelation policy</label><br />
				<input type='submit' name='payment_submit' id='payment_submit' value='PROCESS PAYMENT'><br />
				<input type='button' name='cancel' id='cancel' value='CANCEL'>
				<br />
				REGISTRANTS PLEASE NOTE: While your request is being processed, be patient and do not try to resubmit the request.
				Processing can take up to 150 seconds.

				</form>
			</div>
		</div>
<?php
}
?>		
			<div id="directDebitWrapper">
			<input type="hidden" name="book_travel_partner_YN" id="book_travel_partner_YN" value="<?php echo $_SESSION['book_travel_partner_YN'] ?>" >
<?php
if($_SESSION['book_travel_partner_YN'] != 'Y')
{
?>
				<div class="space"><strong>Direct Deposit (via internet only) – from an Australian Account</strong></div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Account Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						Travelling Fit
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Bank:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						National Australia Bank		 
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>BSB:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">
						082-574		 
					</div>
				</div>
				
				<div class="row more-space">
					<div class="col-xs-12 col-sm-3">
						<strong>Account Number:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						83-544-8081		 
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 field-label">Overseas Telegraphic or Wire Transfer *</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Bank:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						National Australia Bank 
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Address:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						Shop 21, 148 The Entrance Road, Erina, NSW, 2250, Australia
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Account Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						Travelling Fit
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Account Number:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						83-544-8006
					</div>
				</div>
				
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Swift Code:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">	
						NATAAU3302S
					</div>
				</div>
				
				<div class="space">
				* Note: Payment from an overseas bank account (outside of Australia) will incur a bank fee of $25 per transaction. Please remember to add this fee to the total when making payment.
				</div>
<?php
}
?>
			</div>
			
			<div class="row space buttons-container buttons-3" id="prev_book_buttons">
				<div class="col-xs-12 col-sm-4 btn-left">
					<span class='glyphicon glyphicon-circle-arrow-left'></span>
					<input type="button" id="prev_button" value="Previous">
				</div>
				<div class="col-xs-12 col-sm-4 btn-mid">
					<input type="button" id="reset_button" value="Clear Form">
				</div>
				<div class="col-xs-6 col-sm-4 btn-right">
					<input type="button" id="next_button" value="Book">
					<span class='glyphicon glyphicon-circle-arrow-right'></span>
				</div>  
			</div>


          </div>
      </div>
      <?php
      //echo Payment::get_email_invoice($_SESSION['booking_id'], 'DD');
      ?>
  <script src="includes/js/jquery.min.js"></script>
  <script src="includes/js/jquery.dataTables.min.js"></script>
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/validate/jquery.validate.min.js"></script>  
  <script src="includes/js/lib/utilities.js"></script>
  <script src="includes/js/lib/values.js"></script>
  <script src="view/general/payment_method.js"></script>
  <script src="includes/js/cc/jquery.creditCardValidator.js"></script>
</body>
</html>