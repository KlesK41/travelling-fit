<?php

?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Travelling Fit - Payment Confirmation</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="../includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../includes/js/html5shiv.js"></script>
      <script src="../includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
<!--          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>-->
        </div>
      </div>
    </div>
    <div id="page-wrapper" class='container marginTop'>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li>Thank-you</li> 
       </ol>
		<?php
			if (isset($_SESSION['booking_id']) && !empty($_SESSION['booking_id'])) {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: Australian Outback Marathon <noreply@australianoutbackmarathon.com>'. "\r\n";
				$headers .= 'Cc: ' . "\r\n";
//				$headers .= 'Bcc: sales@australianoutbackmarathon.com, software-engineering@hotmail.com, jon@fivebyfive.com.au' . "\r\n";

				$primary_guest = Payment::primary_guest_for_payment_invoice($_SESSION['booking_id']);
//			var_dump($primary_guest);
				if(isset($_SESSION['travel_partner_id']) && $_SESSION['book_travel_partner_YN'] == 'Y')
				{
					if(!isset($_SESSION['transaction_id']))
					{
						$transaction = new Transactions();
						$transaction->booking_id = $_SESSION['booking_id'];
						$transaction->payment_method = "TP";
						$transaction->amount = Payment::deposit($_SESSION['booking_id']);
						$transaction->archived_YN = 'N';
						$transaction->funds_received_YN = 'N';

						$transaction->create();
						$_SESSION['transaction_id'] = $transaction->transaction_id;
					}
					else
					{
						$transaction = new Transactions();
						$transaction->booking_id = $_SESSION['booking_id'];
						$transaction->payment_method = "TP";
						$transaction->amount = Payment::deposit($_SESSION['booking_id']);
						$transaction->archived_YN = 'N';
						$transaction->funds_received_YN = 'N';
						$transaction->transaction_id = $_SESSION['transaction_id'];
						$transaction->updatePaymentMethod();
					}
					$travel_partner_contact = Travel_partner::getContact($_SESSION['travel_partner_id']);

					// // email not to go to client as per MW 2014-11-26
					$to_guest =	$primary_guest[0]['firstname'] . " " . $primary_guest[0]['lastname'] ." <" . $primary_guest[0]['email'] . '>';
					$subjectConf = "Booking Confirmation for " . $primary_guest[0]['firstname'] . " " .
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'];

					$message = "<p>Your travel agent contact is <strong>" . $travel_partner_contact['contact']. ", email: " . $travel_partner_contact['email'] . "</strong></p>";
					$message .= Payment::get_email_confirmation($_SESSION['booking_id'], 'DD');

					if(!mail($to_guest, $subjectConf, $message, $headers))
					{
						echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
					}

					$to =	$to_guest . ',' . $travel_partner_contact['contact'] ." <"  . $travel_partner_contact['email'] . '>';
					$subjectInv = "Booking Invoice for " . $primary_guest[0]['firstname'] . " " .
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'];

					if(!mail($to, $subjectInv, Payment::get_email_invoice($_SESSION['booking_id'], 'DD'), $headers))
					{
						echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
					}
				}
				else
				{
					if(!isset($_SESSION['transaction_id']))
					{
//					$transaction = new Transactions();
//					$transaction->booking_id = $_SESSION['booking_id'];
//					$transaction->payment_method = "DD";
//					$transaction->amount = Payment::deposit($_SESSION['booking_id']);
//					$transaction->archived_YN = 'N';
//					$transaction->funds_received_YN = 'N';
//
//					$transaction->create();
//					$_SESSION['transaction_id'] = $transaction->transaction_id;
					}
					else
					{
//					$transaction = new Transactions();
//					$transaction->booking_id = $_SESSION['booking_id'];
//					$transaction->payment_method = "DD";
//					$transaction->amount = Payment::deposit($_SESSION['booking_id']);
//					$transaction->archived_YN = 'N';
//					$transaction->funds_received_YN = 'N';
//					$transaction->transaction_id = $_SESSION['transaction_id'];
//					$transaction->updatePaymentMethod();
					}
//				 dump($primary_guest);
					$to =	$primary_guest[0]['firstname'] . " " . $primary_guest[0]['lastname'] ." <" . $primary_guest[0]['email'] . '>';
					$subjectConf = "Booking Confirmation for " . $primary_guest[0]['firstname'] . " " .
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'] . ", Transaction ID: " . $_SESSION['transaction_id'];
					$subjectInv = "Booking Invoice for " . $primary_guest[0]['firstname'] . " " .
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'] . ", Transaction ID: " . $_SESSION['transaction_id'];

//                Payment::get_email_confirmation($_SESSION['booking_id'], 'DD');
//                Payment::get_email_invoice($_SESSION['booking_id'], 'DD');

					if(!mail($to, $subjectConf, Payment::get_email_confirmation($_SESSION['booking_id'], 'DD'), $headers))
					{
						echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
					}
					if(!mail($to, $subjectInv, Payment::get_email_invoice($_SESSION['booking_id'], 'DD'), $headers))
					{
						echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
					}
				}
			}
//            var_dump($_SESSION);
			// set up mail headers

		?>

      	<div id="thankyouWrapper">
	      <div class="panel panel-default margins">
			<div class="row space">
				 <div class="col-xs-12 col-sm-2">
				 </div>
				 <div class="col-xs-12 col-sm-8">	
					<p>A confirmation email has been sent along with an invoice.</p>
					<p>If you cannot see the emails in your main folder, please check your junk mail folder.</p>
					<p><strong>Please contact us to make credit card payments</strong></p>
				 </div>
				</div>
			  	<a href="<?php echo $_SERVER['SCRIPT_NAME']?>?v=my_bookings">My bookings</a>
          </div>
      	</div>
      </div>
      <?php
	  unset($_SESSION['bpay_booking_id']);
	  unset($_SESSION['bpay_event_id']);
	  unset($_SESSION['bpay_user_id']);
	  unset($_SESSION['booking_id']);
	  unset($_SESSION['transaction_id']);
//      	session_destroy();
      // echo Payment::get_invoice($_SESSION['booking_id'], 'DD');
      ?>
  <script src="../includes/js/jquery.min.js"></script>
  <script src="../includes/js/jquery.dataTables.min.js"></script>
  <script src="../includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="../includes/js/validate/jquery.validate.min.js"></script>
  <script src="../includes/js/lib/utilities.js"></script>
  <script src="../includes/js/lib/values.js"></script>
  <script src="../view/general/payment_method.js"></script>
</body>
</html>