<?php
if(isset($_POST['firstname']))
{
  class Volunteers extends DatabaseObject
  {
    protected static $table_name="volunteers";

    function __construct() {
      $this->closeConnection();
      $this->openConnection(DB_USER, DB_PASS, DB_SERVER);
      $this->magicQuotesActive = get_magic_quotes_gpc();
      $this->realEscapeStringExists = function_exists( "mysql_real_escape_string" );
    }

    public function create()
    {
      $database = new self; // instance of database object

      $sql = "INSERT INTO volunteers ( firstname, lastname, age, phone, email, shirt_size ) VALUES (" .
      "'" . $database->escapeValue($_POST['firstname']) . "'," .
      "'" . $database->escapeValue($_POST['surname']) . "'," .
      "'" . $database->escapeValue($_POST['age']) . "'," .
      "'" . $database->escapeValue($_POST['phone']) . "'," .
      "'" . $database->escapeValue($_POST['email']) . "'," .
      "'" . $database->escapeValue($_POST['shirt_size']) . "')";
      
      if($database->query($sql)) {
        return $database->insertId();
      } 
      else 
      {
        return false; 
      }

    }
  }

  $volunteer = new Volunteers;
  if($volunteer->create())
  {
    echo "<input type='hidden' id='thankyou' name='thankyou' value='thankyou'>";
  }
  else
  {
    echo "<input type='hidden' id='database_error' name='database_error' value='database_error'>";
  }

}


/*
CREATE TABLE volunteers (
  volunteer_id int(11) NOT NULL AUTO_INCREMENT,
  firstname varchar(255) DEFAULT NULL,
  lastname varchar(255) DEFAULT NULL,
  age varchar(45) DEFAULT NULL,
  phone varchar(45) DEFAULT NULL,
  email varchar(400) DEFAULT NULL,
  shirt_size varchar(15) DEFAULT NULL COMMENT 'Small, Medium, Large, Extra Large',
  created timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (volunteer_id)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
*/

?>


<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Australian Outback Marathon - Volunteer Registration</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
    <style>
    #centre-mainWrapper {
      background-color: #E7D3C6;
      margin-top: 20px;
    }
    
    #volunteer_form {
      margin-left: 20px;
    }
    .external-form input[type="text"], .external-form select, .external-form textarea {
      background-color: #FEE;
    }
    </style>
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#"></a>
        </div>
      </div>
    </div>
    <div id="page-wrapper" class='container marginTop external-form'>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li>Australian Outback Marathon - Volunteer Registration</li> 
       </ol>
      	<!-- information panel !-->
      	<!-- panel 1 !-->
		
		<div id="centre-mainWrapper" style="display:none"> <!-- remove this div in the event no credit card payments are being accepted !-->
			<form id="volunteer_form" action="index.php?v=volunteer" method="post" >
				<div class="row">
          <div class="col-xs-12 col-sm-12">
          &nbsp;
          </div>
        </div>
        
        <div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>First Name*:</strong> 
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" id="surname" name="firstname" size="32" title="Required" required><br />
					</div>
				</div>
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Surname*:</strong> 
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" id="surname" name="surname" size="32" title="Required" required><br />
					</div>
				</div>
        <div class="row space">
          <div class="col-xs-12 col-sm-3">
            <strong>Age*:</strong> 
          </div>
          <div class="col-xs-12 col-sm-9">
            <input type="text" id="age" name="age" size="32" title="Required" required><br />
          </div>
        </div>
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Contact Phone Number*:</strong> 
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" id="phone" name="phone" size="32" title="Required" required><br />
					</div>
				</div>
				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Contact Email Address*:</strong> 
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" id="email" name="email" size="32" title="Required" required><br />
					</div>
				</div>
        <h3>Preferred Shirt Size*</h3>
        <div class="row space">
          <div class="col-xs-12 col-sm-3">
            <label for='shirt_size_1'>Small</label>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type='radio' id='shirt_size_1' name='shirt_size' value='small' title="Required" required>
          </div>
        </div>
        <div class="row space">
          <div class="col-xs-12 col-sm-3">
            <label for='shirt_size_2'>Medium</label>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type='radio' id='shirt_size_2' name='shirt_size' value='medium' title="Required" required>
          </div>
        </div>
        <div class="row space">
          <div class="col-xs-12 col-sm-3">
            <label for='shirt_size_3'>Large</label>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type='radio' id='shirt_size_3' name='shirt_size' value='large' title="Required" required>
          </div>
        </div>
        <div class="row space">
          <div class="col-xs-12 col-sm-3">
            <label for='shirt_size_4'>Extra Large</label>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type='radio' id='shirt_size_4' name='shirt_size' value='extra large' title="Required" required>
          </div>
        </div>


				<input type='submit' name='submit' id='submit' value='Submit'><br />

			</form>
		</div>
    <div id="centre-thankyouWrapper" style="display:none"> <!-- remove this div in the event no credit card payments are being accepted !-->
        <div class="row space">
          <div class="col-xs-12 col-sm-12" style="text-align:center">
            <h1>Thank-you</h1>
            <p>Your details have been submitted.</p>
          </div>
        </div>
		</div>
   


          </div>
      </div>
      <?php
      //echo Payment::get_email_invoice($_SESSION['booking_id'], 'DD');
      ?>
  <script src="includes/js/jquery.min.js"></script>
  <script src="includes/js/jquery.dataTables.min.js"></script>
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/validate/jquery.validate.min.js"></script>  
  <script src="includes/js/lib/utilities.js"></script>
  <script src="includes/js/lib/values.js"></script>
  <script src="view/general/volunteer.js"></script>
</body>
</html>