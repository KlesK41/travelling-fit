$(document).ready(function(){
	//$('#guestsDetailsValidate').validate();
	getGuestFormInputs(function(e){
		// if guest is primary contact, remove child and same as primary contact address options
		if($("#guest_number").val() == '1')
		{
			$("#guestChild").hide();
			$("#child_or_adult").attr('checked', 'false');
			$("#same_address_primary").hide();
		}
		else
		{
			$("#addressWrapper").show();
			$("#address_same_primary_yn").attr('checked', 'true');
		}
		var shirt_type = $("input[name='gender']:checked").val();
		if($('#child_YN').is(':checked')) 
		{
			Util.safe_log('child');
			shirt_type = 'C';
			getEvents("C", function(){
				
				$("input[name='prefered_event_id'][value='"+e.prefered_event_id+"']").prop('checked', true);
			});
		}
		else
		{
			getEvents("A", function(){
				$("input[name='prefered_event_id'][value='"+e.prefered_event_id+"']").prop('checked', true);	
			});
		}

		show_or_hide_address();

		getTshirtSizes(shirt_type, function(){
			$("#tshirt_id").val(e.tshirt_id);
		});

	});
});

	$( "#dob" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy',
		yearRange: "-100:+0"
	});

$("#dob").change(function(){
	Util.safe_log($("#dob").val());
})

$("input[name='gender']").change(function() {
 if(!$("#child_YN").is(':checked')){
	getTshirtSizes($(this).val());
 }
});

$("#address_same_primary_yn").change(function() {
	show_or_hide_address();
})

function show_or_hide_address()
{
	if($("#address_same_primary_yn").is(':checked')){
		$("#addressWrapper").hide();
	}
	else
	{
		$("#addressWrapper").show();
	}
}

$('#child_YN').change(function() 
{
   	if($('#child_YN').is(':checked')) 
	{
		getEvents("C");
		getTshirtSizes("C");
	}
	else
	{
		getEvents("A");
		var shirt_type = $("input[name='gender']:checked").val();
		getTshirtSizes(shirt_type);
	}
});

function childOrAdult()
{
 	if($('#child_YN').is(':checked'))
	{
		$('#trchild').css("display", "block");
		$("#prefered_events").empty();
		$('#address, #phone, #suburb_state, #pcode_country').hide();
	}
	else
	{
	$('#trchild').css("display", "none");
		$('#address, #phone, #suburb_state, #pcode_country').show();
		$("select#tshirt_id option[value!='']").remove();
		var gender_type = $("input[name='gender']:checked").val();
		if(typeof gender_type === 'undefined')
		{
			$('#tshirt_id')
			.append($("<option></option>")
			.attr("value","0")
			.text("Please Select")); 
			$("#prefered_events").empty();
		}
		else
		{
			getTshirtSizes(gender_type);
			$("#prefered_events").empty();
			getEvents("A",  function(){

			});
		}
	}
}

function getTshirtSizes(type, callback)
{

    data        = {};
    data.method     = 'getTshirtSizes';
    data.type = type;
    Util.ajaxData(data, function(returnData){
	  $("select#tshirt_id option[value!='']").remove();
	  for(var i = 0; i < returnData.length; i++)
      {
        $('#tshirt_id')
         .append($("<option></option>")
         .attr("value",returnData[i].tshirt_id)
         .text(returnData[i].size)); 
      }
      if (typeof callback == "function") callback(returnData);
    });

}

function getEvents(type, callback)
{
    data        = {};
    data.method     = 'getEvents';
    data.child_or_adult = type;
    Util.ajaxData(data, function(returnData){
    Util.safe_log(returnData);
    	$("#prefered_events").empty();
		for(var i = 0; i < returnData.length; i++)
		{
			var event_id = returnData[i].event_id;
			 $("#prefered_events").append("<input type='radio' id='prefered_event_id_" + event_id + "' name='prefered_event_id' value='" + event_id + "' required>" +
				"<label for='prefered_event_id_" + event_id + "'>" + 
				returnData[i].event_name + 
				"</label>" + 
				"<br>"
				);            
		}
		//$("#prefered_events").html('');
		$("#prefered_events").append('</fieldset>');
      	//$("#prefered_events").append('<label for="prefered_event_id" class="error" style="display: none;">Required</label>');
     	if (typeof callback == "function") callback(returnData);
    });
}

function afterFormValidate(id, limit){
	id = id.replace("Next_","");
	next_id = parseInt(id) + 1;
	$("#guestsDetailsValidate_"+id).valid();
	if($("#guestsDetailsValidate_"+id).validate().checkForm()){
		$("#guestsDetailsValidate_"+id).css("display","none");
		if(next_id <= limit){
			$("#guestsDetailsValidate_"+next_id).css("display","block");
		}else{
			$("#guestsDetailsValidate_"+limit).css("display","block");
		}
	}
}

function saveGuestFormInputs(url_action){
	data        = {};
    data.method     = 'saveGuestFormInputs';
    data.guest_number = $("#guest_number").val();
    data.post_data = $("#guestsDetailsValidate").values();
    Util.ajaxData(data, function(returnData){
        if(returnData == "S")
		{
			url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
			window.location.href = url_redirect;
		}
		else
		{
			alert('Sorry, we are currently unable to continue this booking. The session may have timed out due to inactivity for more than 30 minutes. Please try again.')
		}
    });
}


$("#next_button").click(function(){

	var guest_number = $("#guest_number").val();
	var number_guests = $("#number_guests").val();
	var url_action;
	if($("#guestsDetailsValidate").valid())
	{
		Util.safe_log('next');
		if(guest_number == number_guests)
		{
			url_action = '../~australianoutbac/booking/index.php?v=optional_tours'
			url_action = 'index.php?v=optional_tours';  // force
		}
		else
		{
			guest_number = parseInt(guest_number) + 1;
			url_action = '../~australianoutbac/booking/index.php?v=guests&i='+ guest_number;
			url_action = 'index.php?v=guests&i='+guest_number;  // force
		}

		saveGuestFormInputs(url_action);
	}
	else
	{
		$("form").find(":input.error:first").focus();
	}
});

$("#prev_button").click(function(){

	var guest_number = $("#guest_number").val();
	var url_action;
	if(guest_number == 1)
	{
		url_action = '../~australianoutbac/booking/index.php?v=package&p=1';
		url_action = 'index.php?v=package&p=1';  // force
	}
	else
	{
		guest_number = parseInt(guest_number) - 1;
		url_action = '../~australianoutbac/booking/index.php?v=guests&i='+guest_number;
		url_action = 'index.php?v=guests&i='+guest_number;  // force
	}

	saveGuestFormInputs(url_action);

});

function getGuestFormInputs(callback)
{
	data        		= {};
	data.guest_number 	= $("#guest_number").val();
    data.method     	= 'getGuestFormInputs';
    Util.ajaxData(data, function(returnData){
    	$("#guestsDetailsValidate").values(returnData);
    	if (typeof callback == "function") callback(returnData);
    });
}