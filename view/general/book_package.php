<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>TravellingFit - Book Package</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
<!--    <link rel="stylesheet" type="text/css" href="../includes/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>-->
    <link href="../includes/styles/jquery-ui.min.css" type="text/css" rel="stylesheet"/>
    <style>
        button {
            background: inherit;
            border: none;
        }
        .step_4 .title_step {
            font-size: 20px;
            font-weight: 700;
        }
        .step_4 .form-group {

        }
        .step_4 .title_tour {
            font-size: 18px;
            font-weight: 600;
            margin-bottom: 5px;
        }
        .error {
            color: red;
            border-color: red!important;
        }
        .tools a {
            width: 18px!important;
            height: 20px!important;
            background-size: cover!important;
        }
        </style>
</head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
    <!-- BEGIN HEADER INNER -->
    <div class="container">
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    Travelling Fit
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=logout';?>" class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="container">
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo BASEPATH;?>">Travelling Fit</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=my_booking';?>">My Booking</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Continue with Booking</span>
                            </li>
                        </ul>
                </div>


                <div class="select-tittle">
                    <div class="form-group">
                        <form action="" method="post">
                            <?php if (isset($completedBookingList) && !empty($completedBookingList)){ ?>
                            <div class="col-md-3">
                                <select class="bs-select form-control input-lg bs-select-hidden" name="completedBookings">
                                    <?php foreach($completedBookingList as $value) { ?>
                                    <option value="<?php echo $value['booking_id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-7">
                                <div class="buttons-wrapp">
                                    <a href="javascript:;" class="btn green">
                                        <button type="submit">
                                            Pre-fill form using these details
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            STEP 1. <span>Primary Contact Details </span>
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="<?php echo ($booking->step >= 1) ? 'expand' : 'collapse';?>" data-original-title=""
                                               title=""> </a>
                                            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
<!--                                            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                                        </div>
                                    </div>
                                    <div class="portlet-body form" <?php if ($booking->step >= 1) echo 'style="display:none"';?>>
                                        <!-- BEGIN FORM-->
                                        <form action="" class="form-horizontal" method="post" id="primaryContactDetailsForm">
                                            <input type="hidden" name="step" value="1">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Title:</label>
                                                    <div class="col-md-4">
                                                        <select class="bs-select form-control input-lg bs-select-hidden" id="primaryContactTitle" name="primaryContactTitle">
                                                            <option value=" ">Please Select</option>
                                                            <?php if (!empty($titles)) {
                                                                foreach ($titles as $title) { ?>
                                                                    <option <?php if ($guest->title == $title['title']) echo "selected";?>><?php echo $title['title'];?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">First Name*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="First Name" id="primaryContactFirstname"
                                                                   name="primaryContactFirstname" value="<?php echo $guest->firstname; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Middle Name:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Middle Name" id="primaryContactMiddle_name"
                                                                   name="primaryContactMiddle_name" value="<?php echo $guest->middle_name; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Last Name:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Last Name" id="primaryContactLastname"
                                                                   name="primaryContactLastname" value="<?php echo $guest->lastname; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Known as (PreferredName):</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Known as (Preferred Name)" id="primaryContactPrefered_name"
                                                                   name="primaryContactPrefered_name" value="<?php echo $guest->prefered_name; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Date of Birth*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">

                                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                                    <input type="text" class="form-control input-lg" name="primaryContactDob"
                                                                           id="primaryContactDob" value="<?php echo date('d/m/Y', strtotime($guest->dob)); ?>">
                                                                    <br>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Gender:</label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios25">
                                                                    <span>
                                                                        <input type="radio" name="primaryContactGender" class="male-radio gender"
                                                                                 id="optionsRadios25" value="M"
                                                                                 <?php if ($guest->gender == "M" || $guest->gender == null) echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                Male </label>
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios26">
                                                                    <span class="checked">
                                                                        <input type="radio" class="female-radio gender"
                                                                                               name="primaryContactGender"
                                                                                               id="optionsRadios26"
                                                                                               value="F" <?php if ($guest->gender == "F") echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                Female </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Event /Participation*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <select class="bs-select form-control input-lg bs-select-hidden" id="primaryContactPartisipation"
                                                                    name="primaryContactPartisipation">
                                                                <option <?php if ($guest->partisipation == 'Marathon') echo "selected";?>>Marathon</option>
                                                                <option <?php if ($guest->partisipation == 'Half Marathon') echo "selected";?>>Half Marathon</option>
                                                                <option <?php if ($guest->partisipation == 'Ultra Marathon') echo "selected";?>>Ultra Marathon</option>
                                                                <option <?php if ($guest->partisipation == 'Multi Stage') echo "selected";?>>Multi Stage</option>
                                                                <option <?php if ($guest->partisipation == 'Other Distance') echo "selected";?>>Other Distance</option>
                                                                <option <?php if ($guest->partisipation == 'Non-Runner') echo "selected";?>>Non-Runner</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Dietry /Medical requirements:</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" rows="3" id="primaryContactDietry_requirements"
                                                                  name="primaryContactDietry_requirements"><?php echo $guest->dietry_requirements; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">

                                                    </label>
                                                    <div class="col-md-9">
                                                        <a href="http://www.travellingfit.com/SiteFiles/travellingfitcomau/pdf/TF_tshirt_size_chart.pdf" target="_blank">
                                                            Please Click Here for Size Chart and Designs
                                                        </a>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Tshirt quantity:</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <select class="bs-select form-control input-lg bs-select-hidden" id="primaryContactTshirt_quantity"
                                                                    name="primaryContactTshirt_quantity">
                                                                <?php for ($i = 0; $i<= 7; $i++) { ?>
                                                                    <option <?php if ($guest->tshirt_quantity == $i) echo "selected";?>><?php echo $i;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="tshirt_size_block" <?php if ($guest->tshirt_quantity == 0) {?>style="display: none" <?php } ?>>
                                                        <label class="col-md-2 control-label">Tshirt size:</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <select class="bs-select form-control input-lg bs-select-hidden" id="tshirt_size" name="tshirt_size">
                                                                    <?php if (!empty($tshirts)) {
                                                                        foreach ($tshirts as $tshirt) { ?>
                                                                            <option <?php if ($guest->tshirt_id == key($tshirt)) echo "selected";?> value="<?php echo key($tshirt);?>"><?php echo current($tshirt);?></option>
                                                                        <?php }
                                                                    } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Singlet quantity:</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <select class="bs-select form-control input-lg bs-select-hidden" id="primaryContactSinglet_quantity"
                                                                    name="primaryContactSinglet_quantity">
                                                                <?php for ($i = 0; $i<= 7; $i++) { ?>
                                                                    <option <?php if ($guest->singlet_quantity == $i) echo "selected";?>><?php echo $i;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div id="singlet_size_block" <?php if ($guest->singlet_quantity == 0) {?>style="display: none" <?php } ?>>
                                                        <label class="col-md-2 control-label">Singlet size:</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <select class="bs-select form-control input-lg bs-select-hidden" id="singlet_size" name="singlet_size">
                                                                    <?php if (!empty($singlets)) {
                                                                        foreach ($singlets as $singlet) { ?>
                                                                            <option <?php if ($guest->singlet_id == key($singlet)) echo "selected";?> value="<?php echo key($singlet);?>"><?php echo current($singlet);?></option>
                                                                        <?php }
                                                                    } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>





                                                <input type="hidden" name="primaryContactTshirt_id" id="primaryContactTshirt_id" value="<?php echo $guest->tshirt_id;?>">

                                                <input type="hidden" name="primaryContactSinglet_id" id="primaryContactSinglet_id" value="<?php echo $guest->singlet_id;?>">

                                                <div class="form-group" id="tshirt_name_block" <?php if ($guest->tshirt_quantity == 0 && $guest->singlet_quantity == 0) {?> style="display: none" <?php } ?>>
                                                    <label class="col-md-3 control-label">Optional name on t-shirt:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Tshirt name" id="primaryContactTshirt_name"
                                                                   name="primaryContactTshirt_name" value="<?php echo $guest->tshirt_name;?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Occupation:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Occupation" id="primaryContactOccupation"
                                                                   name="primaryContactOccupation" value="<?php echo $guest->occupation; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Address*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Address" id="primaryContactAddress_1"
                                                                   name="primaryContactAddress_1" value="<?php echo $guest->address_1; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Suburb*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Suburb" id="primaryContactSuburb"
                                                                   name="primaryContactSuburb" value="<?php echo $guest->suburb; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">State / Province*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="State / Province" id="primaryContactState"
                                                                   name="primaryContactState" value="<?php echo $guest->state; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Postcode*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Postcode" id="primaryContactPostcode"
                                                                   name="primaryContactPostcode" value="<?php echo $guest->postcode; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Country*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
<!--                                                            <input type="text" class="form-control input-lg" placeholder="Country" id="primaryContactCountry"-->
<!--                                                                   name="primaryContactCountry" value="--><?php //echo $guest->country; ?><!--">-->
                                                            <select class="bs-select form-control input-lg bs-select-hidden" id="primaryContactCountry"
                                                                    name="primaryContactCountry">
                                                                <?php foreach ($countries as $country) { ?>
                                                                    <?php if ($guest->country == null && $country == 'Australia') {
                                                                        echo '<option selected>' . $country . '</option>';
                                                                    } else { ?>
                                                                        <option <?php if ($guest->country == $country) echo "selected";?>><?php echo $country;?></option>
                                                                <?php } } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Preferred Phone Number*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder=" Phone Number" id="primaryContactPhone1_preferred"
                                                                   name="primaryContactPhone1_preferred" value="<?php echo $guest->phone1_preferred; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Work Number?:</label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios21">
                                                                    <span>
                                                                        <input type="radio" name="primaryContactPhone1_work_number_yn"
                                                                                 id="optionsRadios21" value="Y" <?php if ($guest->phone1_work_number_yn == "Y") echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                Yes </label>
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios20">
                                                                    <span class="checked">
                                                                        <input type="radio" name="primaryContactPhone1_work_number_yn"
                                                                            id="optionsRadios20" value="N" <?php if ($guest->phone1_work_number_yn == "N") echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Alternate Phone Number:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Alternate Phone Number" id="primaryContactPhone2_alternate"
                                                                   name="primaryContactPhone2_alternate" value="<?php echo $guest->phone2_alternate; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Work Number?:</label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios25">
                                                                    <span>
                                                                        <input type="radio" name="primaryContactPhone2_work_number_yn"
                                                                                 id="optionsRadios25" value="Y"
                                                                            <?php if ($guest->phone2_work_number_yn == "Y") echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                Yes </label>
                                                            <label class="radio-inline">
                                                                <div class="radio" id="uniform-optionsRadios26"><span
                                                                        class="checked">
                                                                        <input type="radio" name="primaryContactPhone2_work_number_yn"
                                                                            id="optionsRadios26" value="N" <?php if ($guest->phone2_work_number_yn == "N") echo "checked"; ?>>
                                                                    </span>
                                                                </div>
                                                                No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Email Address:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Email Address" id="primaryContactEmail"
                                                                   name="primaryContactEmail" value="<?php echo $guest->email; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Emergency Contact Name*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Emergency Contact Name" id="primaryContactEmergency_contact_name"
                                                                   name="primaryContactEmergency_contact_name" value="<?php echo $guest->emergency_contact_name; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Emergency Contact Phone Number*:</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-lg" placeholder="Emergency Contact Phone Number" id="primaryContactEmergency_contact_phone_number"
                                                                   name="primaryContactEmergency_contact_phone_number" value="<?php echo $guest->emergency_contact_phone_number; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <div class="buttons-wrapp">
                                                            <a href="javascript:;" class="btn green">
                                                                <button type="submit">Next
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <i class="fa fa-angle-right"></i>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($booking->step >= 1) { ?>
                <div class="row" id="secondStepContentRow">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green" id="secondStepContent">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            STEP 2. <span>Additional Guest Details</span>
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="<?php echo ($booking->step > 1) ? 'expand' : 'collapse';?>" data-original-title="" title=""> </a>
                                            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
<!--                                            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                                        </div>
                                    </div>
                                    <div class="portlet-body form" <?php if ($booking->step > 1) echo 'style="display:none"';?>>
                                        <!-- BEGIN FORM-->
                                        <form action="" class="form-horizontal" method="post" id="number_guests_form">
                                            <input type="hidden" name="step" value="2">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label>Number of Additional Guest</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Adults</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="bs-select form-control input-lg bs-select-hidden" name="number_adult_guests">
                                                            <?php for ($i = 0; $i <=5; $i++) { ?>
                                                            <option <?php if ($booking->number_adult_guests == $i) echo "selected"; ?>><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Children</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="bs-select form-control input-lg bs-select-hidden" name="number_children_guests">
                                                            <?php for ($i = 0; $i <=5; $i++) { ?>
                                                                <option <?php if ($booking->number_children_guests == $i) echo "selected"; ?>><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="note note-info">
                                                    <p>
                                                        Note: A Child is considered 12 years or under at the time of check in
                                                    </p>
                                                </div>
<!--                                                --><?php //if ($booking->step == 1) {?>
<!--                                                <div class="form-actions">-->
<!--                                                    <div class="row">-->
<!--                                                        <div class="col-md-offset-3 col-md-9">-->
<!--                                                            <div class="buttons-wrapp">-->
<!--                                                                <a href="javascript:;" class="btn green">-->
<!--                                                                    <button type="button" id="choose_number_guests">Next-->
<!--                                                                        <i class="fa fa-angle-right"></i>-->
<!--                                                                        <i class="fa fa-angle-right"></i>-->
<!--                                                                    </button>-->
<!--                                                                </a>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                --><?php //} ?>
                                        </form>
                                        <!-- END FORM-->
                                    </div>

                                    <?php if (!empty($additionalGuests) || $booking->step > 0 ) {
                                        $cnt=1;?>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="" class="form-horizontal" method="post" id="additionalGuestsForm">
                                            <input type="hidden" name="step" value="2">
                                            <input type="hidden" name="additionalGuests" value="">
                                            <?php foreach ($additionalGuests as $agKey => $agValue) {
                                                if ((is_numeric($agValue) && $agValue > 0) || (is_array($agValue) && count($agValue) > 0)) {
                                                    $limit = (is_numeric($agValue)) ? $agValue : count($agValue);
                                                    for ($i = 0; $i < $limit; $i++) { ?>
                                            <div guest-type="<?php echo ($agKey == 'adults') ? "adult" : "child"?>">
                                                <p id="dynamic_pager_content1" class="well">
                                                    Additional Guest <?php echo $cnt; ?> - <?php echo ($agKey == 'adults') ? "Adult:" : "Child:"?>
                                                </p>
                                                <input type="hidden" name="additionalGuestId[]" value="<?php echo (isset($agValue[$i]->user_id) && !empty($agValue[$i]->user_id)) ? $agValue[$i]->user_id : ""; ?>">
                                                <input type="hidden" name="child<?php echo $cnt;?>" value="<?php echo ($agKey == 'adults') ? "N" : "Y"?>">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Title:</label>
                                                        <div class="col-md-9">
    <!--                                                        <input type="text" class="form-control input-lg " value="--><?php //echo (isset($agValue[$i]->title)) ? $agValue[$i]->title : ""; ?><!--"-->
    <!--                                                               placeholder="Title" name="additionalGuestTitle--><?php //echo $cnt;?><!--">-->

                                                            <select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTitle<?php echo $cnt;?>"
                                                                    name="additionalGuestTitle<?php echo $cnt;?>">
                                                                <option value=" ">Please Select</option>
                                                                <?php if (!empty($titles)) {
                                                                    foreach ($titles as $title) { ?>
                                                                        <option <?php if (isset($agValue[$i]->title) && $agValue[$i]->title == $title['title']) echo "selected";?>><?php echo $title['title']?></option>
                                                                <?php } }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">First Name*:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg required"  value="<?php echo (isset($agValue[$i]->firstname)) ? $agValue[$i]->firstname : ""; ?>"
                                                                       placeholder="First Name" name="additionalGuestFirstname<?php echo $cnt;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Middle Name:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg"  value="<?php echo (isset($agValue[$i]->middle_name)) ? $agValue[$i]->middle_name : ""; ?>"
                                                                       placeholder="Middle Name" name="additionalGuestMiddle_name<?php echo $cnt;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Last Name:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg" value="<?php echo (isset($agValue[$i]->lastname)) ? $agValue[$i]->lastname : ""; ?>"
                                                                       placeholder="Last Name" name="additionalGuestLastname<?php echo $cnt;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Known as (Preferred Name):</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg" value="<?php echo (isset($agValue[$i]->prefered_name)) ? $agValue[$i]->prefered_name : ""; ?>"
                                                                       placeholder="Known as (Preferred Name)" name="additionalGuestPrefered_name<?php echo $cnt;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Date of Birth*:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->dob)) ? $agValue[$i]->dob : ""; ?>"
                                                                           name="additionalGuestDob<?php echo $cnt;?>">
                                                                    <br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Gender:</label>
                                                        <div class="col-md-9">
                                                            <div class="radio-list">
                                                                <label class="radio-inline">
                                                                    <div class="radio" id="uniform-optionsRadios25">
                                                                        <span>
                                                                            <input type="radio" name="additionalGuestGender<?php echo $cnt; ?>"
                                                                                    id="optionsRadios25" value="M"
                                                                                    <?php if (isset($agValue[$i]->gender) && $agValue[$i]->gender== "M") echo "checked"; ?>>
                                                                                 </span>
                                                                             </div>
                                                                    Male </label>
                                                                <label class="radio-inline">
                                                                    <div class="radio" id="uniform-optionsRadios26"><span
                                                                            class="checked">
                                                                            <input type="radio"
                                                                                                   name="additionalGuestGender<?php echo $cnt; ?>"
                                                                                                   id="optionsRadios26"
                                                                                                   value="F"
                                                                                                   <?php if (isset($agValue[$i]->gender) && $agValue[$i]->gender== "F") echo "checked"; ?>>
                                                                                               </span>
                                                                                           </div>
                                                                    Female </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Event / Participation*:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
    <!--                                                            <input type="text" class="form-control input-lg required" value="--><?php //echo (isset($agValue[$i]->partisipation)) ? $agValue[$i]->partisipation : ""; ?><!--"-->
    <!--                                                                   placeholder="Event / Partisipation*:" name="additionalGuestPartisipation--><?php //echo $cnt;?><!--">-->

                                                                <select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestPartisipation<?php echo $cnt;?>"
                                                                        name="additionalGuestPartisipation<?php echo $cnt;?>">
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Marathon') echo "selected";?>>Marathon</option>
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Half Marathon') echo "selected";?>>Half Marathon</option>
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Ultra Marathon') echo "selected";?>>Ultra Marathon</option>
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Multi Stage') echo "selected";?>>Multi Stage</option>
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Other Distance') echo "selected";?>>Other Distance</option>
                                                                    <option <?php if (isset($agValue[$i]->partisipation) && $agValue[$i]->partisipation == 'Non-Runner') echo "selected";?>>Non-Runner</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Dietry /Medical requirements:</label>
                                                        <div class="col-md-9">
                                                        <textarea class="form-control" rows="3" id="additionalGuestDietry_requirements<?php echo $cnt;?>"
                                                                  name="additionalGuestDietry_requirements<?php echo $cnt;?>"><?php echo (isset($agValue[$i]->dietry_requirements)) ? $agValue[$i]->dietry_requirements : ""; ?></textarea>
                                                        </div>
                                                    </div>



                                                    <!-- T-shirts and singlets for additional guests start -->
                                                    <?php if ($agValue[$i]->gender == 'F') {
                                                        $tshirts = $femaleTshirts;
                                                        $singlets = $femaleSinglets;
                                                    } elseif ($agValue[$i]->gender == 'M') {
                                                        $tshirts = $maleTshirts;
                                                        $singlets = $maleSinglets;
                                                    }?>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">

                                                        </label>
                                                        <div class="col-md-9">
                                                            <a href="http://www.travellingfit.com/SiteFiles/travellingfitcomau/pdf/TF_tshirt_size_chart.pdf" target="_blank">
                                                                Please Click Here for Size Chart and Designs
                                                            </a>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Tshirt quantity:</label>
                                                        <div class="col-md-3">
                                                            <div class="input-group">
                                                                <select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestTshirt_quantity<?php echo $cnt;?>"
                                                                        name="additionalGuestTshirt_quantity<?php echo $cnt;?>">
                                                                    <?php for ($j = 0; $j<= 7; $j++) { ?>
                                                                        <option <?php if (isset($agValue[$i]->tshirt_quantity) && $agValue[$i]->tshirt_quantity == $j) echo "selected";?>><?php echo $j;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="tshirt_size_block<?php echo $cnt;?>" <?php if ((isset($agValue[$i]->tshirt_quantity) && $agValue[$i]->tshirt_quantity == 0)) {?>style="display: none" <?php } ?>>
                                                            <label class="col-md-2 control-label">Tshirt size:</label>
                                                            <div class="col-md-4">
                                                                <div class="input-group">
                                                                    <select class="bs-select form-control input-lg bs-select-hidden" id="tshirt_size<?php echo $cnt;?>">
                                                                        <?php if (!empty($tshirts)) {
                                                                            foreach ($tshirts as $tshirt) { ?>
                                                                                <option <?php if (isset($agValue[$i]->tshirt_id) && $agValue[$i]->tshirt_id == key($tshirt)) echo "selected";?> value="<?php echo key($tshirt);?>"><?php echo current($tshirt);?></option>
                                                                            <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Singlet quantity:</label>
                                                        <div class="col-md-3">
                                                            <div class="input-group">
                                                                <select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestSinglet_quantity<?php echo $cnt;?>"
                                                                        name="additionalGuestSinglet_quantity<?php echo $cnt;?>">
                                                                    <?php for ($j = 0; $j<= 7; $j++) { ?>
                                                                        <option <?php if (isset($agValue[$i]->singlet_quantity) && $agValue[$i]->singlet_quantity == $j) echo "selected";?>><?php echo $j;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="singlet_size_block<?php echo $cnt;?>" <?php if (isset($agValue[$i]->singlet_quantity) && $agValue[$i]->singlet_quantity == 0) {?>style="display: none" <?php } ?>>
                                                            <label class="col-md-2 control-label">Singlet size:</label>
                                                            <div class="col-md-4">
                                                                <div class="input-group">
                                                                    <select class="bs-select form-control input-lg bs-select-hidden" id="singlet_size<?php echo $cnt;?>"  >
                                                                        <?php if (!empty($singlets)) {
                                                                            foreach ($singlets as $singlet) { ?>
                                                                                <option <?php if (isset($agValue[$i]->singlet_id) && $agValue[$i]->singlet_id == key($singlet)) echo "selected";?> value="<?php echo key($singlet);?>"><?php echo current($singlet);?></option>
                                                                            <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="additionalGuestTshirt_id<?php echo $cnt;?>" id="additionalGuestTshirt_id<?php echo $cnt;?>"
                                                           value="<?php echo (isset($agValue[$i]->tshirt_id)) ? $agValue[$i]->tshirt_id : "";?>">

                                                    <input type="hidden" name="additionalGuestSinglet_id<?php echo $cnt;?>" id="additionalGuestSinglet_id<?php echo $cnt;?>"
                                                           value="<?php echo (isset($agValue[$i]->singlet_id)) ? $agValue[$i]->singlet_id : "";?>">

                                                    <div class="form-group" id="tshirt_name_block<?php echo $cnt;?>"
                                                        <?php if ((isset($agValue[$i]->tshirt_quantity) && $agValue[$i]->tshirt_quantity == 0) && (isset($agValue[$i]->singlet_quantity) && $agValue[$i]->singlet_quantity == 0)) {?> style="display: none" <?php } ?>>
                                                        <label class="col-md-3 control-label">Optional name on t-shirt:</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-lg" placeholder="Tshirt name" id="additionalGuestTshirt_name<?php echo $cnt;?>"
                                                                       name="additionalGuestTshirt_name<?php echo $cnt;?>" value="<?php echo (isset($agValue[$i]->tshirt_name)) ? $agValue[$i]->tshirt_name : ""; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- T-shirts and singlets for additional guests end -->


                                                    <?php if ((isset($agValue[$i]->address_1) && $agValue[$i]->address_1 == $guest->address_1) &&
                                                    (isset($agValue[$i]->suburb) && $agValue[$i]->suburb == $guest->suburb) &&
                                                    (isset($agValue[$i]->state) && $agValue[$i]->state == $guest->state) &&
                                                    (isset($agValue[$i]->postcode) && $agValue[$i]->postcode == $guest->postcode) &&
                                                    (isset($agValue[$i]->country) && $agValue[$i]->country == $guest->country) &&
                                                    (isset($agValue[$i]->phone1_preferred) && $agValue[$i]->phone1_preferred == $guest->phone1_preferred)) {
                                                        $as_primary = true;
                                                    } else {
                                                        $as_primary = false;
                                                    }
                                                    ?>


                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="adressDetails">Adress and Details same as primary contact? </label>
                                                        <div class="col-md-9">
                                                            <div class="checkbox-list">
                                                                <label class="checkbox-inline">
                                                                    <input type="checkbox" id="adressDetails<?php echo $cnt; ?>" value="option1" <?php if ($as_primary) echo "checked"; ?>>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="additionalGuestContactInfo<?php echo $cnt; ?>" <?php if ($as_primary) echo "style='display:none'";?>>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Address*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->address_1)) ? $agValue[$i]->address_1 : ""; ?>"
                                                                           placeholder="Address" name="additionalGuestAddress_1<?php echo $cnt;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Suburb*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->suburb)) ? $agValue[$i]->suburb : ""; ?>"
                                                                           placeholder="Suburb" name="additionalGuestSuburb<?php echo $cnt;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">State / Province*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->state)) ? $agValue[$i]->state : ""; ?>"
                                                                           placeholder="State / Province" name="additionalGuestState<?php echo $cnt;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Postcode*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->postcode)) ? $agValue[$i]->postcode : ""; ?>"
                                                                           placeholder="Postcode" name="additionalGuestPostcode<?php echo $cnt; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Country*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
        <!--                                                            <input type="text" class="form-control input-lg required" value="--><?php //echo (isset($agValue[$i]->country)) ? $agValue[$i]->country : ""; ?><!--"-->
        <!--                                                                   placeholder="Country" name="additionalGuestCountry--><?php //echo $cnt;?><!--">-->
                                                                    <select class="bs-select form-control input-lg bs-select-hidden" id="additionalGuestCountry<?php echo $cnt;?>"
                                                                            name="additionalGuestCountry<?php echo $cnt;?>">
                                                                        <?php foreach ($countries as $country) { ?>
                                                                            <option <?php if (isset($agValue[$i]->country) && $agValue[$i]->country == $country) echo "selected";?>><?php echo $country;?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Preferred Phone Number*:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg required" value="<?php echo (isset($agValue[$i]->phone1_preferred)) ? $agValue[$i]->phone1_preferred : ""; ?>"
                                                                           placeholder=" Phone Number" name="additionalGuestPhone1_preferred<?php echo $cnt;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Email Address:</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-lg" value="<?php echo (isset($agValue[$i]->email)) ? $agValue[$i]->email : ""; ?>"
                                                                           placeholder="Email Address" name="additionalGuestEmail<?php echo $cnt;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $cnt++; } } } ?>

                                            <input type="hidden" name="cnt" value="<?php echo $cnt;?>">
<!--                                            --><?php //if ($cnt > 0) { ?>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <div class="buttons-wrapp">
                                                            <a href="javascript:;" class="btn green" id="additionalGuestsFormSubmit">
                                                                <button type="submit">Next
                                                                <i class="fa fa-angle-right"></i>
                                                                <i class="fa fa-angle-right"></i>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<!--                                            --><?php //} ?>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <?php } ?>
            </div>
            <?php if ($booking->step >= 2) { ?>
<!--                --><?php //var_dump($optionalTours);?>
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        STEP 3. <span>Additional Request / Information Modify</span>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="<?php echo ($booking->step == 2 || empty($optionalTours)) ? 'collapse' : 'expand';?>" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
<!--                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                                    </div>
                                </div>
                                <div class="portlet-body form" <?php if ($booking->step == 3 && !empty($optionalTours) || !empty($optionalTours)) echo 'style="display:none"'; ?>>
                                    <!-- BEGIN FORM-->
                                    <form action="" class="form-horizontal" method="post" id="additional_request_form">
                                        <input type="hidden" value="3" name="step">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Extra Nights – Pre
                                                    Itinerary:</label>
                                                <div class="col-md-1">
                                                    <div class="margin-bottom-10">
                                                        <select class="bs-select form-control" data-width="125px"
                                                                id="extra_nights_pre" name="extra_nights_pre">
                                                            <?php for ($i = 0; $i <= 6; $i++) {
                                                                if ($i == 6) { ?>
                                                                    <option <?php if ($booking->extra_nights_pre > 5) echo "selected"; ?>>>5</option>
                                                                <?php } else { ?>
                                                                <option value="<?php echo $i;?>"
                                                                    <?php if ($booking->extra_nights_pre == $i) echo "selected"; ?>><?php echo $i;?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="extra_nigths_pre_other"style="display: <?php echo ($booking->extra_nights_pre > 5) ? "block" : "none"?>">
                                                    <label class="control-label col-md-2">Other</label>
                                                    <div class="col-md-1">
                                                        <div class="margin-bottom-10">
                                                            <input type="text" class="form-control" name="extra_nigths_pre_other"
                                                                   placeholder="" value="<?php echo ($booking->extra_nights_pre > 5) ? $booking->extra_nights_pre : ""; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Extra Nights – Post
                                                    Itinerary:</label>
                                                <div class="col-md-1">
                                                    <div class="margin-bottom-10">
                                                        <select class="bs-select form-control" data-width="125px"
                                                                id="extra_nights_post" name="extra_nights_post">
                                                                <?php for ($i = 0; $i <= 6; $i++) {
                                                                    if ($i == 6) { ?>
                                                                    <option <?php if ($booking->extra_nights_post > 5) echo "selected"; ?>>>5</option>
                                                                    <?php } else { ?>
                                                                    <option value="<?php echo $i;?>"
                                                                        <?php if ($booking->extra_nights_post == $i) echo "selected"; ?>><?php echo $i;?></option>
                                                                <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="extra_nigths_post_other" style="display: <?php echo ($booking->extra_nights_post > 5) ? "block" : "none"?>">
                                                    <label class="control-label col-md-2">Other</label>
                                                    <div class="col-md-1">
                                                        <div class="margin-bottom-10">
                                                            <input type="text" class="form-control" placeholder="" name="extra_nigths_post_other"
                                                                   value="<?php echo ($booking->extra_nights_post > 5) ? $booking->extra_nights_post : "";?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Active Living Membership
                                                    number:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="<?php echo $booking->active_living_membership;?>"
                                                           placeholder="Active Living Membership number:" name="active_living_membership"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Bedding Configuration:</label>

                                                <div class="col-md-9">
                                                    <select class="form-control" name="bedding_id" id="bedding_id">
                                                        <?php if (!empty($beddings)) {
                                                            foreach ($beddings as $bedding) { ?>
                                                                <option value="<?php echo $bedding->bedding_id?>"
                                                                    <?php if ($bedding->bedding_id == $booking->bedding_configuration_id) echo "selected"; ?>><?php echo $bedding->bedding_name?></option>
                                                        <?php } } ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Room Share Request:</label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        Travelling Fit can help match you with someone of the same
                                                        gender in a twin room to save paying the single supplement. We
                                                        will try our best but this is not guaranteed. Should we be able
                                                        to match you a $55 fee applies.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Do you require a Room
                                                    Share?:</label>

                                                <div class="col-md-9">
                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="share_request_yn" id="optionsRadios4"
                                                                   value="Y" <?php if ($booking->share_request_yn == 'Y') echo "checked"; ?>> Yes </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="share_request_yn" id="optionsRadios5"
                                                                   value="N" <?php if ($booking->share_request_yn == 'N') echo "checked"; ?>> No </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">How did you hear about
                                                    us?:</label>

                                                <div class="col-md-9">
                                                    <select class="form-control" name="how_hear_id" id="how_hear_id">
                                                        <?php if (!empty($hearOptions)) {
                                                            foreach ($hearOptions as $option) { ?>
                                                                <option value="<?php echo $option->how_hear_id?>"
                                                                    <?php if ($booking->how_hear_id == $option->how_hear_id) echo "selected"; ?>><?php echo $option->name;?></option>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Travel Insurance:</label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        Travel Insurance is not included in the package price. Please
                                                        read clause 13 of Booking Terms and Conditions before making
                                                        your decision.
                                                        <a href="<?php echo $_SERVER['SCRIPT_NAME']?>?v=terms_and_conditions">Read Now</a>
                                                    </p>

                                                    <div class="radio-list">
                                                        <label>
                                                            <input type="radio" name="travel_insurance_option_id" id="optionsRadios1"
                                                                   value="1" <?php if ($booking->travel_insurance_option_id == 1) echo "checked"; ?>>
                                                            I will take out travel insurance via the Travelling Fit
                                                            website and earn
                                                            25% discount.
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="travel_insurance_option_id" id="optionsRadios2"
                                                                   value="2" <?php if ($booking->travel_insurance_option_id == 2) echo "checked"; ?>>
                                                            I would like to take out travel insurance but would like
                                                            Travelling Fit to
                                                            issue my policy at the retail price.
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="travel_insurance_option_id" id="optionsRadios3"
                                                                   value="3" <?php if ($booking->travel_insurance_option_id == 3) echo "checked"; ?>>
                                                            I do not wish to take out travel insurance through
                                                            Travelling Fit.
                                                            Warning: please refer to clause 13.2 & 13.3 of the Booking
                                                            Terms
                                                            and Conditions before accepting this option.
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Additional Information:</label>

                                                <div class="col-md-9">
                                                    <textarea class="form-control" rows="3" name="additional_information"><?php echo $booking->additional_information; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp">
                                                            <a href="javascript:;" class="btn green">
                                                                <button type="submit">Next
                                                                <i class="fa fa-angle-right"></i>
                                                                <i class="fa fa-angle-right"></i>
                                                                </button>
                                                            </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if ($booking->step >= 3 && !empty($optionalTours)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            STEP 4. <span>Optional Tours </span>
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
<!--                                            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                                        </div>
                                    </div>
                                    <?php if (!empty($optionalTours)) { ?>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="" class="form-horizontal step_4" method="post">
                                            <input type="hidden" name="step" value="4">
                                            <div class="form-body">
                                            <?php foreach ($optionalTours as $optionalTour) { ?>
                                                <p class="title_step">Which Optional Tours would you like to book?</p>
                                                <div class="form-group">
                                                    <div class="col-md-9">                                                        
                                                        <div class="title_tour"><?php echo $optionalTour->title; ?></div>
                                                        <div class="desc_tour" style="word-break: break-all;"><?php echo $optionalTour->description; ?></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="radio-list">
                                                            <label>
                                                                <div class="radio" id="uniform-optionsRadios22">
                                                                    <span>
                                                                        <input type="radio" name="optionTour<?php echo $optionalTour->optional_tour_id; ?>"
                                                                               id="optionsRadios22" value="<?php echo $optionalTour->optional_tour_id; ?>"
                                                                            <?php if (in_array($optionalTour->optional_tour_id, $bookingOptionalTours)) echo "checked"; ?>>
                                                                    </span>
                                                                </div> Add to Itinerary
                                                            </label>
                                                            <label>
                                                                <div class="radio" id="uniform-optionsRadios23">
                                                                    <span>
                                                                        <input type="radio" name="optionTour<?php echo $optionalTour->optional_tour_id; ?>" id="optionsRadios23" value=""
                                                                            <?php if (!in_array($optionalTour->optional_tour_id, $bookingOptionalTours)) echo "checked"; ?>>
                                                                    </span>
                                                                </div> No, thanks
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>

                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <div class="buttons-wrapp">
                                                            <a href="javascript:;" class="btn green">
                                                                <button type="submit">Next
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <i class="fa fa-angle-right"></i>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <!-- <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
            <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
               title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase
                Metronic!</a>
        </div> -->
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>






<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="/includes/assets/global/plugins/respond.min.js"></script>
<script src="/includes/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="../includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="../includes/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

<script src="../includes/js/lib/utilities.js" type="text/javascript"></script>
<script src="../includes/js/jquery.validate.js"></script>
<script src="../view/general/book_package.js"></script>

<!--<script src="../includes/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>-->
<script src="../includes/js/jquery-ui.min.js" type="text/javascript"></script>

<script>
    maleTshirts = <?php echo $maleTshirtsJSON;?>;
    femaleTshirts = <?php echo $femaleTshirtsJSON;?>;

    console.log(maleTshirts)
//    console.log(femaleTshirts)

    maleSinglets = <?php echo $maleSingletsJSON;?>;
    femaleSinglets = <?php echo $femaleSingletsJSON;?>;

    countries = <?php echo json_encode($countries);?>;
    booking_id = '<?php echo $booking->booking_id;?>';
</script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
