$(document).ready(function() {
  $('#packageValidate').validate();
  // validate the comment form when it is submitted
  // var packages = ;

getPackages().done(defferedPopulateAccommodation);
//getAccommodation();
});

var getPackages = function()
{
    var r = $.Deferred();

    data        = {};
    data.method     = 'getPackages'
    Util.ajaxData(data, function(returnData){
      for(var i = 0; i < returnData.length; i++)
      {
        var package_id = returnData[i].package_id;
         $("div#packages").append("<input type='radio' id='package_id_" + package_id + "' name='package_id' value='" + package_id + "' required>" +
                                  "<label for='package_id_" + package_id + "'>" + 
                                  returnData[i].package_name + 
                                  "</label>" + 
                                  "<br>"
                                  );
        // return returnData;
      }
      r.resolve();
    });

    return r;
}


var defferedPopulateAccommodation = function()
{

      // var getVars = Util.getUrlVars();
      if($("#package").val().length > 0) {
          switch($("#package").val().toLowerCase()) {
            case 'platinum':
              $("input[name='package_id'][value='1']").prop('checked', true); 
            break;
            case 'diamond':
              $("input[name='package_id'][value='2']").prop('checked', true); 
            break;
            case 'gold':
              $("input[name='package_id'][value='3']").prop('checked', true); 
            break;
            case 'silver':
              $("input[name='package_id'][value='4']").prop('checked', true); 
            break;
            case 'red':
              $("input[name='package_id'][value='5']").prop('checked', true); 
            break;

            default:
              $("input:radio[name='package_id']:checked").prop('checked', false).checkboxradio("refresh");
            break;
          }
          numberGuests();
      }
      
     if($("#package_id").val().length > 0) {
            $("input[name='package_id'][value='" + $("#package_id").val() + "']").prop('checked', true); 
            Util.safe_log($("#package_id").val());
      }


      $("input:radio[name='package_id']").change(function() {
        Util.safe_log('radio button changed');
        numberGuests();
      });
      numberGuests();
    $("div#packages").append('<label for="package_id" class="error">Required</label>');
    data        = {};
    data.method     = 'getSavedPackageForm'
    Util.ajaxData(data, function(returnData){
      if(returnData !== ""){
        $("input[name='package_id'][value='"+returnData.package_id+"']").prop('checked', true);       
        // $('select[name="number_adults"] option[name="'+returnData.number_adults+'"]').prop('selected', 'selected');
        // $('select[name="number_children"] option[name="'+returnData.number_children+'"]').prop('selected', 'selected');
        $("#number_guests").text(returnData.number_guests);
        $("#bedding_id").val(returnData.number_adults);
        $('select[name="extra_nights_pre"] option[value="'+returnData.extra_nights_pre+'"]').prop('selected', 'selected');
        $('select[name="extra_nights_post"] option[value="'+returnData.extra_nights_post+'"]').prop('selected', 'selected');
        $("input[name='share_request_YN'][value='"+returnData.share_request_YN+"']").prop('checked', true); 
        $("input[name='rollaway_or_cot'][value='"+returnData.rollaway_or_cot+"']").prop('checked', true);
        if($("#accommodation_id option:selected").text() != 'Please select')
        {
          getAccommodation(returnData, function(){
            Util.safe_log('getAccommodation from deferred function');
            $('select[name="accommodation_id"] option[value="'+returnData.accommodation_id+'"]').prop('selected', 'selected');
            // getBedding(function(){
            //   $('#bedding_id').val(returnData.bedding_id);
            // }); 
          });
        }
      }
    });

    $('input[required]').each(function(){
        Util.safe_log($(this).prev('div'));
        $(this).prev('div').after('*');
    });

}




function getAccommodation(params, callback)
{
    // Util.safe_log('getAccommodation');
    data        = {};
    data.method     = 'getAccommodation';
    data.number_guests = params.number_guests;
    data.package_id = $('input:radio[name^="package_id"]:checked').val();
    Util.ajaxData(data, function(returnData){

      if($('input:radio[name^="package_id"]:checked').val().length > 0)
      {
        $("select#accommodation_id option[value!='']").remove();
        for(var i = 0; i < returnData.length; i++)
        {
          $('#accommodation_id')
           .append($("<option></option>")
           .attr("value",returnData[i].accommodation_id)
           .text(returnData[i].accommodation_name)); 
        }
        if($("#accommodation_id option:selected").val() != "")
        {
          // getBedding();
          getRoomType();
        }
        if (typeof callback == "function") callback();
      }
    });
}

// function getBedding(callback)
// {
//     Util.safe_log("getBedding");
//     data        = {};
//     data.method     = 'getBedding';
//     data.accommodation_id = $("#accommodation_id option:selected").val();
//     data.package_id = $('input:radio[name="package_id"]:checked').val();
//     Util.ajaxData(data, function(returnData){
//       $("select#bedding_id option[value!='']").remove();
//       for(var i = 0; i < returnData.length; i++)
//       {
//         $('#bedding_id')
//          .append($("<option></option>")
//          .attr("value",returnData[i].bedding_id)
//          .text(returnData[i].bedding_name)); 
//       }
//       if (typeof callback == "function") callback();
//     });

// }

function getRoomType(callback)
{
    Util.safe_log("getRoomType");
    data        = {};
    data.method     = 'getRoomType';
    data.accommodation_id = $("#accommodation_id option:selected").val();
    Util.ajaxData(data, function(returnData){
      $("select#room_type_id option[value!='']").remove();
      for(var i = 0; i < returnData.length; i++)
      {
        $('#room_type_id')
         .append($("<option></option>")
         .attr("value",returnData[i].room_type_id)
         .text(returnData[i].bedding_type_name)); 
      }
      if (typeof callback == "function") callback();
    });

}


$('#accommodation_id').change(function(){
  // getBedding();
  getRoomType();
})
$("#number_adults, #number_children").change(function(){
  numberGuests();
})


function numberGuests()
{
  var number_guests = parseInt($("#number_adults").val()) + parseInt($("#number_children").val());
  $("#bedding_id").val($("#number_adults").val());
  $("div#number_guests").html(number_guests);
  var data = {};
  data.number_guests = number_guests;
  data.package_id = $('input:radio[name="package_id"]:checked').val();
  if(number_guests > 0) // changed from > 1 to > 0 as requested
  {
    getAccommodation(data);
    // $("select#bedding_id option[value!='']").remove();
    $("select#room_type_id option[value!='']").remove();
  }
}

function savePackageFormInputs(){
	  data        = {};
    data.method     = 'savePackageFormInputs';
    data.post_data = $("#packageValidate").serialize();
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
		{
			var url_action = $('#packageValidate').attr( 'action' );
      url_action += '&i=1'; // next page id 1, instance number (guest number)
      url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      url_redirect = '/booking/index.php?v=guests&i=1';  // force
      window.location.href = url_redirect;
		}
		 else
		{
			alert('Sorry, we are currently unable to continue this booking. Please try again.')
		}
  });
}

$("#packageValidate").submit(function() {
    if($("#packageValidate").validate({
      focusInvalid: false,
      invalidHandler: function(form, validator) {

          if (!validator.numberOfInvalids())
              return;

          $('html, body').animate({
              scrollTop: $(validator.errorList[0].element).offset().top
          }, 2000);
      }
    }).checkForm())
      {
        savePackageFormInputs();
      }
      else
      {
        $("form").find(":input.error:first").focus();
      }
    return false;
 });
