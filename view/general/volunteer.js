$(document).ready(function() {
  //$("#centre-mainWrapper").css("display","block")
  if($("#database_error").val() === undefined)
  {

    if($("#thankyou").val() !== undefined)
    {
      $("#centre-mainWrapper").fadeOut("slow", function(){
      $("#centre-thankyouWrapper").fadeIn("slow");
      });
    }
    else
    {
      $("#centre-mainWrapper").fadeIn("slow");
    }
  }
  else
  {
    alert('There seems to be a technical problem. Please try submitting again. If it continues, please try again later.')
  }
});
