$(document).ready(function() {
	$("#optionalToursForm").validate()
	getTitlesDescriptions();

  /*$("#users_datagrid").dataTable( 
  {
//      "aoColumnDefs": [ { "bSearchable": false, "aTargets": [ 5 ] } ],
//      "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 5 ] } ],
      "iDisplayLength": 50,
      "bServerSide": true,
      "sAjaxSource": "control/datagrids/users_dg.php"
  }
  );
  $('.instructive-text-home').hide();*/
});

// get by package_id as per MW 30/10/14
// package_id is in $_SESSION in CRUD.php
function getTitlesDescriptions()
{
    data        = {};
    data.method     = 'getTitlesDescriptions';
    Util.ajaxData(data, function(returnData){
	  var j = 2;	
	  for(var i = 0; i < returnData.length; i++)
      {
		var optionalTour = '<div id="divtour2" class="tourbox">' +
								'<h3 id="title2">' + returnData[i].title + '</h3>' +
									'<span id="description">' + returnData[i].description + '</span><br>' +
								'<p class="space" style="margin-top:10px;">' +
									'<input type="radio" name="optional_tour_id_' + returnData[i].optional_tour_id + '" value="' + returnData[i].optional_tour_id + '"> Add to Itinerary<br>' +
									'<input type="radio" name="optional_tour_id_' + returnData[i].optional_tour_id + '" checked value="no"> No Thanks' +
								'</p>' +
							'</div>';
		$("#divtouroptions").append(optionalTour);
      }

		 getOptionToursFormInputs();

    });

}

function getOptionToursFormInputs(callback)
{
	data        		= {};
    data.method     	= 'getSavedOptionalTours';
    Util.ajaxData(data, function(returnData){
    	$("#optionalToursForm").values(returnData);
    	if (typeof callback == "function") callback(returnData);
    });
}

function saveOptionalToursForm(url_action){
	data        = {};
    data.method = 'saveOptionalToursForm';
    data.post_data = $("#optionalToursForm").values();
    Util.ajaxData(data, function(returnData){
        if(returnData == "S")
		{
			// url_redirect = url_action;
			Util.safe_log(url_redirect);
			var url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
			window.location.href = url_redirect;
		}
		 else
		{
			alert('Sorry, we are currently unable to continue this booking. Please try again.')
		}
    });
}

$("#next_button").click(function(){

	var url_action;
	if($("#optionalToursForm").validate().checkForm())
	{
		url_action = '../~australianoutbac/booking/index.php?v=additional_information';
		url_action = 'index.php?v=additional_information'; // force
		saveOptionalToursForm(url_action);
	}
});

$("#prev_button").click(function(){
	var url_action = '../~australianoutbac/booking/index.php?v=guests&i=' + $("#number_guests").val();
	url_action = 'index.php?v=guests&i=' + $("#number_guests").val();  // force
	Util.safe_log(url_action);
	saveOptionalToursForm(url_action);

});


