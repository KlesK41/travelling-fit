<?php
error_reporting(0);
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon</a>
        </div>
      </div>
    </div>
    <div id="page-wrapper" class='container marginTop'>
      <div class="panel panel-default margins">
              <div class="space">
                <h1 style="color: red">Sold out</h1>
              <p>Our packages are now <strong>SOLD OUT</strong>.</p>
              <p><i>Please feel free to peruse our website for future news and events.</i></p>
              <p><i></i></p>
              <p>&nbsp</p>
              <p><i>Kind regards,</i></p>
              <p><i>Australian Outback Marathon</i></p>
              </div>
      </div>
    </div>
  <script src="includes/js/jquery.min.js"></script>
  <script src="includes/js/jquery.dataTables.min.js"></script>
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>

</body>
</html>