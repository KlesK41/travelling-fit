$(document).ready(function() {

      // $('#EPS_CARDNUMBER').validateCreditCard(function(result)
      //   {
      //     Util.safe_log(result);
      //     try{

      //       switch(result.card_type.name)
      //       {
      //         case 'visa':
      //           $("input[name='EPS_CARDTYPE'][value=visa").attr('checked', true)
      //         break;

      //         case 'mastercard':
      //           $("input[name='EPS_CARDTYPE'][value=mastercard").attr('checked', true)
      //         break;

      //         case 'amex':
      //           $("input[name='EPS_CARDTYPE'][value=amex").attr('checked', true)
      //         break;

      //         case 'diners_club_international':
      //           $("input[name='EPS_CARDTYPE'][value=diners_club_international").attr('checked', true)
      //         break;


      //       }
      //     } catch(err) { }
      //       // alert('CC type: ' + result.card_type.name
      //       //   + '\nLength validation: ' + result.length_valid
      //       //   + '\nLuhn validation: ' + result.luhn_valid);
      //   });
  	$("#client_payment_amount_enter").keyup(function(){

      if($("input[name='EPS_CARDTYPE'][value='visa']").is(':checked'))
      {
        $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.015).toFixed(2));
      }

      if($("input[name='EPS_CARDTYPE'][value='mastercard']").is(':checked'))
      {
        $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.015).toFixed(2));
      }

      if($("input[name='EPS_CARDTYPE'][value='amex']").is(':checked'))
      {
        $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.0317787).toFixed(2));
      }

      if($("input[name='EPS_CARDTYPE'][value='diners']").is(':checked'))
      {
        $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.0252651).toFixed(2));
      }
  });

  $("input[name='EPS_CARDTYPE'][value='visa']").click(function(){
    $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.015).toFixed(2));
  });

  $("input[name='EPS_CARDTYPE'][value='mastercard']").click(function(){
    $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.015).toFixed(2));
  });

    $("input[name='EPS_CARDTYPE'][value='amex']").click(function(){
    $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.0317787).toFixed(2));
  });

  $("input[name='EPS_CARDTYPE'][value='diners']").click(function(){
    $("#client_payment_amount").val((parseFloat($("#client_payment_amount_enter").val()) * 1.0252651).toFixed(2));
  });

  $("#creditCardWrapper").hide();
  $("#payment_total_with_fee").hide();
  $("#directDebitWrapper").hide();
  //$("#creditCardFingerPrint").hide();
  if($("#booking_code").length > 0)
  {
    $("#directDebitWrapper").hide();
    $("#chose_pay_method").hide();
    $("#payment_options").hide();
    $("#secureBookingAmount").hide();
    $("#depositWrapper").show();
    $("#creditCardFingerPrint").show();
    $("#prev_book_buttons").hide();
    $("#creditCardWrapper").hide();

    $("#continue_payment").hide();
    $("input#cancel").hide();
    $("#payment_total_with_fee").show();
  }
  
  if($("#payment_method_CC").is(':checked')) {
    
    $("#directDebitWrapper").hide();
    $("#prev_book_buttons").hide();
    $("#creditCardFingerPrint").show();
    $("#payment_total_with_fee").show();
  }
});

$("[name=payment_method]").change(function(){

  if($("#payment_method_DD").is(':checked')) {
    $("#creditCardWrapper").hide();
    $("#payment_total_with_fee").hide();
    $("#creditCardFingerPrint").hide();
    $("#prev_book_buttons").show();
    $("#directDebitWrapper").show();
  }

  if($("#payment_method_CC").is(':checked')) {
    $("#directDebitWrapper").hide();
    $("#prev_book_buttons").hide();
    $("#creditCardFingerPrint").show();
    $("#payment_total_with_fee").show();
  }

  
});


$("input#cancel").click(function(){
    $("#creditCardWrapper").hide();
    $("#payment_total_with_fee").hide();
    $("#creditCardWrapper").hide();
    $("#prev_book_buttons").show();
    $("#directDebitWrapper").hide();
    $("#payment_method_CC").attr('checked', false);
});

$("#proceedToCCPayment").click(function(){
  if($("#cardholders_name").val() !== "" && $("#client_payment_amount_enter").val() !== "")
  {
      data        = {};
      data.method     = 'getFingerPrint';
      data.amount     = $("#client_payment_amount").val();
      data.cardholders_name = $("#cardholders_name").val();
      data.client_payment_amount_enter = $("#client_payment_amount_enter").val();
      Util.ajaxData(data, function(returnData){
        Util.safe_log(returnData.message);
        if(returnData.message == "S")
        {
          $("input[name='EPS_REFERENCEID']").val(returnData.transaction_id);
          $("input[name='EPS_AMOUNT']").val(returnData.amount);
          $("input[name='EPS_TIMESTAMP']").val(returnData.timestamp);
          $("input[name='EPS_FINGERPRINT']").val(returnData.finger_print);



          // great! We have a fingerprint.
          $("#payment_total_with_fee").show();
          $("#creditCardWrapper").show();
          $("#creditCardFingerPrint").hide();
          $("#display_amount_due").val('$' + $("#client_payment_amount").val());

        }
        else if (returnData == "NOT_ENOUGH_FOR_DEPOSIT")
        {
          alert('Sorry, you need specify a bigger amount. If you\'re paying a deposit, please enter the deposit amount');
          return false;
        }
        else
        {
          alert('Sorry, we are currently having trouble with this transaction. Please try again.')
          $("#creditCardWrapper").hide();
          $("#creditCardWrapper").hide();
          $("#payment_total_with_fee").hide();
          $("#prev_book_buttons").show();
          $("#directDebitWrapper").hide();
          $("#payment_method_CC").attr('checked', false);
        }
      });
  }
  else
  {
    alert('Please enter all required fields');
  }
})

$("#payment_submit").submit(function(e)
{
    alert(e.target);
  if($("#payment_submit").valid())
  {
    return true;
  }
  else
  {
    return false;
  }
});

// --------------------------------------------------------------------------- HERE
$("#proceedToCCPayment").click(function(){
  // getFingerPrint
  /*
  data        = {};
  data.method     = 'getFingerPrint';
  Util.ajaxData(data, function(returnData){
  if(returnData == "S")
  {
    // url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
    // window.location.href = url_redirect;
  }
  else
  {
    alert('Sorry, the credit card facility is currently experiencing problems. Please try again.')
  }
  */

});



function getTransactionInformation()
{
    data = {};
    data.method = 'getTransactionInformation';
    Util.ajaxData(data, function(returnData){

    });
}

function processPaymentInputForm(url_action){
    data        = {};
    data.method     = 'processPayment';
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      // url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      // window.location.href = url_redirect;
    }
    else
    {
      alert('Sorry, we are currently unable to continue this booking. Please try again.');
    }
    });
}


function savePaymentMethodFormInputs(url_action){
    data        = {};
    data.method     = 'savePaymentMethod';
    data.post_data  = $("#paymentDetailsValidate").values();
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      window.location.href = url_redirect;
    }
    else
    {
      alert('Sorry, we are currently unable to continue this booking. Please try again.');
    }
    });
}


$("#reset_button").click(function(){
  // clear session variable
    data        = {};
    data.method     = 'clearSession';
    Util.ajaxData(data, function(returnData){
    if(returnData == "S")
    {
      var url_action = 'index.php?v=package';
      url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
      window.location.href = url_redirect;
    }
  });
});


$("#next_button").click(function(){
  if($("#payment_method_CC").is(':checked'))
  {
    var url_action = '../~australianoutbac/booking/index.php?v=payment_confirmation_CC';
    url_action = 'http://australianoutbackmarathon.com/booking/index.php?v=payment_confirmation_CC';  // force
    url_action = 'index.php?v=payment_confirmation_CC'; // force
  }
  else
  {
    var url_action = '../~australianoutbac/booking/index.php?v=payment_confirmation_DD';
    url_action = 'http://australianoutbackmarathon.com/booking/index.php?v=payment_confirmation_DD';   // force
  }
  
  if($("#paymentDetailsValidate").valid())
  {
    // process NAB details AND / OR send emails
    //url_redirect = window.location.protocol+"//"+window.location.host + "/booking/" + url_action;
    window.location.href = url_action;
  }
});

$("#prev_button").click(function(){
    var url_action = 'index.php?v=additional_information';
    url_redirect = "../~australianoutbac/aom/" + url_action;
    url_redirect = "/aom/" + url_action; // force

    window.location.href = url_redirect;
});

/*$("#button1").on("click", function(){
  alert('button1');
});

$("#call_ajax").on("click", function(){
  getSomething(1);
});


$('a.helpLink').on("click", function(){
	Util.safe_log($(this).attr('id'));
	$('div#' + $(this).attr('id') + 'Section').toggle();
	return false;
});

/*
*	getSomething(id)
* 	For demonstration purposes
*	An ajax call
*
*	@param {something_id} - id of an entity
*/

/*function getSomething(something_id)
{
		data 				= {};
		data.method 		= 'getSomething'
		data.something_id	= something_id;
		Util.ajaxData(data, function(returnData){
      Util.safe_log(returnData);
      alert(returnData[0].something_id + " " + returnData[0].attribute1 + " " + returnData[0].attribute2);
		});
}*/

