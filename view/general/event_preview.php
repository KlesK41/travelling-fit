<?php 
$path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
?>


<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Metronic | Form Layouts</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo $path;?>/includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo $path;?>/includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path;?>/includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo $path;?>/includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

  <body class=" page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar">
      <!-- BEGIN HEADER INNER -->
      <div class="container">
        <div class="page-header-inner ">
          <!-- BEGIN LOGO -->
          <div class="page-logo">
            <a href="index.html">
              Travelling Fit  
            </a>
          </div>
          <!-- END LOGO -->
          <!-- BEGIN RESPONSIVE MENU TOGGLER -->
          <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
          <!-- END RESPONSIVE MENU TOGGLER -->
          <!-- BEGIN TOP NAVIGATION MENU -->
          <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
              <!-- END TODO DROPDOWN -->
              <!-- BEGIN USER LOGIN DROPDOWN -->
              <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
              <li class="dropdown dropdown-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <img alt="" class="img-circle" src="<?php echo $path;?>/includes/assets/layouts/layout/img/avatar3_small.jpg" />
                  <span class="username username-hide-on-mobile"> Nick </span>
                  <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                  <li>
                    <a href="page_user_profile_1.html">
                      <i class="icon-user"></i> My Profile </a>
                    </li>
                    <li>
                      <a href="app_calendar.html">
                        <i class="icon-calendar"></i> My Calendar </a>
                      </li>
                      <li>
                        <a href="app_inbox.html">
                          <i class="icon-envelope-open"></i> My Inbox
                          <span class="badge badge-danger"> 3 </span>
                        </a>
                      </li>
                      <li>
                        <a href="app_todo.html">
                          <i class="icon-rocket"></i> My Tasks
                          <span class="badge badge-success"> 7 </span>
                        </a>
                      </li>
                      <li class="divider"> </li>
                      <li>
                        <a href="page_user_lock_1.html">
                          <i class="icon-lock"></i> Lock Screen </a>
                        </li>
                        <li>
                          <a href="page_user_login_1.html">
                            <i class="icon-key"></i> Log Out </a>
                          </li>
                        </ul>
                      </li>
                      <!-- END USER LOGIN DROPDOWN -->
                      <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                      <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                      <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="javascript:;" class="dropdown-toggle">
                          <i class="icon-logout"></i>
                        </a>
                      </li>
                      <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                  </div>
                  <!-- END TOP NAVIGATION MENU -->
                </div>
              </div>
              <!-- END HEADER INNER -->
            </div>
            <div class="container">
              <div class="clearfix"> </div>
              <!-- END HEADER & CONTENT DIVIDER -->
              <!-- BEGIN CONTAINER -->
              <div class="page-container">
                <div class="page-content-wrapper">
                  <!-- BEGIN CONTENT BODY -->
                  <div class="page-content">
                    <div class="page-bar">
                      <ul class="page-breadcrumb">
                        <ul class="page-breadcrumb">
                          <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <i class="fa fa-angle-right"></i>
                          </li>
                          <li>
                            <a href="index.html">Travelling Fit</a>
                            <i class="fa fa-angle-right"></i>
                          </li>
                          <li>
                            <span>Packages</span>
                          </li>
                        </ul>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                              <li>
                                <a href="#tab_0" data-toggle="tab">The Course</a>
                              </li>
                              <li>
                                <a href="#tab_1" data-toggle="tab"> Race Info  </a>
                              </li>
                              <li>
                                <a href="#tab_2" data-toggle="tab">  Review </a>
                              </li>
                              <li>
                                <a href="#tab_3" data-toggle="tab"> Gallery </a>
                              </li>
                              <li>
                                <a href="#tab_5" data-toggle="tab"> Enquire  </a>
                              </li>
                              <li class="active">
                                <a href="#tab_4" data-toggle="tab"> Packages  </a>
                              </li>
                              <li>
                                <a href="#tab_6" data-toggle="tab"> Become Apriority Client  </a>
                              </li>
                            </ul>
                            <div class="tab-content">
                              <div class="tab-pane" id="tab_0">                                
                              </div>
                              <div class="tab-pane" id="tab_1">
                              </div>
                              <div class="tab-pane" id="tab_2">
                              </div>
                              <div class="tab-pane" id="tab_3">                                
                              </div>
                              <div class="tab-pane active" id="tab_4">
                                <div class="row botline">
                                  <div class="wrapp-label">
                                    <div class="col-md-12">
                                      <div class="head-block">
                                        Australian Outback Half Marathon
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="text-desc">
                                        Date: 23 Jul 2015
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="text-desc">
                                        Continent: Oceania
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="text-desc">
                                        Location: AU
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="text-pic">
                                        <img src="">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row margbot">
                                  <div class="col-md-12">
                                    <div class="head-block">
                                      Welcome to the Australian Outback Half Marathon
                                    </div>
                                    <div class="text">
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </div>
                                  </div>
                                </div>
                                <div id="packagesForEvent"></div>
                                


                              </div>
                              <div class="tab-pane" id="tab_5"> 
                              </div>
                              <div class="tab-pane" id="tab_6"> 
                              </div>
                              <!-- END CONTENT BODY -->
                            </div>
                            <!-- END CONTENT -->
                            <!-- BEGIN QUICK SIDEBAR -->
                          </div>
                          <!-- END CONTAINER -->
                          <!-- BEGIN FOOTER -->
                          <div class="page-footer"></div>
                        </div>
                        <!-- END FOOTER -->
    <!--[if lt IE 9]>
<script src="/includes/assets/global/plugins/respond.min.js"></script>
<script src="/includes/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo $path;?>/includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo $path;?>/includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path;?>/includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo $path;?>/includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo $path;?>/includes/js/lib/utilities.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script>
$(document).ready(function () {
  var event_id = '<?php echo $packages[0]->event_id?>';
  console.log(event_id);
  data = {};
  data.event_id = event_id;
  data.method = 'getPackagesForEventPreview';
  Util.ajaxData(data, function(returnData){
    console.log(returnData);
    $('#packagesForEvent').html(returnData);
    
  });
});
</script>
</body> 

</html>
