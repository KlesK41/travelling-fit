<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title> 
    <link type="text/css" href="includes/styles/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
        </div>
      </div>
    </div>
  <?php 
  

  $guest_number     = isset($_GET['i']) ? $_GET['i']:''; // subtract 1 to get position in array

 
  $number_adults    = $_SESSION['number_adults'];
  $number_guests    = $_SESSION['number_guests'];

  unset($_SESSION['guest_pages']);
  for($i=0; $i<$number_adults; $i++)
  {
    $_SESSION['guest_pages'][$i] = "A";
  }
  for($i=$number_adults; $i<$number_guests; $i++)
  {
     $_SESSION['guest_pages'][$i] = "C";
  }
  // logic to determine next or previous page
  if($guest_number == $number_guests)
  {
    $next_page = "v=optional_tours&n=3";
  }

  if($guest_number == '1')
  {
    $guest_type_title = "<h3 id='guestType'>Primary Contact</h3>(Primary contact for booking)";
  }
  else
  {
    $guest_type_title = "<h3 id='guestType'>Guest Number {$guest_number}</h3>";
  }


  echo "<input type='hidden' name='guest_number' id='guest_number' value='" . $guest_number . "'>";
  echo "<input type='hidden' name='child_or_adult' id='child_or_adult' value='" . ($_SESSION['guest_pages'][$guest_number - 1] == "C" ? "C" : "A") . "'>";
  echo "<input type='hidden' name='number_guests' id='number_guests' value='" . $_SESSION['number_guests'] . "'>";

  ?>
  
  <form id="guestsDetailsValidate" action="index.php?v=guests" method="post" class="main">
  <div id="page-wrapper" class='container marginTop external-form'>
  <input type='hidden' name='primary_contact_yn' id='primary_contact_yn' value='<?php echo $guest_number == '1' ? "Y" : "N" ?>'>

      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li id="formName">Guests Details</li> 
       </ol>
      <!-- information panel !-->

    <fieldset>

      <!-- panel 1 !-->
    <div class="panel panel-default">
		<div id="personalDetailsWrapper">
			<div class="bs-personal-details">
		  
            <div class="row guestTypeDescription">
				<?php echo $guest_type_title ?>
				<p id="guestTypeDesription"></p>
            </div>
			
            <div class="row space">
              <div class="col-xs-12 col-sm-3 field-label">First Name*</div>
              <div class="col-xs-12 col-sm-9">
                <input type="text" name="firstname" id="firstname" title="Required" required>
              </div>
            </div>
			
            <div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Last Name*</div>
				<div class="col-xs-12 col-sm-9">
					<input type="text" name="lastname" id="lastname" title="Required" required>
				</div>
            </div>
			
            <div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Preferred Name</div>
				<div class="col-xs-12 col-sm-9">
					<input type="text" name="prefered_name" id="prefered_name">
				</div>
            </div>
			
			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Email Address*</div>
				<div class="col-xs-12 col-sm-9">
					<input type="text" name="email" id="email" title="Required" required="required">
				</div>
			</div>
			
			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Gender*</div>
				<div class="col-xs-12 col-sm-9">
					<fieldset data-role="controlgroup" class="inline-radio-buttons-styling">
					<input type="radio" name="gender" id="gender_M" value="M" required><label for="gender_M">Male</label>
					<input type="radio" name="gender" id="gender_F" value="F" ><label for="gender_F">Female</label>
					</fieldset>
				</div>
			</div>
			
			<div class="row space" id="guestChild">
				<div class="col-xs-12 col-sm-3 field-label">Is this guest a child?*</div>
				<div class="col-xs-12 col-sm-9">
					<input id="child_YN" type="checkbox" value="Y" name="child_YN">Child
				</div>
			</div>
			
            <!-- CHILD ACKNOWLEDGEMENT !-->
			<div class="row space" id="trchild" style="display: none;">
				<div class="col-xs-12 col-sm-3"></div>
				<div class="col-xs-12 col-sm-9">
					<span style="color: red">*</span>
					<input id="childAck" type="checkbox" value="childAck" name="childAck">
					<span style="color: red">I acknowledge that a child is considered to be of the age 12 years or under at the time of check-in*</span>
				</div>
			</div>
			
            <div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Date of Birth (dd/mm/yyyy)*</div>
				<div class="col-xs-12 col-sm-9">
					<input type="text" name="dob" id="dob" title="Required" required>
				</div>
            </div>
			
			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Preferred Event*</div>
				<div id="prefered_events" class="col-xs-12 col-sm-9 inline-radio-buttons-styling"></div>
			</div>

<!-- 			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Shirt Size*</div>
				<div class="col-xs-12 col-sm-9">
					<select id="tshirt_id" name="tshirt_id" title="Required" required>
					<option value="0" selected="selected">Please Select</option>
					</select> <a class="bump" target="_blank" href="http://australianoutbackmarathon.com/wp-content/uploads/2014/11/Travelling-Fit-Shirt-Size-Chart.pdf" data-fancybox-type="iframe">View Size Chart</a> <br>
					<br>
					<span  class="note-text">PLEASE NOTE: <br>
					SHIRTS ARE ONLY INCLUDED FOR BOOKINGS MADE PRIOR TO FRIDAY 15TH MAY. <br>
					A LIMITED NUMBER OF SHIRTS WILL BE AVAILABLE FOR PURCHASE ON FRIDAY 24TH JULY PRIOR TO THE MANDATORY RACE BRIEFING.</span>
				</div>
			</div>
 -->		
 			
 			<input type="hidden" id="tshirt_id" name="tshirt_id"> <!--force !-->
			<div id="phone" class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Contact Phone Number*</div>
				<div class="col-xs-12 col-sm-9">
					<input type="text" id="phone" size="18" autocomplete="OFF" name="phone">
				</div>
			</div>
			
			<div id="address">
				<div class="row space">
					<div class="col-xs-12 col-sm-3 field-label">Nationality</div>
					<div class="col-xs-12 col-sm-9">
						<select id="nationality" name="nationality" title="Required" required> <option value="Afghanistan">Afghanistan</option> <option value="Albania">Albania</option> <option value="Algeria">Algeria</option> <option value="American Samoa">American Samoa</option> <option value="Andorra">Andorra</option> <option value="Angola">Angola</option> <option value="Anguilla">Anguilla</option> <option value="Antarctica">Antarctica</option> <option value="Antigua and Barbuda">Antigua and Barbuda</option> <option value="Argentina">Argentina</option> <option value="Armenia">Armenia</option> <option value="Aruba">Aruba</option> <option value="Australia" selected="">Australia</option> <option value="Austria">Austria</option> <option value="Azerbaijan">Azerbaijan</option> <option value="Bahamas">Bahamas</option> <option value="Bahrain">Bahrain</option> <option value="Bangladesh">Bangladesh</option> <option value="Barbados">Barbados</option> <option value="Belarus">Belarus</option> <option value="Belgium">Belgium</option> <option value="Belize">Belize</option> <option value="Benin">Benin</option> <option value="Bermuda">Bermuda</option> <option value="Bhutan">Bhutan</option> <option value="Bolivia">Bolivia</option> <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> <option value="Botswana">Botswana</option> <option value="Bouvet Island">Bouvet Island</option> <option value="Brazil">Brazil</option> <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> <option value="Brunei Darussalam">Brunei Darussalam</option> <option value="Bulgaria">Bulgaria</option> <option value="Burkina Faso">Burkina Faso</option> <option value="Burundi">Burundi</option> <option value="Cambodia">Cambodia</option> <option value="Cameroon">Cameroon</option> <option value="Canada">Canada</option> <option value="Cape Verde">Cape Verde</option> <option value="Cayman Islands">Cayman Islands</option> <option value="Central African Republic">Central African Republic</option> <option value="Chad">Chad</option> <option value="Chile">Chile</option> <option value="China">China</option> <option value="Christmas Island">Christmas Island</option> <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> <option value="Colombia">Colombia</option> <option value="Comoros">Comoros</option> <option value="Congo">Congo</option> <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option> <option value="Cook Islands">Cook Islands</option> <option value="Costa Rica">Costa Rica</option> <option value="Cote D&#39;Ivoire">Cote D\'Ivoire</option> <option value="Croatia">Croatia</option> <option value="Cuba">Cuba</option> <option value="Cyprus">Cyprus</option> <option value="Czech Republic">Czech Republic</option> <option value="Denmark">Denmark</option> <option value="Djibouti">Djibouti</option> <option value="Dominica">Dominica</option> <option value="Dominican Republic">Dominican Republic</option> <option value="Ecuador">Ecuador</option> <option value="Egypt">Egypt</option> <option value="El Salvador">El Salvador</option> <option value="Equatorial Guinea">Equatorial Guinea</option> <option value="Eritrea">Eritrea</option> <option value="Estonia">Estonia</option> <option value="Ethiopia">Ethiopia</option> <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> <option value="Faroe Islands">Faroe Islands</option> <option value="Fiji">Fiji</option> <option value="Finland">Finland</option> <option value="France">France</option> <option value="French Guiana">French Guiana</option> <option value="French Polynesia">French Polynesia</option> <option value="French Southern Territories">French Southern Territories</option> <option value="Gabon">Gabon</option> <option value="Gambia">Gambia</option> <option value="Georgia">Georgia</option> <option value="Germany">Germany</option> <option value="Ghana">Ghana</option> <option value="Gibraltar">Gibraltar</option> <option value="Greece">Greece</option> <option value="Greenland">Greenland</option> <option value="Grenada">Grenada</option> <option value="Guadeloupe">Guadeloupe</option> <option value="Guam">Guam</option> <option value="Guatemala">Guatemala</option> <option value="Guinea">Guinea</option> <option value="Guinea-Bissau">Guinea-Bissau</option> <option value="Guyana">Guyana</option> <option value="Haiti">Haiti</option> <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> <option value="Honduras">Honduras</option> <option value="Hong Kong">Hong Kong</option> <option value="Hungary">Hungary</option> <option value="Iceland">Iceland</option> <option value="India">India</option> <option value="Indonesia">Indonesia</option> <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> <option value="Iraq">Iraq</option> <option value="Ireland">Ireland</option> <option value="Israel">Israel</option> <option value="Italy">Italy</option> <option value="Jamaica">Jamaica</option> <option value="Japan">Japan</option> <option value="Jordan">Jordan</option> <option value="Kazakhstan">Kazakhstan</option> <option value="Kenya">Kenya</option> <option value="Kiribati">Kiribati</option> <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People\'s Republic of</option> <option value="Korea, Republic of">Korea, Republic of</option> <option value="Kuwait">Kuwait</option> <option value="Kyrgyzstan">Kyrgyzstan</option> <option value="Lao People&#39;s Democratic Republic">Lao People\'s Democratic Republic</option> <option value="Latvia">Latvia</option> <option value="Lebanon">Lebanon</option> <option value="Lesotho">Lesotho</option> <option value="Liberia">Liberia</option> <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> <option value="Liechtenstein">Liechtenstein</option> <option value="Lithuania">Lithuania</option> <option value="Luxembourg">Luxembourg</option> <option value="Macao">Macao</option> <option value="Macedonia, the Former Yugoslav Republic of">Macedonia, the Former Yugoslav Republic of</option> <option value="Madagascar">Madagascar</option> <option value="Malawi">Malawi</option> <option value="Malaysia">Malaysia</option> <option value="Maldives">Maldives</option> <option value="Mali">Mali</option> <option value="Malta">Malta</option> <option value="Marshall Islands">Marshall Islands</option> <option value="Martinique">Martinique</option> <option value="Mauritania">Mauritania</option> <option value="Mauritius">Mauritius</option> <option value="Mayotte">Mayotte</option> <option value="Mexico">Mexico</option> <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> <option value="Moldova, Republic of">Moldova, Republic of</option> <option value="Monaco">Monaco</option> <option value="Mongolia">Mongolia</option> <option value="Montserrat">Montserrat</option> <option value="Morocco">Morocco</option> <option value="Mozambique">Mozambique</option> <option value="Myanmar">Myanmar</option> <option value="Namibia">Namibia</option> <option value="Nauru">Nauru</option> <option value="Nepal">Nepal</option> <option value="Netherlands">Netherlands</option> <option value="Netherlands Antilles">Netherlands Antilles</option> <option value="New Caledonia">New Caledonia</option> <option value="New Zealand">New Zealand</option> <option value="Nicaragua">Nicaragua</option> <option value="Niger">Niger</option> <option value="Nigeria">Nigeria</option> <option value="Niue">Niue</option> <option value="Norfolk Island">Norfolk Island</option> <option value="Northern Mariana Islands">Northern Mariana Islands</option> <option value="Norway">Norway</option> <option value="Oman">Oman</option> <option value="Pakistan">Pakistan</option> <option value="Palau">Palau</option> <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> <option value="Panama">Panama</option> <option value="Papua New Guinea">Papua New Guinea</option> <option value="Paraguay">Paraguay</option> <option value="Peru">Peru</option> <option value="Philippines">Philippines</option> <option value="Pitcairn">Pitcairn</option> <option value="Poland">Poland</option> <option value="Portugal">Portugal</option> <option value="Puerto Rico">Puerto Rico</option> <option value="Qatar">Qatar</option> <option value="Reunion">Reunion</option> <option value="Romania">Romania</option> <option value="Russian Federation">Russian Federation</option> <option value="Rwanda">Rwanda</option> <option value="Saint Helena">Saint Helena</option> <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> <option value="Saint Lucia">Saint Lucia</option> <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option> <option value="Samoa">Samoa</option> <option value="San Marino">San Marino</option> <option value="Sao Tome and Principe">Sao Tome and Principe</option> <option value="Saudi Arabia">Saudi Arabia</option> <option value="Senegal">Senegal</option> <option value="Serbia and Montenegro">Serbia and Montenegro</option> <option value="Seychelles">Seychelles</option> <option value="Sierra Leone">Sierra Leone</option> <option value="Singapore">Singapore</option> <option value="Slovakia">Slovakia</option> <option value="Slovenia">Slovenia</option> <option value="Solomon Islands">Solomon Islands</option> <option value="Somalia">Somalia</option> <option value="South Africa">South Africa</option> <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option> <option value="Spain">Spain</option> <option value="Sri Lanka">Sri Lanka</option> <option value="Sudan">Sudan</option> <option value="Suriname">Suriname</option> <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> <option value="Swaziland">Swaziland</option> <option value="Sweden">Sweden</option> <option value="Switzerland">Switzerland</option> <option value="Syrian Arab Republic">Syrian Arab Republic</option> <option value="Taiwan, Province of China">Taiwan, Province of China</option> <option value="Tajikistan">Tajikistan</option> <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> <option value="Thailand">Thailand</option> <option value="Timor-Leste">Timor-Leste</option> <option value="Togo">Togo</option> <option value="Tokelau">Tokelau</option> <option value="Tonga">Tonga</option> <option value="Trinidad and Tobago">Trinidad and Tobago</option> <option value="Tunisia">Tunisia</option> <option value="Turkey">Turkey</option> <option value="Turkmenistan">Turkmenistan</option> <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> <option value="Tuvalu">Tuvalu</option> <option value="Uganda">Uganda</option> <option value="Ukraine">Ukraine</option> <option value="United Arab Emirates">United Arab Emirates</option> <option value="United Kingdom">United Kingdom</option> <option value="United States">United States</option> <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> <option value="Uruguay">Uruguay</option> <option value="Uzbekistan">Uzbekistan</option> <option value="Vanuatu">Vanuatu</option> <option value="Venezuela">Venezuela</option> <option value="Viet Nam">Viet Nam</option> <option value="Virgin Islands, British">Virgin Islands, British</option> <option value="Virgin Islands, U.s.">Virgin Islands, U.s.</option> <option value="Wallis and Futuna">Wallis and Futuna</option> <option value="Western Sahara">Western Sahara</option> <option value="Yemen">Yemen</option> <option value="Zambia">Zambia</option> <option value="Zimbabwe">Zimbabwe</option></select>
					</div>
				</div>
			</div>
			<div id="address" class="row space"> 
				<div class="col-xs-12 col-sm-3 field-label">Address*</div>
				<div class="col-xs-12 col-sm-9">
					<div>
						<div id="same_address_primary"><input id="address_same_primary_yn" type="checkbox" value="Y" name="address_same_primary_yn">Same address as Primary Contact</div>
					</div>
				</div>
			</div>

			<div id="addressWrapper">
				<div class="row space">
					<div class="col-xs-12 col-sm-3"></div>
					<div class="col-xs-12 col-sm-9">						
						<input type="text" id="address_1" name="address_1" title="Required" class="required" size="48" autocomplete="OFF"  placeholder="Street Address" required="required">
					</div>
				</div>
				
				<div id="suburb_state" class="row space">
					<div class="col-xs-12 col-sm-3"></div>
					<div class="col-xs-12 col-sm-9">
						<input id="city" name="city" class="deftext" type="text" placeholder="City" autocomplete="OFF" size="18" title="required" title="Required" required>
						<input id="state" name="state" class="deftext" type="text" placeholder="State / Province / Region" autocomplete="OFF" size="25" title="Required" required>
					</div>
				</div>
				
				<div id="pcode_country" class="row space">
					<div class="col-xs-12 col-sm-3"></div>
					<div class="col-xs-12 col-sm-9">
						<input id="postcode" name="postcode" class="deftext" type="text" placeholder="Zip / Post Code" size="18" title="Required" required><br>
						<select id="country" name="country" title="Required" required> <option value="Afghanistan">Afghanistan</option> <option value="Albania">Albania</option> <option value="Algeria">Algeria</option> <option value="American Samoa">American Samoa</option> <option value="Andorra">Andorra</option> <option value="Angola">Angola</option> <option value="Anguilla">Anguilla</option> <option value="Antarctica">Antarctica</option> <option value="Antigua and Barbuda">Antigua and Barbuda</option> <option value="Argentina">Argentina</option> <option value="Armenia">Armenia</option> <option value="Aruba">Aruba</option> <option value="Australia" selected="">Australia</option> <option value="Austria">Austria</option> <option value="Azerbaijan">Azerbaijan</option> <option value="Bahamas">Bahamas</option> <option value="Bahrain">Bahrain</option> <option value="Bangladesh">Bangladesh</option> <option value="Barbados">Barbados</option> <option value="Belarus">Belarus</option> <option value="Belgium">Belgium</option> <option value="Belize">Belize</option> <option value="Benin">Benin</option> <option value="Bermuda">Bermuda</option> <option value="Bhutan">Bhutan</option> <option value="Bolivia">Bolivia</option> <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> <option value="Botswana">Botswana</option> <option value="Bouvet Island">Bouvet Island</option> <option value="Brazil">Brazil</option> <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> <option value="Brunei Darussalam">Brunei Darussalam</option> <option value="Bulgaria">Bulgaria</option> <option value="Burkina Faso">Burkina Faso</option> <option value="Burundi">Burundi</option> <option value="Cambodia">Cambodia</option> <option value="Cameroon">Cameroon</option> <option value="Canada">Canada</option> <option value="Cape Verde">Cape Verde</option> <option value="Cayman Islands">Cayman Islands</option> <option value="Central African Republic">Central African Republic</option> <option value="Chad">Chad</option> <option value="Chile">Chile</option> <option value="China">China</option> <option value="Christmas Island">Christmas Island</option> <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> <option value="Colombia">Colombia</option> <option value="Comoros">Comoros</option> <option value="Congo">Congo</option> <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option> <option value="Cook Islands">Cook Islands</option> <option value="Costa Rica">Costa Rica</option> <option value="Cote D&#39;Ivoire">Cote D\'Ivoire</option> <option value="Croatia">Croatia</option> <option value="Cuba">Cuba</option> <option value="Cyprus">Cyprus</option> <option value="Czech Republic">Czech Republic</option> <option value="Denmark">Denmark</option> <option value="Djibouti">Djibouti</option> <option value="Dominica">Dominica</option> <option value="Dominican Republic">Dominican Republic</option> <option value="Ecuador">Ecuador</option> <option value="Egypt">Egypt</option> <option value="El Salvador">El Salvador</option> <option value="Equatorial Guinea">Equatorial Guinea</option> <option value="Eritrea">Eritrea</option> <option value="Estonia">Estonia</option> <option value="Ethiopia">Ethiopia</option> <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> <option value="Faroe Islands">Faroe Islands</option> <option value="Fiji">Fiji</option> <option value="Finland">Finland</option> <option value="France">France</option> <option value="French Guiana">French Guiana</option> <option value="French Polynesia">French Polynesia</option> <option value="French Southern Territories">French Southern Territories</option> <option value="Gabon">Gabon</option> <option value="Gambia">Gambia</option> <option value="Georgia">Georgia</option> <option value="Germany">Germany</option> <option value="Ghana">Ghana</option> <option value="Gibraltar">Gibraltar</option> <option value="Greece">Greece</option> <option value="Greenland">Greenland</option> <option value="Grenada">Grenada</option> <option value="Guadeloupe">Guadeloupe</option> <option value="Guam">Guam</option> <option value="Guatemala">Guatemala</option> <option value="Guinea">Guinea</option> <option value="Guinea-Bissau">Guinea-Bissau</option> <option value="Guyana">Guyana</option> <option value="Haiti">Haiti</option> <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> <option value="Honduras">Honduras</option> <option value="Hong Kong">Hong Kong</option> <option value="Hungary">Hungary</option> <option value="Iceland">Iceland</option> <option value="India">India</option> <option value="Indonesia">Indonesia</option> <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> <option value="Iraq">Iraq</option> <option value="Ireland">Ireland</option> <option value="Israel">Israel</option> <option value="Italy">Italy</option> <option value="Jamaica">Jamaica</option> <option value="Japan">Japan</option> <option value="Jordan">Jordan</option> <option value="Kazakhstan">Kazakhstan</option> <option value="Kenya">Kenya</option> <option value="Kiribati">Kiribati</option> <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People\'s Republic of</option> <option value="Korea, Republic of">Korea, Republic of</option> <option value="Kuwait">Kuwait</option> <option value="Kyrgyzstan">Kyrgyzstan</option> <option value="Lao People&#39;s Democratic Republic">Lao People\'s Democratic Republic</option> <option value="Latvia">Latvia</option> <option value="Lebanon">Lebanon</option> <option value="Lesotho">Lesotho</option> <option value="Liberia">Liberia</option> <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> <option value="Liechtenstein">Liechtenstein</option> <option value="Lithuania">Lithuania</option> <option value="Luxembourg">Luxembourg</option> <option value="Macao">Macao</option> <option value="Macedonia, the Former Yugoslav Republic of">Macedonia, the Former Yugoslav Republic of</option> <option value="Madagascar">Madagascar</option> <option value="Malawi">Malawi</option> <option value="Malaysia">Malaysia</option> <option value="Maldives">Maldives</option> <option value="Mali">Mali</option> <option value="Malta">Malta</option> <option value="Marshall Islands">Marshall Islands</option> <option value="Martinique">Martinique</option> <option value="Mauritania">Mauritania</option> <option value="Mauritius">Mauritius</option> <option value="Mayotte">Mayotte</option> <option value="Mexico">Mexico</option> <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> <option value="Moldova, Republic of">Moldova, Republic of</option> <option value="Monaco">Monaco</option> <option value="Mongolia">Mongolia</option> <option value="Montserrat">Montserrat</option> <option value="Morocco">Morocco</option> <option value="Mozambique">Mozambique</option> <option value="Myanmar">Myanmar</option> <option value="Namibia">Namibia</option> <option value="Nauru">Nauru</option> <option value="Nepal">Nepal</option> <option value="Netherlands">Netherlands</option> <option value="Netherlands Antilles">Netherlands Antilles</option> <option value="New Caledonia">New Caledonia</option> <option value="New Zealand">New Zealand</option> <option value="Nicaragua">Nicaragua</option> <option value="Niger">Niger</option> <option value="Nigeria">Nigeria</option> <option value="Niue">Niue</option> <option value="Norfolk Island">Norfolk Island</option> <option value="Northern Mariana Islands">Northern Mariana Islands</option> <option value="Norway">Norway</option> <option value="Oman">Oman</option> <option value="Pakistan">Pakistan</option> <option value="Palau">Palau</option> <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> <option value="Panama">Panama</option> <option value="Papua New Guinea">Papua New Guinea</option> <option value="Paraguay">Paraguay</option> <option value="Peru">Peru</option> <option value="Philippines">Philippines</option> <option value="Pitcairn">Pitcairn</option> <option value="Poland">Poland</option> <option value="Portugal">Portugal</option> <option value="Puerto Rico">Puerto Rico</option> <option value="Qatar">Qatar</option> <option value="Reunion">Reunion</option> <option value="Romania">Romania</option> <option value="Russian Federation">Russian Federation</option> <option value="Rwanda">Rwanda</option> <option value="Saint Helena">Saint Helena</option> <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> <option value="Saint Lucia">Saint Lucia</option> <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option> <option value="Samoa">Samoa</option> <option value="San Marino">San Marino</option> <option value="Sao Tome and Principe">Sao Tome and Principe</option> <option value="Saudi Arabia">Saudi Arabia</option> <option value="Senegal">Senegal</option> <option value="Serbia and Montenegro">Serbia and Montenegro</option> <option value="Seychelles">Seychelles</option> <option value="Sierra Leone">Sierra Leone</option> <option value="Singapore">Singapore</option> <option value="Slovakia">Slovakia</option> <option value="Slovenia">Slovenia</option> <option value="Solomon Islands">Solomon Islands</option> <option value="Somalia">Somalia</option> <option value="South Africa">South Africa</option> <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option> <option value="Spain">Spain</option> <option value="Sri Lanka">Sri Lanka</option> <option value="Sudan">Sudan</option> <option value="Suriname">Suriname</option> <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> <option value="Swaziland">Swaziland</option> <option value="Sweden">Sweden</option> <option value="Switzerland">Switzerland</option> <option value="Syrian Arab Republic">Syrian Arab Republic</option> <option value="Taiwan, Province of China">Taiwan, Province of China</option> <option value="Tajikistan">Tajikistan</option> <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> <option value="Thailand">Thailand</option> <option value="Timor-Leste">Timor-Leste</option> <option value="Togo">Togo</option> <option value="Tokelau">Tokelau</option> <option value="Tonga">Tonga</option> <option value="Trinidad and Tobago">Trinidad and Tobago</option> <option value="Tunisia">Tunisia</option> <option value="Turkey">Turkey</option> <option value="Turkmenistan">Turkmenistan</option> <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> <option value="Tuvalu">Tuvalu</option> <option value="Uganda">Uganda</option> <option value="Ukraine">Ukraine</option> <option value="United Arab Emirates">United Arab Emirates</option> <option value="United Kingdom">United Kingdom</option> <option value="United States">United States</option> <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> <option value="Uruguay">Uruguay</option> <option value="Uzbekistan">Uzbekistan</option> <option value="Vanuatu">Vanuatu</option> <option value="Venezuela">Venezuela</option> <option value="Viet Nam">Viet Nam</option> <option value="Virgin Islands, British">Virgin Islands, British</option> <option value="Virgin Islands, U.s.">Virgin Islands, U.s.</option> <option value="Wallis and Futuna">Wallis and Futuna</option> <option value="Western Sahara">Western Sahara</option> <option value="Yemen">Yemen</option> <option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select>
					</div>
				</div>
			</div>
			
			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Special Dietary Requirements</div>
				<div class="col-xs-12 col-sm-9">
					<textarea id="dietry_requirements" name="dietry_requirements" title="None" class="deftext" rows="5" cols="40" placeholder="None"></textarea>
				</div> 
			</div>
				
			<div class="row space">
				<div class="col-xs-12 col-sm-3 field-label">Additional Information</div>
				<div class="col-xs-12 col-sm-9">
					<textarea id="additional_info" name="additional_info" title="None" class="deftext" rows="5" cols="40" placeholder="None"></textarea>
				</div>
			</div>
			
			<div class="row space buttons-container">
				<div class="col-xs-12 col-sm-8 btn-left">
					<span class='glyphicon glyphicon-circle-arrow-left'></span>
					<input type="button" id="prev_button" value="Previous">
				</div>
				<div class="col-xs-6 col-sm-4 btn-right">
					<input type="button" id="next_button" value="Next">
					<span class='glyphicon glyphicon-circle-arrow-right'></span>
				</div>  
			</div>
			
	</fieldset>

	<div class="row space">
		<div class="col-xs-12 col-sm-2"></div> 
		<div class="col-xs-12 col-sm-4"></div>
	</div>
	
	</div>
	</div>
	</div>
	</form>

<script src="includes/js/jquery.min.js"></script>
<script src="includes/js/ui/minified/jquery-ui.custom.min.js"></script>  
<script src="includes/js/jquery.dataTables.min.js"></script>
<script src="includes/js/bootstrap/bootstrap.min.js"></script>
<script src="includes/js/validate/jquery.validate.min.js"></script>  
<script src="includes/js/lib/utilities.js"></script>
<script src="includes/js/lib/values.js"></script>
<script src="view/general/guests.js"></script>
</body>
</html>