<?php
error_reporting(0);
?>
<html>
<head>
<style>
@media (min-width: 768px){
    .navbar-nav {
        margin: 0 auto;
        display: table;
        table-layout: auto;
        float: none;
        width: 100%;
    }
    .navbar-nav>li {
        display: table-cell;
        float: none;
        text-align: center;
    }
} 
</style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Package</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body style="text-align: center;">

      <h1>Australian Outback Marathon</h1>
      <h3>Local Events</h3>

      <div class="panel panel-default margins">
              <div class="space">
                <h1 style="color: red">Sold out</h1>
              <p>Our local events are now <strong>SOLD OUT</strong>.</p>
              <p><i>Please feel free to <a href="http://www.australianoutbackmarathon.com.au">peruse our website</a> for future news and events.</i></p>
              <p><i></i></p>
              <p>&nbsp</p>
              <p><i>Kind regards,</i></p>
              <p><i>Australian Outback Marathon</i></p>
              </div>
      </div>
    </div>
  <script src="includes/js/jquery.min.js"></script>
  <script src="includes/js/jquery.dataTables.min.js"></script>
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>

</body>
</html>