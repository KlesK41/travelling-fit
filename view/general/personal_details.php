<?php if(!session_id()) session_start(); ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Booking - Personal Details</title> 
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
        </div>
      </div>
    </div>
    <form id="personalDetailsValidate" action="index.php?v=" method="post" class="main">
    <div id="page-wrapper" class='container marginTop external-form'>
      <fieldset>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li id="formName">Personal Details</li> 
       </ol>
      <!-- information panel !-->

      <!-- panel 1 !-->
      <div class="panel panel-default">
      <div id="personalDetailsWrapper">
        <input type="hidden" name="method" value="storePackageForm" />
          <div class="bs-personal-details">
            <div class="row">
              <h3 id="guestType">Primary Contact</h3>
              <p id="guestTypeDesription"></p>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2" title="Required" required>
                First Name
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="text" name="firstname" id="firstname" title="Required" required>
              </div>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2">
                Last Name
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="text" name="lastname" id="lastname" title="Required" required>
              </div>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2">
                Preferred Name
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="text" name="prefered_name" id="prefered_name" title="Required" required>
              </div>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2">
                Email Address
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="text" name="email" id="email" title="Required" required>
              </div>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2">
                Gender
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="radio" name="gender" id="gender_M" value="M" required><label for="gender_M">Male</label>
                <input type="radio" name="gender" id="gender_F" value="F" required><label for="gender_F">Female</label>
                <label for="gender" class="error">Required</label>
              </div>
            </div>
            <div class="row space" id="guestChild">
              <div class="col-xs-12 col-sm-2">
                Is this guest a child?
              </div>
              <div class="col-xs-12 col-sm-4">
                <input type="checkbox" name="child_or_adult" id="child_or_adult" value="C">
              </div>
            </div>
            <div class="row space">
              <div class="col-xs-12 col-sm-2">
                Date of Birth (dd/mm/yyyy)
              </div>

              <div class="col-xs-12 col-sm-4">
                <input type="text" name="dob" id="dob" title="Required" required>
              </div>
            </div>
            <div class="row">
              Prefered Event
            </div>
            <div class="row">
              <div id="preferedEvents" class="col-xs-12 col-sm-6">
              </div>
            </div>  
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-2">
                
              </div>
              <div class="col-xs-12 col-sm-4">
                
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
              Extra Night(s) - Pre Itinerary
              </div>
              <div class="col-xs-12 col-sm-6">
              <select name="extra_nights_pre" id="extra_nights_pre" title="Required" required>
                <option value="0">No Thanks</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
              Extra Night(s) - Post Itinerary
              </div>
              <div class="col-xs-12 col-sm-6">
              <select name="extra_nights_post" id="extra_nights_post">
                <option value="0">No Thanks</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
              Share Request
              </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
              Do you require a rollaway or cot?
              </div>
              <div class="col-xs-12 col-sm-6">
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_N" value="N" required><label for="rollaway_or_cot_N">No</label>
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_R" value="R" required><label for="rollaway_or_cot_R">Rollaway</label>
                <input type="radio" name="rollaway_or_cot" id="rollaway_or_cot_C" value="C" required><label for="rollaway_or_cot_C">Cot</label>
                <label for="rollaway_or_cot" class="error">Required</label>
              </div>
            </div>
			
            <div class="row">
				<div class="row space">
					<div class="col-xs-12 col-sm-12"></div>
					<div class="col-xs-12 col-sm-8"></div><div class="col-xs-6 col-sm-4"><input class="submit" type="submit" value="Submit"/><span class='glyphicon glyphicon-circle-arrow-right'></span></div>
					<!-- <div class="col-xs-6 col-sm-4"><a role="button" id="call_ajax" class='btn btn-lg btn-primary homeMenu'>call ajax <span class='glyphicon glyphicon-circle-arrow-right'></span></a></div> -->
				</div>
          </div>
      </div>
      
    </div>
    </fieldset>
  </form>
</div>
  <script src="includes/js/jquery.min.js"></script>
  <script src="includes/js/validate/jquery.validate.min.js"></script>  
  <script src="includes/js/ui/minified/jquery-ui.custom.min.js"></script>  
  <script src="includes/js/jquery.dataTables.min.js"></script>
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/lib/utilities.js"></script>
  <script src="view/general/personal_details.js"></script>
</body>
</html>