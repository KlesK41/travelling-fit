<?php
if(isset($_SESSION['NAB']))
{
	if($_SESSION['NAB']['rescode'] == '00' || $_SESSION['NAB']['rescode'] == '08' || $_SESSION['NAB']['rescode'] == '11')
	{
									// NAB refid is transaction_id -- will change to surname + booking id ---- ***  will need to change email transaction_id's below... ***
		Transactions::updatePayment($_SESSION['NAB']['refid'], $_SESSION['cardholders_name'], $_SESSION['client_payment_amount_enter'], $_SESSION['amount_with_fee'], $_SESSION['NAB']['txnid'], $_SESSION['booking_id'], "Y");
		
	}
	else
	{
		// dump(  $_SESSION['NAB']);
		echo "You have entered some incorrect credit card details. Please press 'Back' in you browser and try again";
		Transactions::updatePayment($_SESSION['NAB']['refid'], $_SESSION['cardholders_name'], $_SESSION['client_payment_amount_enter'], $_SESSION['amount_with_fee'], $_SESSION['NAB']['txnid'] . ' ' . $_SESSION['NAB']['rescode'], $_SESSION['booking_id'], "N");
		die();						
	}
}

?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TravellingFit - Payment Confirmation</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="../includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../includes/js/html5shiv.js"></script>
      <script src="../includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Australian Outback Marathon - Booking</a>
        </div>
      </div>
    </div>
    <div id="page-wrapper" class='container marginTop'>
      <!-- breadcrumbs !-->
       <ol class="breadcrumb"> 
         <li>Payment method</li> 
       </ol>
       <?php if(isset($_GET['c']))
       {
       	echo "<input type='hidden' id='booking_code' name='booking_code' value='" . functions::escapeValue($_GET['c']) . "'>";
       }
       ?>
		<?php 
		echo $_SESSION['booking_id'];
			$message_invoice_CC = Payment::get_email_invoice_CC($_SESSION['booking_id'], "CC");
			// echo $message_invoice_CC;exit;

						// set up mail headers
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: Australian Outback Marathon <noreply@australianoutbackmarathon.com>'. "\r\n";
			$headers .= 'Cc: ' . "\r\n";
			// $headers .= 'Bcc: sales@australianoutbackmarathon.com, software-engineering@hotmail.com, jon@fivebyfive.com.au' . "\r\n";

//			$primary_guest = Payment::primary_guest($_SESSION['booking_id']);
            $primary_guest = Payment::primary_guest_for_payment_invoice($_SESSION['booking_id']);
			$to =	$primary_guest[0]['firstname'] . " " . $primary_guest[0]['lastname'] ." <" . $primary_guest[0]['email'] . '>';
			$subjectConf = "Booking Confirmation for " . $primary_guest[0]['firstname'] . " " . 
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'] . ", Transaction ID: " . $_SESSION['NAB']['refid'];
			$subjectInv = "Booking Invoice for " . $primary_guest[0]['firstname'] . " " . 
						$primary_guest[0]['lastname'] . ", Booking ID: " . $_SESSION['booking_id'] . ", Transaction ID: " . $_SESSION['NAB']['refid'];
			
			if(!mail($to, $subjectConf, Payment::get_email_confirmation($_SESSION['booking_id'], 'DD'), $headers))
			{
				echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
			}
			if(!mail($to, $subjectInv, $message_invoice_CC, $headers))
			{
				echo "<p> Sorry, it appears we were unable to email you. Please press the back button and try again.</p>";
			}

		?>

      	<!-- panel 1 !-->
      	<div id="depositWrapper">
	      <div class="panel panel-default margins">
				<div class="space">
					Your payment has been accepted.
				</div>
				<div class="space">
					<strong>A deposit of <?php echo Payment::deposit($_SESSION['booking_id']) ?> is required to secure this booking.</strong> 
				</div>

				<div class="row space">
					<div class="col-xs-12 col-sm-3">
						<strong>Package Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-9">
<!--					--><?php //$package_accommodation = Payment::accommodation($_SESSION['booking_id']); ?>
                    <?php $package_accommodation = Payment::accommodation_for_payment_invoice($_SESSION['booking_id']); ?>
						<?php echo $package_accommodation[0]['package_name']; ?>
					</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>No. of Adults:</strong>
				 	</div>
				 	<div class="col-xs-12 col-sm-9">
						<?php echo Payment::get_number_adults($_SESSION['booking_id']) ?>
				 	</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>Tours:</strong>
				 	</div>
					<div class="col-xs-12 col-sm-9">
						<?php 
//						$tours = Paymnet::tours_park_titles($_SESSION['booking_id']);
                        $tours = Payment::optional_tours($_SESSION['booking_id']);
						for($i=0; $i<count($tours); $i++) {
							echo $tours[$i]['title'] . "<br>";
						}
						?>
				 	</div>
				</div>
				
				<div class="row space">
				 	<div class="col-xs-12 col-sm-3">
						<strong>Deposit Amount:</strong> 
				 	</div>
				 	<div class="col-xs-12 col-sm-9">
						$<?php echo Payment::deposit($_SESSION['booking_id']) ?>
				 	</div>
				 	<div id="totalAmountWrapper">
					 	<div class="col-xs-12 col-sm-3">
							<strong>Total Amount:</strong> 
					 	</div>
					 	<div class="col-xs-12 col-sm-9">
							$<?php echo Payment::total_cost($_SESSION['booking_id']) ?>
					 	</div>
					 	<div class="col-xs-12 col-sm-3">
							<strong>Amount Due:</strong> 
					 	</div>
					 	<div class="col-xs-12 col-sm-9">
							$<?php echo Payment::get_amount_due($_SESSION['booking_id']) ?>
					 	</div>

					 	<div class="col-xs-12 col-sm-9">
					 		&nbsp
					 	</div>
					 	<div class="col-xs-12 col-sm-9">
					 	<i>Please Note: Direct Deposits will be deducted from the amount due once processed.</i>
					 	</div>
					 </div>
				</div>
			</div>
            <a href="<?php echo $_SERVER['SCRIPT_NAME']?>?v=my_bookings">My bookings</a>
		</div>


<!-- 			<div class="row space buttons-container" id="prev_book_buttons">
				<div class="col-xs-12 col-sm-4">
					<span class='glyphicon glyphicon-circle-arrow-left'></span>
					<input type="button" id="prev_button" value="Previous">
				</div>
				<div class="col-xs-12 col-sm-4">
					<input type="button" id="reset_button" value="Clear Form">
				</div>
				<div class="col-xs-6 col-sm-4">
					<input type="button" id="next_button" value="Book">
					<span class='glyphicon glyphicon-circle-arrow-right'></span>
				</div>  
			</div>
 -->          </div>
      </div>
      <?php
	  unset($_SESSION['bpay_booking_id']);
	  unset($_SESSION['bpay_event_id']);
	  unset($_SESSION['bpay_user_id']);
	  unset($_SESSION['booking_id']);
	  unset($_SESSION['transaction_id']);
      unset($_SESSION['NAB']);
//      	session_destroy();
      // echo Payment::get_invoice($_SESSION['booking_id'], 'DD');
      ?>
  <script src="../includes/js/jquery.min.js"></script>
  <script src="../includes/js/jquery.dataTables.min.js"></script>
  <script src="../includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="../includes/js/validate/jquery.validate.min.js"></script>
  <script src="../includes/js/lib/utilities.js"></script>
  <script src="../includes/js/lib/values.js"></script>
  <script src="../view/general/payment_method.js"></script>
</body>
</html>