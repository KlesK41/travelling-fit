$(document).ready(function() {

});

$("#button1").on("click", function(){
  alert('button1');
});

$("#call_ajax").on("click", function(){
  getSomething(1);
});


$('a.helpLink').on("click", function(){
  Util.safe_log($(this).attr('id'));
  $('div#' + $(this).attr('id') + 'Section').toggle();
  return false;
});

/*
* getSomething(id)
*   For demonstration purposes
* An ajax call
*
* @param {something_id} - id of an entity
*/

function getSomething(something_id)
{
    data        = {};
    data.method     = 'getSomething'
    data.something_id = something_id;
    Util.ajaxData(data, function(returnData){
      Util.safe_log(returnData);
      alert(returnData[0].something_id + " " + returnData[0].attribute1 + " " + returnData[0].attribute2);
    });
}

