<?php
start_session();
if(!isset($_SESSION['generic_is_admin']))
functions::redirectTo("index.php?v=login");
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Home</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Admin Home</a>
        </div>
        <div class="userLoggedOn">Username, Admin</div>
      </div>
    </div>


    <div id="page-wrapper" class='container'>
  <div >
    <ul id="topmenu">
      <li><a href="index.php?v=bookings" class="home topMenuButton" data-original-title="" title="">Bookings</a></li>
      <li><a href="index.php?v=form_values" class="home topMenuButton selected" data-original-title="" title="">Form Values</a></li>
      <li><a href="index.php?v=transactions" class="rubricBuilder topMenuButton" data-original-title="" title="">Transactions</a></li>
    </ul>
  </div>
  <br><br>
      <!-- breadcrumbs !-->
<!--        <ol class="breadcrumb"> 
         <li><a href="index.php?v=admin_home">Home</a></li> 
       </ol> -->
      <!-- information panel !-->



      <!-- panel 3 !-->
<!--       <div class="panel panel-default">
        <div class="row row-space">
          
        </div>
      </div>
 -->
      <?php

        require_once ('includes/xcrud/xcrud/xcrud.php');

        $accom_bed  = Xcrud::get_instance();
        
        

        $xcrud = Xcrud::get_instance();
        $xcrud->table('packages');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_packages',
            'data-task' => 'action',
            'data-primary' => '{package_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_events',
            'packages-task' => 'action',
            'data-primary' => '{package_id}'));
        $xcrud->create_action('_movetop_packages', '_movetop_packages');
        $xcrud->create_action('_movebottom_events', '_movebottom_packages');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        $xcrud->columns('package_id,package_name,nights,available_YN,cost,archived_YN');
        echo $xcrud->render();

        $accom      = Xcrud::get_instance();
        $accom->table('accommodation');
        $accom->table_name('Accommodation');
        $accom->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_accom',
            'data-task' => 'action',
            'data-primary' => '{accommodation_id}'));
        $accom->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_accom',
            'data-task' => 'action',
            'data-primary' => '{accommodation_id}'));
        $accom->create_action('_movetop_accom', '_movetop_accom');
        $accom->create_action('_movebottom_accom', '_movebottom_accom');
        $accom->unset_sortable();
        $accom->order_by('sort_order');
        $accom->columns('accommodation_id,accommodation_name,available_YN,sort_order,archived_YN');
        $accom->fields('accommodation_id','accommodation_name','available_YN','sort_order','archived_YN');
        echo $accom->render();


        $accom_bed->table('accommodation_bedding');
        $accom_bed->table_name('Accommodation Bedding Costs');

        // $accom_bed->join('accommodation_id','room_type','accommodation_id');
        // $accom_bed->join('room_type.bedding_type_id','bedding_type','bedding_type_id'); // on profile.token_id = tokens.id

        $accom_bed->columns('package_id, accommodation_id,bedding_id,accommodation_id,cost_adult,cost_child,cost_extra_night');
        $accom_bed->fields('package_id,accommodation_id,bedding_id,cost_adult,cost_child,cost_extra_night,available_YN');
        $accom_bed->relation('accommodation_id','accommodation','accommodation_id','accommodation_name');
        $accom_bed->label('accommodation_id', 'Accommodation');
        $accom_bed->relation('bedding_id', 'bedding', 'bedding_id', 'bedding_name');
        $accom_bed->label('bedding_id', 'Bedding');
        $accom_bed->relation('package_id', 'packages', 'package_id', 'package_name');
        $accom_bed->label('package_id', 'Package');

        // $accom_bed->table('users');
         // join users and profiles on users.id = profiles.user_id
 
// now join 'profiles' and 'tokens' tables
        

// simple actions with fields: default and joined
        //$accom_bed->column('bedding_type.bedding_type_name');

        echo $accom_bed->render();
/*
JOIN to bedding_type via room_type via accommodation_id
SELECT p.package_name, a.accommodation_name, ab.cost_adult, ab.cost_child, ab.cost_extra_night, 
        rt.room_type_code, bt.bedding_type_name
FROM accommodation_bedding ab, packages p, accommodation a, bedding b, room_type rt, bedding_type bt
WHERE p.package_id = ab.package_id AND ab.accommodation_id = a.accommodation_id
AND b.bedding_id = ab.bedding_id AND rt.accommodation_id = a.accommodation_id 
AND rt.bedding_type_id = bt.bedding_type_id
*/
        $bed        = Xcrud::get_instance();
        $bed->table('bedding');
        $bed->table_name('Bedding');
        $bed->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_bed',
            'data-task' => 'action',
            'data-primary' => '{bedding_id}'));
        $bed->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_bed',
            'data-task' => 'action',
            'data-primary' => '{bedding_id}'));
        $bed->create_action('_movetop_bed', '_movetop_bed');
        $bed->create_action('_movebottom_bed', '_movebottom_bed');
        $bed->unset_sortable();
        $bed->order_by('sort_order');
        $bed->columns('bedding_id,bedding_name,max_people,available_YN');
        $bed->fields('bedding_id,bedding_name,max_people,available_YN');
        echo $bed->render();


        $room_type      = Xcrud::get_instance();
        $room_type->table('room_type');
        $room_type->table_name('room_type');
        $room_type->columns('room_type_id,accommodation_id,room_type_code,bedding_type_id,available_YN,sort_order,archived_YN');
        $room_type->fields('room_type_id,accommodation_id,room_type_code,bedding_type_id,available_YN,sort_order,archived_YN');
        $room_type->relation('bedding_type_id','bedding_type','bedding_type_id','bedding_type_name');
        $room_type->label('bedding_type_id', 'Bedding Type');
        echo $room_type->render();

        $bedding_type      = Xcrud::get_instance();
        $bedding_type->table('bedding_type');
        $bedding_type->table_name('bedding_type');
        $bedding_type->columns('bedding_type_id,bedding_type_name');
        $bedding_type->fields('bedding_type_id,bedding_type_name');
        echo $bedding_type->render();




        $xcrud = Xcrud::get_instance();
        $xcrud->table('events');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_events',
            'data-task' => 'action',
            'data-primary' => '{event_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_events',
            'data-task' => 'action',
            'data-primary' => '{event_id}'));
        $xcrud->create_action('_movetop_events', '_movetop_events');
        $xcrud->create_action('_movebottom_events', '_movebottom_events');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        $xcrud->columns('event_id,event_name,child_or_adult,cost');
        echo $xcrud->render();

        $xcrud = Xcrud::get_instance();
        $xcrud->table('tshirts');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_tshirts',
            'data-task' => 'action',
            'data-primary' => '{tshirt_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_tshirts',
            'data-task' => 'action',
            'data-primary' => '{tshirt_id}'));
        $xcrud->create_action('_movetop_tshirts', '_movetop_tshirts');
        $xcrud->create_action('_movebottom_tshirts', '_movebottom_tshirts');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        echo $xcrud->render();


        $xcrud = Xcrud::get_instance();
        $xcrud->table('optional_tours');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_optional_tours',
            'data-task' => 'action',
            'data-primary' => '{optional_tour_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_optional_tours',
            'data-task' => 'action',
            'data-primary' => '{optional_tour_id}'));
        $xcrud->create_action('_movetop_optional_tours', '_movetop_optional_tours');
        $xcrud->create_action('_movebottom_optional_tours', '_movebottom_optional_tours');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        echo $xcrud->render();


        $optional_tour_package      = Xcrud::get_instance();
        $optional_tour_package->table('optional_tour_package');
        $optional_tour_package->columns('optional_tour_package_id,optional_tour_id,package_id');
        $optional_tour_package->label('package_id', 'Package');
        $optional_tour_package->relation('package_id','packages','package_id','package_name');
        $optional_tour_package->relation('optional_tour_id','optional_tours','optional_tour_id','title');
        $optional_tour_package->label('optional_tour_id', 'Optional Tour');
        $optional_tour_package->fields('package_id,optional_tour_id');
        echo $optional_tour_package->render();


        $xcrud = Xcrud::get_instance();
        $xcrud->table('travel_partner');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_travel_partner',
            'data-task' => 'action',
            'data-primary' => '{travel_partner_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_travel_partner',
            'data-task' => 'action',
            'data-primary' => '{travel_partner_id}'));
        $xcrud->create_action('_movetop_travel_partner', '_movetop_travel_partner');
        $xcrud->create_action('_movebottom_travel_partner', '_movebottom_travel_partner');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        echo $xcrud->render();

        $xcrud = Xcrud::get_instance();
        $xcrud->table('how_hear');
        $xcrud->button('#', "Top", 'glyphicon glyphicon-arrow-up icon-arrow-up', 'btn xcrud-action', array(
            'data-action' => '_movetop_how_hear',
            'data-task' => 'action',
            'data-primary' => '{how_hear_id}'));
        $xcrud->button('#', "Bottom", 'glyphicon glyphicon-arrow-down icon-arrow-down', 'btn xcrud-action', array(
            'data-action' => '_movebottom_how_hear',
            'data-task' => 'action',
            'data-primary' => '{how_hear_id}'));
        $xcrud->create_action('_movetop_how_hear', '_movetop_how_hear');
        $xcrud->create_action('_movebottom_how_hear', '_movebottom_how_hear');
        $xcrud->unset_sortable();
        $xcrud->order_by('sort_order');
        echo $xcrud->render();

        // echo Xcrud::get_instance()->table('events');
        // echo Xcrud::get_instance()->table('tshirts');
        // echo Xcrud::get_instance()->table('optional_tours');
        // echo Xcrud::get_instance()->table('travel_partner');
        // echo Xcrud::get_instance()->table('how_hear');

      ?>
    </div>

  
  <script src="includes/js/jquery.dataTables.min.js"></script>
  
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/lib/utilities.js"></script>
  <script src="view/admin/bookings.js"></script>
</body>
</html>