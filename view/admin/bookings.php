<?php
start_session();
error_reporting(0);
if(!isset($_SESSION['generic_is_admin']))
functions::redirectTo("index.php?v=login");
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Home</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Admin Home</a>
        </div>
        <div class="userLoggedOn">Username, Admin</div>
      </div>
    </div>


    <div id="page-wrapper" class='container' style="width:90%">
  <div >
    <ul id="topmenu">
      <li><a href="index.php?v=bookings" class="home topMenuButton selected" data-original-title="" title="">Bookings</a></li>
      <li><a href="index.php?v=form_values" class="rubricBank topMenuButton" data-original-title="" title="">Form Values</a></li>
      <li><a href="index.php?v=transactions" class="rubricBuilder topMenuButton" data-original-title="" title="">Transactions</a></li>
    </ul>
  </div>
  <br><br>
      <!-- breadcrumbs !-->
<!--        <ol class="breadcrumb"> 
         <li><a href="index.php?v=admin_home">Home</a></li> 
       </ol> -->
      <!-- information panel !-->



      <!-- panel 1 !-->
<!--       <div class="panel panel-default">
        <div class="row row-space">
          
        </div>
      </div>
 -->

      <?php

        require_once ('includes/xcrud/xcrud/xcrud.php');
        $bookings = Xcrud::get_instance();
        $bookings->table('bookings');
        $bookings->table_name('Bookings');
        $bookings->where("bookings.booking_id IN (SELECT DISTINCT booking_id FROM transactions)");
        $bookings->order_by('booking_id', 'desc');
        $bookings->subselect('primary_guest','SELECT lastname FROM guests WHERE booking_id = {booking_id} and primary_contact_YN = \'Y\' LIMIT 1'); // other table
        $bookings->label('primary_guest', 'Primary Guest');
        $bookings->relation('package_id','packages','package_id','package_name');
        $bookings->label('package_id', 'Package');
        $bookings->relation('travel_insurance','travel_insurance','travel_insurance','travel_insurance_name');
        $bookings->label('travel_insurance', 'Travel Insurance');
        $bookings->relation('accommodation_id','accommodation','accommodation_id','accommodation_name');
        $bookings->label('accommodation_id', 'Accommodation');
        $bookings->relation('bedding_id','bedding','bedding_id','bedding_name');
        $bookings->label('bedding_id', 'Bedding');
        $bookings->relation('how_hear_id','how_hear','how_hear_id','name');
        $bookings->subselect('paid','SELECT SUM(amount) FROM transactions WHERE booking_id = {booking_id}'); // other table
        $bookings->label('paid', 'Total Paid');
        $bookings->subselect('paid_with_fee','SELECT SUM(amount_with_fee) FROM transactions WHERE booking_id = {booking_id}'); // other table
        $bookings->label('paid_with_fee', 'Total Paid with Fee');
        // $xcrud->change_type('Profit','price','0',array('prefix'=>'$')); // number format
        // $bookings->relation('booking_id,booking_option_tours,booking_id,optional_tour_id,,true');
        // $bookings->label('optional_tour_id', 'Optional Tours');
        
        $bookings->columns('booking_id,primary_guest,timestamp,total_cost,paid,paid_with_fee,accommodation_id,bedding_id,package_id,travel_insurance, number_adults,number_children'); // columns in grid
        $bookings->fk_relation('Optional Tours', 'booking_id','booking_option_tours','booking_id','optional_tour_id','optional_tours', 'optional_tour_id',array('optional_tour_id','title'));
        $bookings->fields('total_cost,accommodation_id,bedding_id,package_id,travel_insurance, number_adults,number_children,extra_nights_pre,extra_nights_post,share_request_YN,rollaway_or_cot',false,'Package');
        $bookings->fields('travel_insurance,inspirational_story,book_travel_partner_YN,travel_partner_id,ec_firstname,ec_lastname,ec_phone,ec_email,how_hear_id,Optional Tours',false,'Additional Information');
        $bookings->change_type('total_cost','price','0',array('prefix'=>'$'));
        $bookings->change_type('paid','price','0',array('prefix'=>'$'));
        $bookings->change_type('paid_with_fee','price','0',array('prefix'=>'$'));
        $bookings->relation('travel_partner_id','travel_partner','travel_partner_id','name');
        // $bookings->fk_relation('optional tour id', 'booking_id','booking_option_tours','booking_id','optional_tour_id','optional_tours','title');
        
        // $book_option_tours = $bookings->nested_table('Optional Tours', 'booking_id', 'booking_option_tours', 'booking_id');

        $bookings->limit(10);
        

        $guests = $bookings->nested_table('guests', 'booking_id', 'guests', 'booking_id');
        $guests->columns('firstname,lastname,prefered_name,email,primary_contact_YN,child_YN,gender,dob,prefered_event_id',false,'Guests - Details');
        
        $guests->fields('firstname,lastname,prefered_name,email,primary_contact_YN,child_YN,gender,dob,prefered_event_id,nationality,address_same_primary_yn,address_1,city,state,postcode,country,phone,dietry_requirements,additional_info,tshirt_id');

        // rename labels for guest entity
        $guests->label('primary_contact_YN', 'Primary Contact');
        $guests->label('child_YN', 'Child');
        $guests = $bookings->label('guests', 'Guests');

        
        $transactions = $bookings->nested_table('transactions', 'booking_id', 'transactions', 'booking_id');
        $transactions->columns('transaction_id,amount,amount_with_fee,paid,paid_with_fee,payment_method,ts,merchant_trans_id,funds_received_YN,comments',false,'Guests - Details');
        $transactions->subselect('paid','SELECT SUM(amount) FROM transactions WHERE booking_id = {booking_id}'); // other table
        $transactions->change_type('paid','price','0',array('prefix'=>'$'));
        $transactions->label('paid', 'Total Paid');
        $transactions->change_type('amount','price','0',array('prefix'=>'$'));
        $transactions->change_type('amount_with_fee','price','0',array('prefix'=>'$'));

        $transactions->subselect('paid_with_fee','SELECT SUM(amount_with_fee) FROM transactions WHERE booking_id = {booking_id}'); // other table
        $transactions->label('paid_with_fee', 'Total Paid with Fee');
        $transactions->change_type('paid_with_fee','price','0',array('prefix'=>'$'));
        $transactions->fields('transaction_id,amount,amount_with_fee,payment_method,ts,merchant_trans_id,funds_received_YN,comments,archived_YN');

        $transactions->label('merchant_trans_id', 'Merchant ID');

        // $book_option_tours = $bookings->nested_table('Optional Tours', 'booking_id', 'booking_option_tours', 'booking_id');
        // $book_option_tours->columns('booking_id,optional_tour_id');
        // $book_option_tours->fields('booking_id,optional_tour_id');
        // $book_option_tours->table_name('Optional Tours', 'Optional Tours');
        
        // $book_option_tours = $bookings->table('booking_option_tours');
        // $book_option_tours->join('optional_tour_id','optional_tours','optional_tour_id');
        // $book_option_tours->join('booking_id','bookings','booking_id');

        // $book_option_tours->columns('booking_id,optional_tour_id,title');
        // $book_option_tours->relation('booking_id','bookings','booking_id');

        // $xcrud->relation('events','event_id', 'event_name');
        echo $bookings->render();
        // echo Xcrud::get_instance()->table('bookings');
      ?>
    </div>

  
  <script src="includes/js/jquery.dataTables.min.js"></script>
  
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/lib/utilities.js"></script>
  <script src="view/admin/bookings.js"></script>
</body>
</html>