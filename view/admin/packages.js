$(document).ready(function() {
    packageInfo = {};
    editors = []

    tinymce.init({

        selector: '.editable',
        plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern paste",
            "responsivefilemanager"
        ],
        toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
        toolbar2: "searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
        toolbar3: "hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
        external_filemanager_path: "/includes/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "/includes/filemanager/plugin.min.js"},
        height: 300,
        setup: function(editor) {
            editor.on("init", function(){
                editors.push(editor);
            })
        },

});




    // packages form validate start
    $.validator.addMethod("greaterThan",
        function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date(params);
            }
            return isNaN(value) && isNaN(params)
                || (Number(value) >= Number(params));
        },'Must be greater than {0}');
    $.validator.addMethod("greaterThanStart",
        function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return (new Date(value) >= new Date(params)) && (new Date(value) > new Date($('#packageStartDate').val()));
            }
            return isNaN(value) && isNaN(params)
                || (Number(value) >= Number(params));
        },'Must be greater than Start Date');
    var packagesValidator = $('#add-edit-package-form').validate({
        rules: {
            packageName: "required",
            packageDescription: "required",
            packageStartDate: {
                required: true,
                greaterThan: $('#packageStartDate').attr('min-value')
            },
            packageFinishDate: {
                required: true,
                greaterThanStart: $('#packageFinishDate').attr('min-value')
            }
        }
    });
    $('#add-edit-package-form').on("submit", function(e) {
        e.preventDefault();
        for (var i = 0; i < editors.length; i++) {
            if (editors[i].id == 'packageDescription') {
                var packageText = $(editors[i].iframeElement.contentDocument.body).html();
                if (packageText == '<p><br data-mce-bogus="1"></p>') {
                    packageText = '';
                }
                $("#packageDescription").text(packageText);
            }
        }
        if (packagesValidator.form()) {
            savePackage();
        }
    });
    // packages form validate end


    // room types form validate start
    var roomTypesValidator = $('#add-edit-room-type-form').validate({
        rules: {
            roomTypePackage: "required",
            roomTypeName: "required",
            roomTypePrice: {
                required: true,
                number: true,
            },
            roomTypeHotelName: "required",
            roomTypeCapacity: {
                required: true,
                number: true,
            },
        }
    });
    $('#add-edit-room-type-form').on("submit", function(e) {
        e.preventDefault();
        if (roomTypesValidator.form()) {
            saveRoomType();
        }
    });
    // room types form validate end

    // optional tours form validate start
    var optionalToursValidator = $('#add-edit-optional-tour-form').validate({
        rules: {
            optionalTourTitle: "required",
            optionalTourDescription: "required",
            optionalTourStatus: "required"
        }
    });
    $('#add-edit-optional-tour-form').on("submit", function(e) {
        e.preventDefault();
        for (var i = 0; i < editors.length; i++) {
            if (editors[i].id == 'optionalTourDescription') {
                var optionalTourtText = $(editors[i].iframeElement.contentDocument.body).html();
                console.log(optionalTourtText);
                if (optionalTourtText == '<p><br data-mce-bogus="1"></p>') {
                    optionalTourtText = '';
                }
                $("#optionalTourDescription").text(optionalTourtText);
            }
        }
        if (optionalToursValidator.form()) {
            saveOptionalTour();
        }
    });
    // optional tours form validate end

    // room types for optional tours validate start
    var roomTypesForOptionalToursValidator = $('#add-edit-room-types-for-optional-tours-form').validate({
        rules: {
            roomTypeForOptionalToursName: "required",
            roomTypeForOptionalToursPrice: {
                required: true,
                number: true,
            },
            roomTypeForOptionalToursCapacity: {
                required: true,
                number: true,
            },
        }
    });
    $('#add-edit-room-types-for-optional-tours-form').on("submit", function(e) {
        e.preventDefault();
        if (roomTypesForOptionalToursValidator.form()) {
            saveRoomTypeForOptionalTours();
        }
    });
    // room types for optional tours validate end

});

// Get data for event start
function getDataForEvent() {
    var id = $('[name="events"]').val();
    if (id != '' && id != undefined && !isNaN(parseInt(id))) {
        data        = {};
        data.method     = 'getDataForEvent';
        data.event_id = id;
        Util.ajaxData(data, function(returnData){
            $('#list_of_packages').html(returnData.packagesTemplate);
            $('#list_of_room_types').html(returnData.roomTypesTemplate);
            $('#list_of_optional_tours').html(returnData.optionalToursTemplate);
            $('#list-of-room-types-for-optional-tours').html(returnData.roomTypesForOptionalToursTemplate);
            packageInfo.packages = (!Array.isArray(returnData.packages)) ? returnData.packages : {};
            packageInfo.roomTypes = (!Array.isArray(returnData.roomTypes)) ? returnData.roomTypes : {};
            packageInfo.optionalTours = (!Array.isArray(returnData.optionalTours)) ? returnData.optionalTours : {};
            packageInfo.roomTypesForOptionalTours = (!Array.isArray(returnData.roomTypesForOptionalTours)) ? returnData.roomTypesForOptionalTours : {};
            //console.log(returnData.roomTypes)
            //Util.safe_log(returnData);
            console.log(packageInfo);
            $('#event-data').show();
            var options = '';
            for (var i in packageInfo.packages) {
                options += '<option value="' + packageInfo.packages[i].package_id + '">' + packageInfo.packages[i].name + '</option>';
            }
            $('#roomTypePackage').html(options);
            //options = '';
            //for (var i in packageInfo.optionalTours) {
            //    options += '<option value="' + packageInfo.optionalTours[i].optional_tour_id + '">' + packageInfo.optionalTours[i].title + '</option>';
            //}
            //$('#roomTypeOptionalTour').html(options);
        });
    }
}
// Get data for event end

// Packages start
function savePackage() {
    var eventId = $('[name="events"]').val();
    if (eventId != '' && eventId != undefined && !isNaN(parseInt(eventId))) {
        var packageName = $('#packageName').val();
        var packageDescription = $('#packageDescription').val();
        var packageStartDate = $('#packageStartDate').val();
        var packageFinishDate = $('#packageFinishDate').val();
        var packageStatus = $('#packageStatus').val();
        data = {};
        data.event_id = eventId;
        data.name = packageName;
        data.start_date = packageStartDate;
        data.end_date = packageFinishDate;
        data.description = packageDescription;
        data.status = packageStatus;
        if (packageInfo.updatePackage !== undefined) {
            data.method = 'updatePackageForEvent';
            data.package_id = packageInfo.updatePackage;
            updatePackageAJAX(data);
        } else {
            data.method = 'addPackageToEvent';
            addPackageAJAX(data);
        }
    }
}
function updatePackageAJAX(data) {
    Util.ajaxData(data, function(returnData){
        $('#package-tr-' + packageInfo.updatePackage).html(returnData.template);
        ////Util.safe_log(returnData);
        packageInfo.packages[packageInfo.updatePackage] = returnData.package;
        $('#add-edit-package-form').trigger('reset');
        delete(packageInfo.updatePackage);
    });
}
function addPackageAJAX(data) {
    Util.ajaxData(data, function(returnData){
        $('#list_of_packages').append(returnData.template);
        //Util.safe_log(returnData);
        for (var i in returnData.package) {
            var option = '<option value="' + returnData.package[i].package_id + '">' + returnData.package[i].name + '</option>';
            packageInfo.packages[i] = returnData.package[i];
            $('#roomTypePackage').append(option);
        }
        $('#add-edit-package-form').trigger('reset');


    });
}
function deletePackage(id) {
    if (id != '' && id != undefined && !isNaN(parseInt(id))) {
        data = {};
        data.method = 'deletePackage';
        data.package_id = id;
        Util.ajaxData(data, function(returnData){
            $('#package-tr-' + id).remove();
            $('[package_id="' + id + '"]').remove();
            if (id in packageInfo.packages) {
                delete packageInfo.packages[id];
            }
            //Util.safe_log(returnData);
            console.log(packageInfo.packages);
        });
    }
}
function editPackage(id) {
    if (id in packageInfo.packages) {
        var package = packageInfo.packages[id];
        console.log(package);
        packageInfo.updatePackage = id;
        $('#packageName').val(package['name']);
        $('#packageDescription').text(package['description']);
        $('#packageStartDate').val(package['start_date']);
        $('#packageFinishDate').val(package['end_date']);
        $('#packageStatus').val(package['available'].toUpperCase());
        for (var i = 0; i < editors.length; i++) {
            if (editors[i].id == 'packageDescription') {
                editors[i].setContent(package['description']);
            }
        }
    }
}
// Packages end

// Room types start
function saveRoomType() {
    var eventId = $('[name="events"]').val();
    if (eventId != '' && eventId != undefined && !isNaN(parseInt(eventId))) {
        var roomTypePackage = $('#roomTypePackage').val();
        var roomTypeName = $('#roomTypeName').val();
        var roomTypePrice = $('#roomTypePrice').val();
        var roomTypeCapacity = $('#roomTypeCapacity').val();
        var roomTypeStatus = $('#roomTypeStatus').val();
        var roomTypeHotelName = $('#roomTypeHotelName').val();
        data = {};
        data.event_id = eventId;
        data.name = roomTypeName;
        data.price = roomTypePrice;
        data.capacity = roomTypeCapacity;
        data.status = roomTypeStatus;
        data.hotel_name = roomTypeHotelName;
        data.optional_tour = 'N';
        data.package_id = roomTypePackage;
        if (packageInfo.updateRoomType !== undefined) {
            data.method = 'updateRoomTypeForEvent';
            data.room_type_id = packageInfo.updateRoomType;
            updateRoomTypeAJAX(data);
        } else {
            data.method = 'addRoomTypeForEvent';
            addRoomTypeAJAX(data);
        }


    }
}
function deleteRoomType(id) {
    if (id != '' && id != undefined && !isNaN(parseInt(id))) {
        data = {};
        data.method = 'deleteRoomType';
        data.room_type_id = id;
        deleteRoomTypeAJAX(data);
    }
}
function editRoomType(id) {
    if (id in packageInfo.roomTypes) {
        var roomType = packageInfo.roomTypes[id];
        console.log(roomType);
        var package_id = roomType.package_id;
        packageInfo.updateRoomType = id;
        $('#roomTypeHotelName').attr('disabled', true);
        $('#roomTypePackage').attr('disabled', true);
        $('#roomTypePackage').val(package_id);
        $('#roomTypeName').val(roomType['name']);
        $('#roomTypeDescription').val(roomType['description']);
        $('#roomTypeHotelName').val(roomType['hotel_name']);
        $('#roomTypePrice').val(roomType['price']);
        $('#roomTypeCapacity').val(roomType['capacity']);
        $('#roomTypeStatus').val(roomType['status'].toUpperCase());
    }
}
function updateRoomTypeAJAX(data) {
    Util.ajaxData(data, function(returnData){
        $('#room-type-tr-' + packageInfo.updateRoomType).html(returnData.template);
        ////Util.safe_log(returnData);
        packageInfo.roomTypes[packageInfo.updateRoomType] = returnData.roomType;
        $('#add-edit-room-type-form').trigger('reset');
        delete(packageInfo.updateRoomType);
    });
}
function addRoomTypeAJAX(data) {
        Util.ajaxData(data, function(returnData){
            console.log(packageInfo.roomTypes);
            $('#list_of_room_types').append(returnData.template);
            //Util.safe_log(returnData);
            for (var i in returnData.room_type) {
                packageInfo.roomTypes[i] = returnData.room_type[i];
            }
            $('#add-edit-room-type-form').trigger('reset');
            console.log(packageInfo);
        });
    }
function deleteRoomTypeAJAX(data) {
    var id = data.room_type_id;
    Util.ajaxData(data, function(returnData){
        $('#room-type-tr-' + id).remove();
        if (id in packageInfo.roomTypes) {
            delete packageInfo.roomTypes[id]
        }
        //Util.safe_log(returnData);
    });
}
// Room types end

// Optional tours start
function saveOptionalTour() {
    var eventId = $('[name="events"]').val();
    if (eventId != '' && eventId != undefined && !isNaN(parseInt(eventId))) {
        var optionalTourTitle = $('#optionalTourTitle').val();
        var optionalTourDescription = $('#optionalTourDescription').val();
        var optionalTourStatus = $('#optionalTourStatus').val();
        data = {};
        data.event_id = eventId;
        data.title = optionalTourTitle;
        data.description = optionalTourDescription;
        data.status = optionalTourStatus;
        if (packageInfo.updateOptionalTour !== undefined) {
            data.method = 'updateOptionalTourForEvent';
            data.optional_tour_id = packageInfo.updateOptionalTour;
            updateOptionalTourAJAX(data);
        } else {
            data.method = 'addOptionalTourForEvent';
            addOptionalTourAJAX(data);
        }
    }
}
function addOptionalTourAJAX(data) {
    Util.ajaxData(data, function(returnData){
        console.log(packageInfo.optionalTours);
        $('#list_of_optional_tours').append(returnData.template);
        //Util.safe_log(returnData);
        for (var i in returnData.optional_tour) {
            packageInfo.optionalTours[i] = returnData.optional_tour[i];
        }
        $('#add-edit-optional-tour-form').trigger('reset');
        console.log(packageInfo);
    });
}
function deleteOptionalTour(id) {
    if (id != '' && id != undefined && !isNaN(parseInt(id))) {
        data = {};
        data.method = 'deleteOptionalTour';
        data.optional_tour_id = id;
        deleteOptionalTourAJAX(data);
    }
}
function deleteOptionalTourAJAX(data) {
    var id = data.optional_tour_id;
    Util.ajaxData(data, function(returnData){
        $('#optional-tour-tr-' + id).remove();
        if (id in packageInfo.optionalTours) {
            delete packageInfo.optionalTours[id]
        }
        //Util.safe_log(returnData);
    });
}
function editOptionalTour(id) {
    if (id in packageInfo.optionalTours) {
        var optionalTour = packageInfo.optionalTours[id];
        packageInfo.updateOptionalTour = id;
        $('#optionalTourTitle').val(optionalTour['title']);
        $('#optionalTourDescription').text(optionalTour['description']);
        $('#optionalTourStatus').val(optionalTour['available_yn'].toUpperCase());
        for (var i = 0; i < editors.length; i++) {
            if (editors[i].id == 'optionalTourDescription') {
                editors[i].setContent(package['description']);
            }
        }
    }
}
function updateOptionalTourAJAX(data) {
    Util.ajaxData(data, function(returnData){
        $('#optional-tour-tr-' + packageInfo.updateOptionalTour).html(returnData.template);
        ////Util.safe_log(returnData);
        packageInfo.optionalTours[packageInfo.updateOptionalTour] = returnData.optionalTour;
        $('#add-edit-optional-tour-form').trigger('reset');
        delete(packageInfo.updateOptionalTour);
    });
}
// Optional tours end

// Room types for optional tours start
function saveRoomTypeForOptionalTours() {
    var eventId = $('[name="events"]').val();
    if (eventId != '' && eventId != undefined && !isNaN(parseInt(eventId))) {
        var roomTypeForOptionalToursName = $('#roomTypeForOptionalToursName').val();
        var roomTypeForOptionalToursPrice = $('#roomTypeForOptionalToursPrice').val();
        var roomTypeForOptionalToursCapacity = $('#roomTypeForOptionalToursCapacity').val();
        var roomTypeForOptionalToursStatus = $('#roomTypeForOptionalToursStatus').val();
        data = {};
        data.event_id = eventId;
        data.name = roomTypeForOptionalToursName;
        data.price = roomTypeForOptionalToursPrice;
        data.capacity = roomTypeForOptionalToursCapacity;
        data.status = roomTypeForOptionalToursStatus;
        data.hotel_name = '';
        data.optional_tour = 'Y';
        if (packageInfo.updateRoomTypeForOptionalTours !== undefined) {
            data.method = 'updateRoomTypeForEvent';
            data.room_type_id = packageInfo.updateRoomTypeForOptionalTours;
            updateRoomTypeForOptionalToursAJAX(data);
        } else {
            data.method = 'addRoomTypeForEvent';
            addRoomTypeForOptionalToursAJAX(data);
        }


    }
}
function deleteRoomTypeForOptionalTours(id) {
    console.log(packageInfo.roomTypesForOptionalTours);
    if (id != '' && id != undefined && !isNaN(parseInt(id))) {
        data = {};
        data.method = 'deleteRoomType';
        data.room_type_id = id;
        deleteRoomTypeForOptionalToursAJAX(data);
    }
}
function editRoomTypeForOptionalTours(id) {
    if (id in packageInfo.roomTypesForOptionalTours) {
        var roomTypeForOptionalTours = packageInfo.roomTypesForOptionalTours[id];
        packageInfo.updateRoomTypeForOptionalTours = id;
        $('#roomTypeForOptionalToursName').val(roomTypeForOptionalTours['name']);
        $('#roomTypeForOptionalToursPrice').val(roomTypeForOptionalTours['price']);
        $('#roomTypeForOptionalToursCapacity').val(roomTypeForOptionalTours['capacity']);
        $('#roomTypeForOptionalToursStatus').val(roomTypeForOptionalTours['status'].toUpperCase());
    }
}
function updateRoomTypeForOptionalToursAJAX(data) {
    //console.log(packageInfo);
    Util.ajaxData(data, function(returnData){
        $('#room-type-for-optional-tours-tr-' + packageInfo.updateRoomTypeForOptionalTours).html(returnData.template);
        ////Util.safe_log(returnData);
        packageInfo.roomTypesForOptionalTours[packageInfo.updateRoomTypeForOptionalTours] = returnData.roomTypeForOptionalTours;
        $('#add-edit-room-types-for-optional-tours-form').trigger('reset');
        delete(packageInfo.updateRoomTypeForOptionalTours);
        console.log(packageInfo);
    });
}
function addRoomTypeForOptionalToursAJAX(data) {
    Util.ajaxData(data, function(returnData){
        $('#list-of-room-types-for-optional-tours').append(returnData.template);
        //Util.safe_log(returnData);
        for (var i in returnData.room_type) {
            packageInfo.roomTypesForOptionalTours[i] = returnData.room_type[i];
        }
        $('#add-edit-room-types-for-optional-tours-form').trigger('reset');
    });
}
function deleteRoomTypeForOptionalToursAJAX(data) {
    var id = data.room_type_id;
    Util.ajaxData(data, function(returnData){
        $('#room-type-for-optional-tours-tr-' + id).remove();
        if (id in packageInfo.roomTypesForOptionalTours) {
            delete packageInfo.roomTypesForOptionalTours[id]
        }
        console.log(packageInfo.roomTypesForOptionalTours);
        //Util.safe_log(returnData);
    });
}
// Room types for optional tours end