<?php
start_session();
if(!isset($_SESSION['generic_is_admin']))
functions::redirectTo("index.php?v=login");
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Home</title>
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="includes/styles/general.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="includes/js/html5shiv.js"></script>
      <script src="includes/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Admin Home</a>
        </div>
        <div class="userLoggedOn">Username, Admin</div>
      </div>
    </div>


    <div id="page-wrapper" class='container' style="width:90%">
  <div >
    <ul id="topmenu">
      <li><a href="index.php?v=bookings" class="home topMenuButton" data-original-title="" title="">Bookings</a></li>
      <li><a href="index.php?v=form_values" class="home topMenuButton" data-original-title="" title="">Form Values</a></li>
      <li><a href="index.php?v=transactions" class="home topMenuButton selected" data-original-title="" title="">Transactions</a></li>
    </ul>
  </div>
      <!-- breadcrumbs !-->
<!--        <ol class="breadcrumb"> 
         <li><a href="index.php?v=admin_home">Home</a></li> 
       </ol> -->
      <!-- information panel !-->



      <!-- panel 3 !-->
      <div class="panel panel-default">
        <div class="row row-space">
          
        </div>
      </div>

            <!-- panel 3 !-->
      <div class="panel panel-default">
        <div class="row row-space">
          <p></p>
        </div>
      </div>
      <?php

        require_once ('includes/xcrud/xcrud/xcrud.php');
        $transactions = Xcrud::get_instance();

        $transactions->table('transactions');
        $transactions->columns('transaction_id,booking_id,amount,amount_with_fee,paid,total_paid_with_fee,payment_method,merchant_trans_id,funds_received_YN,comments, ts'); // columns in grid
        $transactions->order_by('transaction_id', 'desc');
        //$bookings->fk_relation('Optional Tours', 'booking_id','booking_option_tours','booking_id','optional_tour_id','optional_tours', 'optional_tour_id',array('optional_tour_id','title'));
        $transactions->columns('transaction_id,booking_id,amount,amount_with_fee,paid,total_paid_with_fee,payment_method,merchant_trans_id,funds_received_YN,comments, ts', false,'Transactions');
        $transactions->change_type('amount','price','0',array('prefix'=>'$'));
        $transactions->change_type('amount_with_fee','price','0',array('prefix'=>'$'));
        $transactions->label('funds_received_YN', 'Received');
        $transactions->subselect('paid','SELECT SUM(amount) FROM transactions t, bookings b WHERE t.booking_id = b.booking_id and t.booking_id = {booking_id}'); // other table
        $transactions->change_type('paid','price','0',array('prefix'=>'$'));
        $transactions->label('paid', 'Total Paid');
        $transactions->label('amount_with_fee', 'With Fee');

        $transactions->subselect('total_paid_with_fee','SELECT SUM(amount_with_fee) FROM transactions t, bookings b WHERE t.booking_id = b.booking_id and t.booking_id = {booking_id}'); // other table
        $transactions->change_type('total_paid_with_fee','price','0',array('prefix'=>'$'));
        $transactions->label('total_paid_with_fee', 'With Fee');


        //$transactions->fields('travel_insurance,inspirational_story,book_travel_partner_yn,travel_partner_id,ec_firstname,ec_lastname,ec_phone,ec_email,how_hear_id,Optional Tours',false,'Additional Information');
       
       echo $transactions->render();

      ?>
    </div>

  
  <script src="includes/js/jquery.dataTables.min.js"></script>
  
  <script src="includes/js/bootstrap/bootstrap.min.js"></script>
  <script src="includes/js/lib/utilities.js"></script>
  <script src="view/admin/bookings.js"></script>
</body>
</html>