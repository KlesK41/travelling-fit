<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>Metronic | Form Layouts</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="/includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="/includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="/includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="/includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
    <!-- BEGIN HEADER INNER -->
    <div class="container">
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    Travelling Fit
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle" src="/includes/assets/layouts/layout/img/avatar3_small.jpg"/>
                            <span class="username username-hide-on-mobile"> Nick </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="page_user_profile_1.html">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a href="app_calendar.html">
                                    <i class="icon-calendar"></i> My Calendar </a>
                            </li>
                            <li>
                                <a href="app_inbox.html">
                                    <i class="icon-envelope-open"></i> My Inbox
                                    <span class="badge badge-danger"> 3 </span>
                                </a>
                            </li>
                            <li>
                                <a href="app_todo.html">
                                    <i class="icon-rocket"></i> My Tasks
                                    <span class="badge badge-success"> 7 </span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="page_user_lock_1.html">
                                    <i class="icon-lock"></i> Lock Screen </a>
                            </li>
                            <li>
                                <a href="page_user_login_1.html">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="javascript:;" class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="container">
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.html">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="index.html">Travelling Fit</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>My Booking</span>
                            </li>
                        </ul>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav">
                        <li><i class="fa fa-th fa-2x"
                               style="padding-top:20px;margin-left:-15px;padding-right:50px;"></i></li>
                        <li><a href="#">Bookings</a></li>
                        <li><a href="#">Transactions</a></li>
                        <li><a href="#">Add a VIP</a></li>
                        <li class="active"><a href="#">Packages Section</a></li>
                        <li><a href="#">Booking Form Values</a></li>
                    </ul>

                        <span class="fa-stack fa-lg" style="float:right;padding-top:20px;color:#93a2a9;">
                          <i class="fa fa-circle-thin fa-stack-2x"></i>
                          <i class="fa fa-question fa-stack-1x"></i>
                        </span>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <div class="col-md-12">
                                <div class="head-block">

                                </div>
                            </div>
                            <div class="portlet-body form new-table" style="display: block !important;">
                                <!-- BEGIN FORM-->
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            <h3 style="margin: 0;">Select Event</h3></label>

                                        <div class="col-md-7">
                                            <select class="form-control input-lg" name="events">
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="buttons-wrapp" style="    text-align: right;">
                                                <a href="javascript:;" class="btn green"
                                                   onclick="getDataForEvent();">GO</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div id="event-data" hidden>
                                        <div class="col-md-12">
                                            <div class="head-block">
                                                Existing Packages for this Event
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="sample_editable_1_wrapper" class="dataTables_wrapper no-footer">
                                                <div class="table-scrollable">
                                                    <table
                                                        class="table table-striped table-hover table-bordered dataTable no-footer"
                                                        id="sample_editable_1" role="grid"
                                                        aria-describedby="sample_editable_1_info">
                                                        <thead>
                                                        <tr role="row" style="color: #93a2a9;">
                                                            <th style="width: 263px;">Package Name</th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Full Name : activate to sort column ascending"
                                                                style="width: 293px;"> Package ID
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Points : activate to sort column ascending"
                                                                style="width: 193px;"> Status
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Notes : activate to sort column ascending"
                                                                style="width: 201px;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="list_of_packages"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Add/Update Package start -->
                                        <form action="#" class="form-edit-package" id="add-edit-package-form">
                                            <div class="col-md-12">
                                                <div class="head-block">
                                                    Add / Edit Package
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Package Name</label>

                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-lg"
                                                           placeholder="Package name" name="packageName"
                                                           id="packageName">
                                          <span class="help-block">
                                          <span class="fa-stack fa-lg" style="padding-top:10px;color:#93a2a9;">
                                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                                            <i class="fa fa-question fa-stack-1x"></i>
                                          </span>  A lot of packages have text / images before them. This is where that text is entered: </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Package Description</label>

                                                <div class="col-md-9">
                                                    <textarea class="form-control editable" rows="3" data-gramm=""
                                                              data-txt_gramm_id="ae6f562f-122f-c1e8-c6b0-b4f8c9393e47"
                                                              name="packageDescription"
                                                              id="packageDescription"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Start Date</label>
                                        <span class="fa-stack fa-lg" style="padding-top:15px;color:#93a2a9;">
                                          <i class="fa fa-circle-thin fa-stack-2x"></i>
                                          <i class="fa fa-question fa-stack-1x"></i>
                                        </span>

                                                <div class="col-md-4">
                                                    <div class="input-group date date-picker"
                                                         data-date-format="dd-mm-yyyy">
                                                        <input type="date" class="form-control input-lg"
                                                               min-value="2000-01-02" name="packageStartDate"
                                                               id="packageStartDate">
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Finish Date</label>
                                        <span class="fa-stack fa-lg" style="padding-top:15px;color:#93a2a9;">
                                          <i class="fa fa-circle-thin fa-stack-2x"></i>
                                          <i class="fa fa-question fa-stack-1x"></i>
                                        </span>

                                                <div class="col-md-4">
                                                    <div class="input-group date date-picker"
                                                         data-date-format="dd-mm-yyyy">
                                                        <input type="date" class="form-control input-lg"
                                                               min-value="2000-01-02" name="packageFinishDate"
                                                               id="packageFinishDate">
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> Status </label>
                                        <span class="fa-stack fa-lg" style="padding-top:15px;color:#93a2a9;">
                                          <i class="fa fa-circle-thin fa-stack-2x"></i>
                                          <i class="fa fa-question fa-stack-1x"></i>
                                        </span>

                                                <div class="col-md-4">
                                                    <select class="form-control input-lg" name="packageStatus"
                                                            id="packageStatus">
                                                        <option value="Y">Available</option>
                                                        <option value="N">Not available</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp" style="    text-align: right;">
                                                        <a href="javascript:;" class="btn green"
                                                           onclick="$('#add-edit-package-form').submit();">Add / Update
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Add/Update Package end -->

                                        <!-- Add/Update Room Type start -->
                                        <form action="" class="form-edit-room-types" method="post"
                                              id="add-edit-room-type-form">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" style="text-a;lign:left;">Hotel
                                                    Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-lg"
                                                           placeholder="Large Input"
                                                           style="width:92%;display:inline-block;"
                                                           name="roomTypeHotelName" id="roomTypeHotelName">
                                                    <span class="fa-stack fa-lg"
                                                          style="padding-top:15px;color:#93a2a9;float:right;">
                                                      <i class="fa fa-circle-thin fa-stack-2x"></i>
                                                      <i class="fa fa-question fa-stack-1x"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> Package </label>
                                                <div class="col-md-8">
                                                    <select class="form-control input-lg" name="roomTypePackage"
                                                            id="roomTypePackage"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="head-block">
                                                    Room Types
                                                <span class="fa-stack fa-lg"
                                                      style="padding-top:10px;color:#93a2a9;font-size:14px;">
                                                  <i class="fa fa-circle-thin fa-stack-2x"></i>
                                                  <i class="fa fa-question fa-stack-1x"></i>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="sample_editable_1_wrapper"
                                                     class="dataTables_wrapper no-footer">
                                                    <div class="table-scrollable">
                                                        <table
                                                            class="table table-striped table-hover table-bordered dataTable no-footer"
                                                            id="sample_editable_1" role="grid"
                                                            aria-describedby="sample_editable_1_info">
                                                            <thead>
                                                            <tr role="row" style="color: #93a2a9;">
                                                                <th style="width: 263px;"> Name</th>
                                                                <th class="" tabindex="0"
                                                                    aria-controls="sample_editable_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label=" Full Name : activate to sort column ascending"
                                                                    style="width: 193px;"> Price
                                                                </th>
                                                                <th class="" tabindex="0"
                                                                    aria-controls="sample_editable_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label=" Full Name : activate to sort column ascending"
                                                                    style="width: 93px;"> Capacity
                                                                </th>
                                                                <th class="" tabindex="0"
                                                                    aria-controls="sample_editable_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label=" Points : activate to sort column ascending"
                                                                    style="width: 193px;"> Status
                                                                </th>
                                                                <th class="" tabindex="0"
                                                                    aria-controls="sample_editable_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label=" Notes : activate to sort column ascending"
                                                                    style="width: 201px;"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="list_of_room_types">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="head-block">
                                                    Add / Edit Room Types
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-md-1">Name</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-2"
                                                           name="roomTypeName" id="roomTypeName">
                                                </div>

                                                <label class="col-md-1">Price</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-3"
                                                           name="roomTypePrice" id="roomTypePrice">
                                                </div>

                                                <label class="col-md-1">Capacity</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-4"
                                                           name="roomTypeCapacity" id="roomTypeCapacity">
                                                </div>

                                                <label class="col-md-1 control-label"
                                                       style="margin-top: 20px;text-align: left;"> Status</label>

                                                <div class="col-md-3" style="margin-top: 20px;">
                                                    <select class="form-control" name="roomTypeStatus"
                                                            id="roomTypeStatus">
                                                        <option value="Y">Available</option>
                                                        <option value="N">Not available</option>
                                                    </select>
                                                </div>

                                          <span class="fa-stack fa-lg" style="padding-top:30px;color:#93a2a9;">
                                              <i class="fa fa-circle-thin fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp" style="    text-align: right;">
                                                        <a href="javascript:;" class="btn green"
                                                           onclick="$('#add-edit-room-type-form').submit();">Add /
                                                            Update
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Add/Update Room Type end -->


                                        <div class="col-md-12">
                                            <div class="head-block">
                                                Existing Optional Tour for this Event
                                            <span class="fa-stack fa-lg"
                                                  style="padding-top:5px;color:#93a2a9;font-size:14px;">
                                              <i class="fa fa-circle-thin fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="sample_editable_1_wrapper" class="dataTables_wrapper no-footer">
                                                <div class="table-scrollable">
                                                    <table
                                                        class="table table-striped table-hover table-bordered dataTable no-footer"
                                                        id="sample_editable_1" role="grid"
                                                        aria-describedby="sample_editable_1_info">
                                                        <thead>
                                                        <tr role="row" style="color: #93a2a9;">
                                                            <th style="width: 263px;"> Optional Tour Name</th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Full Name : activate to sort column ascending"
                                                                style="width: 293px;"> Optional Tour ID
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Points : activate to sort column ascending"
                                                                style="width: 193px;"> Status
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Notes : activate to sort column ascending"
                                                                style="width: 201px;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="list_of_optional_tours"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Add/Update Optional Tour start-->
                                        <form action="" class="form-edit-optional-tour" method="post"
                                              id="add-edit-optional-tour-form">
                                            <div class="col-md-12">
                                                <div class="head-block">
                                                    Add / Edit Optional Tour
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Optional Tour Name</label>

                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-lg"
                                                           placeholder="Large Input" id="optionalTourTitle"
                                                           name="optionalTourTitle">
                                            <span class="help-block">
                                            <span class="fa-stack fa-lg"
                                                  style="padding-top:5px;color:#93a2a9;font-size:14px;">
                                              <i class="fa fa-circle-thin fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                             A lot of packages have text / images before them. This is where that text is entered: </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Optional Tour Description</label>

                                                <div class="col-md-9">
                                                    <textarea class="form-control editable" rows="3" data-gramm=""
                                                              data-txt_gramm_id="ae6f562f-122f-c1e8-c6b0-b4f8c9393e47"
                                                              id="optionalTourDescription"
                                                              name="optionalTourDescription"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" style="margin-top: 20px;"> Status

                                                </label>

                                                <div class="col-md-4" style="margin-top: 20px;">
                                                    <select class="form-control input-lg" id="optionalTourStatus"
                                                            name="optionalTourStatus">
                                                        <option value="Y">Available</option>
                                                        <option value="N">Not available</option>
                                                    </select>
                                                </div>
                                          <span class="fa-stack fa-lg" style="padding-top:35px;color:#93a2a9;">
                                              <i class="fa fa-circle-thin fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp" style="    text-align: right;">
                                                        <a href="javascript:;" class="btn green"
                                                           onclick="$('#add-edit-optional-tour-form').submit();">Add /
                                                            Update
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Add/Update Optional Tour end-->

                                        <div class="col-md-12">
                                            <div class="head-block">
                                                Room Types
                                            <span class="fa-stack fa-lg"
                                                  style="padding-top:7px;color:#93a2a9;font-size:14px;">
                                              <i class="fa fa-circle-thin fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="sample_editable_1_wrapper" class="dataTables_wrapper no-footer">
                                                <div class="table-scrollable">
                                                    <table
                                                        class="table table-striped table-hover table-bordered dataTable no-footer"
                                                        id="sample_editable_1" role="grid"
                                                        aria-describedby="sample_editable_1_info">
                                                        <thead>
                                                        <tr role="row" style="color: #93a2a9;">
                                                            <th style="width: 263px;"> Name</th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Full Name : activate to sort column ascending"
                                                                style="width: 193px;"> Price
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Full Name : activate to sort column ascending"
                                                                style="width: 93px;"> Capacity
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Points : activate to sort column ascending"
                                                                style="width: 193px;"> Status
                                                            </th>
                                                            <th class="" tabindex="0" aria-controls="sample_editable_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label=" Notes : activate to sort column ascending"
                                                                style="width: 201px;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="list-of-room-types-for-optional-tours"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Add/Update room types for optional tours start -->
                                        <form action="" class="edit-room-types" method="post"
                                              id="add-edit-room-types-for-optional-tours-form">
                                            <div class="col-md-12">
                                                <div class="head-block">
                                                    Add / Edit Room Types
                                                </div>
                                            </div>

<!--                                            <div class="row">-->
<!--                                                <label class="col-md-1">Optional Tour</label>-->
<!--                                                <div class="col-md-9">-->
<!--                                                    <select class="form-control input-lg" name="roomTypeOptionalTour" id="roomTypeOptionalTour"></select>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <br/>-->

                                            <div class="row">

                                                <label class="col-md-1">Name</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-2"
                                                           id="roomTypeForOptionalToursName"
                                                           name="roomTypeForOptionalToursName">
                                                </div>
                                                <label class="col-md-1">Price</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-3"
                                                           id="roomTypeForOptionalToursPrice"
                                                           name="roomTypeForOptionalToursPrice">
                                                </div>
                                                <label class="col-md-1">Capacity</label>

                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder=".col-md-4"
                                                           id="roomTypeForOptionalToursCapacity"
                                                           name="roomTypeForOptionalToursCapacity">
                                                </div>
                                                <label class="col-md-1 control-label"
                                                       style="margin-top: 20px;text-align:left;"> Status</label>

                                                <div class="col-md-3" style="margin-top: 20px;">
                                                    <select class="form-control" id="roomTypeForOptionalToursStatus"
                                                            name="roomTypeForOptionalToursStatus">
                                                        <option value="Y">Available</option>
                                                        <option value="N">Not available</option>
                                                    </select>
                                                </div>
                                                <span class="fa-stack fa-lg" style="padding-top:30px;color:#93a2a9;font-size:14px;">
                                                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                                                    <i class="fa fa-question fa-stack-1x"></i>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp" style="    text-align: right;">
                                                        <a href="javascript:;" class="btn green"
                                                           onclick="$('#add-edit-room-types-for-optional-tours-form').submit();">Add
                                                            / Update</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Add/Update room types for optional tours end -->

                                        <form action="" class="form-package-section-text">
                                            <div class="big-textarea">
                                                <div class="col-md-12">
                                                    <div class="head-block">
                                                        Packages Section Text
                                              <span class="fa-stack fa-lg"
                                                    style="padding-top:7px;color:#93a2a9;font-size:14px;">
                                                <i class="fa fa-circle-thin fa-stack-2x"></i>
                                                <i class="fa fa-question fa-stack-1x"></i>
                                              </span>
                                                    </div>
                                                </div>
                                                <p style="padding-left: 80px;">
                                                    Enter text and images that will go in the Packages section for the
                                                    selected event.
                                                    <br> To insert a Package from above, type [package_id=1]
                                                    <br> To insert an Optional Tour from above, type
                                                    [optional_tour_id=1]
                                                    <br> Package ID's and Optional Tour ID's are listed above.
                                                </p>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <textarea class="form-control" rows="3" data-gramm=""
                                                                  data-txt_gramm_id="ae6f562f-122f-c1e8-c6b0-b4f8c9393e47"> lorem</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <div class="buttons-wrapp"
                                                         style="margin-top: 20px; text-align: right;">
                                                        <a href="javascript:;" class="btn green">Save
                                                        </a>
                                                        <a href="javascript:;" class="btn green">Preview
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONTENT BODY -->
                            <!-- END CONTENT -->
                            <!-- BEGIN QUICK SIDEBAR -->
                        </div>
                        <!-- END CONTAINER -->
                        <!-- BEGIN FOOTER -->
                        <div class="page-footer">
                            <div class="scroll-to-top">
                                <i class="icon-arrow-up"></i>
                            </div>
                        </div>
                    </div>
                    <!-- END FOOTER -->
                    <!--[if lt IE 9]>
                    <script src="/includes/assets/global/plugins/respond.min.js"></script>
                    <script src="/includes/assets/global/plugins/excanvas.min.js"></script>
                    <![endif]-->
                    <!-- BEGIN CORE PLUGINS -->
                    <script src="/includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                    <script src="/includes/assets/global/plugins/bootstrap/js/bootstrap.min.js"
                            type="text/javascript"></script>
                    <script
                        src="/includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
                        type="text/javascript"></script>
                    <script src="/includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
                            type="text/javascript"></script>
                    <script src="/includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                    <script src="/includes/assets/global/plugins/uniform/jquery.uniform.min.js"
                            type="text/javascript"></script>
                    <script src="/includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
                            type="text/javascript"></script>
                    <!-- END CORE PLUGINS -->
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN THEME GLOBAL SCRIPTS -->
                    <script src="/includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
                    <!-- END THEME GLOBAL SCRIPTS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script src="/includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL SCRIPTS -->
                    <!-- BEGIN THEME LAYOUT SCRIPTS -->
                    <script src="/includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
                    <script src="/includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
                    <script src="/includes/assets/layouts/global/scripts/quick-sidebar.min.js"
                            type="text/javascript"></script>

                    <script src="/includes/js/lib/utilities.js" type="text/javascript"></script>
                    <script src="/view/admin/packages.js" type="text/javascript"></script>
                    <script src="/includes/js/jquery.validate.js"></script>
                    <script src="/includes/js/tinymce/tinymce.min.js"></script>
                    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
