<?php
start_session();
// if(!isset($_SESSION['generic_is_admin']))
// functions::redirectTo("index.php?v=login");

$path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>TravellingFit - Transactions</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
          rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
          rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet"
          id="style_components" type="text/css"/>
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet"
          type="text/css"
          id="style_color"/>
    <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>


    <link type="text/css" href="../includes/styles/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/bootstrap-glyphicons.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/jquery.dataTables.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/general.css" rel="stylesheet" />
    <link type="text/css" href="../includes/styles/my_style.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <script src="../includes/js/html5shiv.js"></script>
      <script src="../includes/js/respond.min.js"></script>
    <style type="text/css">
      .xcrud .xcrud-th th {
        vertical-align: top;
        white-space: normal;
      }
      .xcrud .xcrud-th th:nth-child(2) {
        white-space: nowrap;
      }
    </style>
</head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
    <!-- BEGIN HEADER INNER -->
    <div class="container">
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    Travelling Fit
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="http://67.225.210.86/~travellingfit/wp-login.php?action=logout" class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="container">
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo BASEPATH;?>">Travelling Fit</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Transactions</span>
                            </li>
                        </ul>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav">
                        <li><i class="fa fa-th fa-2x"
                               style="padding-top:20px;margin-left:-15px;padding-right:50px;"></i></li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_bookings'; ?>">Bookings</a></li>
                        <li class="active" ><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_transactions'; ?>">Transactions</a>
                        </li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_guests'; ?>">Guests</a></li>
                        <li ><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_packages'; ?>">Package Inclusions</a></li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_form_values'; ?>">Booking Form
                                Values</a></li>
                    </ul>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <div class="col-md-12">
                                <div class="head-block">

                                </div>
                            </div>
                            <div class="portlet-body form new-table" style="display: block !important;">
                                <!-- BEGIN FORM-->
                                <div class="form-horizontal">
                                    <div class="form-group">

                                        <?php

                                        require_once ('../includes/xcrud/xcrud/xcrud.php');
                                        $transactions = Xcrud::get_instance();

                                        $transactions->table('transactions');
                                        $transactions->columns('transaction_id,booking_id,cardholders_name, amount, payment_method,merchant_trans_id,funds_received_YN, bpay_code, ts'); // columns in grid
                                        $transactions->fields('transaction_id,booking_id,cardholders_name, amount, payment_method,merchant_trans_id,funds_received_YN, bpay_code, user_id, ts', false, 'Transactions'); // columns in grid
                                        $transactions->order_by('transaction_id', 'desc');
                                        $transactions->change_type('amount','price','0',array('prefix'=>'$'));
                                        $transactions->change_type('funds_received_YN','select','N','N, Y');
                                        $transactions->change_type('payment_method','select','CC','CC, DD, BP');

                                        $transactions->relation('user_id', 'guests', 'user_id', array('firstname', 'lastname'), 'primary_contact_YN = "Y"');
//                                        $transactions->relation('user_id', 'users', 'user_id', 'user_id');
                                        $transactions->relation('booking_id', 'bookings', 'booking_id', 'booking_id');
                                        $transactions->validation_required('user_id, boking_id');

                                        $transactions->disabled('user_id, booking_id', 'edit');

                                        $transactions->label('funds_received_YN', 'Received');
                                        $transactions->label('user_id', 'Primary Guest');
//                                        $transactions->benchmark(false);


                                        echo $transactions->render();

                                        ?>

                                    </div>
                                </div>
                            </div>
                            <!-- END CONTENT BODY -->
                            <!-- END CONTENT -->
                            <!-- BEGIN QUICK SIDEBAR -->
                        </div>
                        <!-- END CONTAINER -->
                        <!-- BEGIN FOOTER -->
                        <div class="page-footer">
                            <div class="scroll-to-top">
                                <i class="icon-arrow-up"></i>
                            </div>
                        </div>
                    </div>
                    <!-- END FOOTER -->
                    <!--[if lt IE 9]>
                    <script src="/includes/assets/global/plugins/respond.min.js"></script>
                    <script src="/includes/assets/global/plugins/excanvas.min.js"></script>
                    <![endif]-->
                    <!-- BEGIN CORE PLUGINS -->
<!--                    <script src="--><?php //echo $path; ?><!--/includes/assets/global/plugins/jquery.min.js"-->
<!--                            type="text/javascript"></script>-->
                    <script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js"
                            type="text/javascript"></script>
                    <script
                        src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
                        type="text/javascript"></script>
                    <script
                        src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
                        type="text/javascript"></script>
                    <script src="../includes/assets/global/plugins/jquery.blockui.min.js"
                            type="text/javascript"></script>
                    <script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js"
                            type="text/javascript"></script>
                    <script
                        src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
                        type="text/javascript"></script>
                    <!-- END CORE PLUGINS -->
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN THEME GLOBAL SCRIPTS -->
<!--                    <script src="--><?php //echo $path; ?><!--/includes/assets/global/scripts/app.min.js"-->
<!--                            type="text/javascript"></script>-->
                    <!-- END THEME GLOBAL SCRIPTS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script src="../includes/assets/pages/scripts/form-samples.min.js"
                            type="text/javascript"></script>
                    <!-- END PAGE LEVEL SCRIPTS -->
                    <!-- BEGIN THEME LAYOUT SCRIPTS -->
<!--                    <script src="--><?php //echo $path; ?><!--/includes/assets/layouts/layout/scripts/layout.min.js"-->
<!--                            type="text/javascript"></script>-->
<!--                    <script src="--><?php //echo $path; ?><!--/includes/assets/layouts/layout/scripts/demo.min.js"-->
<!--                            type="text/javascript"></script>-->
<!--                    <script src="--><?php //echo $path; ?><!--/includes/assets/layouts/global/scripts/quick-sidebar.min.js"-->
<!--                            type="text/javascript"></script>-->


                    <script src="../includes/js/jquery.dataTables.min.js"></script>

                    <script src="../includes/js/bootstrap/bootstrap.min.js"></script>
                    <script src="../includes/js/lib/utilities.js"></script>
                    <script src="../view/admin/bookings.js"></script>
                    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
