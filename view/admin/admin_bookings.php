<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>TravellingFit - Bookings</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>


    <link type="text/css" href="../includes/styles/bootstrap.css" rel="stylesheet"/>
    <link type="text/css" href="../includes/styles/bootstrap-glyphicons.css" rel="stylesheet"/>
    <link type="text/css" href="../includes/styles/jquery.dataTables.css" rel="stylesheet"/>
    <link type="text/css" href="../includes/styles/general.css" rel="stylesheet"/>
    <link type="text/css" href="../includes/styles/my_style.css" rel="stylesheet"/>
    <style type="text/css">
        .xcrud .xcrud-th th {
            vertical-align: top;
            white-space: normal;
        }

        .xcrud .xcrud-th th:nth-child(2) {
            white-space: nowrap;
        }
    </style>
</head>
<!-- END HEAD -->

<body class=" page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
    <!-- BEGIN HEADER INNER -->
    <div class="container">
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    Travelling Fit
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="http://67.225.210.86/~travellingfit/wp-login.php?action=logout"
                           class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="container">
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo BASEPATH; ?>">Travelling Fit</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Bookings</span>
                            </li>
                        </ul>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><i class="fa fa-th fa-2x"
                               style="padding-top:20px;margin-left:-15px;padding-right:50px;"></i></li>
                        <li class="active"><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_bookings'; ?>">Bookings</a>
                        </li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_transactions'; ?>">Transactions</a>
                        </li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_guests'; ?>">Guests</a></li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_packages'; ?>">Package
                                Inclusions</a></li>
                        <li><a href="<?php echo $_SERVER['SCRIPT_NAME'] . '?v=admin_form_values'; ?>">Booking Form
                                Values</a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <div class="col-md-12">
                                <div class="head-block">

                                </div>
                            </div>
                            <div class="portlet-body form new-table" style="display: block !important;">
                                <!-- BEGIN FORM-->
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <?php
                                        require_once '../includes/xcrud/xcrud/xcrud.php';
                                        $bookings = Xcrud::get_instance();
                                        $bookings->table('bookings');
                                        $bookings->table_name('Bookings');
                                        //                                $bookings->where("bookings.booking_id IN (SELECT DISTINCT booking_id FROM transactions)");
                                        $bookings->order_by('booking_id', 'desc');
                                        $bookings->subselect('primary_guest', 'SELECT lastname FROM guests WHERE booking_id = {booking_id} and primary_contact_YN = \'Y\' LIMIT 1'); // other table
                                        $bookings->label('primary_guest', 'Primary Guest');
                                        $bookings->relation('package_id', 'packages', 'package_id', 'name');
                                        $bookings->label('package_id', 'Package');
                                        $bookings->relation('travel_insurance_option_id', 'travel_insurance', 'travel_insurance', 'travel_insurance_name');
                                        $bookings->label('travel_insurance_option_id', 'Travel Insurance');
                                        $bookings->relation('bedding_configuration_id', 'bedding', 'bedding_id', 'bedding_name');
                                        $bookings->label('bedding_configuration_id', 'Bedding');
                                        $bookings->relation('how_hear_id', 'how_hear', 'how_hear_id', 'name');
                                        $bookings->change_type('share_request_YN', 'select', 'N', 'N, Y');
                                        $bookings->label('number_adult_guests', 'Number Adults');
                                        $bookings->label('number_children_guests', 'Number Children');

                                        $bookings->validation_pattern('number_adult_guests, number_children_guests, extra_nights_pre, extra_nights_post', 'numeric');
                                        $bookings->validation_required('bedding_configuration_id, package_id');

                                        $bookings->columns('booking_id,primary_guest,timestamp,total_paid,total_cost,bedding_configuration_id,package_id,travel_insurance_option_id, number_adult_guests,number_children_guests'); // columns in grid

                                        $bookings->fk_relation('Optional Tours', 'booking_id', 'booking_option_tours', 'booking_id', 'optional_tour_id', 'optional_tours', 'optional_tour_id', array('title'));

                                        $bookings->fields('total_cost,bedding_configuration_id,package_id,number_adult_guests,number_children_guests,extra_nights_pre,extra_nights_post,share_request_YN', false, 'Package');
                                        $bookings->fields('travel_insurance_option_id,how_hear_id,Optional Tours', false, 'Additional Information');

                                        $bookings->limit(10);

                                        $guests = $bookings->nested_table('guests', 'booking_id', 'guests', 'booking_id');
                                        $guests->columns('user_id, firstname,lastname,prefered_name,email,primary_contact_YN, vip_yn, gender,child_YN,dob', false, 'Guests - Details');
                                        $guests->validation_pattern('email', 'email');
                                        $guests->change_type('gender', 'select', 'M', 'M, F');
                                        $guests->change_type('child_YN', 'select', 'N', 'N, Y');
                                        $guests->change_type('primary_contact_YN', 'select', 'N', 'N, Y');
                                        $guests->label('primary_contact_YN', 'Primary Contact');
                                        $guests->label('child_YN', 'Child');
                                        $guests->label('address_1', 'Address');
                                        $guests->label('phone1_preferred', 'Phone');
                                        $guests->validation_pattern('email', 'email');
                                        $guests->fields('user_id, firstname,lastname,prefered_name,email,primary_contact_YN, vip_yn, gender,child_YN,dob, phone1_preferred, address_1, suburb, state, postcode, country', false);
                                        $guests->label('vip_yn', 'VIP');
                                        //                                $guests->benchmark(false);
                                        //
                                        //
                                        $transactions = $bookings->nested_table('transactions', 'booking_id', 'transactions', 'booking_id');
                                        $transactions->columns('transaction_id,booking_id,cardholders_name, amount, payment_method,merchant_trans_id,funds_received_YN, bpay_code, user_id, ts', false, 'Guests - Details');
                                        $transactions->change_type('amount', 'price', '0', array('prefix' => '$'));
                                        $transactions->change_type('funds_received_YN', 'select', 'N', 'N, Y');
                                        $transactions->change_type('payment_method', 'select', 'CC', 'CC, DD, BP');
                                        $transactions->fields('transaction_id,cardholders_name, amount, payment_method,merchant_trans_id,funds_received_YN, bpay_code, user_id, ts'); // columns in grid
                                        $transactions->label('merchant_trans_id', 'Merchant ID');
                                        // $transactions->relation('user_id', 'users', 'user_id', 'user_id');
                                        $transactions->relation('user_id', 'guests', 'user_id', array('firstname', 'lastname'), 'primary_contact_YN = "Y"');
                                        $transactions->label('user_id', 'Primary Guest');

                                        echo $bookings->render();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
        <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
           title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase
            Metronic!</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
</div>
<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="includes/assets/global/plugins/respond.min.js"></script>
<script src="includes/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<!--<script src="includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<!--<script src="includes/assets/global/scripts/app.min.js" type="text/javascript"></script>-->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../includes/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!--<script src="includes/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>-->
<!--<script src="includes/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>-->
<!--<script src="includes/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->


<script src="../includes/js/jquery.dataTables.min.js"></script>
<script src="../includes/js/bootstrap/bootstrap.min.js"></script>
<script src="../includes/js/lib/utilities.js"></script>
<script src="../view/admin/bookings.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
