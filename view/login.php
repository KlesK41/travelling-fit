<?php ob_start();
$path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (isset($_GET['token']) && !empty($_GET['token'])) {
    $user = Users::findByToken($_GET['token']);
    if (!empty($user)) {
      $activated = $user->activateUser();
    }
  }
}

?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>User Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../includes/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../includes/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="../includes/assets/pages/css/login-3.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class=" login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <!-- <a href="index.html">
        <img src="../assets/pages/img/logo-big.png" alt="" /> </a> -->
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="<?php echo $_SERVER['SCRIPT_NAME']?>?v=login" method="post">
            <h3 class="form-title">Login to your account</h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter any username and password. </span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="on" placeholder="Email" name="username" /> </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="1" /> Remember me </label>
                <button type="submit" class="btn green pull-right"> Login <i class="m-icon-swapright m-icon-white" name="login"></i>
                </button>
            </div>
            <?php if (isset($activated) && $activated) { ?>
              <p>Your account has been activated</p>
            <?php } ?>
            <?php if (isset($_SESSION['restored']) && $_SESSION['restored']) {?>
                <p>Your password has been restored, please check your email</p>
            <?php
                unset($_SESSION['restored']);
            } ?>
            <div class="space">
                <?php
                if(isset($_GET['i']))echo "Incorrect username and / or password.";
                ?>
            </div>
            <div class="login-options">
                <h4>Or login with</h4>
                <ul class="social-icons">
                    <li>
                        <?php
                          $fb = new Facebook\Facebook([
                              'app_id' => '572973799518112',
                              'app_secret' => '6e6002d3b8a154e5558296edc08c6f71',
                              'default_graph_version' => 'v2.5',
                              //'default_access_token' => '644988485583207|8Wn_JWx_VefAVwOdKTK-_ZtN2Yo'
                          ]);

                          $helper = $fb->getRedirectLoginHelper();
                          $permissions = ['email']; // optional
                          $path = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
                          $loginUrl = $helper->getLoginUrl('http://' . $_SERVER['HTTP_HOST'] . $path . 'login-callback.php', $permissions);
                          // var_dump($loginUrl);
                        ?>
                        <a class="facebook" data-original-title="facebook" href="<?php echo $loginUrl; ?>"> </a>
                    </li>
                    <!-- <li>
                        <a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a>
                    </li> -->
                </ul>
            </div>

            <div class="forget-password">
                <h4>Forgot your password ?</h4>
                <p> no worries, click
                    <a href="javascript:;" id="forget-password"> here </a> to reset your password. </p>
            </div>

            <div class="create-account">
                <p> Don't have an account yet ?
                    <a href="<?php echo $_SERVER['SCRIPT_NAME']?>?v=sign_up" id="register"> Create an account </a>
                </p>
            </div>
        </form>

        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="<?php echo $_SERVER['SCRIPT_NAME']?>?v=login" method="post">
            <h3>Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="restore_email" required/> </div>
            </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn grey-salsa btn-outline"> Back </button>
                <button type="submit" class="btn green pull-right"> Submit </button>
            </div>
        </form>
        <!-- END FORGOT PASSWORD FORM -->
    </div>
    <!-- END LOGIN -->
    <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="../includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../includes/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="../includes/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="../includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../includes/assets/pages/scripts/login.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>

<?php

if(!empty($_POST))
{
    if(!empty($_POST['username']) && !empty($_POST['password'])) {

        $foundUser = Users::authenticate($_POST['username'], $_POST['password']);

        // User already enroled in system
        if ($foundUser && $foundUser->status == 'enabled') 
        {

            $session->login($foundUser);
        // var_dump($_SESSION);exit();
            if (isset($_SESSION['redirect'])) {
                $location = "Location: ". $_SESSION['redirect'];
                unset($_SESSION['redirect']);
            } else {
                //$location = "Location: index.php?v=package&s=99";
              if ($_SESSION['generic_is_admin']) {
                $location = "Location: " . $_SERVER['SCRIPT_NAME'] .  "?v=admin_packages";
              } else {
                $location = "Location: " . $_SERVER['SCRIPT_NAME'] . "?v=my_booking";
              }
                
            }
        }
        else
        {
            // User not in system or incorrect Password
            $location = "Location: " . $_SERVER['SCRIPT_NAME'] . "?v=login&i=1";
        }

        header($location);
    } elseif (isset($_POST['restore_email']) && !empty($_POST['restore_email'])) {
        $email = $_POST['restore_email'];

        $user = Users::findByEmail($email);

        if (!empty($user)) {
            $user = $user[0];
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 8; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            $password = crypt(implode($pass), $email);
//            var_dump($password);exit;
            $user->password = $password;

            if ($user->restorePassword()) {
                $to = $user->email;
                $subject = 'Restore password on Travelling Fit';
                $message = 'You new password is: ' . $user->password;
                if (mail($to, $subject, $message)) {
                    $_SESSION['restored'] = true;
                }
            }
        }
        $location = "Location: " . $_SERVER['SCRIPT_NAME'] . "?v=login";
        header($location);
    }

}

ob_flush();

?>