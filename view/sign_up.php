<?php 
	if (isset($_POST) && !empty($_POST)) {
		$email = trim(htmlspecialchars($_POST['email']));
		$password = trim(htmlspecialchars($_POST['password']));
		$confirm_password = trim(htmlspecialchars($_POST['confirm_password']));
		$error = array();


		$email_reg_exp = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
		if (!preg_match($email_reg_exp, $email)) {
			$error['email'] = 'Incorrect email';
		} else {
			$user = Users::findByEmail($email);
			if (!empty($user)) {
				$error['email'] = 'Email already exists';
			}
		}
		if (strlen($password) < 6) {
			$error['password'] = 'Password must be greater than 6 symbols';
		}
		if ($password !== $confirm_password) {
			$error['compare'] = 'Passwords must be compared';
		}

		if (empty($error)) {
			$token = sha1($email . $password . time());
			$user = new Users;
			$user->email = $email;
			$user->password = crypt($password, $email);
			// $user->password = $password;

			// $user->status = 'enabled';

			$user->status = 'disabled';
			$user->token = $token;
			$user->access_level = 2;
			if ($user_id = $user->createUser()) {
				$to = $user->email;
				$subject = 'Confirm registration on travellingfit';
				$message = 'To complete your registration please go for a link ' . $_SERVER['HOST'] . $_SERVER['SCRIPT_NAME'] . '?v=login&token=' . $token;
				if (mail($to, $subject, $message)) {
					$complete = true;
				} 

			}
		}
		
	}

	$path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Client registration</title>
        <!-- Bootstrap -->
    	<link href="../includes/styles/bootstrap.min.css" rel="stylesheet">
    	<link href="../includes/styles/font-awesome.css" rel="stylesheet">
        <link href='../includes/styles/style.css' rel='stylesheet' type='text/css'>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
      	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->  
    </head>
    <body>
    	<?php if (!isset($complete)) { ?>
    	<div class="wrapper">
    		<div class="content">
    			<form class="registration-form" action="" method="post">
    				<?php
                          $fb = new Facebook\Facebook([
                              'app_id' => '572973799518112',
                              'app_secret' => '6e6002d3b8a154e5558296edc08c6f71',
                              'default_graph_version' => 'v2.5',
                              //'default_access_token' => '644988485583207|8Wn_JWx_VefAVwOdKTK-_ZtN2Yo'
                          ]);

                          $helper = $fb->getRedirectLoginHelper();
                          $permissions = ['email']; // optional
                          $path = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
                          $loginUrl = $helper->getLoginUrl('http://' . $_SERVER['HTTP_HOST'] . $path . 'login-callback.php', $permissions);
                          // var_dump($loginUrl);
                        ?>
    				<a href="<?php echo $loginUrl;?>"><button type="button" class="btn social social-fb">Sign up with Facebook</button></a>
    				<!-- <button type="button" class="btn social social-gp">Sign up with Google+</button> -->
    				<div class="input-group">    					
    					<input type="e-text" class="form-controll" name="email" placeholder="E-mail">   					
    				</div>
    				<div>
    					<?php if (isset($error['email'])) echo $error['email']; ?>	
    				</div>
    				<div class="input-group">    					
    					<input type="password" class="form-controll" placeholder="Password" name="password">
    				</div>
    				<div>
    					<?php if (isset($error['password'])) echo $error['password']; ?>	
    				</div>
    				<div class="input-group">    					
    					<input type="password" class="form-controll" placeholder="Re-type Your Password" name="confirm_password">
    				</div>
    				<div>
    					<?php if (isset($error['compare'])) echo $error['compare']; ?>	
    				</div>
    				<button type="button" id="return_to_login" class="btn default"><i class="fa fa-arrow-circle-o-left"></i> Back</button>
    				<button type="submit" class="btn green">Sign Up <i class="fa fa-arrow-circle-o-right"></i></button>
    			</form>
    		</div>
    	</div>



    	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    	<!-- Include all compiled plugins (below), or include individual files as needed -->
    	<script src="../includes/js/bootstrap.min.js"></script>
    	<?php } elseif (isset($complete) && $complete) { ?>

			<p>
				Please check your email inbox for a verification email from Travelling Fit.
			</p>
			<p>
				If you cannot see it in the inbox, please check your spam folder.
			</p>
			<p>
				Click on the verification link provided in the email to proceed.
			</p>

			<script>
				setTimeout(function() {
					document.location = '<?php echo $_SERVER['SCRIPT_NAME']?>' + '?v=login';	
				}, 6000)
			</script>
		<?php } ?>
		<script>
			$(document).ready(function() {
				$('#return_to_login').on("click", function() {
					document.location = '<?php echo $_SERVER['SCRIPT_NAME']?>' + '?v=login';
				})
			}) 
		</script>
    </body>
    </html>

