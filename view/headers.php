<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Document Builder</title>
<link rel="stylesheet" href="includes/theme/jquery.ui.all.css">
<!--<link type="text/css" href="includes/theme/demos.css" rel="stylesheet" />
<link type="text/css" href="includes/css/screen.css" rel="stylesheet" />-->
<script src="includes/js/jquery-1.4.2.js"></script>
<script src="includes/js/jquery.tooltip.js"></script>
<script src="includes/js/jquery.bgiframe.js"></script>
<script src="includes/js/jquery.dimensions.js"></script>
<script src="includes/js/chili-1.7.pack.js"></script>
<script src="includes/js/jquery.validate.js"></script>
<script src="includes/js/ui/jquery.ui.core.js"></script>
<script src="includes/js/ui/jquery.ui.widget.js"></script>
<script src="includes/js/ui/jquery.ui.mouse.js"></script>
<script src="includes/js/ui/jquery.ui.button.js"></script>
<!--<script src="includes/js/ui/jquery.ui.datepicker.js"></script>
    <script src="includes/js/ui/jquery.ui.slider.js"></script>
	<script src="includes/js/ui/jquery.ui.accordion.js"></script>
    <script src="includes/js/jquery.confirm-1.3.js"></script> !-->
<script src="includes/js/jHtmlArea-0.7.0.js"></script>
<link rel="Stylesheet" type="text/css" href="includes/styles/jHtmlArea.css" />
<link type="text/css" href="includes/styles/jquery.tooltip.css" rel="stylesheet" />
<link type="text/css" href="includes/styles/builder.css" rel="stylesheet" />

<!-- <link type="text/css" href="includes/styles/<?php //echo $_SESSION['generic_document_id'] ?>.css" rel="stylesheet" /> -->
<link rel="stylesheet" type="text/css" media="print" href="includes/styles/print.css"/>
<script src="includes/js/jquery.populate.js"></script>
<script>
$(function() {
		$('#instructions').click(function() {
		$('#help').fadeIn(300);
		return false;
	});
	
	$('#help').click(function() {
		$('#help').fadeOut(300);
	}); 
	
	var viewport = $(window).height();
	$("#help").css({height: viewport});
});
</script>
</head>
<body>
<?php
function headerTextStudent($documentReadyCode)
{
?>
<!--<div id="header">
    <p>Logged in as: <span><?php //echo $_SESSION['generic_currentUser'] ?>, STUDENT</span></p>
</div>-->

<?php
}
?>
<?php
function headerTextAdmin($documentReadyCode)
{
?>
<?php
}

function headerLogin($documentReadyCode)
{
?>

<!--<div id="header">
</div>
<div id="site">-->

<?php
}
?>
<div id="help">&nbsp;</div>
<!-- <img src="images/instructions.gif" alt="" class="alt" /> !-->
<div id="site">
