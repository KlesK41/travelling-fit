<?php

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $packages = Packages::getAllForEvent($_GET['id']);
    $roomTypes = Room_type::getAllForEvent($_GET['id']);
//    var_dump($roomTypes);exit;
    $file = str_replace(__DIR__, '', __FILE__);
    require_once('view/general' . $file);
} else {
    echo "There is no events";
}
