<?php
if (!isset($_SESSION['generic_currentUser'])) {
    $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
    functions::redirectTo("index.php?v=login");
}

//var_dump($_SESSION);

$bookings = Bookings::getAllForUser($_SESSION['generic_user_id']);

//var_dump($bookings);

if (!empty($bookings)) {
    foreach ($bookings as $booking) {
        if (($booking['paid'] == 'Y' && $booking['completed'] == 'Y') || time() > strtotime($booking['end_date'])) {
            $pastBookings[] = $booking;
        } elseif ($booking['paid'] == 'N' || $booking['paid'] == null || $booking['paid'] == 'P') {
            $currentBookings[] = $booking;
        }
    }
}





$file = str_replace(__DIR__, '', __FILE__);
require_once('../view/general' . $file);