<?php

if (!isset($_SESSION['generic_currentUser'])) {
    $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
    functions::redirectTo("index.php?v=login");
}

if (isset($_GET['booking_id']) && is_numeric($_GET['booking_id'])) {
    if (isset($_SESSION['generic_user_id']) && is_numeric($_SESSION['generic_user_id'])) {
        $booking = new Bookings();
        $booking->booking_id = $_GET['booking_id'];
        $booking->user_id = $_SESSION['generic_user_id'];
        $booking->archieved_yn = 'Y';

        if ($booking->archieveBooking()) {
            $location = $_SERVER['SCRIPT_NAME'] . "?v=my_booking";
            functions::redirectTo($location);
            functions::redirectTo($location);
        }
    }
} else {
    die("Page not found");
}