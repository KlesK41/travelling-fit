<?php
session_start();
require_once("../../includes/initialize.php");
$database = new DatabaseObject();
/*
 * Script:    DataTables server-side script for PHP and Oracle
 * Description: A re-write of 'Allan Jardine's server-side script for PHP and mySQL'
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

// stop direct access of this script	
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
}
else
{
	die('This page cannot be accessed directly.');
}


// Error reporting
error_reporting(1);
// Array of database columns which should be read and sent back to DataTables. 
 
$aColumns = array('user_id', 'username', 'firstname', 'lastname', 'al.access_level', 'user_notes', 'active_yn');

// database details
require_once("../../model/VO/users.php"); // for user access restriction

/* 
 * Paging
 */
$sLimit = "";
if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".$database->escapeValue( $_GET['iDisplayStart'] ).", ".
		$database->escapeValue( $_GET['iDisplayLength'] ); // for mySQL

	$sUpperLimit = $database->escapeValue( $_GET['iDisplayLength'] ); // oracle
	$sLowerLimit = $database->escapeValue( $_GET['iDisplayStart'] ) + 1; // oracle
	$sUpperLimit += $sLowerLimit - 1;
}


/*
 * Ordering
 */
if ( isset( $_GET['iSortCol_0'] ) )
{
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
	{
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
		{
			$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
			 	".$database->escapeValue( $_GET['sSortDir_'.$i] ) .", ";
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if ( $sOrder == "ORDER BY" )
	{
		$sOrder = "";
	}
}

$sWhere = '';
/* 
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */

if ( $_GET['sSearch'] != "" )
{
	$sWhere .= "AND (";
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		$sWhere .= "lower(" . $aColumns[$i].") LIKE lower('%".$database->escapeValue( $_GET['sSearch'] )."%') OR ";
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
}

/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ )

{
	if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
	{
		if ( $sWhere == "" )
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		$sWhere .= $aColumns[$i]." LIKE '%".$database->escapeValue($_GET['sSearch_'.$i])."%' ";
	}
}

if($sWhere == "AND") $sWhere = "";


$sQuery =  "select * from 
			  ( select a.*, ROWNUM rnum from 
			    (
			    	SELECT u.user_id, u.username, u.firstname, u.lastname, al.user_type as access_level, u.user_notes, active_yn
					FROM NB_USERS u, NB_ACCESS_LEVELS al
					WHERE u.access_level = al.access_level 
			  		$sWhere
					$sOrder
			   ) a 
			  where ROWNUM <= $sUpperLimit )
			where rnum  >= $sLowerLimit";

$result_set = $database->query($sQuery);

$sQuery = 	"
			select nvl(max(rownum), 0) filtered_count
			from
			(
				SELECT u.user_id
				FROM NB_USERS u, NB_ACCESS_LEVELS al
				WHERE u.access_level = al.access_level 
				$sWhere
				$sOrder
			)
			";


// Data set length after filtering
$rResultFilterTotal = $database->query($sQuery);
$aResultFilterTotal =  $database->fetchArray($rResultFilterTotal);
$iTotal = $iFilteredTotal = $aResultFilterTotal[0];

// total result count
$sQuery = 	"
			SELECT COUNT(u.user_id) AS total_count 
			FROM NB_USERS u, NB_ACCESS_LEVELS al
			WHERE u.access_level = al.access_level 
			";


// Total data set length
$rResultTotal = $database->query($sQuery);
$aResultTotal =  $database->fetchArray($rResultTotal);
$iTotal = $aResultTotal[0];


/*
 * Output
 */
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

while ($aRow = $database->fetchArray($result_set)) 
{

	$row = array();
	// edit  clone  assign user  delete
	$actions = '<input type="checkbox" name="user_id[]" value="' . $aRow[ 'USER_ID' ] .  '">';
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		/* General output */
		$row[] = $aRow[ $i ];
	}

	$row[] = $actions;

	$output['aaData'][] = $row;
}

echo json_encode( $output );
?>
