<?php
require_once("../includes/functions.php");
require_once("../includes/SessionHandler.php");
require_once("../includes/Template.php");
require_once("../includes/Paginator.php");


// database type - MySQL or Oracle
require_once("../includes/definitions.php");
defined('DATABASE_TYPE') ? null : define ('DATABASE_TYPE', 'MySQLDatabase');

// load database connection credentials
require_once("../includes/config.php");

// load basic functions next so that everything after can use them
// require_once("../includes/definitions.php");
require_once("../includes/initialize.php");
error_reporting(0);

// include all value objects
foreach(glob("../model/VO/*.php") as $filename) require_once $filename;

// include all business objects
foreach(glob("../model/business/*.php") as $filename) require_once $filename;


/*********************************************
 *
 */

// get the type of VO we are using to add data
$method		= $_POST['method'];

switch ($method)
{

	case 'clearSession':

		if(session_destroy())
		{
			echo json_encode("S");
		}

		break;

	case 'getFingerPrint':
		// print_r($_SESSION);
		// die();
		// safeguard against insert the same transaction more than once...


		if(trim(Payment::is_desosit_paid()) == "N" && (floatval($_POST['amount']) < floatval(DEPOSIT_AMOUNT)))
		{
			echo json_encode('NOT_ENOUGH_FOR_DEPOSIT');
			return false;
		}



		// if(isset($_SESSION['thank']) && ($_SESSION['thank'] != 'you'))
		// {
		$_SESSION['cardholders_name'] = $_POST['cardholders_name'];
		$_SESSION['client_payment_amount_enter'] = $_POST['client_payment_amount_enter'];
		$_SESSION['amount_with_fee'] = $_POST['amount'];

		$transaction = new Transactions;
		$transaction->booking_id = $_SESSION['booking_id'];
		$transaction->payment_method = 'CC'; // credit card
		$transaction->merchant_trans_id = 'CC'; // credit card
		$transaction->fund_received_YN = 'N'; // received payment
		$transaction->amount = $_POST['client_payment_amount_enter'];
		$transaction->cardholders_name = $_POST['cardholders_name'];
		$transaction->amount_with_fee = $_POST['amount'];
		$transaction->comments = "";
		$transaction->archived_YN = "N";

		$sql = "SELECT user_id FROM guests WHERE user = " . $_SESSION['generic_user_id'] . " AND booking_id = " . $_SESSION['booking_id'] . " AND primary_contact_YN = 'Y'";
		$guest = Guests::findBySql($sql);

//			$transaction->user_id = $_SESSION['generic_user_id'];

		$transaction->user_id = $guest[0]->user_id;

		if($transactionId = $transaction->create())
		{
			$booking = Bookings::getById($_SESSION['booking_id']);
			$booking->paid = ($_POST['payment_amount'] == $_POST['amount_due']) ? 'Y' : 'P';
			$booking->total_paid = $booking->total_paid + $_POST['payment_amount'];
			// $booking->total_paid = null;
			$booking->updateBookingPaidStatus();

			$_SESSION['thank'] = "you";
			$_SESSION['transaction_id'] = $transaction->transaction_id;
			$_SESSION['ref_id'] =  $_POST['primary_guest_last_name'] . "#" . $_SESSION['booking_id'] ;
		}
		// }

		$timestamp=gmdate('YmdHis', time());

		// live url - uncomment and comment the test url once approved
//		$url = "https://transact.nab.com.au/live/directpost/genfingerprint";
//		$EPS_MERCHANT = "YNX0010";
//		$EPS_PASSWORD = "j9mdhFG8";

		// test url
		$url = "https://transact.nab.com.au/test/directpost/genfingerprint";
		$EPS_MERCHANT = "YNX0010";
		$EPS_PASSWORD = "7JqQKTZa";

		// merchant details are in: EPS_MERCHANT and EPS_PASSWORD

		$fields = array(	'EPS_MERCHANT'=>urlencode($EPS_MERCHANT),
			'EPS_PASSWORD'=>urlencode($EPS_PASSWORD),
			'EPS_AMOUNT'=>urlencode(number_format((float)$_POST['amount'], 2, '.', '')),
			'EPS_REFERENCEID'=>urlencode($_SESSION['transaction_id']),
			'EPS_TIMESTAMP'=>urlencode($timestamp)	);
		$fields_string = "";



		foreach($fields as $key=>$value) {
			$fields_string .=
				$key.'='.$value.'&';
		}
		rtrim($fields_string,'&');

//		var_dump($fields_string);

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// execute post
		$fingerPrint = curl_exec($ch);
		$_SESSION['finger_print'] = $fingerPrint;
		$_SESSION['timestamp'] = $timestamp;
		// close connection
		// dump($_SESSION['finger_print']);
		// dump($_SESSION['timestamp']);

//		var_dump($fingerPrint);exit;

		curl_close($ch);
		if(strlen($_SESSION['finger_print']) > 0)
		{
			echo json_encode(array(
				"message" => "S",
				"transaction_id" => $_SESSION['transaction_id'],
				"amount" => urlencode(number_format((float)$_POST['amount'], 2, '.', '')),
				"finger_print" => $_SESSION['finger_print'],
				"timestamp" => $_SESSION['timestamp'],
				"tranactionId" => $transactionId,
				"date" => date("d/m/Y", time()),
				"booking" => $_SESSION['booking_id'],
				"user" => $transaction->user_id,
			));
			exit;
		}



		echo json_encode(array(
			"tranactionId" => $transactionId,
			"date" => date("d/m/Y", time()),
			"booking" => $_SESSION['booking_id'],
			"user" => $transaction->user_id
		));

		break;

	case 'getFingerPrintManualPayment':

		// if(isset($_SESSION['thank']) && ($_SESSION['thank'] != 'you'))
		// {
		$_SESSION['cardholders_name'] = $_POST['cardholders_name'];
		$_SESSION['client_payment_amount_enter'] = $_POST['client_payment_amount_enter'];
		$_SESSION['amount_with_fee'] = $_POST['amount'];
		$_SESSION['booking_id'] = $_POST['booking_id'];

		$transaction = new Transactions;
		$transaction->booking_id =
		$transaction->payment_method = 'CC'; // credit card
		$transaction->merchant_trans_id = 'CC'; // credit card
		$transaction->fund_received_YN = 'N'; // received payment
		$transaction->amount = $_POST['client_payment_amount_enter'];
		$transaction->cardholders_name = $_POST['cardholders_name'];
		$transaction->amount_with_fee = $_POST['amount'];
		$transaction->comments = "Email: " . $_POST['email'] . ", Surname: " . $_POST['surname'];
		$transaction->archived_YN = "N";
		if($transaction->create())
		{
			$_SESSION['thank'] = "you";
			$_SESSION['transaction_id'] = $transaction->transaction_id;
			$_SESSION['ref_id'] =  $_POST['surname'] . "#" . $_POST['booking_id'];
		}
		// }

		$timestamp=gmdate('YmdHis', time());

		// live url - uncomment and comment the test url once approved
//		$url = "https://transact.nab.com.au/live/directpost/genfingerprint";
//		$EPS_MERCHANT = "YNX0010";
//		$EPS_PASSWORD = "j9mdhFG8";


		// test url
		 $url = "https://transact.nab.com.au/test/directpost/genfingerprint";
		 $EPS_MERCHANT = "YNX0010";
		 $EPS_PASSWORD = "1N6GMluI";

		// merchant details are in: EPS_MERCHANT and EPS_PASSWORD

		$fields = array(	'EPS_MERCHANT'=>urlencode($EPS_MERCHANT),
			'EPS_PASSWORD'=>urlencode($EPS_PASSWORD),
			'EPS_AMOUNT'=>urlencode(number_format((float)$_POST['amount'], 2, '.', '')),
			'EPS_REFERENCEID'=>urlencode($_SESSION['transaction_id']),
			'EPS_TIMESTAMP'=>urlencode($timestamp)	);
		$fields_string = "";
		foreach($fields as $key=>$value) {
			$fields_string .=
				$key.'='.$value.'&';
		}
		rtrim($fields_string,'&');

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// execute post
		$fingerPrint = curl_exec($ch);
		$_SESSION['finger_print'] = $fingerPrint;
		$_SESSION['timestamp'] = $timestamp;
		// close connection
		// dump($_SESSION['finger_print']);
		// dump($_SESSION['timestamp']);

		curl_close($ch);
		if(strlen($_SESSION['finger_print']) > 0)
		{
			echo json_encode(array("message" => "S", "transaction_id" => $_SESSION['transaction_id'], "amount" => urlencode(number_format((float)$_POST['amount'], 2, '.', '')), "finger_print" => $_SESSION['finger_print'], "timestamp" => $_SESSION['timestamp']));
		}

		break;

	case 'getFingerPrintManualPayment2':

		// if(isset($_SESSION['thank']) && ($_SESSION['thank'] != 'you'))
		// {
		$_SESSION['cardholders_name'] = $_POST['cardholders_name'];
		$_SESSION['client_payment_amount_enter'] = $_POST['client_payment_amount_enter'];
		$_SESSION['amount_with_fee'] = $_POST['client_payment_amount_enter'];
		$_SESSION['booking_id'] = $_POST['booking_id'];



		$transaction = new Transactions;
		$transaction->booking_id =
		$transaction->payment_method = 'CC'; // credit card
		$transaction->merchant_trans_id = 'CC'; // credit card
		$transaction->fund_received_YN = 'N'; // received payment
		$transaction->amount = $_SESSION['amount_with_fee'];
		$transaction->cardholders_name = $_POST['cardholders_name'];
		$transaction->amount_with_fee = $_SESSION['amount_with_fee'];
		$transaction->comments = "Email: " . $_POST['email'] . ", Surname: " . $_POST['surname'];
		$transaction->archived_YN = "N";
		if($transaction->create())
		{
			$_SESSION['thank'] = "you";
			$_SESSION['transaction_id'] = $transaction->transaction_id;
			$_SESSION['ref_id'] =  $_POST['surname'] . "#" . $_POST['booking_id'];
		}
		// }

		$timestamp=gmdate('YmdHis', time());

		// live url - uncomment and comment the test url once approved
		$url = "https://transact.nab.com.au/live/directpost/genfingerprint";
		$EPS_MERCHANT = "YNX0010";
		$EPS_PASSWORD = "j9mdhFG8";

		// test url
		// force
		// $url = "https://transact.nab.com.au/test/directpost/genfingerprint";
		// $EPS_MERCHANT = "YNX0010";
		// $EPS_PASSWORD = "7JqQKTZa";

		// merchant details are in: EPS_MERCHANT and EPS_PASSWORD

		$fields = array(	'EPS_MERCHANT'=>urlencode($EPS_MERCHANT),
			'EPS_PASSWORD'=>urlencode($EPS_PASSWORD),
			'EPS_AMOUNT'=>urlencode(number_format((float)$_SESSION['amount_with_fee'], 2, '.', '')),
			'EPS_REFERENCEID'=>urlencode($_SESSION['transaction_id']),
			'EPS_TIMESTAMP'=>urlencode($timestamp)	);
		$fields_string = "";
		foreach($fields as $key=>$value) {
			$fields_string .=
				$key.'='.$value.'&';
		}
		rtrim($fields_string,'&');

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// execute post
		$fingerPrint = curl_exec($ch);
		$_SESSION['finger_print'] = $fingerPrint;
		$_SESSION['timestamp'] = $timestamp;
		// close connection
		// dump($_SESSION['finger_print']);
		// dump($_SESSION['timestamp']);

		curl_close($ch);
		if(strlen($_SESSION['finger_print']) > 0)
		{
			echo json_encode(array("message" => "S", "transaction_id" => $_SESSION['transaction_id'], "amount" => urlencode(number_format((float)$_SESSION['amount_with_fee'], 2, '.', '')), "finger_print" => $_SESSION['finger_print'], "timestamp" => $_SESSION['timestamp']));
		}
		else // NAB tracsact off line
		{
			echo json_encode(array("message" => "F"));
		}

		break;


	case 'processPayment':

		//-----deposit amount


		$_SESSION['thanks'] = "yes";
		$timestamp=gmdate('YmdHis', time());

		// live url - uncomment and comment the test url once approved
		$url = "https://transact.nab.com.au/live/directpost/genfingerprint";

		// test url
		// $url = "https://transact.nab.com.au/test/directpost/genfingerprint";

		// merchant details are in: EPS_MERCHANT and EPS_PASSWORD
		$fields = array('EPS_MERCHANT'=>urlencode("RKZ0010"),
			'EPS_PASSWORD'=>urlencode("j9mdhFG8"),
			'EPS_AMOUNT'=>urlencode($_SESSION['total_cost']),
			'EPS_REFERENCEID'=>urlencode($_SESSION['booking_id']),
			'EPS_TIMESTAMP'=>urlencode($timestamp));
		foreach($fields as $key=>$value) {
			$fields_string .=
				$key.'='.$value.'&';
		}
		rtrim($fields_string,'&');

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// execute post
		$fingerPrint = curl_exec($ch);
		$_SESSION['finger_print'] = $fingerPrint;
		$_SESSION['timestamp'] = $timestamp;
		// close connection
		curl_close($ch);


		break;

	case 'processPaymentManual':

		//-----deposit amount


		$_SESSION['thanks'] = "yes";
		$timestamp=gmdate('YmdHis', time());

		// live url - uncomment and comment the test url once approved
		$url = "https://transact.nab.com.au/live/directpost/genfingerprint";

		// test url // force
		// $url = "https://transact.nab.com.au/test/directpost/genfingerprint";

		// merchant details are in: EPS_MERCHANT and EPS_PASSWORD
		$fields = array('EPS_MERCHANT'=>urlencode("RKZ0010"),
			'EPS_PASSWORD'=>urlencode("j9mdhFG8"),
			'EPS_AMOUNT'=>urlencode($_SESSION['total_cost']),
			'EPS_REFERENCEID'=>urlencode($_SESSION['booking_id']),
			'EPS_TIMESTAMP'=>urlencode($timestamp));
		foreach($fields as $key=>$value) {
			$fields_string .=
				$key.'='.$value.'&';
		}
		rtrim($fields_string,'&');

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// execute post
		$fingerPrint = curl_exec($ch);
		$_SESSION['finger_print'] = $fingerPrint;
		$_SESSION['timestamp'] = $timestamp;
		// close connection
		curl_close($ch);


		break;

	case 'savePaymentMethod':



		// transaction_id;
		// amount;
		// payment_method;
		// ts;
		// merchant_trans_id;
		// booking_id;
		// archived_yn;


		break;

	case 'savePackageFormInputs':
		$params = array();
		parse_str($_POST['post_data'], $params);
		foreach($params as $key => $value)
		{
			$_SESSION[$key] = $value;
		}

		$_SESSION['number_guests'] = (int)$_SESSION['number_adults'] +  (int)$_SESSION['number_children'];

		echo json_encode("S");
		break;

	case 'getSavedPackageForm':
		$sessionsArray = "";
		if(isset($_SESSION['package_id'])){
			$returnSession = array();
			foreach($_SESSION as $key => $value)
			{
				$returnSession[$key] = $value;
			}

			echo json_encode($returnSession);
		}

		break;

	case 'getPackages':
		$result = Packages::getAllActive();
		echo json_encode($result);
		break;




	case 'saveGuestFormInputs':
		$guest_number = $_POST['guest_number'];
		$_SESSION['guest_data'][$guest_number] = $_POST['post_data'];
		echo json_encode("S");

		break;

	case 'getGuestFormInputs':
		$guest_number = $_POST['guest_number'];
		if($_SESSION['guest_data'][$guest_number])
		{
			echo json_encode($_SESSION['guest_data'][$guest_number]);
		}
		else
		{
			echo json_encode('value');
		}
		break;


	// additional form 2, 01/06/2015 -- SB
	case 'saveForm2':
		$_SESSION['form2'] = $_POST['post_data'];
		// $_SESSION['form2'] = $_POST['prefered_event_id'];
		echo json_encode("S");
		break;

	case 'saveAdditionalInformationForm':

		$_SESSION['additional_information'] = $_POST['post_data'];
		// dump($_SESSION['additional_information']);
		echo json_encode("S");
		break;

	case 'getSavedAdditionalInformationForm':
		echo json_encode($_SESSION['additional_information']);
		break;


	case 'saveOptionalToursForm':
		$_SESSION['optional_tours'] = $_POST['post_data'];
		echo json_encode("S");
		break;

	case 'getSavedOptionalTours':

		unset($_SESSION['optional_tours']['number_guests']);
		echo json_encode($_SESSION['optional_tours']);
		break;


	case 'storeBooking':
		if(isset($_SESSION['booking_id']))
		{

		}
		else
		{

		}

		break;


	case 'getAccommodation':

		if(isset($_POST['package_id']))
		{
			$result = Accommodation::getAccommodation($_POST['number_guests'],   $_POST['package_id']);
		}
		else
		{
			$result = 'F';
		}

		echo json_encode($result);

		break;


	case 'getBedding':

		if(strlen($_POST['accommodation_id'] > 0))
		{
			$result = Bedding::getByAccommodationId($_POST['accommodation_id'],$_POST['package_id']);
		}
		else
		{
			$result = 'F';
		}

		echo json_encode($result);

		break;


	case 'getRoomType':

		if(strlen($_POST['accommodation_id'] > 0))
		{
			$result = Room_type::getByAccommodationId($_POST['accommodation_id']);
		}
		else
		{
			$result = 'F';
		}

		echo json_encode($result);

		break;

	case 'getEvents':
		$result = Events::getEventsByChildOrAdult($_POST['child_or_adult']);
		echo json_encode($result);

		break;


	case 'getTshirtSizes':
		$result = Tshirts::findBySql("SELECT * FROM tshirts WHERE available_YN = 'Y' AND type = '" . $_POST['type'] . "' AND archived_YN <> 'Y' ORDER BY sort_order ASC");
		echo json_encode($result);
		break;



	case 'getInternationalTravelPartners':
		$result = Travel_partner::findBySql("SELECT * FROM travel_partner WHERE available_YN = 'Y' AND archived_YN <> 'Y' ORDER BY sort_order ASC");
		echo json_encode($result);

		break;






	// case 'storePersonalDataForm':
	// 	$guests = new Guests;
	// 	$guests->processPostData($_POST['post_data']);
	// 	$result  = $guests->create();
	// 	echo json_encode($result);
	// break;

	case 'getTitlesDescriptions':
		$result = Optional_tours::findBySql("select * from optional_tours ot, optional_tour_package otp
											 where ot.available_YN = 'Y' and ot.optional_tour_id = otp.optional_tour_id
											 and otp.package_id = " . $_SESSION['package_id'] . "
											 order by ot.sort_order asc");
		echo json_encode($result);
		break;

	case 'getHearAboutUs':
		$result = How_hear::findBySql("select * from how_hear where available_YN = 'Y' order by sort_order asc");
		echo json_encode($result);
		break;


	/**************************************************
	 *				ACCESS LEVELS
	 **************************************************/
	case 'getUserTypes':
		if(Users::isAdmin())
		{
			$userTypes = Access_levels::findAll();
		}
		else
		{
			$userTypes = Access_levels::findBySql('SELECT * FROM access_levels WHERE access_level < ' . ADMIN);
		}

		echo json_encode($userTypes);
		break;

	case 'getUserType':
		switch($_SESSION['generic_access_level'])
		{
			case ADMIN:
				echo json_encode("ADMIN");
				break;

			case EDITOR:
				echo json_encode("EDITOR");
				break;

			case GENERAL:
				echo json_encode("GENERAL");
				break;

			default:
				echo json_encode("UNDEFINED");
				break;
		}
		break;

	/**************************************************
	 *				USERS
	 **************************************************/

	case 'addUser':

		$user 	= new Users();
		$message = $user->addUser($_POST['guid'], $_POST['access_level']);

		echo json_encode($message);

		break;

	case 'deleteUsers':
		foreach ($_POST['user_id'] as $key => $user_id)
		{
			Users::deleteByUserId($user_id);
		}

		echo json_encode("S");
		break;

	case 'assignUser':
		$user 	= new Users();
		$message = $user->assignUser($_POST['guid'], $_POST['document_id']);

		echo json_encode($message);

		break;


	case 'getDataForEvent':
		$result = array();

		$packages = Packages::getAllForEvent($_POST['event_id']);
		Template::renderPackagesList($packages);
		$result['packagesTemplate'] = Template::getTemplate();
		$result['packages'] = Packages::preparePackagesToJSON($packages);

		$roomTypes = Room_type::getAllForEvent($_POST['event_id']);
		Template::renderRoomTypesList($roomTypes);
		$result['roomTypesTemplate'] = Template::getTemplate();
		$result['roomTypes'] = Room_type::prepareRoomTypesToJSON($roomTypes);
		Template::renderRoomTypesForOptionalToursList($roomTypes);
		$result['roomTypesForOptionalToursTemplate'] = Template::getTemplate();
		$result['roomTypesForOptionalTours'] = Room_type::prepareRoomTypesToJSON($roomTypes, 'Y');

//		$optionalTours = Optional_tours::getAllForEvent($_POST['event_id']);
		$optionalTours = Optional_tours::getAllForEventAdmin($_POST['event_id']);
		Template::renderOptionalTourList($optionalTours);
		$result['optionalToursTemplate'] = Template::getTemplate();
		$result['optionalTours'] = Optional_tours::prepareOptionalToursToJSON($optionalTours);

		echo json_encode($result);

		break;

	//Packages for event start
	case 'addPackageToEvent':
		$result = array();
		$package = new Packages();
		$package->event_id = $_POST['event_id'];
		$package->name = $_POST['name'];
		$package->start_date = $_POST['start_date'];
		$package->end_date = $_POST['end_date'];
		$package->description = $_POST['description'];
		$package->available = $_POST['status'];
		if ($packageId = $package->add()) {
			$package->package_id = $packageId;
			Template::renderAddedPackage($package);
			$result['template'] = Template::getTemplate();
			$result['package'] = Packages::preparePackagesToJSON([$package]);
			echo json_encode($result);
		}

		break;
	case 'deletePackage':
		$package = new Packages();
		$package->package_id = $_POST['package_id'];
		if ($package->delete()) {
			$package->removeOptionalToursWithPackage();
			$roomType = new Room_type();
			$roomType->package_id = $_POST['package_id'];
			if ($roomType->deleteByPackageId()) {
				echo json_encode(true);
			}
		}
		break;
	case 'updatePackageForEvent':
		$result = array();
		$package = new Packages();
		$package->event_id = $_POST['event_id'];
		$package->package_id = $_POST['package_id'];
		$package->name = $_POST['name'];
		$package->start_date = $_POST['start_date'];
		$package->end_date = $_POST['end_date'];
		$package->description = $_POST['description'];
		$package->available = $_POST['status'];
		if ($package->update()) {
			Template::renderAddedPackage($package, 'update');
			$result['package'] = $package;
			$result['template'] = Template::getTemplate();
			echo json_encode($result);
		}
		break;
	// Packages for event end

	// Room types for event start
	case 'addRoomTypeForEvent':
		$result = array();
		$roomType = new Room_type();
		$roomType->package_id = $_POST['package_id'];
		$roomType->name = $_POST['name'];
		$roomType->price = $_POST['price'];
		$roomType->capacity = $_POST['capacity'];
		$roomType->status = $_POST['status'];
		$roomType->hotel_name = $_POST['hotel_name'];
		$roomType->event_id = $_POST['event_id'];
		$roomType->optional_tour = $_POST['optional_tour'];

		if ($roomTypeId = $roomType->add()) {
			$roomType->room_type_id = $roomTypeId;
			if ($_POST['optional_tour'] === 'N') {
				Template::renderAddedRoomType($roomType);
			} else {
				Template::renderAddedRoomTypeForOptionalTours($roomType);
			}
			$result['template'] = Template::getTemplate();
			$result['room_type'] = Room_type::prepareRoomTypesToJSON([$roomType], $roomType->optional_tour);


			echo json_encode($result);
		}

		break;
	case 'deleteRoomType':
		$roomType = new Room_type();
		$roomType->room_type_id = $_POST['room_type_id'];
		if ($roomType->delete()) {
			echo json_encode(true);
		}
		break;
	case 'updateRoomTypeForEvent':
		$result = array();
		$roomType = new Room_type();
		$roomType->room_type_id = $_POST['room_type_id'];
		$roomType->name = $_POST['name'];
		$roomType->price = $_POST['price'];
		$roomType->capacity = $_POST['capacity'];
		$roomType->status = $_POST['status'];
		$roomType->hotel_name = $_POST['hotel_name'];
		$roomType->event_id = $_POST['event_id'];
		$roomType->optional_tour = $_POST['optional_tour'];
		if ($roomType->update()) {
			if ($_POST['optional_tour'] == 'N') {
				Template::renderAddedRoomType($roomType, 'update');
			} else {
				Template::renderAddedRoomTypeForOptionalTours($roomType, 'update');
			}
			$result['roomType'] = $roomType;
			$result['template'] = Template::getTemplate();
			echo json_encode($result);
		}
		break;
	// Room types for event end

	//Optional tours for event start
	case 'addOptionalTourForEvent':
//		var_dump($_POST);exit;
		$result = array();
		$optionalTour = new Optional_tours();
		$optionalTour->title = $_POST['title'];
		$optionalTour->description = $_POST['description'];
		$optionalTour->available_yn = $_POST['status'];
		$optionalTour->event_id = $_POST['event_id'];
		$optionalTour->cost = $_POST['cost'];

		if ($optionalTourId = $optionalTour->add()) {
			$optionalTour->optional_tour_id = $optionalTourId;
			$optionalTour->package_id = $_POST['package_id'];
			$optionalTour->addOptionalTourToPackage();
			Template::renderAddedOptionalTour($optionalTour);
			$result['template'] = Template::getTemplate();
			$result['optional_tour'] = Optional_tours::prepareOptionalToursToJSON([$optionalTour]);
			echo json_encode($result);
		}

		break;
	case 'deleteOptionalTour':
		$optionalTour = new Optional_tours();
		$optionalTour->optional_tour_id = $_POST['optional_tour_id'];
		$optionalTour->package_id = $_POST['package_id'];
		if ($optionalTour->delete()) {
			$optionalTour->removeFromPackages();
			$optionalTour->removeFromBookings();
			echo json_encode(true);
		}
		break;
	case 'updateOptionalTourForEvent':
		$result = array();
		$optionalTour = new Optional_tours();
		$optionalTour->optional_tour_id = $_POST['optional_tour_id'];
		$optionalTour->title = $_POST['title'];
		$optionalTour->description = $_POST['description'];
		$optionalTour->available_yn = $_POST['status'];
		$optionalTour->event_id = $_POST['event_id'];
		$optionalTour->cost = $_POST['cost'];
		if ($optionalTour->update()) {
			Template::renderAddedOptionalTour($optionalTour, 'update');
			$result['optionalTour'] = $optionalTour;
			$result['template'] = Template::getTemplate();
			echo json_encode($result);
		}
		break;
	//Optional tours for event end

	// Create booking by user start
	case 'savePrimaryContactDetails':
		$booking = new Bookings();
		$booking->package_id = $_POST['package_id'];
		if ($bookingId = $booking->firstStepCreate()) {
			$guest = new Guests();
			$_POST['booking_id'] = $bookingId;
			$guest = $guest->setValues($guest, $_POST);
			if ($guest->firstStepCreateGuest()) {
				Template::secondStepTemplate();
				$template = Template::getTemplate();
				echo json_encode($template);
			} else {
				echo json_encode(false);
			}
		}

		break;
	// Create booking by user end

	//Packages for event preview API start
	case 'getPackagesForEventPreview':
		$packages = Packages::getAllForEvent($_POST['event_id']);
		$roomTypes = Room_type::getAllForEvent($_POST['event_id']);
		// Template::templateForPackagesForEventPreview($packages, $roomTypes);
		// $template = Template::getTemplate();

		$optionalTours = Optional_tours::getAllForPackagesWP($packages);

		$result['packages'] = $packages;
		$result['rooms'] = $roomTypes;
		$result['opt_tours'] = $optionalTours;
		// var_dump($packages);
		echo json_encode($result);
		break;
	//Packages for event preview API end


	//Make payment using Bpay start
	case 'getBpayRef':
		if ($_POST['booking_id'] == $_SESSION['bpay_booking_id'] && $_POST['user_id'] == $_SESSION['bpay_user_id'] && $_POST['event_id'] == $_SESSION['bpay_event_id']) {
			$result = array();
			$bpayRef = Payment::generateBpayRef($_POST['booking_id'], $_POST['user_id'], $_POST['event_id']);

			$_SESSION['Bpay_ref'] = $bpayRef;

			$sql = "SELECT user_id FROM guests WHERE user = " . $_POST['user_id'] . " AND booking_id = " . $_POST['booking_id'] . " AND primary_contact_YN = 'Y'";
			$guest = Guests::findBySql($sql);



			$transaction = new Transactions();
			$transaction->bpay_code = $bpayRef;
			$transaction->amount = $_POST['payment_amount'];
			$transaction->funds_received_YN = 'N';
			$transaction->payment_method = 'BP';
			$transaction->booking_id = $_POST['booking_id'];
//			$transaction->user_id = $_POST['user_id'];

			$transaction->user_id = $guest[0]->user_id;


			if ($transactionId = $transaction->createBpayTransaction()) {

				$_SESSION['transaction_id'] = $transactionId;
				$booking = Bookings::getById($_POST['booking_id']);
				$booking->paid = ($_POST['payment_amount'] == $_POST['amount_due']) ? 'Y' : 'P';
				$booking->total_paid = $booking->total_paid + $_POST['payment_amount'];
				// $booking->total_paid = null;
				if ($booking->updateBookingPaidStatus()) {
					$result['bpay_code'] = $bpayRef;
					$result['paid'] = $booking->total_paid;
					$result['amount_due'] = $_POST['amount_due'] - $_POST['payment_amount'];
					$result['transaction']['date'] = date('d/m/Y', time());
					$result['transaction']['id'] = $transactionId;
					$result['transaction']['type'] = 'BPay';
					$result['transaction']['amount'] = $_POST['payment_amount'];
					$result['returnUrl'] = "/~travellingfit/app/client/index.php?v=payment_confirmation_BP";
//					 $result['returnUrl'] = "/client/index.php?v=payment_confirmation_BP";
					echo json_encode($result);
				}
			}
		}
		break;

	case 'makeDDPayment':
		if ($_POST['booking_id'] == $_SESSION['bpay_booking_id'] && $_POST['user_id'] == $_SESSION['bpay_user_id'] && $_POST['event_id'] == $_SESSION['bpay_event_id']) {
			$result = array();
			$transaction = new Transactions();
			$transaction->amount = $_POST['payment_amount'];
			$transaction->funds_received_YN = 'N';
			$transaction->payment_method = 'DD';
			$transaction->booking_id = $_POST['booking_id'];

			$sql = "SELECT user_id FROM guests WHERE user = " . $_POST['user_id'] . " AND booking_id = " . $_POST['booking_id'] . " AND primary_contact_YN = 'Y'";
			$guest = Guests::findBySql($sql);

//			$transaction->user_id = $_POST['user_id'];

			$transaction->user_id = $guest[0]->user_id;

			if ($transactionId = $transaction->createBpayTransaction()) {
				$_SESSION['transaction_id'] = $transactionId;
				$booking = Bookings::getById($_POST['booking_id']);
				$booking->paid = ($_POST['payment_amount'] == $_POST['amount_due']) ? 'Y' : 'P';
				$booking->total_paid = $booking->total_paid + $_POST['payment_amount'];
				// $booking->total_paid = null;
				if ($booking->updateBookingPaidStatus()) {
					$result['bpay_code'] = $bpayRef;
					$result['paid'] = $booking->total_paid;
					$result['amount_due'] = $_POST['amount_due'] - $_POST['payment_amount'];
					$result['transaction']['date'] = date('d/m/Y', time());
					$result['transaction']['id'] = $transactionId;
					$result['transaction']['type'] = 'Direct Deposit';
					$result['transaction']['amount'] = $_POST['payment_amount'];
					$result['returnUrl'] = "/~travellingfit/app/client/index.php?v=payment_confirmation_DD";
					// $result['returnUrl'] = "/client/index.php?v=payment_confirmation_DD";
					echo json_encode($result);
				}
			}
		}
		break;

	case 'getTransactionsForPage':
		$sql = "SELECT user_id FROM guests WHERE user = " . $_SESSION['generic_user_id'] . " AND booking_id = " . $_POST['booking'] . " AND primary_contact_YN = 'Y'";
		$guest = Guests::findBySql($sql);

		$transactionsCount = Transactions::getCountForUser($guest[0]->user_id, $_POST['booking']);


		$paginatorObj = new Paginator($transactionsCount, $_POST['page']);
//        var_dump($transactionsCount);exit;
		$paginator = $paginatorObj->render();
		$limit = $paginatorObj->getLimit();




		$transactions = Transactions::getForUser($guest[0]->user_id, $_POST['booking'], $_POST['page'], $limit);

//        var_dump($transactions);exit;

		Template::getTemplateForTransactions($transactions);
		$template = Template::getTemplate();

		$result['paginator'] = $paginator;
		$result['template'] = $template;

		echo json_encode($result);
		break;

//		var_dump($_POST);exit;


	//Make payment using Bpay start

	case 'updateAdditionalGuestsCount':
//		var_dump($_POST);exit;
		$booking = Bookings::getById($_POST['booking_id']);
		$booking->number_adult_guests = $_POST['adults'];
		$booking->number_children_guests = $_POST['children'];
		$booking->user_id = $_SESSION['generic_user_id'];
		if ($bookingId = $booking->updateBooking()) {
			echo json_encode($bookingId);
		} else {
			echo json_encode(false);
		}
		break;
}

?>