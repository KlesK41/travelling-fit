<?php
// test mode
// $logged = true;
$logged = false;
foreach ($_COOKIE as $key => $value) {
	if (strpos($key, 'wordpress_logged_in_') !== false) {
		$logged = true;
	}
}

if ($logged) {
    $file = str_replace(__DIR__, '', __FILE__);
    require_once('../view/admin' . $file);
} else {
    $location = "http://67.225.210.86/~travellingfit/wp-login.php?redirect_to=http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];
    header("Location: " . $location);
}