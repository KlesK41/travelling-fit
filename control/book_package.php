<?php
if (!isset($_SESSION['generic_currentUser'])) {
    $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
    functions::redirectTo("index.php?v=login");
}

if (isset($_GET['package']) && is_numeric($_GET['package']) && isset($_GET['event']) && is_numeric($_GET['event']) && isset($_GET['room']) && is_numeric($_GET['room'])) {

    $path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
    require_once(__DIR__ . '/../includes/countries.php');



    $packageId = $_GET['package'];
    $roomId = $_GET['room'];
    $eventId = $_GET['event'];

    $booking = new Bookings();
    $guest = new Guests();
    $additionalGuests = array();


    if (isset($_GET['booking'])) {
        if (is_numeric($_GET['booking'])) {


            $booking = Bookings::getBookingForUser($_GET['booking'], $_SESSION['generic_user_id']);
            $bookingId = $booking->booking_id;
            if (empty($booking)) die("Page not found");

            if ($booking->total_paid > 0) header("Location: " . $_SERVER["SCRIPT_NAME"] . "?v=client_payment&booking_id=" . $booking->booking_id);

            $guests = Guests::getGuestForBook($_SESSION['generic_user_id'], $booking->booking_id);
            $guest = $additionalGuests = array();

            foreach ($guests as $value) {
                if ($value->primary_contact_yn == 'Y') {
                    $guest = $value;
                } elseif ($value->primary_contact_yn == 'N') {
                    if ($value->child_yn == 'Y') {
                        $additionalGuests['children'][] = $value;
                    } elseif ($value->child_yn == 'N') {
                        $additionalGuests['adults'][] = $value;
                    }
                }
            }


            if (isset($_GET['from_completed'])) {
                if (is_numeric($_GET['from_completed'])) {
                    $completedBooking = Bookings::getById($_GET['from_completed']);

//                    if ($booking->step == 1) {
                        if ($booking->number_adult_guests == $completedBooking->number_adult_guests && $booking->number_children_guests == $completedBooking->number_children_guests) {
//                            $guests = Guests::getNotPrimaryGuestForBook($_SESSION['generic_user_id'], $completedBooking->booking_id, 'N');
//
//                            foreach ($guests as $value) {
//                                if ($value->child_yn == 'Y') {
//                                    $additionalGuests['children'][] = $value;
//                                } elseif ($value->child_yn == 'N') {
//                                    $additionalGuests['adults'][] = $value;
//                                }
//                            }
                        } else {
                            if ($booking->number_adult_guests == null && $booking->number_children_guests == null) {
                                $booking->number_adult_guests = $completedBooking->number_adult_guests;
                                $booking->number_children_guests = $completedBooking->number_children_guests;
                                $additionalGuests = array();
                            } else {
//                                $additionalGuests['children'] = $booking->number_children_guests;
//                                $additionalGuests['adults'] = $booking->number_adult_guests;
                            }

                            if (empty ($additionalGuests)) {
                                $guests = Guests::getNotPrimaryGuestForBook($_SESSION['generic_user_id'], $completedBooking->booking_id, 'N');
                                if (!empty($guests)) {
                                    foreach ($guests as $value) {
                                        if ($value->child_yn == 'Y') {
                                            $additionalGuests['children'][] = $value;
                                        } elseif ($value->child_yn == 'N') {
                                            $additionalGuests['adults'][] = $value;
                                        }
                                    }
                                }
                            }
                        }
//                    }
                } else {
                    die("Page not found");
                }
            } else {
                if (empty($additionalGuests)) {
                    $additionalGuests['children'] = $booking->number_children_guests;
                    $additionalGuests['adults'] = $booking->number_adult_guests;
                }
            }

            if ($booking->step >= 2) {
                if (isset($completedBooking) && $booking->step == 2) {
                    $booking->extra_nights_pre = $completedBooking->extra_nights_pre;
                    $booking->extra_nights_post = $completedBooking->extra_nights_post;
                    $booking->active_living_membership = $completedBooking->active_living_membership;
                    $booking->bedding_id = $completedBooking->bedding_id;
                    $booking->share_request_yn = $completedBooking->share_request_yn;
                    $booking->how_hear_id = $completedBooking->how_hear_id;
                    $booking->travel_insurance_option_id = $completedBooking->travel_insurance_option_id;
                    $booking->additional_information = $completedBooking->additional_information;
                }
                $beddings = Bedding::getAll();
                $hearOptions = How_hear::getAll();
            }
            if ($booking->step >= 3) {
                $optionalTours = Optional_tours::getAllForEventAdmin($booking->event_id);

                $bookingOptionalTours = Optional_tours::getAllForBooking($booking->event_id, $booking->booking_id, $_SESSION['generic_user_id']);
            }
        } else {
            die("Page not found");
        }
    } else {
        $completedBookingList = Bookings::getCompleted($_SESSION['generic_user_id']);
        $booking->event_id = $eventId;
        $booking->package_id = $packageId;
        $booking->room_type_id = $roomId;
        $booking->user_id = $_SESSION['generic_user_id'];

        // Check if user has booked this package earlier

//        $bookingId = $booking->checkBookForUser();
//        if ($bookingId !== null ) {
//            functions::redirectTo($_SERVER['REQUEST_URI'] . "&booking=" . $bookingId);
//        }

        if (isset($_POST['completedBookings']) && is_numeric($_POST['completedBookings'])) {
            functions::redirectTo($_SERVER['REQUEST_URI'] . "&from_completed=" . $_POST['completedBookings']);
        }

        if (isset($_GET['from_completed'])) {
            if (is_numeric($_GET['from_completed'])) {
                $completedBooking = Bookings::getById($_GET['from_completed']);
                if ($booking->tshirt_id == null) $booking->tshirt_id = $completedBooking->tshirt_id;
                if ($booking->tshirt_quantity == null) $booking->tshirt_quantity = $completedBooking->tshirt_quantity;
                if ($booking->singlet_id == null) $booking->singlet_id = $completedBooking->singlet_id;
                if ($booking->singlet_quantity == null) $booking->singlet_quantity = $completedBooking->singlet_quantity;
                if ($booking->tshirt_name == null) $booking->tshirt_name = $completedBooking->tshirt_name;
//                var_dump($completedBooking);
                $guest = Guests::getPrimaryGuestForBook($_SESSION['generic_user_id'], $completedBooking->booking_id);
            } else {
                die("Page not found");
            }
        }
    }




    $tshirtsQuery = "SELECT * FROM tshirts WHERE available_YN = 'Y'";
    $tshirts = Tshirts::findBySql($tshirtsQuery);
    $maleTshirts = $femaleTshirts = $unisexTshirts = array();
    if (!empty($tshirts)) {
        foreach ($tshirts as $tshirt) {
            if ($tshirt->type == "M") {
                $maleTshirts[][(string)$tshirt->tshirt_id] = $tshirt->size;
            }
            if ($tshirt->type == "F") {
                $femaleTshirts[][(string)$tshirt->tshirt_id] = $tshirt->size;
            }
            if ($tshirt->type == "U") {
                $unisexTshirts[][(string)$tshirt->tshirt_id] = $tshirt->size;
            }
        }
    }

    $singletsQuery = "SELECT * FROM singlets WHERE available_YN = 'Y'";
    $singlets = Tshirts::findBySql($singletsQuery);
    $maleSinglets = $femaleSinglets = array();
    if (!empty($singlets)) {
        foreach ($singlets as $singlet) {
            if ($singlet->type == "M") {
                $maleSinglets[][(string)$singlet->singlet_id] = $singlet->size;
            }
            if ($singlet->type == "F") {
                $femaleSinglets[][(string)$singlet->singlet_id] = $singlet->size;
            }
        }
    }

    $femaleSinglets = array_merge($femaleSinglets, $maleSinglets);
    $maleSingletsJSON = json_encode($maleSinglets);
    $femaleSingletsJSON = json_encode($femaleSinglets);

//    var_dump($guest);
    if (!empty($guest)) {
        if ($guest->gender == "M" || $guest->gender == null) {
            $singlets = $maleSinglets;
        } elseif ($guest->gender == "F") {
            $singlets = $femaleSinglets;
        }
    }

    $maleTshirts = $maleTshirts;
    $femaleTshirts = array_merge($femaleTshirts, $maleTshirts);
    $maleTshirtsJSON = json_encode($maleTshirts);
    $femaleTshirtsJSON = json_encode($femaleTshirts);

    if (!empty($guest)) {
        if ($guest->gender == "M" || $guest->gender == null) {
            $tshirts = $maleTshirts;
        } elseif ($guest->gender == "F") {
            $tshirts = $femaleTshirts;
        }
    }

    $titles = Guests::getTitles();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($booking->step == 1 && $_POST['step'] == 2) {
            $booking->step = 1;
        } else {
            $booking->step = ($booking->step < $_POST['step']) ? $_POST['step'] : $booking->step;
        }


        // First step start
        if ($_POST['step'] == 1) {
            if (isset($_GET['booking']) && is_numeric($_GET['booking'])) {
                $booking->updateBooking();
                $guest = $guest->setValues($guest, $_POST);
                if ($guest->update()) {
                    functions::redirectTo($_SERVER['REQUEST_URI']);
                }
            } else {
                $booking->package_id = $packageId;
                $booking->room_type_id = $roomId;
                $booking->event_id = $eventId;
                $booking->completed = 'N';
                $booking->user_id = $_SESSION['generic_user_id'];
                if ($bookingId = $booking->firstStepCreate()) {
                    $_POST['booking_id'] = $bookingId;
                    $_POST['primary_contact_yn'] = 'Y';
                    $_POST['user'] = $_SESSION['generic_user_id'];
                    $_POST['prefered_event_id'] = $eventId;
                    $guest = $guest->setValues($guest, $_POST);
                    $guest->user = $_SESSION['generic_user_id'];
                    $guest->firstStepCreateGuest();
                    functions::redirectTo($_SERVER['REQUEST_URI'] . "&booking=" . $bookingId);
                }

//                var_dump(2);exit;
            }

        }
        // First step end

        // Second step start
        if ($_POST['step'] == 2) {
//            var_dump($_POST);exit;
            if (isset($_POST['number_adult_guests']) && isset($_POST['number_children_guests'])) {
                // var_dump(1);exit;
                $booking->number_adult_guests = $_POST['number_adult_guests'];
                $booking->number_children_guests= $_POST['number_children_guests'];
                if ($_POST['number_adult_guests'] == 0 && $_POST['number_children_guests'] == 0) {
                    $guest->booking_id = $booking->booking_id;
                    $guest->user = $_SESSION['generic_user_id'];
                    $guest->deleteByBookingForGuest();
                    $booking->step = 2;
                    $booking->updateBooking();
                } else {
                    $additionalGuests['adults'] = $_POST['number_adult_guests'];
                    $additionalGuests['children'] = $_POST['number_children_guests'];
                    $booking->step = 1;
                    $booking->updateBooking();
                }
                functions::redirectTo($_SERVER['REQUEST_URI']);
            }
            if (isset($_POST['additionalGuests'])) {
               if ($_POST['cnt'] == 1) {
                    $guest->booking_id = $booking->booking_id;
                    $guest->user = $_SESSION['generic_user_id'];
                    $guest->deleteByBookingForGuest();
                    $booking->step = 2;
                    $booking->updateStep();
               } else {
                    $step = $_POST['step'];
                    unset($_POST['step']);
                    unset($_POST['additionalGuests']);
                    unset($_POST['optionsRadios']);
                    $cnt = $_POST['cnt'] - 1;
                    unset($_POST['cnt']);
                    unset($_POST['tshirt_size1']);
                    unset($_POST['']);


                    $additionalGuestIds = array();
                    if (isset($_POST['additionalGuestId']) && !empty($_POST['additionalGuestId'])) {
                        foreach ($_POST['additionalGuestId'] as $id) {
                            if ($id !== '') $additionalGuestIds[] = $id;
                        }
                    }

                    if (isset($_GET['from_completed']) && $booking->step == 1 )  {
                        unset($_POST['additionalGuestId']);
                        $additionalGuestIds = array();
                    }


                    $guest->booking_id = $booking->booking_id;
                    $guest->user = $_SESSION['generic_user_id'];

                    $guest->deleteByBookingForGuest();

                    if (!empty($additionalGuestIds)) {
                        // var_dump(1);exit;
                        unset($_POST['additionalGuestId']);
    //                    $guest->secondStepUpdateAddtitionalGuest($_POST, $additionalGuestIds, $cnt);

                        $guest->secondStepCreateAddtitionalGuest($_POST, $eventId, $bookingId, $cnt);

                    } else {
                        // var_dump(2);exit;
                        unset($_POST['additionalGuestId']);
                        $guest->secondStepCreateAddtitionalGuest($_POST, $eventId, $bookingId, $cnt);
                        if ($booking->step == 1){
                            $booking->step = 2;
                            $booking->updateStep();
                        }
                    }
                }

            }
            functions::redirectTo($_SERVER['REQUEST_URI']);
        }
        // Second step end

        // Third step start
        if ($_POST['step'] == 3) {
//            var_dump($_POST);exit;
            if (!empty($_POST['extra_nigths_pre_other'])) $_POST['extra_nights_pre'] = $_POST['extra_nigths_pre_other'];
            if (!empty($_POST['extra_nigths_post_other'])) $_POST['extra_nights_post'] = $_POST['extra_nigths_post_other'];
            $booking = $booking->setValues($booking, $_POST);
            $booking->updateBooking();

//            $optionalTours = Optional_tours::getAllForEvent($booking->event_id);
            $optionalTours = Optional_tours::getAllForEventAdmin($booking->event_id);
//            var_dump($optionalTours);exit;
            if (empty($optionalTours)) {
//                var_dump(123);exit;
                $booking->total_cost = Payment::total_cost($booking->booking_id);
                $booking->completed = 'Y';
                $booking->updateBooking();
                functions::redirectTo($_SERVER['SCRIPT_NAME'] . "?v=client_payment&booking_id=" . $booking->booking_id);
            }
//            var_dump(321);exit;
            functions::redirectTo($_SERVER['REQUEST_URI']);
        }
        // Third step end

        // Fourth step start
        if ($_POST['step'] == 4) {
            Optional_tours::deleteFromOptionalTourPackage($bookingId, $_SESSION['generic_user_id']);
            foreach ($_POST as $key => $value) {
                if ($key != 'step' && !empty($value)) {
                    $optionalToursArray[$key] = $value;
                }
            }
            if (isset($optionalToursArray) && !empty($optionalToursArray)) {
                Optional_tours::bookOptionalTours($optionalToursArray, $eventId, $bookingId, $_SESSION['generic_user_id'], $packageId);
            }
            $booking->total_cost = Payment::total_cost($booking->booking_id);
            $booking->completed = 'Y';
//            var_dump($booking);exit;
            $booking->updateBooking();
            functions::redirectTo($_SERVER['SCRIPT_NAME'] . "?v=client_payment&booking_id=" . $booking->booking_id);
        }
        // Fourth step end

    } elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {


    }


    $file = str_replace(__DIR__, '', __FILE__);
    require_once('../view/general' . $file);
} else {
    die('Page not found');
}

