<?php
if (!isset($_SESSION['generic_currentUser'])) {
    $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
    functions::redirectTo("index.php?v=login");
}

// $path = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);

// include($path . '/includes/Paginator.php');

include(dirname(__FILE__). '/../includes/Paginator.php');

if (isset($_GET['booking_id']) && is_numeric($_GET['booking_id'])) {





    $booking = Bookings::getById($_GET['booking_id'], $_SESSION['generic_user_id']);
    $package = Packages::getById($booking->package_id);
    $roomType = Room_type::getById($booking->room_type_id);

    $optionalTours = Optional_tours::getAllForBookingPayment($booking->event_id, $booking->booking_id, $_SESSION['generic_user_id']);

    // var_dump($optionalTours);exit;
    $booking->total_paid = Transactions::getPaidSum($booking->booking_id);
    $booking->total_paid = ($booking->total_paid != null) ? $booking->total_paid : 0;
//    var_dump($booking->total_paid);

    // var_dump($booking);

    $_SESSION['bpay_booking_id'] = $booking->booking_id;
    $_SESSION['bpay_event_id'] = $booking->event_id;
    $_SESSION['bpay_user_id'] = $_SESSION['generic_user_id'];
    $_SESSION['booking_id'] = $booking->booking_id;

    $sql = "SELECT user_id FROM guests WHERE user = " . $_SESSION['generic_user_id'] . " AND booking_id = " . $booking->booking_id . " AND primary_contact_YN = 'Y'";
    $guest = Guests::findBySql($sql);

    $transactionsCount = Transactions::getCountForUser($guest[0]->user_id, $booking->booking_id);


    $paginatorObj = new Paginator($transactionsCount);
    $paginator = $paginatorObj->render();
    $limit = $paginatorObj->getLimit();


    $curlStr = "event_id=" . $booking->event_id;

//    var_dump($curlStr);exit;
    $ch = curl_init(); // create cURL handle (ch)
    if (!$ch) {
        die("Couldn't initialize a cURL handle");
    }
    // set some cURL options
    $ret = curl_setopt($ch, CURLOPT_URL,            "http://67.225.210.86/~travellingfit/wp-get-event-by-id.php");
    $ret = curl_setopt($ch, CURLOPT_HEADER,             0);
    $ret = curl_setopt($ch, CURLOPT_POST,               1);
    $ret = curl_setopt($ch, CURLOPT_POSTFIELDS,   $curlStr);
    $ret = curl_setopt($ch, CURLOPT_FOLLOWLOCATION,     1);
    $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER,     1);
    $ret = curl_setopt($ch, CURLOPT_TIMEOUT,            30);

    // execute
    $ret = curl_exec($ch);
    $event = json_decode($ret);

//    var_dump($guest)


    $transactions = Transactions::getForUser($guest[0]->user_id, $booking->booking_id, 1, $limit);

//    var_dump($guest);


//    var_dump($transactionsCount);
//    $totalCost = Payment::total_cost($booking->booking_id);

    $totalCost = $booking->total_cost;



    // test value;
//    $totalCost = 4870;



    $file = str_replace(__DIR__, '', __FILE__);
    require_once('../view/general' . $file);
} else {
    die("Page not found!");
}
