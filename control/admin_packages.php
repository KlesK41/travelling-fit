<?php 
$logged = false;
foreach ($_COOKIE as $key => $value) {
	if (strpos($key, 'wordpress_logged_in_') !== false) {
		$logged = true;
	}
}

if ($logged) {
	$ch = curl_init(); // create cURL handle (ch)
	if (!$ch) {
	    die("Couldn't initialize a cURL handle");
	}
	// set some cURL options
	$ret = curl_setopt($ch, CURLOPT_URL,            "http://67.225.210.86/~travellingfit/wp-get-events.php");
	$ret = curl_setopt($ch, CURLOPT_HEADER,         0);
	$ret = curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$ret = curl_setopt($ch, CURLOPT_TIMEOUT,        30);

	// execute
	$ret = curl_exec($ch);

	$events = json_decode($ret);

	if (!empty($events)) {
		$file = str_replace(__DIR__, '', __FILE__);
		require_once('../view/admin' . $file);
	}
} else {
	$location = "http://67.225.210.86/~travellingfit/wp-login.php?redirect_to=http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];
	header("Location: " . $location);
}